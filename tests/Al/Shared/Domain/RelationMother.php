<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain;

use App\Al\Shared\Domain\Relation;
use App\Tests\Shared\Domain\DuplicatorMother;
use App\Tests\Shared\Domain\NumberMother;
use App\Tests\Shared\Domain\UrlMother;
use App\Tests\Shared\Domain\WordMother;

final class RelationMother
{
    public static function create(string $url, $id, string $googlePackage): Relation
    {
        return new Relation($url, $id, $googlePackage);
    }

    public static function random(): Relation
    {
        return self::create(UrlMother::random(), NumberMother::random(),
            WordMother::random() . '.' . WordMother::random() . '.' . WordMother::random());
    }

    public static function withUrl(string $url): Relation
    {
        return self::create($url, NumberMother::random(),
            WordMother::random() . '.' . WordMother::random() . '.' . WordMother::random());
    }

    public static function withValues(array $array): Relation
    {
        return DuplicatorMother::with(self::random(),$array);
    }
}
