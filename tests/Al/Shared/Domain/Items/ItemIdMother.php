<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain\Items;

use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Shared\Domain\MotherCreator;

final class ItemIdMother
{
    public static function create(int $id): ItemId
    {
        return new ItemId($id);
    }

    public static function random(): ItemId
    {
        return self::create(MotherCreator::random()->numberBetween(1, 1000));
    }
}
