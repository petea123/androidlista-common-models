<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain\Items;

use App\Al\Items\Domain\ValueObject\ItemDownloads;
use App\Tests\Shared\Domain\MotherCreator;

final class ItemDownloadsMother
{
    public static function create(int $id): ItemDownloads
    {
        return new ItemDownloads($id);
    }

    public static function random(): ItemDownloads
    {
        return self::create(MotherCreator::random()->randomNumber());
    }

    public static function withDownloads(int $downloads): ItemDownloads
    {
        return self::create($downloads);
    }
}
