<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain\Apks;

use App\Al\Shared\Domain\Apks\ApkSource;
use App\Tests\Shared\Domain\NumberMother;

final class ApkSourceMother
{
    private const SOURCE = [ApkSource::APKPURE, ApkSource::UPTODOWN, ApkSource::APKMONK];

    public static function create(string $source): ApkSource
    {
        return new ApkSource($source);
    }

    public static function random(): ApkSource
    {
        return self::create(self::SOURCE[NumberMother::between(0,2)]);
    }
}
