<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain\Apks;

use App\Al\Apks\Domain\ValueObject\ApkDate;
use App\Tests\Shared\Domain\DateTimeMother;

final class ApkDateMother
{
    public static function create(\DateTimeInterface $date): ApkDate
    {
        return new ApkDate($date);
    }

    public static function random(): ApkDate
    {
        return self::create(DateTimeMother::random());
    }

    public static function createFromTimestamp(int $timestamp): ApkDate
    {
        return ApkDate::createFromTimestamp($timestamp);
    }
}
