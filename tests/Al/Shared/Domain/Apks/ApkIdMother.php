<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain\Apks;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Tests\Shared\Domain\UuidMother;

final class ApkIdMother
{
    public static function create(string $id): ApkId
    {
        return new ApkId($id);
    }

    public static function random(): ApkId
    {
        return self::create(UuidMother::random());
    }
}
