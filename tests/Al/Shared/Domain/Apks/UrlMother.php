<?php

declare(strict_types=1);

namespace App\Tests\Al\Shared\Domain\Apks;

use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Tests\Shared\Domain\UrlMother as GenericUrlMother;
use App\Tests\Shared\Domain\UuidMother;
use App\Tests\Shared\Domain\WordMother;

final class UrlMother
{
    public static function create(string $url): Url
    {
        return new Url($url);
    }

    public static function random(): Url
    {
        return self::create(GenericUrlMother::random());
    }

    public static function localRandom(): Url
    {
        $word = WordMother::random();
        return self::create($word.'/'.UuidMother::random().'/'.$word.'.apk');
    }
}
