<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\Service\ItemUptodownRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;
use App\Al\Shared\Domain\Relation;
use App\Al\Shared\Domain\WebParserInterface;
use App\Tests\Al\Apks\Domain\ValueObject\VersionMother;
use App\Tests\Al\Items\Domain\Event\ItemApkImportStartedMother;
use App\Tests\Shared\Domain\UrlMother;
use App\Tests\Shared\Domain\WordMother;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

abstract class ItemsUptodownModuleUnitTestCase extends ModuleUnitTestCase
{

    private $repository;

    /** @return ItemUptodownRepositoryInterface|MockInterface */
    protected function repository()
    {
        return $this->repository = $this->repository ?: $this->mock(ItemUptodownRepositoryInterface::class);
    }

    protected function shouldParseRelation(ExternalUrl $externalUrl, Relation $relation)
    {
        $this->webParser()
            ->shouldReceive('relation')
            ->with($this->similarTo($externalUrl))
            ->once()
            ->andReturn($relation);
    }

    protected function shouldGetUrlInfo(ExternalUrl $externalUrl, array $array = []): void
    {
        $this->webParser()
            ->shouldReceive('infoFor')
            ->with($this->similarTo($externalUrl))
            ->once()
            ->andReturn($array);
    }

    protected function shouldInfoByPackage(string $package, Relation $relation = null): void
    {
        $this->webParser()
            ->shouldReceive('infoByPackage')
            ->with($this->similarTo($package))
            ->once()
            ->andReturn($relation);
    }

    protected function shouldGetUrlVersions($externalId, array $array = []): void
    {
        $this->webParser()
            ->shouldReceive('versions')
            ->with($externalId)
            ->once()
            ->andReturn($array);
    }

    protected function shouldSaveItemUptodown(ItemUptodown $itemUptodown): void
    {
        $this->repository()
            ->shouldReceive('save')
            ->with($this->similarTo($itemUptodown))
            ->once()
            ->andReturnNull();
    }

    protected function shouldSearchItemUptodownWithItemId(ItemId $itemId, ItemUptodown $itemUptodown = null): void
    {
        $this->repository()
            ->shouldReceive('searchByItemId')
            ->with($this->similarTo($itemId))
            ->once()
            ->andReturn($itemUptodown);
    }

    protected function shouldSearchItemUptodownWithExternalUrl(
        ExternalUrl $externalUrl,
        ItemUptodown $itemUptodown = null
    ): void {
        $this->repository()
            ->shouldReceive('searchByExternalUrl')
            ->with($this->equalTo($externalUrl))
            ->once()
            ->andReturn($itemUptodown);
    }

    protected function generateVersions(int $total): array
    {
        $versions = [];

        for ($i = 0; $i < $total; ++$i) {
            $versions[VersionMother::random()->value()] = UrlMother::random().'/'.WordMother::random().'/'.WordMother::random();
        }

        return $versions;
    }

    protected function generateEventsToImport(array $versions): array
    {
        $events = [];

        for ($i = 0; $i < 10; ++$i) {
            $events[] = ItemApkImportStartedMother::random();
        }

        return $events;
    }
}
