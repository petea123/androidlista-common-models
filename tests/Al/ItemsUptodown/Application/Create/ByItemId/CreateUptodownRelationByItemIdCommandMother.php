<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Application\Create\ByItemId;

use App\Al\ItemsUptodown\Application\Create\ByItemId\CreateUptodownRelationByItemIdCommand;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\ItemsUptodown\Domain\ItemUptodownIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class CreateUptodownRelationByItemIdCommandMother
{
    public static function create(
        ItemUptodownId $id,
        ItemId $itemId
    ): CreateUptodownRelationByItemIdCommand {
        return new CreateUptodownRelationByItemIdCommand($id->value(), $itemId->value());
    }

    public static function random(): CreateUptodownRelationByItemIdCommand
    {
        return self::create(
            ItemUptodownIdMother::random(),
            ItemIdMother::random()
        );
    }
}