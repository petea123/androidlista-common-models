<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Application\Create\ByItemId;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Application\Create\ByItemId\CreateUptodownRelationByItemIdCommandHandler;
use App\Al\ItemsUptodown\Application\Create\ByItemId\Creator;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Items\Application\Find\FindItemQueryMother;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\ItemsUptodown\Domain\Event\ItemUptodownCreatedMother;
use App\Tests\Al\ItemsUptodown\ItemsUptodownModuleUnitTestCase;
use App\Tests\Al\Items\Application\Find\ItemResponseMother;
use App\Tests\Al\ItemsUptodown\Domain\ItemUptodownMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\RelationMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class CreatorTest extends ItemsUptodownModuleUnitTestCase
{
    /** @var CreateUptodownRelationByItemIdCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $creator = new Creator(
            $this->repository(),
            $this->queryBus(),
            $this->publisher(),
            $this->webParserFactory()
        );

        $this->handler = new CreateUptodownRelationByItemIdCommandHandler($creator);
    }

    /** @test */
    public function itShouldCreateRelation(): void
    {
        $command = CreateUptodownRelationByItemIdCommandMother::random();

        /** @var ItemUptodown $itemUptodown */
        $itemUptodown = ItemUptodownMother::fromCreateUptodownRelationByItemIdCommand($command);

        $itemResponse = ItemResponseMother::withId($itemUptodown->itemId());
        $itemGoogle = ItemGoogleMother::withPackage($itemResponse->googleId());

        $event = ItemUptodownCreatedMother::fromAggregate($itemUptodown);

        $this->shouldSearchItemUptodownWithItemId($itemUptodown->itemId(), null);
        $this->shouldAsk(
            FindItemQueryMother::create($itemUptodown->itemId()),
            $itemResponse
        );

        $this->shouldInstanceWebParser(ApkSourceMother::create('Uptodown'), $itemGoogle, true);
        $this->shouldGetWebParserApp($itemGoogle, true);
        $this->shouldGetWebParserApkUrl($itemUptodown->externalUrl());
        $this->shouldGetUptodownId($itemUptodown->uptodownId());
        $this->shouldSaveItemUptodown($itemUptodown);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }
}
