<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Application\Create;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Application\Create\CreateUptodownRelationCommandHandler;
use App\Al\ItemsUptodown\Application\Create\Creator;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Tests\Al\Items\Application\Find\ByPackageName\FindItemByPackageNameQueryMother;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\ItemsUptodown\Domain\Event\ItemUptodownCreatedMother;
use App\Tests\Al\ItemsUptodown\ItemsUptodownModuleUnitTestCase;
use App\Tests\Al\Items\Application\Find\ItemResponseMother;
use App\Tests\Al\ItemsUptodown\Domain\ItemUptodownMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\RelationMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class CreatorTest extends ItemsUptodownModuleUnitTestCase
{
    /** @var CreateUptodownRelationCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $creator = new Creator(
            $this->repository(),
            $this->queryBus(),
            $this->publisher(),
            $this->webParserFactory()
        );

        $this->handler = new CreateUptodownRelationCommandHandler($creator);
    }

    /** @test */
    public function itShouldCreateRelation(): void
    {
        $command = CreateUptodownRelationCommandMother::random();

        $id = new ItemUptodownId($command->id());
        $package = ItemGoogleMother::create($command->packageName());

        $itemUptodown = ItemUptodownMother::withId($id);

        $itemResponse = ItemResponseMother::withId($itemUptodown->itemId());
        $event = ItemUptodownCreatedMother::create($itemUptodown->id(), $itemUptodown->itemId(), $itemUptodown->externalUrl());

        $this->shouldInstanceWebParser(ApkSourceMother::create('Uptodown'), $package, true);
        $this->shouldGetWebParserApp($package, true);

        $this->shouldAsk(
            FindItemByPackageNameQueryMother::create(ItemGoogleMother::withPackage($itemResponse->googleId())),
            $itemResponse
        );

        $this->shouldSearchItemUptodownWithItemId($itemUptodown->itemId(), null);
        $this->shouldGetWebParserApkUrl($itemUptodown->externalUrl());
        $this->shouldGetUptodownId($itemUptodown->uptodownId());
        $this->shouldSaveItemUptodown($itemUptodown);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }
}
