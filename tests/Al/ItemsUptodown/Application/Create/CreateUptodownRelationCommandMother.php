<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Application\Create;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\ItemsUptodown\Application\Create\CreateUptodownRelationCommand;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\ItemsUptodown\Domain\ItemUptodownIdMother;

final class CreateUptodownRelationCommandMother
{
    public static function create(
        ItemUptodownId $id,
        ItemGoogle $package
    ): CreateUptodownRelationCommand {
        return new CreateUptodownRelationCommand($id->value(), $package->value());
    }

    public static function random(): CreateUptodownRelationCommand
    {
        return self::create(
            ItemUptodownIdMother::random(),
            ItemGoogleMother::random()
        );
    }
}
