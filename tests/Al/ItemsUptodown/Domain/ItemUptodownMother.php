<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Domain;

use App\Al\ItemsUptodown\Application\Create\ByItemId\CreateUptodownRelationByItemIdCommand;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class ItemUptodownMother
{
    public static function withId(ItemUptodownId $id): ItemUptodown
    {
        return self::create(
            $id,
            ItemIdMother::random(),
            ExternalUrlMother::random(),
            UptodownIdMother::random()
        );
    }

    public static function create(
        ItemUptodownId $id,
        ItemId $itemId,
        ExternalUrl $externalUrl,
        UptodownId $uptodownId
    ): ItemUptodown {

        return ItemUptodown::create(
            $id,
            $itemId,
            $externalUrl,
            $uptodownId
        );
    }

    public static function random(): ItemUptodown
    {
        return self::create(
            ItemUptodownIdMother::random(),
            ItemIdMother::random(),
            ExternalUrlMother::random(),
            UptodownIdMother::random()
        );
    }

    public static function fromCreateUptodownRelationByItemIdCommand(CreateUptodownRelationByItemIdCommand $command)
    {
        return self::create(
            ItemUptodownIdMother::create($command->id()),
            ItemIdMother::create($command->itemId()),
            ExternalUrlMother::random(),
            UptodownIdMother::random()
        );
    }

    public static function withValues(array $array): ItemUptodown
    {
        return DuplicatorMother::with(self::random(),$array);
    }
}
