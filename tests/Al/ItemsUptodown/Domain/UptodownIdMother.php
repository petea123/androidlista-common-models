<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Domain;

use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Tests\Shared\Domain\NumberMother;

final class UptodownIdMother
{
    public static function create(int $id): UptodownId
    {
        return new UptodownId($id);
    }

    public static function random(): UptodownId
    {
        return self::create(NumberMother::random());
    }
}
