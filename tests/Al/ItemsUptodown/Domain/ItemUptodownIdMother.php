<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Domain;

use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Tests\Shared\Domain\UuidMother;

final class ItemUptodownIdMother
{
    public static function create(string $id): ItemUptodownId
    {
        return new ItemUptodownId($id);
    }

    public static function random(): ItemUptodownId
    {
        return self::create(UuidMother::random());
    }
}
