<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsUptodown\Domain\Event;

use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Domain\Event\ItemUptodownCreated;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\ItemsUptodown\Domain\ItemUptodownIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemUptodownCreatedMother
{
    public static function create(
        ItemUptodownId $id,
        ItemId $itemId,
        ExternalUrl $url
    ): ItemUptodownCreated {
        return new ItemUptodownCreated(
            $id->value(), $itemId->value(), $url->value()
        );
    }

    public static function random(): ItemUptodownCreated
    {
        return self::create(
            ItemUptodownIdMother::random(),
            ItemIdMother::random(),
            ExternalUrlMother::random()
        );
    }

    public static function fromAggregate(ItemUptodown $itemUptodown): ItemUptodownCreated
    {
        return self::create(
            $itemUptodown->id(),
            $itemUptodown->itemId(),
            $itemUptodown->externalUrl()
        );
    }
}
