<?php

declare(strict_types=1);

namespace App\Tests\Al\Items;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\PackageValueObject;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

abstract class ItemModuleUnitTestCase extends ModuleUnitTestCase
{

    private $repository;

    /** @return ItemRepositoryInterface|MockInterface */
    protected function repository()
    {
        return $this->repository = $this->repository ?: $this->mock(ItemRepositoryInterface::class);
    }

    protected function shouldSaveItem(Item $item): void
    {
        $this->repository()
            ->shouldReceive('save')
            ->with($this->similarTo($item))
            ->once()
            ->andReturnNull();
    }

    protected function shouldSearchItem(ItemId $id, Item $item = null): void
    {
        $this->repository()
            ->shouldReceive('search')
            ->with($this->similarTo($id))
            ->once()
            ->andReturn($item);
    }

    protected function shouldSearchItemByPackage(PackageValueObject $package, Item $item = null): void
    {
        $this->repository()
            ->shouldReceive('searchByPackage')
            ->with($this->similarTo($package))
            ->once()
            ->andReturn($item);
    }

    protected function shouldSearchVersionInItem(ItemId $itemId, Version $version, Apk $apk = null): void
    {
        $this->repository()
            ->shouldReceive('searchVersionInItem')
            ->with($this->similarTo($itemId), $this->similarTo($version))
            ->once()
            ->andReturn($apk);
    }

    protected function shouldGenerateNextItemId(ItemId $id): void
    {
        $this->repository()
            ->shouldReceive('nextId')
            ->andReturn($id);
    }

}
