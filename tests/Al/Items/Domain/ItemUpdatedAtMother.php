<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain;

use App\Al\Items\Domain\ItemUpdatedAt;
use App\Tests\Shared\Domain\DateMother;
use DateTimeInterface;

final class ItemUpdatedAtMother
{
    public static function create(DateTimeInterface $dateTime): ItemUpdatedAt
    {
        return new ItemUpdatedAt($dateTime);
    }

    public static function random(): ItemUpdatedAt
    {
        return self::create(DateMother::random()->value());
    }

    public static function after(ItemUpdatedAt $actual): ItemUpdatedAt
    {
        return self::create($actual->value()->add(new \DateInterval('P1D')));
    }

    public static function before(ItemUpdatedAt $actual): ItemUpdatedAt
    {
        return self::create($actual->value()->sub(new \DateInterval('P1D')));
    }

}
