<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Tests\Shared\Domain\NumberMother;

final class ItemStatusMother
{
    public static function create(
        int $statusBr,
        int $statusEn,
        int $statusFr,
        int $statusIt,
        int $statusDe,
        int $statusTr,
        int $statusNl,
        int $statusVn,
        int $statusPl,
        int $statusGr,
        int $statusTh,
        int $statusRo,
        int $statusJa,
        int $statusKo,
        int $statusRu,
        int $statusId,
        int $statusEs
    ): ItemStatus {
        return new ItemStatus($statusBr,
            $statusEn,
            $statusFr,
            $statusIt,
            $statusDe,
            $statusTr,
            $statusNl,
            $statusVn,
            $statusPl,
            $statusGr,
            $statusTh,
            $statusRo,
            $statusJa,
            $statusKo,
            $statusRu,
            $statusId,
            $statusEs
        );
    }

    public static function random(): ItemStatus
    {
        return self::create(NumberMother::between(0, 1024), NumberMother::between(0, 1024),
            NumberMother::between(0, 1024), NumberMother::between(0, 1024), NumberMother::between(0, 1024),
            NumberMother::between(0, 1024), NumberMother::between(0, 1024), NumberMother::between(0, 1024),
            NumberMother::between(0, 1024), NumberMother::between(0, 1024), NumberMother::between(0, 1024),
            NumberMother::between(0, 1024), NumberMother::between(0, 1024), NumberMother::between(0, 1024),
            NumberMother::between(0, 1024), NumberMother::between(0, 1024), NumberMother::between(0, 1024));
    }

    public static function publicItem(): ItemStatus
    {
        $value = ItemStatus::PUBLISHED + ItemStatus::ADSENSE;

        return self::withStatus($value);
    }

    public static function unpublished(): ItemStatus
    {
        return self::create(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
    }

    public static function publish(ItemStatus $status): ItemStatus
    {
        $status = clone $status;
        return $status->publishApk();
    }

    public static function withStatus(int $status)
    {
        return self::create($status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status,$status);
    }

    public static function unpublish(ItemStatus $statuses)
    {
        $status = clone $statuses;
        return $status->unpublishApk();
    }

    public static function as404(ItemStatus $statuses)
    {
        $status = clone $statuses;
        return $status->markAs404();
    }

    public static function markedRelevant(ItemStatus $status, LocaleValueObject $locale)
    {
        $status = clone $status;
        return $status->markRelevant($locale);
    }

    public static function publicForApkPure()
    {
        $value = ItemStatus::PUBLISHED + ItemStatus::ADSENSE + ItemStatus::APK_PURE + ItemStatus::APK_PUBLISH;
        return self::withStatus($value);
    }
}
