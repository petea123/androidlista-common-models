<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain\Event;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Items\Domain\Event\ItemApkImportStarted;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemApkImportStartedMother
{
    public static function create(
        ItemId $itemId,
        ApkId $apkId,
        ExternalUrl $externalUrl,
        ApkSource $source
    ): ItemApkImportStarted {
        return new ItemApkImportStarted((string)$itemId->value(), $apkId->value(), $externalUrl->value(), $source->value());
    }

    public static function random(): ItemApkImportStarted
    {
        return self::create(
            ItemIdMother::random(),
            ApkIdMother::random(),
            ExternalUrlMother::random(),
            ApkSourceMother::random()
        );
    }
}
