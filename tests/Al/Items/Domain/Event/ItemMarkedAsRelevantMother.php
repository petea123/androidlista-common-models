<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain\Event;

use App\Al\Items\Domain\Event\ItemMarkedAsRelevant;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\LocaleMother;

final class ItemMarkedAsRelevantMother
{
    public static function create(ItemId $itemId, LocaleValueObject $locale): ItemMarkedAsRelevant
    {
        return new ItemMarkedAsRelevant((string)$itemId->value(), $locale->value());
    }

    public static function random(): ItemMarkedAsRelevant
    {
        return self::create(
            ItemIdMother::random(),
            LocaleMother::random()
        );
    }
}
