<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ItemCreatedAt;
use App\Al\Items\Domain\ItemUpdatedAt;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\IntegerMother;

final class ItemMother
{

    public static function withId(ItemId $id): Item
    {
        return self::create(
            $id,
            ItemStatusMother::random(),
            ItemGoogleMother::random(),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random()
        );
    }

    public static function create(
        ItemId $id,
        ItemStatus $statuses,
        ItemGoogle $google,
        ItemName $name,
        ItemUpdatedAt $updatedAt,
        ItemCreatedAt $createdAt,
        int $votes,
        float $note
    ): Item {
        return new Item(
            $id,
            $statuses,
            $google,
            $name,
            $updatedAt,
            $createdAt,
            $votes,
            $note
        );
    }

    public static function random(): Item
    {
        return self::create(
            ItemIdMother::random(),
            ItemStatusMother::random(),
            ItemGoogleMother::random(),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random()
        );
    }

    public static function withIdUnpublished(ItemId $id): Item
    {
        return self::create(
            $id,
            ItemStatusMother::unpublished(),
            ItemGoogleMother::random(),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random()
        );
    }

    public static function withIdAndStatus(ItemId $id, int $APK_PUBLISH)
    {
        return self::create(
            $id,
            ItemStatusMother::withStatus($APK_PUBLISH),
            ItemGoogleMother::random(),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random()
        );
    }

    public static function withPackageAndPublic(ItemGoogle $google)
    {
        return self::create(
            ItemIdMother::random(),
            ItemStatusMother::publicItem(),
            $google,
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random()
        );
    }
}
