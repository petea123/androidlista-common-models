<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain;

use App\Al\Items\Domain\ItemCreatedAt;
use App\Tests\Shared\Domain\DateMother;
use DateTimeInterface;

final class ItemCreatedAtMother
{
    public static function create(DateTimeInterface $dateTime): ItemCreatedAt
    {
        return new ItemCreatedAt($dateTime);
    }

    public static function random(): ItemCreatedAt
    {
        return self::create(DateMother::random()->value());
    }

    public static function after(ItemCreatedAt $actual): ItemCreatedAt
    {
        return self::create($actual->value()->add(new \DateInterval('P1D')));
    }

    public static function before(ItemCreatedAt $actual): ItemCreatedAt
    {
        return self::create($actual->value()->sub(new \DateInterval('P1D')));
    }

}
