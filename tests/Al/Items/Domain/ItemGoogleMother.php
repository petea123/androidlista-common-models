<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Tests\Shared\Domain\UrlMother;
use App\Tests\Shared\Domain\WordMother;

final class ItemGoogleMother
{
    public static function create(string $google): ItemGoogle
    {
        return new ItemGoogle($google);
    }

    public static function random(): ItemGoogle
    {
        return self::create(UrlMother::random().'/details?id='.WordMother::random().'.'.WordMother::random().'.'.WordMother::random());
    }

    public static function withPackage(string $package): ItemGoogle
    {
        return ItemGoogle::createFromPackage($package);
    }
}
