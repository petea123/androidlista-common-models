<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain;

use App\Al\Items\Domain\ValueObject\ItemName;
use App\Tests\Shared\Domain\WordMother;

final class ItemNameMother
{
    public static function create(
        string $value,
        string $nameBr,
        string $nameEs,
        string $nameFr,
        string $nameId,
        string $nameIt,
        string $nameDe,
        string $nameTr,
        string $nameRu,
        string $nameJa,
        string $nameKo,
        string $namePl,
        string $nameRo,
        string $nameVn,
        string $nameTh,
        string $nameNl,
        string $nameGr
    ): ItemName {
        return new ItemName(
            $value,
            $nameBr,
            $nameEs,
            $nameFr,
            $nameId,
            $nameIt,
            $nameDe,
            $nameTr,
            $nameRu,
            $nameJa,
            $nameKo,
            $namePl,
            $nameRo,
            $nameVn,
            $nameTh,
            $nameNl,
            $nameGr
        );
    }

    public static function random(): ItemName
    {
        return self::create(
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
        );
    }
}
