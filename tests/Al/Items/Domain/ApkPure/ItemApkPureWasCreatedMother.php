<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain\ApkPure;

use App\Al\Items\Domain\ApkPure\ItemApkPure;
use App\Al\Items\Domain\ApkPure\ItemApkPureWasCreated;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemApkPureWasCreatedMother
{
    public static function create(ItemId $id): ItemApkPureWasCreated
    {
        return new ItemApkPureWasCreated((string)$id->value());
    }

    public static function fromItem(ItemApkPure $item): ItemApkPureWasCreated
    {
        return self::create($item->id());
    }

    public static function random(): ItemApkPureWasCreated
    {
        return self::create(ItemIdMother::random());
    }
}
