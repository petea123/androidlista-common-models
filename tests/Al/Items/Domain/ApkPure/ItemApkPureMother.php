<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain\ApkPure;

use App\Al\Items\Domain\ApkPure\ItemApkPure;
use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Al\Items\Domain\ItemCategory;
use App\Al\Items\Domain\ItemCreatedAt;
use App\Al\Items\Domain\ItemDataMinOsVersion;
use App\Al\Items\Domain\ItemDataOfficialUrl;
use App\Al\Items\Domain\ItemDataPrice;
use App\Al\Items\Domain\ItemDataProvider;
use App\Al\Items\Domain\ItemDataSize;
use App\Al\Items\Domain\ItemDataVersion;
use App\Al\Items\Domain\ItemUpdatedAt;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Items\Domain\ItemCreatedAtMother;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\Items\Domain\ItemNameMother;
use App\Tests\Al\Items\Domain\ItemStatusMother;
use App\Tests\Al\Items\Domain\ItemUpdatedAtMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\IntegerMother;
use App\Tests\Shared\Domain\NumberMother;
use App\Tests\Shared\Domain\WordMother;
use Symfony\Component\Config\Definition\FloatNode;
use function Lambdish\Phunctional\map;

final class ItemApkPureMother
{
    public static function create(
        ItemId $id,
        ItemStatus $statuses,
        ItemGoogle $google,
        ItemName $name,
        ItemUpdatedAt $updatedAt,
        ItemCreatedAt $createdAt,
        int $votes,
        float $note,
        ItemApkPurePackage $package,
        float $price,
        string $size,
        string $minOsVersion,
        string $provider,
        string $officialUrl,
        string $version,
        array $categories
    ): ItemApkPure {
        $item = new ItemApkPure(
            $id,
            $statuses,
            $google,
            $name,
            $updatedAt,
            $createdAt,
            $votes,
            $note,
            $package
        );

        $price = new ItemDataPrice($item, $price);
        $size = new ItemDataSize($item, $size);
        $minOsVersion = new ItemDataMinOsVersion($item, $minOsVersion);
        $provider = new ItemDataProvider($item, $provider);
        $officialUrl = new ItemDataOfficialUrl($item, $officialUrl);
        $version = new ItemDataVersion($item, $version);
        $categories = map(function ($category) use ($item) {
            return new ItemCategory($item, $category);
        }, $categories);

        $item->addData($price, $size, $minOsVersion, $provider, $officialUrl, $version);
        $item->addCategory(...$categories);

        return $item;
    }

    public static function random(): ItemApkPure
    {
        return self::create(
            ItemIdMother::random(),
            ItemStatusMother::random(),
            ItemGoogleMother::random(),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random(),
            ItemApkPurePackageMother::random(),
            NumberMother::float(2),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            [IntegerMother::random(), IntegerMother::random()]
        );
    }

    public static function withPackage(ItemApkPurePackage $package): ItemApkPure
    {
        return self::create(
            ItemIdMother::random(),
            ItemStatusMother::random(),
            ItemGoogleMother::random(),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random(),
            $package,
            NumberMother::float(2),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            [IntegerMother::random(), IntegerMother::random()]
        );
    }

    public static function createNewWithPackage(ItemApkPurePackage $package): ItemApkPure
    {
        return self::create(
            ItemIdMother::random(),
            ItemStatusMother::withStatus(ItemStatus::APK_PURE + ItemStatus::GOOGLE_PLAY_REMOVED),
            ItemGoogle::createFromPackage($package->id()),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random(),
            $package,
            NumberMother::float(2),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            [IntegerMother::random(), IntegerMother::random()]
        );
    }

    public static function withPackageAndPublic(ItemApkPurePackage $package)
    {

        return self::create(
            ItemIdMother::random(),
            ItemStatusMother::publicForApkPure(),
            ItemGoogle::createFromPackage($package->id()),
            ItemNameMother::random(),
            ItemUpdatedAtMother::random(),
            ItemCreatedAtMother::random(),
            IntegerMother::random(),
            IntegerMother::random(),
            $package,
            NumberMother::float(2),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            [IntegerMother::random(), IntegerMother::random()]
        );
    }
}
