<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Domain\ApkPure;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Tests\Shared\Domain\WordMother;

final class ItemApkPurePackageMother
{
    public static function create(string $web): ItemApkPurePackage
    {
        return new ItemApkPurePackage($web);
    }

    public static function random(): ItemApkPurePackage
    {
        return ItemApkPurePackage::createFromPackage(
            sprintf('%s.%s.%s', WordMother::random(), WordMother::random(), WordMother::random())
        );
    }

    public static function withPackage(string $package): ItemApkPurePackage
    {
        return ItemApkPurePackage::createFromPackage($package);
    }
}
