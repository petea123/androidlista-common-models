<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Infrastructure\Projections;

use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Items\Domain\ItemNameMother;
use App\Tests\Shared\Domain\MotherCreator;

final class ItemViewMother
{
    public static function create(
        int $id,
        string $web,
        string $originalName,
        string $name,
        array $images,
        string $href,
        int $price = 0,
        string $sentence = ''
    ): ItemView {
        return new ItemView($id, $web, $originalName, $name, $images, $href, $price, $sentence);
    }

    public static function withItemId(ItemId $itemId): ItemView
    {
        return self::create(
            $itemId->value(),
            ExternalUrlMother::create(
                sprintf(
                    '%s/details?id=%s',
                    MotherCreator::random()->url,
                    implode('.', MotherCreator::random()->words()
                    ))
            )->value(),
            ItemNameMother::random()->value(),
            ItemNameMother::random()->value(),
            [],
            ExternalUrlMother::random()->value(),
        );
    }
}
