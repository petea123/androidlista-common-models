<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Infrastructure\Persistence;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Items\Domain\ValueObject\ItemDownloads;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Al\Shared\Domain\Items\ItemId;

final class InMemoryItemRepository implements ItemRepositoryInterface
{

    private $items;

    public function __construct()
    {
        $item = Item::create(
            new ItemId(3),
            new ItemStatus(129,1,1,129,129,129,1,1,129,129,129,1,1,129,129,129,1),
            new ItemGoogle('https://play.google.com/store/apps/details?id=com.imangi.templerun&feature=search_result'),
            new ItemName('Temple Run')
        );

        $this->save($item);

        $item2 = Item::create(
            new ItemId(4),
            new ItemStatus(129,1,1,129,129,129,1,1,129,129,129,1,1,129,129,129,1),
            new ItemGoogle('https://play.google.com/store/apps/details?id=com.imangi.templerun&feature=search_result'),
            new ItemName('Temple Run')
        );

        $this->save($item2);

        $item3 = Item::create(
            new ItemId(10),
            new ItemStatus(129,1,1,129,129,129,1,1,129,129,129,1,1,129,129,129,1),
            new ItemGoogle('https://play.google.com/store/apps/details?id=com.imangi.templerun&feature=search_result'),
            new ItemName('Temple Run')
        );

        $this->save($item3);

        $item4 = Item::create(
            new ItemId(8),
            new ItemStatus(129,1,1,129,129,129,1,1,129,129,129,1,1,129,129,129,1),
            new ItemGoogle('https://play.google.com/store/apps/details?id=com.imangi.templerun&feature=search_result'),
            new ItemName('Temple Run')
        );

        $this->save($item4);

        $item5 = Item::create(
            new ItemId(12),
            new ItemStatus(129,1,1,129,129,129,1,1,129,129,129,1,1,129,129,129,1),
            new ItemGoogle('https://play.google.com/store/apps/details?id=com.imangi.templerun&feature=search_result'),
            new ItemName('Temple Run')
        );

        $this->save($item5);
    }

    public function search(ItemId $id): ?Item
    {
        return $this->items[$id->value()];
    }

    public function save(Item $item): void
    {
        $this->items[$item->id()->value()] = $item;
    }

    public function totalDownloads(ItemId $id): ItemDownloads
    {
        // TODO: Implement totalDownloads() method.
    }

    public function searchByUptodownUrl(string $url): ?Item
    {
        // TODO: Implement searchByUptodownUrl() method.
    }

    public function searchByPackage(ItemGoogle $package): ?Item
    {
        // TODO: Implement searchByPackage() method.
    }

    public function searchPrice(ItemId $id): float
    {
        // TODO: Implement searchPrice() method.
    }

    public function searchValid(int $from = 1, int $to = 1, ItemId $id = null): array
    {
        // TODO: Implement searchValidItems() method.
    }

    public function forFree(ItemId $id): void
    {
        // TODO: Implement forFree() method.
    }
}
