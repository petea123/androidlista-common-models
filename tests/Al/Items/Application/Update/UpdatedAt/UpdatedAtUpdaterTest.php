<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\UpdatedAt;

use App\Al\Items\Application\Update\UpdatedAt\UpdatedAtUpdater;
use App\Al\Items\Application\Update\UpdatedAt\UpdateUpdatedAtCommandHandler;
use App\Tests\Al\Items\Domain\ItemMother;
use App\Tests\Al\Items\Domain\ItemUpdatedAtMother;
use App\Tests\Al\Items\ItemModuleUnitTestCase;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class UpdatedAtUpdaterTest extends ItemModuleUnitTestCase
{
    /** @var UpdateUpdatedAtCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $updater = new UpdatedAtUpdater($this->repository());

        $this->handler = new UpdateUpdatedAtCommandHandler($updater);
    }

    /** @test */
    public function itShouldUpdateUpdatedAtWhenIsAfter(): void
    {
        $id = ItemIdMother::random();
        $item = ItemMother::withId($id);
        $item->pullDomainEvents();
        $updatedAt = ItemUpdatedAtMother::after($item->updatedAt());
        $newItem = DuplicatorMother::with($item, ['updatedAt' => $updatedAt]);

        $command = UpdateUpdatedAtCommandMother::create($id, $updatedAt);

        $this->shouldSearchItem($id, $item);
        $this->shouldSaveItem($newItem);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldntUpdateUpdatedAtWhenIsBefore(): void
    {
        $id = ItemIdMother::random();
        $item = ItemMother::withId($id);
        $item->pullDomainEvents();
        $updatedAt = ItemUpdatedAtMother::before($item->updatedAt());

        $command = UpdateUpdatedAtCommandMother::create($id, $updatedAt);

        $this->shouldSearchItem($id, $item);

        $this->dispatch($command, $this->handler);
    }
}
