<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\UpdatedAt;

use App\Al\Items\Application\Update\UpdatedAt\UpdateUpdatedAtCommand;
use App\Al\Items\Domain\ItemUpdatedAt;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class UpdateUpdatedAtCommandMother
{
    public static function create(ItemId $itemId, ItemUpdatedAt $updatedAt): UpdateUpdatedAtCommand
    {
        return new UpdateUpdatedAtCommand($itemId->value(), $updatedAt->timestamp());
    }

    public static function random(): UpdateUpdatedAtCommand
    {
        return self::create(ItemIdMother::random(), ItemUpdatedAtMother::random());
    }
}
