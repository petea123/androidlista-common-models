<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Apk;

use App\Al\Items\Application\Update\Status\Apk\UnpublishApkStatusOnApkDangerous;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkWasDangerousMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class UnpublishOnApkWasDangerousTest extends ApkModuleUnitTestCase
{

    /** @var UnpublishApkStatusOnApkDangerous */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new UnpublishApkStatusOnApkDangerous($this->commandBus());

    }

    /** @test */
    public function itShouldPublishOnApkWasProcessed(): void
    {
        $event = ApkWasDangerousMother::random();

        $itemId = ItemIdMother::create((int)$event->itemId());

        $command = UnpublishApkStatusCommandMother::create($itemId);
        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);

    }
}
