<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Apk;

use App\Al\Items\Application\Update\Status\Apk\UnpublishApkStatusCommand;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class UnpublishApkStatusCommandMother
{

    public static function create(ItemId $itemId): UnpublishApkStatusCommand
    {
        return new UnpublishApkStatusCommand($itemId->value());
    }

    public static function random(): UnpublishApkStatusCommand
    {
        return self::create(ItemIdMother::random());
    }
}
