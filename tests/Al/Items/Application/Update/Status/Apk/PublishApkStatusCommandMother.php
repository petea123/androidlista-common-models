<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Apk;

use App\Al\Items\Application\Update\Status\Apk\PublishApkStatusCommand;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class PublishApkStatusCommandMother
{

    public static function create(ItemId $itemId): PublishApkStatusCommand
    {
        return new PublishApkStatusCommand($itemId->value());
    }

    public static function random(): PublishApkStatusCommand
    {
        return self::create(ItemIdMother::random());
    }
}
