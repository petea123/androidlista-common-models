<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Apk;

use App\Al\Apks\Domain\ValueObject\State;
use App\Al\Items\Application\Update\Status\Apk\UnpublishApkStatusCommandHandler;
use App\Al\Items\Application\Update\Status\Apk\UnpublisherApkStatus;
use App\Al\Items\Domain\Event\ItemApkStatusWasUpdated;
use App\Al\Items\Domain\Exception\ItemDontNeedUpdateStatus;
use App\Al\Items\Domain\Exception\ItemWithProcessedApk;
use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Tests\Al\Apks\Application\Search\ApksResponseMother;
use App\Tests\Al\Apks\Application\Search\ByCriteria\SearchApksByCriteriaQueryMother;
use App\Tests\Al\Items\Domain\ItemMother;
use App\Tests\Al\Items\Domain\ItemStatusMother;
use App\Tests\Al\Items\ItemModuleUnitTestCase;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class UnpublisherApkStatusTest extends ItemModuleUnitTestCase
{
    /** @var UnpublishApkStatusCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $unpublisher = new UnpublisherApkStatus(
            $this->repository(),
            $this->queryBus(),
            $this->publisher()
        );

        $this->handler = new UnpublishApkStatusCommandHandler($unpublisher);
    }

    /** @test */
    public function itShouldUnpublishApkStatus(): void
    {
        $command = UnpublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());
        $item = ItemMother::withIdAndStatus($id, ItemStatus::APK_PUBLISH);
        $item->pullDomainEvents();

        /** @var Item $updatedItem */
        $updatedItem = DuplicatorMother::with(
            $item,
            [
                'statuses' => ItemStatusMother::unpublish($item->statuses())
            ]
        );

        $this->shouldSearchItem($id, $item);
        $this->shouldAsk(SearchApksByCriteriaQueryMother::create([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => $item->id()->value()
            ],
            [
                'field' => 'state',
                'operator' => '=',
                'value' => State::processed()
            ]
        ]), ApksResponseMother::empty());
        $this->shouldSaveItem($updatedItem);
        $this->shouldRecordDomainEvents(new ItemApkStatusWasUpdated(
            (string)$id->value(), 'unpublish'
        ));

        $this->dispatch($command, $this->handler);
    }


    /** @test */
    public function itShouldThrowExceptionWhenHaventProcessedApks(): void
    {
        $this->expectException(ItemWithProcessedApk::class);

        $command = UnpublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());
        $item = ItemMother::withIdAndStatus($id, ItemStatus::APK_PUBLISH);
        $item->pullDomainEvents();

        $this->shouldSearchItem($id, $item);
        $this->shouldAsk(SearchApksByCriteriaQueryMother::create([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => $item->id()->value()
            ],
            [
                'field' => 'state',
                'operator' => '=',
                'value' => State::processed()
            ]
        ]), ApksResponseMother::withItemId($id));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldThrowExceptionWhenDontNeedUpdateStatus(): void
    {
        $this->expectException(ItemDontNeedUpdateStatus::class);

        $command = UnpublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());
        $item = ItemMother::withIdUnpublished($id);
        $item->pullDomainEvents();

        $this->shouldSearchItem($id, $item);
        $this->shouldAsk(SearchApksByCriteriaQueryMother::create([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => $item->id()->value()
            ],
            [
                'field' => 'state',
                'operator' => '=',
                'value' => State::processed()
            ]
        ]), ApksResponseMother::empty());

        $this->dispatch($command, $this->handler);
    }
}
