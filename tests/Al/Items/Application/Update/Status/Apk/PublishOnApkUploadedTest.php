<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Apk;

use App\Al\Items\Application\Update\Status\Apk\PublishApkStatusOnApkUploaded;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkWasUploadedMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class PublishOnApkUploadedTest extends ApkModuleUnitTestCase
{

    /** @var PublishApkStatusOnApkUploaded */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new PublishApkStatusOnApkUploaded($this->commandBus());

    }

    /** @test */
    public function itShouldPublishOnApkWasUploaded(): void
    {
        $event = ApkWasUploadedMother::random();

        $itemId = ItemIdMother::create((int)$event->itemId());

        $command = PublishApkStatusCommandMother::create($itemId);
        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);

    }
}
