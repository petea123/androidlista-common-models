<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Apk;

use App\Al\Apks\Domain\ValueObject\State;
use App\Al\Items\Application\Update\Status\Apk\PublishApkStatusCommandHandler;
use App\Al\Items\Application\Update\Status\Apk\PublisherApkStatus;
use App\Al\Items\Domain\Event\ItemApkStatusWasPublished;
use App\Al\Items\Domain\Exception\ItemDontNeedUpdateStatus;
use App\Al\Items\Domain\Exception\ItemNotFound;
use App\Al\Items\Domain\Exception\ItemWithoutProcessedApk;
use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Tests\Al\Apks\Application\Search\ApksResponseMother;
use App\Tests\Al\Apks\Application\Search\ByCriteria\SearchApksByCriteriaQueryMother;
use App\Tests\Al\Items\Domain\ItemMother;
use App\Tests\Al\Items\Domain\ItemStatusMother;
use App\Tests\Al\Items\ItemModuleUnitTestCase;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class PublisherApkStatusTest extends ItemModuleUnitTestCase
{
    /** @var PublishApkStatusCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $publisher = new PublisherApkStatus(
            $this->repository(),
            $this->queryBus(),
            $this->publisher()
        );

        $this->handler = new PublishApkStatusCommandHandler($publisher);
    }

    /** @test */
    public function itShouldPublishApkStatus(): void
    {
        $command = PublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());
        $item = ItemMother::withIdUnpublished($id);
        $item->pullDomainEvents();

        /** @var Item $updatedItem */
        $updatedItem = DuplicatorMother::with(
            $item,
            [
                'statuses' => ItemStatusMother::publish($item->statuses())
            ]
        );

        $this->shouldSearchItem($id, $item);
        $this->shouldAsk(SearchApksByCriteriaQueryMother::create([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => $item->id()->value()
            ],
            [
                'field' => 'state',
                'operator' => '=',
                'value' => State::processed()
            ]
        ]), ApksResponseMother::withItemId($id));
        $this->shouldSaveItem($updatedItem);
        $this->shouldRecordDomainEvents(new ItemApkStatusWasPublished((string)$id->value()));

        $this->dispatch($command, $this->handler);
    }


    /** @test */
    public function itShouldThrowExceptionWhenItemNotFound(): void
    {
        $this->expectException(ItemNotFound::class);

        $command = PublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());

        $this->shouldSearchItem($id);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldThrowExceptionWhenHaventProcessedApks(): void
    {
        $this->expectException(ItemWithoutProcessedApk::class);

        $command = PublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());
        $item = ItemMother::withIdAndStatus($id, ItemStatus::APK_PUBLISH);
        $item->pullDomainEvents();

        $this->shouldSearchItem($id, $item);
        $this->shouldAsk(SearchApksByCriteriaQueryMother::create([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => $item->id()->value()
            ],
            [
                'field' => 'state',
                'operator' => '=',
                'value' => State::processed()
            ]
        ]), ApksResponseMother::empty());

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldThrowExceptionWhenDontNeedUpdateStatus(): void
    {
        $this->expectException(ItemDontNeedUpdateStatus::class);

        $command = PublishApkStatusCommandMother::random();

        $id = ItemIdMother::create($command->itemId());
        $item = ItemMother::withIdAndStatus($id, ItemStatus::APK_PUBLISH);
        $item->pullDomainEvents();

        $this->shouldSearchItem($id, $item);
        $this->shouldAsk(SearchApksByCriteriaQueryMother::create([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => $item->id()->value()
            ],
            [
                'field' => 'state',
                'operator' => '=',
                'value' => State::processed()
            ]
        ]), ApksResponseMother::withItemId($id));

        $this->dispatch($command, $this->handler);
    }
}
