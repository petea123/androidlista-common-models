<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Relevant\ByPackageId;

use App\Al\Items\Application\Update\Status\Relevant\ByPackageId\MarkItemAsRelevantByPackageIdAndLanguageCommand;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Shared\Domain\LocaleMother;

final class MarkItemAsRelevantByPackageIdAndLanguageCommandMother
{
    public static function create(
        ItemGoogle $google,
        LocaleValueObject $locale
    ): MarkItemAsRelevantByPackageIdAndLanguageCommand {
        return new MarkItemAsRelevantByPackageIdAndLanguageCommand($google->id(), $locale->value());
    }

    public static function random(): MarkItemAsRelevantByPackageIdAndLanguageCommand
    {
        return self::create(
            ItemGoogleMother::random(),
            LocaleMother::random()
        );
    }
}
