<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Update\Status\Relevant\ByPackageId;

use App\Al\Items\Application\Update\Status\Relevant\ByPackageId\MarkItemAsRelevantByPackageIdAndLanguageCommandHandler;
use App\Al\Items\Application\Update\Status\Relevant\ByPackageId\RelevantMarkerByPackageIdAndLanguage;
use App\Tests\Al\Items\Domain\ApkPure\ItemApkPureMother;
use App\Tests\Al\Items\Domain\ApkPure\ItemApkPurePackageMother;
use App\Tests\Al\Items\Domain\Event\ItemMarkedAsRelevantMother;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\Items\Domain\ItemMother;
use App\Tests\Al\Items\Domain\ItemStatusMother;
use App\Tests\Al\Items\ItemModuleUnitTestCase;
use App\Tests\Shared\Domain\DuplicatorMother;
use App\Tests\Shared\Domain\LocaleMother;
use DomainException;

final class RelevantMarkerByPackageIdAndLanguageTest extends ItemModuleUnitTestCase
{
    /** @var MarkItemAsRelevantByPackageIdAndLanguageCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $marker = new RelevantMarkerByPackageIdAndLanguage(
            $this->repository(),
            $this->publisher()
        );

        $this->handler = new MarkItemAsRelevantByPackageIdAndLanguageCommandHandler($marker);
    }

    /** @test */
    public function itShouldMarkAsRelevantItem(): void
    {
        $command = MarkItemAsRelevantByPackageIdAndLanguageCommandMother::random();

        $package = ItemGoogleMother::withPackage($command->packageId());
        $locale = LocaleMother::create($command->locale());


        $item = ItemMother::withPackageAndPublic($package);
        $item = DuplicatorMother::with($item, ['statuses' => ItemStatusMother::markedRelevant($item->statuses(), $locale)]);

        $event = ItemMarkedAsRelevantMother::create($item->id(), $locale);

        $this->shouldSearchItemByPackage($package, $item);
        $this->shouldSaveItem($item);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldMarkAsRelevantItemApkPure(): void
    {
        $command = MarkItemAsRelevantByPackageIdAndLanguageCommandMother::random();

        $package = ItemGoogleMother::withPackage($command->packageId());
        $apkPurePackage = ItemApkPurePackageMother::withPackage($command->packageId());
        $locale = LocaleMother::create($command->locale());


        $item = ItemApkPureMother::withPackageAndPublic($apkPurePackage);
        $item = DuplicatorMother::with($item, ['statuses' => ItemStatusMother::markedRelevant($item->statuses(), $locale)]);

        $event = ItemMarkedAsRelevantMother::create($item->id(),  $locale);

        $this->shouldSearchItemByPackage($package, $item);
        $this->shouldSaveItem($item);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldntMarkAsRelevantWith404(): void
    {
        $command = MarkItemAsRelevantByPackageIdAndLanguageCommandMother::random();

        $package = ItemGoogleMother::withPackage($command->packageId());
        $apkPurePackage = ItemApkPurePackageMother::withPackage($command->packageId());

        $item = ItemApkPureMother::withPackageAndPublic($apkPurePackage);
        $item = DuplicatorMother::with($item, ['statuses' => ItemStatusMother::as404($item->statuses())]);

        $this->shouldSearchItemByPackage($package, $item);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldThrowExceptionMarkAsRelevantItemApkPureWithoutApk(): void
    {
        $this->expectException(DomainException::class);
        $command = MarkItemAsRelevantByPackageIdAndLanguageCommandMother::random();

        $package = ItemGoogleMother::withPackage($command->packageId());
        $apkPurePackage = ItemApkPurePackageMother::withPackage($command->packageId());

        $item = ItemApkPureMother::withPackageAndPublic($apkPurePackage);
        $item = DuplicatorMother::with($item, ['statuses' => ItemStatusMother::unpublish($item->statuses())]);

        $this->shouldSearchItemByPackage($package, $item);

        $this->dispatch($command, $this->handler);
    }
}
