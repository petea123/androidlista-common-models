<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Create\ApkPure;

use App\Al\Items\Application\Create\ApkPure\CreateItemApkPureByPackageIdCommandHandler;
use App\Al\Items\Application\Create\ApkPure\ItemApkPureCreator;
use App\Al\Items\Domain\ItemCategory;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Tests\Al\Items\Domain\ApkPure\ItemApkPureMother;
use App\Tests\Al\Items\Domain\ApkPure\ItemApkPurePackageMother;
use App\Tests\Al\Items\Domain\ApkPure\ItemApkPureWasCreatedMother;
use App\Tests\Al\Items\ItemModuleUnitTestCase;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use function Lambdish\Phunctional\map;

final class ItemApkPureCreatorTest extends ItemModuleUnitTestCase
{
    /** @var CreateItemApkPureByPackageIdCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $publisher = new ItemApkPureCreator(
            $this->repository(),
            $this->webParserFactory(),
            $this->publisher()
        );

        $this->handler = new CreateItemApkPureByPackageIdCommandHandler($publisher);
    }

    /** @test */
    public function itShouldCreateItemApkPure(): void
    {
        $command = CreateItemApkPureByPackageIdCommandMother::random();

        $package = ItemApkPurePackageMother::withPackage($command->packageId());
        $item = ItemApkPureMother::createNewWithPackage($package);
        $event = ItemApkPureWasCreatedMother::fromItem($item);

        $this->shouldSearchItemByPackage($package);
        $this->shouldInstanceWebParser(ApkSource::apkpure(), $package, true);
        $this->shouldGetWebParserApp($package, true);

        $this->shouldGetWebParserAppNames($item->name());
        $this->shouldGetWebParserAppVotes($item->votes());
        $this->shouldGetWebParserAppRating($item->note());
        $this->shouldGetWebParserAppPrice($item->price()->value());
        $this->shouldGetWebParserAppSize($item->size()->value());
        $this->shouldGetWebParserAppMinOsVersion($item->minOsVersion()->value());
        $this->shouldGetWebParserAppProvider($item->provider()->value());
        $this->shouldGetWebParserAppCategories(map(function (ItemCategory $category) {
            return $category->categoryId();
        }, $item->categories()));
        $this->shouldGetWebParserAppOfficialUrl($item->officialUrl()->value());
        $this->shouldGetWebParserAppVersion($item->version()->value());

        $this->shouldGenerateNextItemId($item->id());
        $this->shouldSaveItem($item);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }
}
