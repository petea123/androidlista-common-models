<?php

declare(strict_types=1);

namespace App\Tests\Al\Items\Application\Create\ApkPure;

use App\Al\Items\Application\Create\ApkPure\CreateItemApkPureByPackageIdCommand;
use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Tests\Al\Items\Domain\ApkPure\ItemApkPurePackageMother;

final class CreateItemApkPureByPackageIdCommandMother
{
    public static function create(ItemApkPurePackage $package): CreateItemApkPureByPackageIdCommand
    {
        return new CreateItemApkPureByPackageIdCommand($package->id());
    }

    public static function random(): CreateItemApkPureByPackageIdCommand
    {
        return self::create(ItemApkPurePackageMother::random());
    }
}
