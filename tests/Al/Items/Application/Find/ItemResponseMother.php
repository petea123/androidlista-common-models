<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find;

use App\Al\Items\Application\Find\ItemResponse;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\Items\Domain\ItemNameMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class ItemResponseMother
{
    public static function create(
        ItemId $id,
        ItemGoogle $googleIdDirify,
        ItemName $name
    ): ItemResponse {
        return new ItemResponse($id->value(), $googleIdDirify->id(), $googleIdDirify->value(), $name->value());
    }

    public static function withId(ItemId $id): ItemResponse
    {
        return DuplicatorMother::with(self::random(), ['id' => $id->value()]);
    }

    public static function random(): ItemResponse
    {
        return self::create(
            ItemIdMother::random(),
            ItemGoogleMother::random(),
            ItemNameMother::random()
        );
    }

    public static function withValues(array $array): ItemResponse
    {
        return DuplicatorMother::with(self::random(),$array);
    }
}
