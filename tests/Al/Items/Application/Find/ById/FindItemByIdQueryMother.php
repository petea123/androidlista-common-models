<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find\ById;

use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class FindItemByIdQueryMother
{
    public static function create(ItemId $id): FindItemByIdQuery
    {
        return new FindItemByIdQuery($id->value(), 'en');
    }

    public static function random(): FindItemByIdQuery
    {
        return self::create(ItemIdMother::random());
    }
}
