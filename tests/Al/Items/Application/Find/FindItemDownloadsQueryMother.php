<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find;

use App\Al\Items\Application\Find\FindItemDownloadsQuery;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class FindItemDownloadsQueryMother
{
    public static function create(ItemId $id): FindItemDownloadsQuery
    {
        return new FindItemDownloadsQuery($id->value());
    }

    public static function random(): FindItemDownloadsQuery
    {
        return self::create(ItemIdMother::random());
    }
}
