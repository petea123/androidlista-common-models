<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find;

use App\Al\Items\Application\Find\ItemDownloadsResponse;
use App\Al\Items\Domain\ValueObject\ItemDownloads;
use App\Tests\Al\Shared\Domain\Items\ItemDownloadsMother;
use App\Tests\Shared\Domain\NumberMother;

final class ItemDownloadsResponseMother
{
    public static function create(
        ItemDownloads $downloads
    ): ItemDownloadsResponse {
        return new ItemDownloadsResponse($downloads->value());
    }

    public static function random(): ItemDownloadsResponse
    {
        return self::create(ItemDownloadsMother::random());
    }

    public static function withDownloads(): ItemDownloadsResponse
    {
        return self::create(ItemDownloadsMother::withDownloads(NumberMother::between(20)));
    }

    public static function empty(): ItemDownloadsResponse
    {
        return self::create(ItemDownloadsMother::create(0));
    }
}
