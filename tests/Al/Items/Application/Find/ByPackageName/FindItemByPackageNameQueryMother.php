<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find\ByPackageName;

use App\Al\Items\Application\Find\ByPackageName\FindItemByPackageNameQuery;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Tests\Al\Items\Domain\ItemGoogleMother;

final class FindItemByPackageNameQueryMother
{
    public static function create(ItemGoogle $google): FindItemByPackageNameQuery
    {
        return new FindItemByPackageNameQuery($google->id());
    }

    public static function random(): FindItemByPackageNameQuery
    {
        return self::create(ItemGoogleMother::random());
    }
}
