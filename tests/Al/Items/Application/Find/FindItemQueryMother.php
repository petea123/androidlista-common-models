<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find;

use App\Al\Items\Application\Find\FindItemQuery;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class FindItemQueryMother
{
    public static function create(ItemId $id): FindItemQuery
    {
        return new FindItemQuery($id->value());
    }

    public static function random(): FindItemQuery
    {
        return self::create(ItemIdMother::random());
    }
}
