<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find\Price;

use App\Al\Items\Application\Find\Price\ItemPriceResponse;
use App\Tests\Shared\Domain\NumberMother;

final class ItemPriceResponseMother
{
    public static function create(float $price): ItemPriceResponse
    {
        return new ItemPriceResponse($price);
    }

    public static function random(): ItemPriceResponse
    {
        return self::create(NumberMother::between(1));
    }
}
