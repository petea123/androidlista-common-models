<?php

declare(strict_types = 1);

namespace App\Tests\Al\Items\Application\Find\Price;

use App\Al\Items\Application\Find\Price\FindItemPriceQuery;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class FindItemPriceQueryMother
{
    public static function create(ItemId $id): FindItemPriceQuery
    {
        return new FindItemPriceQuery($id->value());
    }

    public static function random(): FindItemPriceQuery
    {
        return self::create(ItemIdMother::random());
    }
}
