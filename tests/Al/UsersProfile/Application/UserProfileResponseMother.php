<?php

declare(strict_types=1);

namespace App\Tests\Al\UsersProfile\Application;

use App\Al\UsersProfile\Application\UserProfileResponse;
use App\Tests\Al\UsersProfile\Domain\UserProfileAvatarMother;
use App\Tests\Al\UsersProfile\Domain\UserProfileFirstnameMother;
use App\Tests\Al\UsersProfile\Domain\UserProfileIdMother;
use App\Tests\Al\UsersProfile\Domain\UserProfileLastnameMother;
use App\Tests\Shared\Domain\Users\UserIdMother;

final class UserProfileResponseMother
{
    public static function create(int $id, int $userId, string $firstname, string $lastname, string $avatar): UserProfileResponse
    {
        return new UserProfileResponse($id, $userId, $firstname, $lastname, $avatar);
    }

    public static function withUserId(int $userId): UserProfileResponse
    {
        return new UserProfileResponse(
            UserProfileIdMother::random()->value(),
            UserIdMother::create($userId)->value(),
            UserProfileFirstnameMother::random()->value(),
            UserProfileLastnameMother::random()->value(),
            UserProfileAvatarMother::random()->value()
        );
    }
}
