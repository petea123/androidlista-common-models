<?php

declare(strict_types=1);

namespace App\Tests\Al\UsersProfile\Domain;

use App\Al\UsersProfile\Domain\UserProfileAvatar;

final class UserProfileAvatarMother
{
    public static function create(string $avatar): UserProfileAvatar
    {
        return new UserProfileAvatar($avatar);
    }

    public static function random(): UserProfileAvatar
    {
        return self::create('http://lorempixel.com/640/480/dafd.jpg');
    }
}
