<?php

declare(strict_types=1);

namespace App\Tests\Al\UsersProfile\Domain;

use App\Al\UsersProfile\Domain\UserProfileLastname;
use App\Tests\Shared\Domain\MotherCreator;

final class UserProfileLastnameMother
{
    public static function create(string $username): UserProfileLastname
    {
        return new UserProfileLastname($username);
    }

    public static function random(): UserProfileLastname
    {
        return self::create(MotherCreator::random()->word);
    }
}
