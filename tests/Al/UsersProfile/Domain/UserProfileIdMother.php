<?php

declare(strict_types=1);

namespace App\Tests\Al\UsersProfile\Domain;

use App\Al\UsersProfile\Domain\UserProfileId;
use App\Tests\Shared\Domain\IntegerMother;

final class UserProfileIdMother
{

    public static function create(int $id): UserProfileId
    {
        return new UserProfileId($id);
    }
    public static function random(): UserProfileId
    {
        return self::create(IntegerMother::random());
    }
}
