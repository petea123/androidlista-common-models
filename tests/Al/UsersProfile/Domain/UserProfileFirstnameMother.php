<?php

declare(strict_types=1);

namespace App\Tests\Al\UsersProfile\Domain;

use App\Al\UsersProfile\Domain\UserProfileFirstname;
use App\Tests\Shared\Domain\MotherCreator;

final class UserProfileFirstnameMother
{
    public static function create(string $username): UserProfileFirstname
    {
        return new UserProfileFirstname($username);
    }

    public static function random(): UserProfileFirstname
    {
        return self::create(MotherCreator::random()->word);
    }
}
