<?php

declare(strict_types=1);

namespace App\Tests\Al\Collections\Application;

use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Application\CollectionsResponse;
use App\Al\Collections\Domain\CollectionId;

final class CollectionsResponseMother
{
    public static function create(CollectionResponse ...$collections): CollectionsResponse
    {
        return new CollectionsResponse(...$collections);
    }

    public static function withOne(CollectionId $collectionId): CollectionsResponse
    {
        return self::create(CollectionResponseMother::withId($collectionId));
    }
}
