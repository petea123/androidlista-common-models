<?php

declare(strict_types=1);

namespace App\Tests\Al\Collections\Application;

use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Domain\CollectionId;
use App\Tests\Al\Collections\Domain\CollectionIdMother;
use App\Tests\Shared\Domain\IntegerMother;
use App\Tests\Shared\Domain\LocaleMother;
use App\Tests\Shared\Domain\WordMother;

final class CollectionResponseMother
{
    public static function create(
        int $id,
        CollectionId $collectionId,
        string $name,
        string $description,
        string $shortname,
        string $dirify,
        string $language
    ): CollectionResponse {
        return new CollectionResponse(
            $id,
            $collectionId->value(),
            $name,
            $description,
            $shortname,
            $dirify,
            $language
        );
    }

    public static function withId(CollectionId $collectionId): CollectionResponse
    {
        return self::create(
            IntegerMother::random(),
            $collectionId,
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            LocaleMother::random()->value()
        );
    }

    public static function random(): CollectionResponse
    {
        return self::create(
            IntegerMother::random(),
            CollectionIdMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            WordMother::random(),
            LocaleMother::random()->value()
        );
    }
}
