<?php

declare(strict_types=1);

namespace App\Tests\Al\Collections\Domain;

use App\Al\Collections\Domain\CollectionId;
use App\Tests\Shared\Domain\IntegerMother;

final class CollectionIdMother
{

    public static function create(int $id): CollectionId
    {
        return new CollectionId($id);
    }

    public static function random(): CollectionId
    {
        return self::create(IntegerMother::random());
    }
}
