<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Create;

use App\Al\Apks\Application\Create\CreateLegacyApkOnApkUploaded;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkWasUploadedMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class CreateLegacyApkOnApkWasUploadedTest extends ApkModuleUnitTestCase
{

    /** @var CreateLegacyApkOnApkUploaded */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new CreateLegacyApkOnApkUploaded($this->commandBus());

    }

    /** @test */
    public function itShouldCreateLegacyApkOnApkWasUploaded(): void
    {
        $event = ApkWasUploadedMother::random();

        $apkId = ApkIdMother::create($event->aggregateId());

        $command = CreateLegacyApkCommandMother::create($apkId);

        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);

        $this->notify($event, $this->subscriber);

    }
}
