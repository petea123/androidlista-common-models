<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Create;

use App\Al\Apks\Application\Create\CreateLegacyApkCommandHandler;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\ApkMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class CreateLegacyApkTest extends ApkModuleUnitTestCase
{
    /** @var CreateLegacyApkCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->handler = new CreateLegacyApkCommandHandler($this->repository(), $this->repository());
    }

    /** @test */
    public function itShouldCreateLegacyRow(): void
    {
        $command = CreateLegacyApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk->pullDomainEvents();

        $this->shouldSearchApk($id, $apk);
        $this->shouldSaveApk($apk);

        $this->dispatch($command, $this->handler);
    }
}
