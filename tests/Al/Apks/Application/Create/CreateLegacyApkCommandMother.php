<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Create;

use App\Al\Apks\Application\Create\CreateLegacyApkCommand;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class CreateLegacyApkCommandMother
{

    public static function create(ApkId $id): CreateLegacyApkCommand
    {
        return new CreateLegacyApkCommand($id->value());
    }

    public static function random(): CreateLegacyApkCommand
    {
        return self::create(ApkIdMother::random());
    }
}
