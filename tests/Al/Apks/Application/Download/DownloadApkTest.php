<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Download;

use App\Al\Apks\Application\Download\ApkDownloader;
use App\Al\Apks\Application\Download\DownloadApkCommandHandler;
use App\Al\Apks\Domain\Exception\VersionExists;
use App\Shared\Domain\ValueObject\PathValueObject;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Application\Search\ApkCriteriaMother;
use App\Tests\Al\Apks\Domain\ApkMother;
use App\Tests\Al\Apks\Domain\Event\ApkWasDownloadedMother;
use App\Tests\Al\Items\Application\Find\ById\FindItemByIdQueryMother;
use App\Tests\Al\Items\Domain\ItemGoogleMother;
use App\Tests\Al\Items\Infrastructure\Projections\ItemViewMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class DownloadApkTest extends ApkModuleUnitTestCase
{
    /** @var DownloadApkCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $downloader = new ApkDownloader(
            $this->downloader(),
            $this->filesystem(),
            $this->queryBus(),
            $this->repository(),
            $this->publisher(),
            $this->parser(),
            $this->webParserFactory()
        );

        $this->handler = new DownloadApkCommandHandler($downloader);
    }

    /** @test */
    public function itShouldDownloadAnApk(): void
    {
        $command = DownloadApkCommandMother::random();

        $apkId = ApkIdMother::create($command->id());
        $apkCreated = ApkMother::created(ApkMother::withId($apkId));
        $apk = ApkMother::downloaded($apkCreated);

        $event = ApkWasDownloadedMother::fromApk($apk);

        $itemResponse = ItemViewMother::withItemId($apk->itemId());
        $itemGoogle = ItemGoogleMother::create($itemResponse->web());

        $this->shouldSearchApk($apkId, $apkCreated);
        $this->shouldAsk(FindItemByIdQueryMother::create($apk->itemId()), $itemResponse);

        $this->shouldInstanceWebParser($apk->source(), $itemGoogle, true);
        $this->shouldGetWebParserApp($itemGoogle, true);
        $this->shouldGetWebParserApkDownloadUrl($apk->externalUrl());
        $this->shouldDownload($apk->externalUrl(), $this->file());

        $this->shouldSetApkStream($this->file());
        $this->shouldParseDisplayedVersion($apk->version());

        $this->shouldCriteriaMatching(ApkCriteriaMother::forEnsureVersionInItem($apk));

        $this->shouldParseSize($apk->size());
        $this->shouldParseSha1($apk->sha1());
        $this->shouldParseSignature($apk->signature());
        $this->shouldPutStream(PathValueObject::create('local://'.$apk->url()->value()), $this->file(), true);
        $this->shouldSaveApk($apk);
        $this->shouldRecordDomainEvents($event);
        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldNotDownloadApkWhenTheItemNotExist(): void
    {
        $command = DownloadApkCommandMother::random();

        $apkId = ApkIdMother::create($command->id());
        $apk = ApkMother::created(ApkMother::withId($apkId));

        $this->shouldSearchApk($apkId, $apk);
        $this->shouldAsk(FindItemByIdQueryMother::create($apk->itemId()));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldDeleteApkWhenVersionExistInItem(): void
    {
        $command = DownloadApkCommandMother::random();

        $apkId = ApkIdMother::create($command->id());
        $apkCreated = ApkMother::created(ApkMother::withId($apkId));
        $apk = ApkMother::downloaded($apkCreated);

        $itemResponse = ItemViewMother::withItemId($apk->itemId());
        $itemGoogle = ItemGoogleMother::create($itemResponse->web());

        $this->shouldSearchApk($apkId, $apkCreated);
        $this->shouldAsk(FindItemByIdQueryMother::create($apk->itemId()), $itemResponse);

        $this->shouldInstanceWebParser($apk->source(), $itemGoogle, true);
        $this->shouldGetWebParserApp($itemGoogle, true);
        $this->shouldGetWebParserApkDownloadUrl($apk->externalUrl());
        $this->shouldDownload($apk->externalUrl(), $this->file());

        $this->shouldSetApkStream($this->file());
        $this->shouldParseDisplayedVersion($apk->version());

        $this->shouldCriteriaMatching(ApkCriteriaMother::forEnsureVersionInItem($apk), [$apk]);
        $this->shouldDeleteApk($apkCreated);
        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldDoNothingIfNoDownload(): void
    {
        $command = DownloadApkCommandMother::random();

        $apkId = ApkIdMother::create($command->id());
        $apkCreated = ApkMother::created(ApkMother::withId($apkId));
        $apk = ApkMother::downloaded($apkCreated);

        $itemResponse = ItemViewMother::withItemId($apk->itemId());
        $itemGoogle = ItemGoogleMother::create($itemResponse->web());

        $this->shouldSearchApk($apkId, $apkCreated);
        $this->shouldAsk(FindItemByIdQueryMother::create($apk->itemId()), $itemResponse);

        $this->shouldInstanceWebParser($apk->source(), $itemGoogle, true);
        $this->shouldGetWebParserApp($itemGoogle, true);
        $this->shouldGetWebParserApkDownloadUrl($apk->externalUrl());
        $this->shouldDownload($apk->externalUrl());

        $this->dispatch($command, $this->handler);
    }
}
