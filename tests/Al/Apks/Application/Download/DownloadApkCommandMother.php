<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Download;

use App\Al\Apks\Application\Download\DownloadApkCommand;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class DownloadApkCommandMother
{

    public static function create(ApkId $id): DownloadApkCommand
    {
        return new DownloadApkCommand($id->value());
    }

    public static function random(): DownloadApkCommand
    {
        return self::create(ApkIdMother::random());
    }
}
