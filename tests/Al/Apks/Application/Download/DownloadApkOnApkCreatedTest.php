<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Download;

use App\Al\Apks\Application\Download\DownloadApkOnApkCreated;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkWasCreatedMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class DownloadApkOnApkCreatedTest extends ApkModuleUnitTestCase
{

    /** @var DownloadApkOnApkCreated */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new DownloadApkOnApkCreated($this->commandBus());
    }

    /** @test */
    public function itShouldDownloadApkOnApkCreated(): void
    {
        $event = ApkWasCreatedMother::random();

        $apkId = ApkIdMother::create($event->aggregateId());

        $command = DownloadApkCommandMother::create($apkId);
        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);
    }
}
