<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Search;

use App\Al\Apks\Domain\Apk;
use App\Shared\Domain\Criteria\Criteria;
use App\Tests\Shared\Domain\Criteria\CriteriaMother;
use App\Tests\Shared\Domain\Criteria\FilterMother;
use App\Tests\Shared\Domain\Criteria\FiltersMother;

final class ApkCriteriaMother
{
    public static function forEnsureVersionInItem(Apk $apk): Criteria
    {
        return CriteriaMother::create(FiltersMother::create([
            FilterMother::fromValues([
                'field' => 'itemId',
                'operator' => '=',
                'value' => (string)$apk->itemId()->value()
            ]),
            FilterMother::fromValues([
                'field' => 'version.value',
                'operator' => '=',
                'value' => $apk->version()->value()
            ]),
        ]));
    }
}
