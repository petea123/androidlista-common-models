<?php

declare(strict_types = 1);

namespace App\Tests\Al\Apks\Application\Search;

use App\Al\Apks\Application\Search\ApkResponse;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Signature;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\SignatureMother;
use App\Tests\Al\Apks\Domain\ValueObject\VersionMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ApkResponseMother
{
    public static function create(
        ApkId $id,
        ItemId $itemId,
        Signature $signature,
        Version $version
    ): ApkResponse {
        return new ApkResponse($id->value(), $itemId->value(), $signature->value(), $version->value());
    }

    public static function random(): ApkResponse
    {
        return self::create(
            ApkIdMother::random(),
            ItemIdMother::random(),
            SignatureMother::random(),
            VersionMother::random()
        );
    }

    public static function withId(ApkId $id): ApkResponse
    {
        return self::create(
            $id,
            ItemIdMother::random(),
            SignatureMother::random(),
            VersionMother::random()
        );
    }

    public static function withIdAndItemId(ApkId $id, ItemId $itemId): ApkResponse
    {
        return self::create(
            $id,
            $itemId,
            SignatureMother::random(),
            VersionMother::random()
        );
    }

    public static function withItemId(ItemId $itemId)
    {
        return self::create(
            ApkIdMother::random(),
            $itemId,
            SignatureMother::random(),
            VersionMother::random()
        );
    }
}
