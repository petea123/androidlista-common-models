<?php

declare(strict_types = 1);

namespace App\Tests\Al\Apks\Application\Search;

use App\Al\Apks\Application\Search\ApkResponse;
use App\Al\Apks\Application\Search\ApksResponse;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Shared\Domain\NumberMother;

final class ApksResponseMother
{
    public static function create(ApkResponse ...$apksResponse): ApksResponse
    {
        return new ApksResponse(...$apksResponse);
    }

    public static function random(): ApksResponse
    {
        $elements = NumberMother::between(1,4);

        $array = [];

        for ($i = 0 ; $i <= $elements ; ++$i) {
            $array[] = ApkResponseMother::random();
        }

        return self::create(...$array);
    }

    public static function withId(ApkId $id): ApksResponse
    {
        $elements = NumberMother::between(1,4);

        $array = [];

        for ($i = 0 ; $i <= $elements ; ++$i) {
            $array[] = ApkResponseMother::withId($id);
        }

        return self::create(...$array);
    }

    public static function withIdAndItemId(ApkId $id, ItemId $itemId): ApksResponse
    {
        $elements = NumberMother::between(1,4);

        $array = [];

        for ($i = 0 ; $i <= $elements ; ++$i) {
            $array[] = ApkResponseMother::withIdAndItemId($id, $itemId);
        }

        return self::create(...$array);
    }

    public static function withItemId(ItemId $itemId): ApksResponse
    {
        $elements = NumberMother::between(1,4);

        $array = [];

        for ($i = 0 ; $i <= $elements ; ++$i) {
            $array[] = ApkResponseMother::withItemId($itemId);
        }

        return self::create(...$array);
    }

    public static function empty(): ApksResponse
    {
        return self::create(...[]);
    }
}
