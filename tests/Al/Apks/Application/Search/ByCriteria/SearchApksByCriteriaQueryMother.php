<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Search\ByCriteria;

use App\Al\Apks\Application\Search\ByCriteria\SearchApksByCriteriaQuery;

final class SearchApksByCriteriaQueryMother
{
    public static function create(array $filters): SearchApksByCriteriaQuery
    {
        return new SearchApksByCriteriaQuery($filters);
    }
}
