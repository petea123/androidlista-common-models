<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Scan;

use App\Al\Apks\Application\Scan\RetrieveApkReportCommandHandler;
use App\Al\Apks\Application\Scan\RetrieveReport;
use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Event\ApkWasDangerous;
use App\Al\Apks\Domain\Event\ApkWasScanned;
use App\Al\Apks\Domain\Exception\ApkReportNotRetrieved;
use App\Shared\Domain\ValueObject\PathValueObject;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\ApkMother;
use App\Tests\Al\Apks\Domain\ValueObject\ReportMother;
use App\Tests\Al\Apks\Domain\ValueObject\StateMother;
use App\Tests\Al\Items\Application\Find\FindItemDownloadsQueryMother;
use App\Tests\Al\Items\Application\Find\ItemDownloadsResponseMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class RetrieveReportTest extends ApkModuleUnitTestCase
{
    /** @var RetrieveApkReportCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $retriever = new RetrieveReport(
            $this->scanner(),
            $this->repository(),
            $this->queryBus(),
            $this->publisher(),
            $this->filesystem(),
        );

        $this->handler = new RetrieveApkReportCommandHandler($retriever);
    }

    /** @test */
    public function itShouldPendingUploadWhenRetrieveValidReport(): void
    {
        $command = RetrieveApkReportCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);

        $pendingReport = ReportMother::pendingReport();

        /** @var Apk $apk */
        $apk = DuplicatorMother::with(
            $apk,
            [
                'report' => $pendingReport
            ]
        );

        $validReport = ReportMother::validatePendingReport($pendingReport);

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $validReport,
                'state' => StateMother::pendingUpload()
            ]
        );

        $this->shouldSearchApk($id, $apk);
        $this->shouldRetrieveReport($pendingReport->resource(), $validReport->value());
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(new ApkWasScanned(
            $id->value(), $updatedApk->itemId()->value(), $validReport->permalink(), $updatedApk->source()->value(), $updatedApk->version()->value()));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldThrowExceptionWhenRetrievePendingReport(): void
    {
        $this->expectException(ApkReportNotRetrieved::class);

        $command = RetrieveApkReportCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $pendingReport = ReportMother::pendingReport();

        /** @var Apk $apk */
        $apk = DuplicatorMother::with(
            $apk,
            [
                'report' => $pendingReport
            ]
        );


        $this->shouldSearchApk($id, $apk);
        $this->shouldRetrieveReport($pendingReport->resource(), $pendingReport->value());

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldBypassReportWhenRetrievePositiveReportWithMoreDownloads(): void
    {
        $command = RetrieveApkReportCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $dangerousReport = ReportMother::dangerousReport();

        /** @var Apk $apk */
        $apk = DuplicatorMother::with(
            $apk,
            [
                'report' => $dangerousReport
            ]
        );

        $validReport = ReportMother::bypassReport($dangerousReport->resource());

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $validReport,
                'state' => StateMother::pendingUpload()
            ]
        );

        $itemDownloadsResponse = ItemDownloadsResponseMother::withDownloads();

        $this->shouldSearchApk($id, $apk);
        $this->shouldRetrieveReport($dangerousReport->resource(), $dangerousReport->value());
        $this->shouldAsk(FindItemDownloadsQueryMother::create($apk->itemId()), $itemDownloadsResponse);
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(new ApkWasScanned(
            $id->value(), $updatedApk->itemId()->value(), $validReport->permalink(), $updatedApk->source()->value(),$updatedApk->version()->value()));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldMarkDangerousWhenRetrieveReportWithLessDownloads(): void
    {
        $command = RetrieveApkReportCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $pendingReport = ReportMother::pendingReport();

        /** @var Apk $apk */
        $apk = DuplicatorMother::with(
            $apk,
            [
                'report' => $pendingReport
            ]
        );

        $dangerousReport = ReportMother::markDangerous($pendingReport);

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $dangerousReport,
                'state' => StateMother::dangerous()
            ]
        );

        $events = [
            new ApkWasDangerous(
                $id->value(), $updatedApk->itemId()->value(), $updatedApk->source()->value(), $updatedApk->version()->value()
            )
        ];

        $itemDownloadsResponse = ItemDownloadsResponseMother::empty();

        $this->shouldSearchApk($id, $apk);
        $this->shouldRetrieveReport($dangerousReport->resource(), $dangerousReport->value());
        $this->shouldAsk(FindItemDownloadsQueryMother::create($apk->itemId()), $itemDownloadsResponse);
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(...$events);

        $this->dispatch($command, $this->handler);
    }
}
