<?php

declare(strict_types=1);


namespace App\Tests\Al\Apks\Application\Scan;

use App\Al\Apks\Application\Scan\ScanVirusOnApkDownloaded;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkWasDownloadedMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class ScanVirusOnApkCreatedTest extends ApkModuleUnitTestCase
{

    /** @var ScanVirusOnApkDownloaded */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new ScanVirusOnApkDownloaded($this->commandBus());

    }

    /** @test */
    public function itShouldScanApkOnApkWasDownloaded(): void
    {
        $event = ApkWasDownloadedMother::random();

        $command = ScanApkCommandMother::create(ApkIdMother::create($event->aggregateId()));
        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);
    }
}