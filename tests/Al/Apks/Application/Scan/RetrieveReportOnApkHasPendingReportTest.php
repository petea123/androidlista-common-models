<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Scan;

use App\Al\Apks\Application\Scan\RetrieveReportOnApkPendingReport;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkHasPendingReportMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class RetrieveReportOnApkHasPendingReportTest extends ApkModuleUnitTestCase
{
    /** @var RetrieveReportOnApkPendingReport */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new RetrieveReportOnApkPendingReport($this->commandBus());
    }

    /** @test */
    public function itShouldScanApkOnApkWasDownloaded(): void
    {
        $event = ApkHasPendingReportMother::random();

        $command = RetrieveApkReportCommandMother::create(ApkIdMother::create($event->aggregateId()));
        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);
    }

}
