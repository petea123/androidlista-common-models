<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Scan;

use App\Al\Apks\Application\Scan\RetrieveApkReportCommand;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class RetrieveApkReportCommandMother
{

    public static function create(ApkId $id): RetrieveApkReportCommand
    {
        return new RetrieveApkReportCommand($id->value());
    }

    public static function random(): RetrieveApkReportCommand
    {
        return self::create(ApkIdMother::random());
    }
}
