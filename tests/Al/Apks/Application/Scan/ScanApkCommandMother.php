<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Scan;

use App\Al\Apks\Application\Scan\ScanApkCommand;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class ScanApkCommandMother
{

    public static function create(ApkId $id): ScanApkCommand
    {
        return new ScanApkCommand($id->value());
    }

    public static function random(): ScanApkCommand
    {
        return self::create(ApkIdMother::random());
    }
}
