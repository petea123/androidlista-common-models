<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Scan;

use App\Al\Apks\Application\Scan\ScanApkCommandHandler;
use App\Al\Apks\Application\Scan\ScanVirus;
use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Event\ApkHasPendingReport;
use App\Al\Apks\Domain\Event\ApkWasDangerous;
use App\Al\Apks\Domain\Event\ApkWasScanned;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\ApkMother;
use App\Tests\Al\Apks\Domain\ValueObject\ReportMother;
use App\Tests\Al\Apks\Domain\ValueObject\StateMother;
use App\Tests\Al\Items\Application\Find\FindItemDownloadsQueryMother;
use App\Tests\Al\Items\Application\Find\ItemDownloadsResponseMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;
use App\Tests\Shared\Domain\UrlMother;

final class ScanVirusTest extends ApkModuleUnitTestCase
{
    /** @var ScanApkCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $scanner = new ScanVirus(
            $this->scanner(),
            $this->repository(),
            $this->queryBus(),
            $this->publisher()
        );

        $this->handler = new ScanApkCommandHandler($scanner);
    }

    /** @test */
    public function itShouldPendingUploadWhenScanAValidApk(): void
    {
        $command = ScanApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $validReport = ReportMother::validReport();

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $validReport,
                'state' => StateMother::pendingUpload()
            ]
        );

        $link = UrlMother::random();
        $this->shouldSearchApk($id, $apk);
        $this->shouldAnalyze($apk, $validReport->value());
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(new ApkWasScanned(
            $id->value(), $updatedApk->itemId()->value(), $validReport->permalink(), $updatedApk->source()->value(), $updatedApk->version()->value()));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldPendingUploadWhenScanAnApkWithPendingReport(): void
    {
        $command = ScanApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $pendingReport = ReportMother::pendingReport();

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $pendingReport
            ]
        );
        $link = UrlMother::random();
        $this->shouldSearchApk($id, $apk);
        $this->shouldAnalyze($apk, $pendingReport->value());
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(new ApkHasPendingReport(
            $id->value(), $updatedApk->itemId()->value(), $pendingReport->resource(), $updatedApk->source()->value(), $updatedApk->version()->value())
        );

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldBypassReportWhenScanAnApkWithMoreDownloads(): void
    {
        $command = ScanApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $dangerousReport = ReportMother::dangerousReport();
        $validReport = ReportMother::bypassReport($dangerousReport->resource());

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $validReport,
                'state' => StateMother::pendingUpload()
            ]
        );

        $itemDownloadsResponse = ItemDownloadsResponseMother::withDownloads();
        $link = UrlMother::random();
        $this->shouldSearchApk($id, $apk);
        $this->shouldAnalyze($apk, $dangerousReport->value());
        $this->shouldAsk(FindItemDownloadsQueryMother::create($apk->itemId()), $itemDownloadsResponse);
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(new ApkWasScanned(
            $id->value(), $updatedApk->itemId()->value(), $validReport->permalink(), $updatedApk->source()->value(), $updatedApk->version()->value()
        ));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldMarkDangerousWhenScanAnApkWithLessDownloads(): void
    {
        $command = ScanApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::withId($id);
        $apk = ApkMother::created($apk);
        $apk = ApkMother::downloaded($apk);
        $apk->pullDomainEvents();

        $dangerousReport = ReportMother::dangerousReport();

        /** @var Apk $updatedApk */
        $updatedApk = DuplicatorMother::with(
            $apk,
            [
                'report' => $dangerousReport,
                'state' => StateMother::dangerous()
            ]
        );

        $events = [
            new ApkWasDangerous(
                $id->value(), $updatedApk->itemId()->value(), $updatedApk->source()->value(), $updatedApk->version()->value()
            )
        ];

        $itemDownloadsResponse = ItemDownloadsResponseMother::empty();
        $this->shouldSearchApk($id, $apk);
        $this->shouldAnalyze($apk, $dangerousReport->value());
        $this->shouldAsk(FindItemDownloadsQueryMother::create($apk->itemId()), $itemDownloadsResponse);
        $this->shouldSaveApk($updatedApk);
        $this->shouldRecordDomainEvents(...$events);

        $this->dispatch($command, $this->handler);
    }
}
