<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Upload;

use App\Al\Apks\Application\Upload\ApkUploader;
use App\Al\Apks\Application\Upload\UploadApkCommandHandler;
use App\Shared\Domain\ValueObject\PathValueObject;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\ApkMother;
use App\Tests\Al\Apks\Domain\Event\ApkWasUploadedMother;
use App\Tests\Al\Apks\Domain\ValueObject\StateMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\UrlMother;

final class ApkUploaderTest extends ApkModuleUnitTestCase
{
    /** @var UploadApkCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $downloader = new ApkUploader(
            $this->repository(),
            $this->publisher(),
            $this->filesystem()
        );

        $this->handler = new UploadApkCommandHandler($downloader);
    }

    /** @test */
    public function itShouldUploadAnApk(): void
    {
        $command = UploadApkCommandMother::random();

        $id = ApkIdMother::create($command->id());

        $apk = ApkMother::random();
        $apk = ApkMother::scanned($apk);

        $uploadedApk = ApkMother::uploading($apk);

        $this->shouldSearchApk($id, $apk);
        $this->shouldMoveFile(
            PathValueObject::create('local://'.$apk->url()->value()),
            PathValueObject::create('s3://'.$apk->url()->value()),
            true
        );

        $this->shouldSaveApk($uploadedApk);
        $this->shouldRecordDomainEvents(ApkWasUploadedMother::fromApk($uploadedApk));

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldNoUploadWhenApkIsNotPendingUpload(): void
    {
        $command = UploadApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::scanned(ApkMother::random());
        $apk = ApkMother::uploading($apk);

        $this->shouldSearchApk($id, $apk);
        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldDeleteApkWhenApkFailUploading(): void
    {
        $command = UploadApkCommandMother::random();

        $id = ApkIdMother::create($command->id());
        $apk = ApkMother::random();
        $apk = ApkMother::scanned($apk);
        $path = PathValueObject::create('local://'.$apk->url()->value());

        $this->shouldSearchApk($id, $apk);
        $this->shouldMoveFile(
            $path,
            PathValueObject::create('s3://'.$apk->url()->value()),
            false
        );
        $this->shouldDelete($path, true);

        $this->dispatch($command, $this->handler);
    }
}
