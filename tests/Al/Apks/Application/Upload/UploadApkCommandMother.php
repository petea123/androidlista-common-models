<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Application\Upload;

use App\Al\Apks\Application\Upload\UploadApkCommand;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

final class UploadApkCommandMother
{

    public static function create(ApkId $id): UploadApkCommand
    {
        return new UploadApkCommand($id->value());
    }

    public static function random(): UploadApkCommand
    {
        return self::create(ApkIdMother::random());
    }
}
