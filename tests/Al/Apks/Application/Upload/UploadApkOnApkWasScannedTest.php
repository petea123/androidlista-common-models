<?php

declare(strict_types=1);


namespace App\Tests\Al\Apks\Application\Upload;

use App\Al\Apks\Application\Upload\UploadApkOnApkScanned;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;
use App\Tests\Al\Apks\Domain\Event\ApkWasScannedMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;

class UploadApkOnApkWasScannedTest  extends ApkModuleUnitTestCase
{


    /** @var UploadApkOnApkScanned */
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new UploadApkOnApkScanned($this->commandBus());

    }

    /** @test */
    public function itShouldUploadApkOnApkWasScanned(): void
    {
        $event = ApkWasScannedMother::random();

        $command = UploadApkCommandMother::create(ApkIdMother::create($event->aggregateId()));
        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);
    }
}
