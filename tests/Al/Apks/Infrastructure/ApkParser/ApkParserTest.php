<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Infrastructure\ApkParser;

use App\Al\Apks\Infrastructure\Androguard\Androguard;
use App\Al\Apks\Infrastructure\ApkParser\ApkParser;
use App\Al\Apks\Infrastructure\ApkParser\BadZipFileException;
use App\Tests\Al\Apks\ApkModuleUnitTestCase;

final class ApkParserTest extends ApkModuleUnitTestCase
{
    private const PATH = __DIR__.'/../test.apk';

    private const BADZIP = __DIR__.'/badzipfile.zip';

    /** @var ApkParser */
    private $parser;
    /**
     * @var \SplFileInfo
     */
    private $apk;
    /**
     * @var \SplFileInfo
     */
    private $badzip;

    public function setUp()
    {
        parent::setUp();

        $this->apk = new \SplFileInfo(self::PATH);
        $this->badzip = new \SplFileInfo(self::BADZIP);
        $this->parser = new ApkParser(
            new Androguard()
        );
    }

    /**
     * @test
     *
     * @group unit
     *
     */
//    public function itShouldParseSignature(): void
//    {
//        $this->parser->setApkStream($this->apk);
//        self::assertSame(strtoupper('67bbd593eba753a6c7dc332e0f9285a6c0c6bdc6'), $this->parser->signature()->value());
//    }

    /**
     * @test
     *
     * @group unit
     *
     */
    public function itShouldParseSha1(): void
    {
        $this->parser->setApkStream($this->apk);
        self::assertSame(strtoupper('14c544156be0001f0a888fead090a99221f47255'), $this->parser->sha1()->value());
    }

    /**
     * @test
     *
     * @group unit
     *
     */
    public function itShouldParseSize(): void
    {
        $this->parser->setApkStream($this->apk);
        self::assertSame(83280, $this->parser->size()->value());
    }

    /**
     * @test
     *
     * @group unit
     *
     */
//    public function itShouldParseDisplayedVersion(): void
//    {
//        $this->parser->setApkStream($this->apk);
//        self::assertSame('1.0', $this->parser->displayedVersion()->value());
//    }
}
