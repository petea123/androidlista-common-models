<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Service\ApkParserInterface;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\Service\ScannerInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Sha1;
use App\Al\Apks\Domain\ValueObject\File\Signature;
use App\Al\Apks\Domain\ValueObject\File\Size;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Shared\Domain\Criteria\Criteria;
use App\Al\Shared\Domain\Items\ItemId;
use App\Al\Shared\Domain\UploaderInterface;
use App\Shared\Domain\FilesystemInterface;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

abstract class ApkModuleUnitTestCase extends ModuleUnitTestCase
{

    private $repository;
    private $parser;
    private $scanner;
    private $uploader;
    private $filesystem;

    /** @return ApkRepositoryInterface|MockInterface */
    protected function repository()
    {
        return $this->repository = $this->repository ?: $this->mock(ApkRepositoryInterface::class);
    }

    /** @return UploaderInterface|MockInterface */
    protected function uploader()
    {
        return $this->uploader = $this->uploader ?: $this->mock(UploaderInterface::class);
    }

    /** @return ApkParserInterface|MockInterface */
    protected function parser()
    {
        return $this->parser = $this->parser ?: $this->mock(ApkParserInterface::class);
    }

    /** @return ScannerInterface|MockInterface */
    protected function scanner()
    {
        return $this->scanner = $this->scanner ?: $this->mock(ScannerInterface::class);
    }

    /** @return FilesystemInterface|MockInterface */
    protected function filesystem()
    {
        return $this->filesystem = $this->filesystem ?: $this->mock(FilesystemInterface::class);
    }

    protected function shouldSaveApk(Apk $apk): void
    {
        $this->repository()
            ->shouldReceive('save')
            ->with($this->similarTo($apk))
            ->once()
            ->andReturnNull();
    }

    protected function shouldSearchApk(ApkId $id, Apk $apk = null): void
    {
        $this->repository()
            ->shouldReceive('search')
            ->with($this->similarTo($id))
            ->once()
            ->andReturn($apk);
    }

    protected function shouldDeleteApk(Apk $apk): void
    {
        $this->repository()
            ->shouldReceive('delete')
            ->with($this->similarTo($apk))
            ->once()
            ->andReturnNull();
    }

    protected function shouldSearchVersionInItem(ItemId $itemId, Version $version, Apk $apk = null): void
    {
        $this->repository()
            ->shouldReceive('matching')
            ->with($this->similarTo($itemId), $this->similarTo($version))
            ->once()
            ->andReturn($apk);
    }

    protected function shouldCriteriaMatching(Criteria $criteria, array $apk = []): void
    {
        $this->repository()
            ->shouldReceive('matching')
            ->with($this->similarTo($criteria))
            ->once()
            ->andReturn($apk);
    }

    protected function shouldUpload(string $url, bool $response): void
    {
        $this->uploader()
            ->shouldReceive('upload')
            ->once()
            ->with($this->similarTo($url))
            ->andReturn($response);
    }

    protected function shouldSetApkStream(\SplFileInfo $apk): void
    {
        $this->parser()
            ->shouldReceive('setApkStream')
            ->once()
            ->with($apk)
            ->andReturnNull();
    }

    protected function shouldParseSignature(Signature $signature = null): void
    {
        $this->parser()
            ->shouldReceive('signature')
            ->once()
            ->andReturn($signature);
    }

    protected function shouldParseSize(Size $size = null): void
    {
        $this->parser()
            ->shouldReceive('size')
            ->once()
            ->andReturn($size);
    }

    protected function shouldParseDisplayedVersion(Version $version = null): void
    {
        $this->parser()
            ->shouldReceive('displayedVersion')
            ->once()
            ->andReturn($version);
    }

    protected function shouldParseSha1(Sha1 $sha1 = null): void
    {
        $this->parser()
            ->shouldReceive('sha1')
            ->once()
            ->andReturn($sha1);
    }

    protected function shouldAnalyze(Apk $apk, array $report): void
    {
        $this->scanner()
            ->shouldReceive('analyze')
            ->once()
            ->with($apk)
            ->andReturn($report);
    }

    protected function shouldRetrieveReport(string $resource, array $report): void
    {
        $this->scanner()
            ->shouldReceive('getReportAllInfo')
            ->once()
            ->with($resource)
            ->andReturn($report);
    }
}
