<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject;

use App\Al\Apks\Domain\ValueObject\Version;
use App\Tests\Shared\Domain\WordMother;

final class VersionMother
{
    public static function create(string $version): Version
    {
        return new Version($version);
    }

    public static function random(): Version
    {
        return self::create(WordMother::random());
    }
}
