<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject;

use App\Al\Apks\Domain\ValueObject\Report;
use App\Tests\Shared\Domain\Sha1Mother;
use App\Tests\Shared\Domain\UrlMother;

final class ReportMother
{
    public static function emptyReport(): Report
    {
        return self::create([]);
    }

    public static function create(array $report): Report
    {
        return new Report($report);
    }

    public static function random(): Report
    {
        return self::create([]);
    }

    public static function validReport(): Report
    {
        return self::create(
            [
                'positives' => 0,
                'resource' => Sha1Mother::random(),
                'permalink' => UrlMother::random()
            ]
        );
    }

    public static function dangerousReport(): Report
    {
        return self::create(
            [
                'positives' => 1,
                'resource' => Sha1Mother::random(),
                'permalink' => UrlMother::random()
            ]
        );
    }

    public static function bypassReport(string $resource): Report
    {
        return self::create(
            [
                'resource' => $resource,
                'positives' => 0,
                'isDangerous' => 0,
                'scans' => [],
                'permalink' => ''
            ]
        );
    }

    public static function pendingReport(): Report
    {
        return self::create(
            [
                'resource' => Sha1Mother::random(),
                'pendingReport' => 1
            ]
        );
    }

    public static function validatePendingReport(Report $report): Report
    {
        return self::create(
            [
                'resource' => $report->resource(),
                'positives' => 0,
                'isDangerous' => 0,
                'scans' => [],
                'permalink' => $report->permalink()
            ]
        );
    }

    public static function markDangerous(Report $report): Report
    {
        return self::create(
            [
                'resource' => $report->resource(),
                'positives' => 1,
                'isDangerous' => 1,
                'scans' => [],
                'permalink' => $report->permalink()
            ]
        );
    }
}
