<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject\File;

use App\Al\Apks\Domain\ValueObject\File\Size;
use App\Tests\Shared\Domain\NumberMother;

final class SizeMother
{
    public static function create(int $size): Size
    {
        return new Size($size);
    }

    public static function random(): Size
    {
        return self::create(NumberMother::random());
    }
}
