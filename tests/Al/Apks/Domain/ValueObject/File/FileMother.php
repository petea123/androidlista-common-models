<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject\File;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Apks\Domain\ValueObject\File\File;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\Apks\UrlMother;

final class FileMother
{

    public static function create(
        string $signature,
        int $size,
        string $sha1,
        string $url,
        string $externalUrl,
        string $source
    ): File {
        return File::create(
            $signature,
            $size,
            $sha1,
            $url,
            $externalUrl,
            $source
        );
    }

    public static function random(): File
    {
        return self::create(
            SignatureMother::random()->value(),
            SizeMother::random()->value(),
            Sha1Mother::random()->value(),
            UrlMother::random()->value(),
            ExternalUrlMother::random()->value(),
            ApkSourceMother::random()->value()
        );
    }

    public static function withUrl(Url $url): File
    {
        return self::create(
            SignatureMother::random()->value(),
            SizeMother::random()->value(),
            Sha1Mother::random()->value(),
            $url->value(),
            ExternalUrlMother::random()->value(),
            ApkSourceMother::random()->value()
        );
    }

    public static function withExternalUrlAndSource(ExternalUrl $url, ApkSource $source): File
    {
        return self::create(
            SignatureMother::random()->value(),
            SizeMother::random()->value(),
            Sha1Mother::random()->value(),
            UrlMother::localRandom()->value(),
            $url->value(),
            $source->value()
        );
    }
}
