<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject\File;

use App\Al\Apks\Domain\ValueObject\File\Signature;
use App\Tests\Shared\Domain\Sha1Mother;

final class SignatureMother
{
    public static function create(string $signature): Signature
    {
        return new Signature($signature);
    }

    public static function random(): Signature
    {
        return self::create(Sha1Mother::random());
    }
}
