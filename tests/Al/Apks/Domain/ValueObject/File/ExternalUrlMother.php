<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject\File;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Tests\Shared\Domain\UrlMother;

final class ExternalUrlMother
{
    public static function create(string $url): ExternalUrl
    {
        return new ExternalUrl($url);
    }

    public static function random(): ExternalUrl
    {
        return self::create(UrlMother::random());
    }
}
