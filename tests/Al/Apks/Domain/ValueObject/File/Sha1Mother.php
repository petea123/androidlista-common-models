<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject\File;

use App\Al\Apks\Domain\ValueObject\File\Sha1;
use App\Tests\Shared\Domain\Sha1Mother as GenericSha1Mother;
final class Sha1Mother
{
    public static function create(string $signature): Sha1
    {
        return new Sha1($signature);
    }

    public static function random(): Sha1
    {
        return self::create(GenericSha1Mother::random());
    }
}
