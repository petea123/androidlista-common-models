<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\ValueObject;

use App\Al\Apks\Domain\ValueObject\State;
use App\Tests\Shared\Domain\NumberMother;

final class StateMother
{
    public static function pending(): State
    {
        return self::create(0);
    }

    public static function create(int $state): State
    {
        return new State($state);
    }

    public static function random(): State
    {
        return self::create(NumberMother::between(0, 3));
    }

    public static function dangerous(): State
    {
        return self::create(2);
    }

    public static function pendingUpload(): State
    {
        return self::create(3);
    }

    public static function processed(): State
    {
        return self::create(1);
    }
}
