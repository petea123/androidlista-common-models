<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Event\ApkHasPendingReport;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\Report;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\ReportMother;
use App\Tests\Al\Apks\Domain\ValueObject\VersionMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ApkHasPendingReportMother
{
    public static function create(
        ItemId $itemId,
        ApkId $apkId,
        Report $report,
        Version $version,
        ApkSource $source
    ): ApkHasPendingReport {
        return new ApkHasPendingReport($apkId->value(), $itemId->value(), $report->resource(), $source->value(), $version->value());
    }

    public static function random(): ApkHasPendingReport
    {
        return self::create(
            ItemIdMother::random(),
            ApkIdMother::random(),
            ReportMother::pendingReport(),
            VersionMother::random(),
            ApkSourceMother::random()
        );
    }
}
