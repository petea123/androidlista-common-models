<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Event\ApkWasDangerous;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\VersionMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ApkWasDangerousMother
{
    public static function create(
        ItemId $itemId,
        ApkId $apkId,
        Version $version,
        ApkSource $source
    ): ApkWasDangerous {
        return new ApkWasDangerous(
            $apkId->value(), $itemId->value(), $source->value(), $version->value()
        );
    }

    public static function random(): ApkWasDangerous
    {
        return self::create(
            ItemIdMother::random(),
            ApkIdMother::random(),
            VersionMother::random(),
            ApkSourceMother::random()
        );
    }
}
