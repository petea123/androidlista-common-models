<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Event\ApkWasCreated;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\Apks\UrlMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ApkWasCreatedMother
{
    public static function create(
        ItemId $itemId,
        ApkId $apkId,
        Url $url,
        ExternalUrl $externalUrl,
        ApkSource $source
    ): ApkWasCreated {
        return new ApkWasCreated($apkId->value(), $itemId->value(), $url->value(), $externalUrl->value(), $source->value());
    }

    public static function random(): ApkWasCreated
    {
        return self::create(
            ItemIdMother::random(),
            ApkIdMother::random(),
            UrlMother::random(),
            ExternalUrlMother::random(),
            ApkSourceMother::random()
        );
    }
}
