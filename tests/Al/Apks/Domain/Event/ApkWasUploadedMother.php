<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Event\ApkWasUploaded;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\VersionMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\Al\Shared\Domain\Apks\UrlMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ApkWasUploadedMother
{
    public static function create(
        ItemId $itemId,
        ApkId $apkId,
        Url $url,
        Version $version,
        ApkSource $source
    ): ApkWasUploaded {
        return new ApkWasUploaded(
            $apkId->value(),
            $itemId->value(),
            $url->value(),
            $source->value(),
            $version->value()
        );
    }

    public static function random(): ApkWasUploaded
    {
        return self::create(
            ItemIdMother::random(),
            ApkIdMother::random(),
            UrlMother::random(),
            VersionMother::random(),
            ApkSourceMother::random()
        );
    }

    public static function fromApk(Apk $apk): ApkWasUploaded
    {
        return self::create($apk->itemId(), $apk->id(), $apk->url(), $apk->version(), $apk->source());
    }
}
