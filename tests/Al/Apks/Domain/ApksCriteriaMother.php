<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain;

use App\Shared\Domain\Criteria\Criteria;
use App\Tests\Shared\Domain\Criteria\CriteriaMother;
use App\Tests\Shared\Domain\Criteria\FilterMother;
use App\Tests\Shared\Domain\Criteria\FiltersMother;

final class ApksCriteriaMother
{
    public static function withFilters(array $filters): Criteria
    {
        $f = [];
        foreach ($filters as $filter) {
            $f[] = FilterMother::fromValues($filter);
        }
        return CriteriaMother::create(
            FiltersMother::create($f)
        );
    }
}
