<?php

declare(strict_types=1);

namespace App\Tests\Al\Apks\Domain;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\ValueObject\ApkDate;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\File;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Apks\Domain\ValueObject\Report;
use App\Al\Apks\Domain\ValueObject\State;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\FileMother;
use App\Tests\Al\Apks\Domain\ValueObject\ReportMother;
use App\Tests\Al\Apks\Domain\ValueObject\StateMother;
use App\Tests\Al\Apks\Domain\ValueObject\VersionMother;
use App\Tests\Al\Shared\Domain\Apks\ApkDateMother;
use App\Tests\Al\Shared\Domain\Apks\ApkIdMother;
use App\Tests\Al\Shared\Domain\Apks\UrlMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ApkMother
{

    public static function withId(ApkId $id): Apk
    {
        return self::create(
            $id,
            ItemIdMother::random(),
            VersionMother::random(),
            FileMother::random(),
            ReportMother::random(),
            StateMother::pending(),
            ApkDateMother::random()
        );
    }

    public static function create(
        ApkId $id,
        ItemId $itemId,
        ?Version $version,
        File $file,
        Report $report,
        ?State $state,
        ApkDate $date
    ): Apk {
        return new Apk(
            $id,
            $itemId,
            $version,
            $file,
            $report,
            $state,
            $date
        );
    }

    public static function random(): Apk
    {
        return self::create(
            ApkIdMother::random(),
            ItemIdMother::random(),
            VersionMother::random(),
            FileMother::random(),
            ReportMother::emptyReport(),
            StateMother::pending(),
            ApkDateMother::random()
        );
    }

    public static function withIdAndStateAndUrl(ApkId $id, State $state, Url $url): Apk
    {
        return self::create(
            $id,
            ItemIdMother::random(),
            VersionMother::random(),
            FileMother::withUrl($url),
            ReportMother::random(),
            $state,
            ApkDateMother::random()
        );
    }

    public static function created(Apk $apk): Apk
    {
        return self::create(
            ApkIdMother::create($apk->id()->value()),
            ItemIdMother::create($apk->itemId()->value()),
            null,
            FileMother::create(
                $apk->file()->signature()->value(),
                $apk->file()->size()->value(),
                $apk->file()->sha1()->value(),
                UrlMother::localRandom()->value(),
                $apk->file()->externalUrl()->value(),
                $apk->file()->source()->value()
            ),
            ReportMother::create($apk->report()->value()),
            State::created(),
            ApkDateMother::create($apk->date()->value())
        );
    }

    public static function downloaded(Apk $apk): Apk
    {
        return self::create(
            ApkIdMother::create($apk->id()->value()),
            ItemIdMother::create($apk->itemId()->value()),
            VersionMother::random(),
            FileMother::create(
                $apk->file()->signature()->value(),
                $apk->file()->size()->value(),
                $apk->file()->sha1()->value(),
                $apk->file()->url()->value(),
                $apk->file()->externalUrl()->value(),
                $apk->file()->source()->value()
            ),
            ReportMother::create($apk->report()->value()),
            StateMother::pending(),
            ApkDateMother::create($apk->date()->value())
        );
    }

    public static function scanned(Apk $apk): Apk
    {
        return self::create(
            ApkIdMother::create($apk->id()->value()),
            ItemIdMother::create($apk->itemId()->value()),
            VersionMother::create($apk->version()->value()),
            FileMother::create(
                $apk->file()->signature()->value(),
                $apk->file()->size()->value(),
                $apk->file()->sha1()->value(),
                UrlMother::localRandom()->value(),
                $apk->file()->externalUrl()->value(),
                $apk->file()->source()->value()
            ),
            ReportMother::create($apk->report()->value()),
            StateMother::pendingUpload(),
            ApkDateMother::create($apk->date()->value())
        );
    }


    public static function uploading(Apk $apk): Apk
    {
        $uploading = self::create(
            ApkIdMother::create($apk->id()->value()),
            ItemIdMother::create($apk->itemId()->value()),
            VersionMother::create($apk->version()->value()),
            FileMother::create(
                $apk->file()->signature()->value(),
                $apk->file()->size()->value(),
                $apk->file()->sha1()->value(),
                $apk->file()->url()->value(),
                $apk->file()->externalUrl()->value(),
                $apk->file()->source()->value()
            ),
            ReportMother::create($apk->report()->value()),
            StateMother::processed(),
            ApkDateMother::create($apk->date()->value())
        );
        $uploading->file()->publishToCdn();

        return $uploading;
    }
}
