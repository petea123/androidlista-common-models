<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Application\Update\Icon;

use App\Al\ItemsImages\Application\Update\Icon\UpdateItemIconCommand;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class UpdateItemIconCommandMother
{
    public static function create(
        ItemId $itemId,
        string $downloadUrl
    ): UpdateItemIconCommand {
        return new UpdateItemIconCommand($itemId->value(), $downloadUrl);
    }

    public static function random(): UpdateItemIconCommand
    {
        return self::create(
            ItemIdMother::random(),
            ExternalUrlMother::random()->value(),
        );
    }
}
