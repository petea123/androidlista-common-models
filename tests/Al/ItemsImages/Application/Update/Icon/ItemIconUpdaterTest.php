<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Application\Update\Icon;

use App\Al\ItemsImages\Application\Update\Icon\ItemIconUpdater;
use App\Al\ItemsImages\Application\Update\Icon\UpdateItemIconCommandHandler;
use App\Al\ItemsImages\Domain\Icon\ItemImageIcon;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Shared\Domain\ValueObject\PathValueObject;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\ItemsImages\Domain\Icon\ItemImageIconMother;
use App\Tests\Al\ItemsImages\Domain\Icon\ItemImageIconWasUpdatedMother;
use App\Tests\Al\ItemsImages\Domain\ItemImageFileHashMother;
use App\Tests\Al\ItemsImages\ItemsImagesModuleUnitTestCase;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\DuplicatorMother;

final class ItemIconUpdaterTest extends ItemsImagesModuleUnitTestCase
{
    /** @var UpdateItemIconCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $updater = new ItemIconUpdater(
            $this->filesystem(),
            $this->hashGenerator(),
            $this->iconRepository(),
            $this->downloader(),
            $this->publisher()
        );

        $this->handler = new UpdateItemIconCommandHandler($updater);
    }

    /** @test */
    public function itShouldUpdateIcon(): void
    {
        $command = UpdateItemIconCommandMother::random();

        $itemId = ItemIdMother::create($command->itemId());
        $icon = ItemImageIconMother::withItemId($itemId);
        $hash = ItemImageFileHashMother::random();

        $this->shouldDownload(ExternalUrlMother::create($command->downloadUrl()), $this->image());
        $this->shouldSearchIconByItemId($icon->itemId(), $icon);
        $this->shouldGenerateHash($this->image(), $hash);
        $this->shouldPutStream(PathValueObject::create($icon->path()->value()), $this->image(), true);
        $this->shouldSaveIcon($icon);
        $this->shouldRecordDomainEvents(ItemImageIconWasUpdatedMother::create($icon->id(), $itemId));
        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldNotUpdateSameHashIcon(): void
    {
        $command = UpdateItemIconCommandMother::random();

        $icon = ItemImageIconMother::withItemId(ItemIdMother::create($command->itemId()));

        $this->shouldDownload(ExternalUrlMother::create($command->downloadUrl()), $this->image());
        $this->shouldSearchIconByItemId($icon->itemId(), $icon);
        $this->shouldGenerateHash($this->image(), $icon->hash());
        $this->dispatch($command, $this->handler);
    }
}
