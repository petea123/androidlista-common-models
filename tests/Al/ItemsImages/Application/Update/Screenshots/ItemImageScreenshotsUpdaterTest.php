<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Application\Update\Screenshots;

use App\Al\ItemsImages\Application\Update\Screenshots\ItemImageScreenshotsUpdater;
use App\Al\ItemsImages\Application\Update\Screenshots\UpdateItemImageScreenshotsCommandHandler;
use App\Shared\Domain\ValueObject\PathValueObject;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Items\Application\Find\ById\FindItemByIdQueryMother;
use App\Tests\Al\Items\Infrastructure\Projections\ItemViewMother;
use App\Tests\Al\ItemsImages\Domain\ItemImageFileHashMother;
use App\Tests\Al\ItemsImages\Domain\ItemImageFilePathMother;
use App\Tests\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotsMother;
use App\Tests\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotWasAddedMother;
use App\Tests\Al\ItemsImages\ItemsImagesModuleUnitTestCase;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemImageScreenshotsUpdaterTest extends ItemsImagesModuleUnitTestCase
{
    /** @var UpdateItemImageScreenshotsCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $updater = new ItemImageScreenshotsUpdater(
            $this->screenshotsRepository(),
            $this->downloader(),
            $this->hashGenerator(),
            $this->filesystem(),
            $this->queryBus(),
            $this->uuidGenerator(),
            $this->publisher()
        );

        $this->handler = new UpdateItemImageScreenshotsCommandHandler($updater);
    }

    /** @test */
    public function itShouldUpdateScreenshots(): void
    {
        $command = UpdateItemImageScreenshotsCommandMother::random();

        $screenshots = ItemImageScreenshotsMother::withItemId(
            ItemIdMother::create($command->itemId()),
            count($command->urls())
        );

        $this->shouldSearchScreenshotsByItemId($screenshots->itemId(), $screenshots);

        $itemView = ItemViewMother::withItemId($screenshots->itemId());
        $this->shouldAsk(FindItemByIdQueryMother::create($screenshots->itemId()), $itemView);
        $events = [];
        foreach ($command->urls() as $key => $url) {
            $hash = ItemImageFileHashMother::random();
            $path = ItemImageFilePathMother::withValues($screenshots->itemId(), $itemView->dirify(), $key + 1);
            $this->shouldDownload(ExternalUrlMother::create($url), $this->image());
            $this->shouldGenerateHash($this->image(), $hash);
            $this->shouldPutStream(PathValueObject::create($path->value()), $this->image(), true);
            $events[] = ItemImageScreenshotWasAddedMother::create($screenshots->id(), $screenshots->itemId());
        }

        $this->shouldSaveScreenshots($screenshots);
        $this->shouldRecordDomainEvents(...$events);
        $this->dispatch($command, $this->handler);
    }
}
