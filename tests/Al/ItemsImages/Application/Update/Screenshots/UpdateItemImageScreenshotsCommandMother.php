<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Application\Update\Screenshots;

use App\Al\ItemsImages\Application\Update\Screenshots\UpdateItemImageScreenshotsCommand;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use function Lambdish\Phunctional\repeat;

final class UpdateItemImageScreenshotsCommandMother
{
    public static function create(
        ItemId $itemId,
        array $urls
    ): UpdateItemImageScreenshotsCommand {
        return new UpdateItemImageScreenshotsCommand($itemId->value(), $urls);
    }

    public static function random(): UpdateItemImageScreenshotsCommand
    {
        return self::create(
            ItemIdMother::random(),
            repeat(self::randomUrlGenerator(), rand(0, 10)),
        );
    }

    private static function randomUrlGenerator(): callable
    {
        return static function () {
            return ExternalUrlMother::random()->value();
        };
    }
}
