<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain;

use App\Al\ItemsImages\Domain\ItemImageFileName;
use App\Tests\Shared\Domain\MotherCreator;

final class ItemImageFileNameMother
{
    public static function create(string $name): ItemImageFileName
    {
        return new ItemImageFileName($name);
    }

    public static function random(): ItemImageFileName
    {
        return self::create(MotherCreator::random()->words(3, true));
    }
}
