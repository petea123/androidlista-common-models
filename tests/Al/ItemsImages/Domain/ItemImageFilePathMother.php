<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain;

use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Shared\Domain\MotherCreator;

final class ItemImageFilePathMother
{
    public static function create(string $path): ItemImageFilePath
    {
        return new ItemImageFilePath($path);
    }

    public static function random(): ItemImageFilePath
    {
        return self::create('/tmp/image.jpg');
    }

    public static function withValues(ItemId $itemId, $name, $count): ItemImageFilePath
    {
        return ItemImageFilePath::createWithParams($itemId, $name, $count);
    }
}
