<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain\Screenshots;

use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotWasRemoved;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\ItemsImages\Domain\ItemImageIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemImageScreenshotWasRemovedMother
{
    public static function create(ItemImageId $id, ItemId $itemId): ItemImageScreenshotWasRemoved
    {
        return new ItemImageScreenshotWasRemoved($id->value(), $itemId->value());
    }

    public static function random(): ItemImageScreenshotWasRemoved
    {
        return self::create(ItemImageIdMother::random(), ItemIdMother::random());
    }
}
