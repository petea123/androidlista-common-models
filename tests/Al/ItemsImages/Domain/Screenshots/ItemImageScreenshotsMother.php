<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain\Screenshots;

use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshots;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\ItemsImages\Domain\ItemImageFileMother;
use App\Tests\Al\ItemsImages\Domain\ItemImageIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use function Lambdish\Phunctional\repeat;

final class ItemImageScreenshotsMother
{
    public static function create(
        ItemImageId $id,
        ItemId $itemId,
        ItemImageFile ...$files
    ): ItemImageScreenshots {
        return ItemImageScreenshots::create(
            $id,
            $itemId,
            ...$files,
        );
    }

    public static function random($totalFiles = 10): ItemImageScreenshots
    {
        return self::create(
            ItemImageIdMother::random(),
            ItemIdMother::random(),
            ...repeat(self::randomFileGenerator(), $totalFiles)
        );
    }

    public static function withItemId(ItemId $itemId, $totalFiles = 10): ItemImageScreenshots
    {
        return self::create(
            ItemImageIdMother::random(),
            $itemId,
            ...repeat(self::randomFileGenerator(), $totalFiles)
        );
    }

    private static function randomFileGenerator(): callable
    {
        return static function () {
            return ItemImageFileMother::random();
        };
    }
}
