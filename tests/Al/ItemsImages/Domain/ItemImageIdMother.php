<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain;

use App\Al\ItemsImages\Domain\ItemImageId;
use App\Tests\Shared\Domain\UuidMother;

final class ItemImageIdMother
{
    public static function create(string $id): ItemImageId
    {
        return new ItemImageId($id);
    }

    public static function random(): ItemImageId
    {
        return self::create(UuidMother::random());
    }
}
