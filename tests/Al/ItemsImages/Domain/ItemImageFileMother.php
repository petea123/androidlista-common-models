<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain;

use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFileName;
use App\Al\ItemsImages\Domain\ItemImageFilePath;

final class ItemImageFileMother
{
    public static function create(
        ItemImageFileName $name,
        ItemImageFilePath $path,
        ItemImageFileDimensions $dimensions,
        string $extension,
        ItemImageFileHash $hash
    ): ItemImageFile {
        return ItemImageFile::create(
            $name,
            $path,
            $dimensions,
            $extension,
            $hash
        );
    }

    public static function random(): ItemImageFile
    {
        return self::create(
            ItemImageFileNameMother::random(),
            ItemImageFilePathMother::random(),
            ItemImageFileDimensionsMother::random(),
            'jpg',
            ItemImageFileHashMother::random(),
        );
    }
}
