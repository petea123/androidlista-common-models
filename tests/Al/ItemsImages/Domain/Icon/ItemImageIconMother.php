<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain\Icon;

use App\Al\ItemsImages\Domain\Icon\ItemImageIcon;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\ItemsImages\Domain\ItemImageFileMother;
use App\Tests\Al\ItemsImages\Domain\ItemImageIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemImageIconMother
{
    public static function create(
        ItemImageId $id,
        ItemId $itemId,
        ItemImageFile $file
    ): ItemImageIcon {
        return new ItemImageIcon(
            $id,
            $itemId,
            $file,
        );
    }

    public static function random(): ItemImageIcon
    {
        return self::create(
            ItemImageIdMother::random(),
            ItemIdMother::random(),
            ItemImageFileMother::random(),
        );
    }

    public static function withItemId(ItemId $itemId): ItemImageIcon
    {
        return self::create(
            ItemImageIdMother::random(),
            $itemId,
            ItemImageFileMother::random(),
        );
    }
}
