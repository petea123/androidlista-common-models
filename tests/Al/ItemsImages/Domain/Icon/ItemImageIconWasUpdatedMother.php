<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain\Icon;

use App\Al\ItemsImages\Domain\Icon\ItemImageIconWasUpdated;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Al\ItemsImages\Domain\ItemImageIdMother;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;

final class ItemImageIconWasUpdatedMother
{
    public static function create(ItemImageId $id, ItemId $itemId): ItemImageIconWasUpdated
    {
        return new ItemImageIconWasUpdated($id->value(), $itemId->value());
    }

    public static function random(): ItemImageIconWasUpdated
    {
        return self::create(ItemImageIdMother::random(), ItemIdMother::random());
    }
}
