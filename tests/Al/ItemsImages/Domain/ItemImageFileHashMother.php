<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain;

use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Tests\Shared\Domain\MotherCreator;

final class ItemImageFileHashMother
{
    public static function create(string $hash): ItemImageFileHash
    {
        return new ItemImageFileHash($hash);
    }

    public static function random(): ItemImageFileHash
    {
        return self::create(MotherCreator::random()->md5);
    }
}
