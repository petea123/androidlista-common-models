<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages\Domain;

use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Tests\Shared\Domain\IntegerMother;

final class ItemImageFileDimensionsMother
{
    public static function create(int $width, int $height): ItemImageFileDimensions
    {
        return new ItemImageFileDimensions($width, $height);
    }

    public static function random(): ItemImageFileDimensions
    {
        return self::create(IntegerMother::between(1, 512), IntegerMother::between(1, 512));
    }
}
