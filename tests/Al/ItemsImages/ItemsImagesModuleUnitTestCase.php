<?php

declare(strict_types=1);

namespace App\Tests\Al\ItemsImages;

use App\Al\ItemsImages\Domain\Icon\ItemImageIconRepository;
use App\Al\ItemsImages\Domain\Icon\ItemImageIcon;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshots;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotsRepository;
use App\Al\Shared\Domain\HashGenerator;
use App\Al\Shared\Domain\Items\ItemId;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

abstract class ItemsImagesModuleUnitTestCase extends ModuleUnitTestCase
{

    private $iconRepository;
    private $hashGenerator;
    private $screenshotsRepository;

    /** @return ItemImageIconRepository|MockInterface */
    protected function iconRepository()
    {
        return $this->iconRepository = $this->iconRepository ?: $this->mock(ItemImageIconRepository::class);
    }

    /** @return ItemImageScreenshotsRepository|MockInterface */
    protected function screenshotsRepository()
    {
        return $this->screenshotsRepository = $this->screenshotsRepository ?:
            $this->mock(ItemImageScreenshotsRepository::class);
    }

    /** @return HashGenerator|MockInterface */
    protected function hashGenerator()
    {
        return $this->hashGenerator = $this->hashGenerator ?: $this->mock(HashGenerator::class);
    }

    public function image(): \SplFileInfo
    {
        return new \SplFileInfo(__DIR__.'/icon-image.jpg');
    }

    public function shouldSearchIconByItemId(ItemId $itemId, ?ItemImageIcon $icon): void
    {
        $this->iconRepository()
            ->shouldReceive('searchByItemId')
            ->with($this->similarTo($itemId))
            ->once()
            ->andReturn($icon);
    }

    public function shouldSearchScreenshotsByItemId(ItemId $itemId, ?ItemImageScreenshots $screenshots): void
    {
        $this->screenshotsRepository()
            ->shouldReceive('searchByItemId')
            ->with($this->similarTo($itemId))
            ->once()
            ->andReturn($screenshots);
    }

    public function shouldGenerateHash(\SplFileInfo $image, ?ItemImageFileHash $hash): void
    {
        $this->hashGenerator()
            ->shouldReceive('hash')
            ->once()
            ->with($this->similarTo($image))
            ->andReturn($hash);
    }

    public function shouldSaveIcon(ItemImageIcon $icon): void
    {
        $this->iconRepository()
            ->shouldReceive('save')
            ->once()
            ->with($this->similarTo($icon))
            ->andReturnNull();
    }

    public function shouldSaveScreenshots(ItemImageScreenshots $screenshots): void
    {
        $this->screenshotsRepository()
            ->shouldReceive('save')
            ->once()
            ->with($this->similarTo($screenshots))
            ->andReturnNull();
    }
}
