<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Amp\StoriesList\Domain\StoriesListTitle;
use App\Tests\Shared\Domain\MotherCreator;

final class StoriesListTitleMother
{

    public static function create(string $title): StoriesListTitle
    {
        return new StoriesListTitle($title);
    }

    public static function random(): StoriesListTitle
    {
        return self::create(MotherCreator::random()->words(3, true));
    }
}
