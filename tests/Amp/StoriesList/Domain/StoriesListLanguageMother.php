<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Amp\StoriesList\Domain\StoriesListLanguage;

final class StoriesListLanguageMother
{
    public static function create(string $language): StoriesListLanguage
    {
        return new StoriesListLanguage($language);
    }

    public static function random(): StoriesListLanguage
    {
        return StoriesListLanguage::random();
    }
}
