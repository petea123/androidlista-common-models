<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Amp\StoriesList\Domain\StoriesListId;
use App\Tests\Shared\Domain\UuidMother;

final class StoriesListIdMother
{

    public static function create(string $id): StoriesListId
    {
        return new StoriesListId($id);
    }

    public static function random(): StoriesListId
    {
        return self::create(UuidMother::random());
    }
}
