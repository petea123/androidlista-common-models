<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Amp\StoriesList\Domain\StoriesListUpdatedAt;
use App\Tests\Shared\Domain\DateMother;

final class StoriesListUpdatedAtMother
{
    public static function create(\DateTimeInterface $dateTime): StoriesListUpdatedAt
    {
        return new StoriesListUpdatedAt($dateTime);
    }

    public static function random(): StoriesListUpdatedAt
    {
        return self::create(DateMother::random()->value());
    }
}
