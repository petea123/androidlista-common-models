<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Amp\StoriesList\Domain\StoriesListCreatedAt;
use App\Amp\StoriesList\Domain\StoriesListDescription;
use App\Amp\StoriesList\Domain\StoriesListId;
use App\Amp\StoriesList\Domain\StoriesListLanguage;
use App\Amp\StoriesList\Domain\StoriesListTitle;
use App\Amp\StoriesList\Domain\StoriesListUpdatedAt;
use App\Tests\Al\Collections\Domain\CollectionIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Shared\Domain\Repeater;

final class StoriesListMother
{

    public static function create(
        StoriesListId $id,
        CollectionId $collectionId,
        StoriesListLanguage $language,
        StoriesListTitle $title,
        StoriesListDescription $description,
        StoriesListCreatedAt $createdAt,
        StoriesListUpdatedAt $updatedAt,
        StoryId ...$stories
    ): StoriesList {
        return new StoriesList($id, $collectionId, $language, $title, $description, $createdAt, $updatedAt, ...$stories);
    }

    public static function withOne(StoryId $storyId): StoriesList
    {
        return self::create(
            StoriesListIdMother::random(),
            CollectionIdMother::random(),
            StoriesListLanguageMother::random(),
            StoriesListTitleMother::random(),
            StoriesListDescriptionMother::random(),
            StoriesListCreatedAtMother::random(),
            StoriesListUpdatedAtMother::random(),
            $storyId
        );
    }

    public static function random(): StoriesList
    {
        return self::create(
            StoriesListIdMother::random(),
            CollectionIdMother::random(),
            StoriesListLanguageMother::random(),
            StoriesListTitleMother::random(),
            StoriesListDescriptionMother::random(),
            StoriesListCreatedAtMother::random(),
            StoriesListUpdatedAtMother::random(),
            ...Repeater::random(StoryIdMother::creator())
        );
    }
}
