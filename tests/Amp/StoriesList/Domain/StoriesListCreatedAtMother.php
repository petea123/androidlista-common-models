<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Amp\StoriesList\Domain\StoriesListCreatedAt;
use App\Tests\Shared\Domain\DateMother;

final class StoriesListCreatedAtMother
{

    public static function create(\DateTimeInterface $dateTime): StoriesListCreatedAt
    {
        return new StoriesListCreatedAt($dateTime);
    }

    public static function random(): StoriesListCreatedAt
    {
        return self::create(DateMother::random()->value());
    }
}
