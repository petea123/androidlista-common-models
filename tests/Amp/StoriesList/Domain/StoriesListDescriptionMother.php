<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Domain;

use App\Amp\StoriesList\Domain\StoriesListDescription;
use App\Tests\Shared\Domain\MotherCreator;

final class StoriesListDescriptionMother
{

    public static function create(string $description): StoriesListDescription
    {
        return new StoriesListDescription($description);
    }

    public static function random(): StoriesListDescription
    {
        return self::create(MotherCreator::random()->sentence);
    }
}
