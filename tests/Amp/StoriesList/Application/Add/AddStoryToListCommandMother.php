<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Application\Add;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Application\Add\AddStoryToListCommand;
use App\Amp\StoriesList\Domain\StoriesListLanguage;
use App\Tests\Al\Collections\Domain\CollectionIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\StoriesList\Domain\StoriesListLanguageMother;

final class AddStoryToListCommandMother
{

    public static function create(CollectionId $id, StoryId $storyId, StoriesListLanguage $language): AddStoryToListCommand
    {
        return new AddStoryToListCommand($id->value(), $storyId->value(), $language->value());
    }

    public static function random(): AddStoryToListCommand
    {
        return self::create(CollectionIdMother::random(), StoryIdMother::random(), StoriesListLanguageMother::random());
    }
}
