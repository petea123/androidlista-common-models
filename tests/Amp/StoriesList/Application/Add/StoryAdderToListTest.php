<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList\Application\Add;

use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Application\Search\ByCriteria\SearchCollectionsByCriteriaQuery;
use App\Amp\StoriesList\Application\Add\AddStoryToListCommandHandler;
use App\Amp\StoriesList\Application\Add\StoryAdderToList;
use App\Tests\Al\Collections\Application\CollectionsResponseMother;
use App\Tests\Al\Collections\Domain\CollectionIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\StoriesList\Domain\StoriesListLanguageMother;
use App\Tests\Amp\StoriesList\Domain\StoriesListMother;
use App\Tests\Amp\StoriesList\Domain\StoriesListTitleMother;
use App\Tests\Amp\StoriesList\StoriesListModuleUnitTestCase;
use App\Tests\Shared\Domain\DuplicatorMother;

final class StoryAdderToListTest extends StoriesListModuleUnitTestCase
{
    /** @var AddStoryToListCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $adder = new StoryAdderToList($this->repository(), $this->queryBus(), $this->uuidGenerator());

        $this->handler = new AddStoryToListCommandHandler($adder);
    }

    /** @test */
    public function itShouldCreateNewList(): void
    {
        $command = AddStoryToListCommandMother::random();

        $storyId = StoryIdMother::create($command->storyId());
        $collectionId = CollectionIdMother::create($command->collectionId());
        $collections = CollectionsResponseMother::withOne($collectionId);

        $collections = DuplicatorMother::with($collections, [
            0 => [
                'language' => $command->language()
            ]
        ]);
        /** @var CollectionResponse $collection */
        $collection = $collections->collections()[0];

        $list = StoriesListMother::withOne($storyId);

        $list = DuplicatorMother::with($list, [
            'language' => StoriesListLanguageMother::create($command->language()),
            'collectionId' => $collectionId,
            'title' => StoriesListTitleMother::create($collection->shortname()),
            'description' => $collection->description()
        ]);

        $this->shouldMatching();
        $this->shouldAsk(new SearchCollectionsByCriteriaQuery([
            [
                'field' => 'collectionGroup',
                'operator' => '=',
                'value' => (string)$list->collectionId()->value()
            ],
            [
                'field' => 'language',
                'operator' => '=',
                'value' => $list->language()->value()
            ]
        ]), $collections);
        $this->shouldGenerateUuid($list->id()->value());
        $this->shouldSaveStoriesList($list);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldAddStoryOnExistingList(): void
    {
        $command = AddStoryToListCommandMother::random();

        $storyId = StoryIdMother::create($command->storyId());
        $list = StoriesListMother::random();
        $stories = $list->stories();
        array_unshift($stories, $storyId);

        $added = DuplicatorMother::with($list, [
            'stories' => $stories
        ]);

        $this->shouldMatching([$list]);
        $this->shouldSaveStoriesList($added);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function itShouldNotAddAlreadyStoryAdded(): void
    {
        $command = AddStoryToListCommandMother::random();

        $storyId = StoryIdMother::create($command->storyId());
        $list = StoriesListMother::withOne($storyId);

        $this->shouldMatching([$list]);

        $this->dispatch($command, $this->handler);
    }
}
