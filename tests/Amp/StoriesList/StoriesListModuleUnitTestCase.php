<?php

declare(strict_types=1);

namespace App\Tests\Amp\StoriesList;

use App\Amp\StoriesList\Domain\Service\StoriesListRepository;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

class StoriesListModuleUnitTestCase extends ModuleUnitTestCase
{
    private $repository;

    /** @return StoriesListRepository|MockInterface */
    protected function repository()
    {
        return $this->repository = $this->repository ?: $this->mock(StoriesListRepository::class);
    }

    protected function shouldSearch(?StoriesList $list): void
    {
        $this->repository()
            ->shouldReceive('search')
            ->once()
            ->andReturn($list);
    }

    protected function shouldMatching(array $list = []): void
    {
        $this->repository()
            ->shouldReceive('matching')
            ->once()
            ->andReturn($list);
    }

    protected function shouldSaveStoriesList(StoriesList $list): void
    {
        $this->repository()
            ->shouldReceive('save')
            ->with($this->similarTo($list))
            ->once()
            ->andReturnNull();
    }
}
