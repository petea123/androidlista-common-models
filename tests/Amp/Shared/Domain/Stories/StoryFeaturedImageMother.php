<?php

declare(strict_types=1);

namespace App\Tests\Amp\Shared\Domain\Stories;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;

final class StoryFeaturedImageMother
{
    public static function create(string $video): StoryFeaturedImage
    {
        return new StoryFeaturedImage($video);
    }

    public static function random(): StoryFeaturedImage
    {
        return self::create('http://lorempixel.com/640/480/dafd.jpg');
    }
}
