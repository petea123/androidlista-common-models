<?php

declare(strict_types=1);

namespace App\Tests\Amp\Shared\Domain\Stories;

use App\Amp\Shared\Domain\Stories\StoryLanguage;

final class StoryLanguageMother
{
    public static function create(string $language): StoryLanguage
    {
        return new StoryLanguage($language);
    }

    public static function random(): StoryLanguage
    {
        return StoryLanguage::random();
    }
}
