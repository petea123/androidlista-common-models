<?php

declare(strict_types=1);

namespace App\Tests\Amp\Shared\Domain\Stories;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Tests\Shared\Domain\UuidMother;

final class StoryIdMother
{
    public static function create(string $id): StoryId
    {
        return new StoryId($id);
    }

    public static function creator(): callable
    {
        return static function () {
            return self::random();
        };
    }

    public static function random(): StoryId
    {
        return self::create(UuidMother::random());
    }
}
