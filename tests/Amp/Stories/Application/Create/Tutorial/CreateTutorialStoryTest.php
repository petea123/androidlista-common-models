<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Application\Create\Tutorial;

use App\Al\UsersProfile\Application\Find\FindUserProfileQuery;
use App\Amp\Stories\Application\Create\Tutorial\CreateTutorialStoryCommandHandler;
use App\Amp\Stories\Application\Create\Tutorial\TutorialStoryCreator;
use App\Amp\Stories\Domain\Story;
use App\Tests\Al\UsersProfile\Application\UserProfileResponseMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryFeaturedImageMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryLanguageMother;
use App\Tests\Amp\Stories\Domain\Tutorial\TutorialStoryMother;
use App\Tests\Amp\Stories\Domain\Tutorial\TutorialStoryWasCreatedMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryCreatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryGroupIdMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryLikesMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryUpdatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryViewsMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserAvatarMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserUsernameMother;
use App\Tests\Amp\Stories\StoryModuleUnitTestCase;
use App\Tests\Shared\Domain\AggregateVersionMother;
use App\Tests\Shared\Domain\Users\UserIdMother;
use function Lambdish\Phunctional\map;

final class CreateTutorialStoryTest extends StoryModuleUnitTestCase
{
    /** @var CreateTutorialStoryCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $creator = new TutorialStoryCreator($this->tutorialRepository(), $this->queryBus(), $this->publisher());

        $this->handler = new CreateTutorialStoryCommandHandler($creator);
    }

    /** @test */
    public function itShouldCreateTutorialStory(): void
    {
        $command = CreateTutorialStoryCommandMother::random();
        $userProfile = UserProfileResponseMother::withUserId($command->user());

        $id = StoryIdMother::create($command->id());
        $groupId = StoryGroupIdMother::create($command->groupId());
        $categories = map(Story::categoryFromValue(), $command->categories());
        $user = StoryUserMother::create(
            UserIdMother::create($command->user()),
            StoryUserUsernameMother::create($userProfile->username()),
            StoryUserAvatarMother::create($userProfile->avatar())
        );
        $language = StoryLanguageMother::create($command->language());
        $title = StoryTitleMother::create($command->title());
        $description = StoryDescriptionMother::create($command->description());
        $featuredImage = StoryFeaturedImageMother::create($command->featuredImage());
        $pages = map(Story::pageFromValues(), $command->pages());
        $views = StoryViewsMother::create(0);
        $likes = StoryLikesMother::create(0);
        $createdAt = StoryCreatedAtMother::random();
        $updatedAt = StoryUpdatedAtMother::random();

        $story = TutorialStoryMother::create(
            $id,
            $groupId,
            $command->postHref(),
            $categories,
            $user,
            $language,
            false,
            $title,
            $description,
            $featuredImage,
            $views,
            $likes,
            $createdAt,
            $updatedAt,
            ...$pages
        );

        $event = TutorialStoryWasCreatedMother::create(
            $id,
            $groupId,
            $categories,
            $user,
            $language,
            $title,
            $description,
            $featuredImage,
            $views,
            $likes,
            $createdAt,
            AggregateVersionMother::create(1),
            ...$pages
        );

        $this->shouldAsk(new FindUserProfileQuery($user->userId()->value()), $userProfile);
        $this->shouldSaveTutorialStory($story);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }
}
