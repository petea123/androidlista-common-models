<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Application\Create;

use App\Al\UsersProfile\Application\Find\FindUserProfileQuery;
use App\Amp\Stories\Application\Create\CreateStoryCommandHandler;
use App\Amp\Stories\Application\Create\StoryCreator;
use App\Amp\Stories\Domain\Story;
use App\Tests\Al\UsersProfile\Application\UserProfileResponseMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryFeaturedImageMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryLanguageMother;
use App\Tests\Amp\Stories\Domain\Event\StoryWasCreatedMother;
use App\Tests\Amp\Stories\Domain\StoryMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryCreatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryGroupIdMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryLikesMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTemplateMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryUpdatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryViewsMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserAvatarMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserUsernameMother;
use App\Tests\Amp\Stories\StoryModuleUnitTestCase;
use App\Tests\Shared\Domain\AggregateVersionMother;
use App\Tests\Shared\Domain\Users\UserIdMother;
use function Lambdish\Phunctional\map;

final class CreateStoryTest extends StoryModuleUnitTestCase
{
    /** @var CreateStoryCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $creator = new StoryCreator($this->repository(), $this->queryBus(), $this->publisher());

        $this->handler = new CreateStoryCommandHandler($creator);
    }

    /** @test */
    public function itShouldCreateStory(): void
    {
        $command = CreateStoryCommandMother::random();
        $userProfile = UserProfileResponseMother::withUserId($command->user());

        $id = StoryIdMother::create($command->id());
        $groupId = StoryGroupIdMother::create($command->groupId());
        $categories = map(Story::categoryFromValue(), $command->categories());
        $user = StoryUserMother::create(UserIdMother::create($command->user()), StoryUserUsernameMother::create($userProfile->username()), StoryUserAvatarMother::create($userProfile->avatar()));
        $language = StoryLanguageMother::create($command->language());
        $title = StoryTitleMother::create($command->title());
        $description = StoryDescriptionMother::create($command->description());
        $featuredImage = StoryFeaturedImageMother::create($command->featuredImage());
        $pages = map(Story::pageFromValues(), $command->pages());
        $views = StoryViewsMother::create(0);
        $likes = StoryLikesMother::create(0);
        $createdAt = StoryCreatedAtMother::random();
        $updatedAt = StoryUpdatedAtMother::random();

        $story = StoryMother::create($id, $groupId, $categories, $user, $language, false, $title,
            $description, $featuredImage, $views, $likes, $createdAt, $updatedAt, ...$pages);

        $event = StoryWasCreatedMother::create(
            $id,
            $groupId,
            $categories,
            $user,
            $language,
            $title,
            $description,
            $featuredImage,
            $views,
            $likes,
            $createdAt,
            AggregateVersionMother::create(1),
            ...$pages
        );

        $this->shouldAsk(new FindUserProfileQuery($user->userId()->value()), $userProfile);
        $this->shouldSaveStory($story);
        $this->shouldRecordDomainEvents($event);

        $this->dispatch($command, $this->handler);
    }
}
