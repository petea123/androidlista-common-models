<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Application\Create;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Shared\Domain\Users\UserId;
use App\Amp\Stories\Application\Create\CreateStoryCommand;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Tests\Amp\Shared\Domain\Stories\StoryFeaturedImageMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryLanguageMother;
use App\Tests\Amp\Stories\Domain\StoryMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryGroupIdMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use App\Tests\Shared\Domain\Users\UserIdMother;
use function Lambdish\Phunctional\map;

final class CreateStoryCommandMother
{
    public static function create(
        StoryId $id,
        StoryGroupId $groupId,
        array $categories,
        UserId $user,
        StoryLanguage $language,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryPage ...$pages
    ): CreateStoryCommand {

        return new CreateStoryCommand(
            $id->value(),
            $groupId->value(),
            map(Story::categoryToValues(), $categories),
            $user->value(),
            $language->value(),
            $title->value(),
            $description->value(),
            $featuredImage->value(),
            map(Story::pageToValues(), $pages)
        );
    }

    public static function random(): CreateStoryCommand
    {
        return self::create(
            StoryIdMother::random(),
            StoryGroupIdMother::random(),
            StoryMother::randomCategories(),
            UserIdMother::random(),
            StoryLanguageMother::random(),
            StoryTitleMother::random(),
            StoryDescriptionMother::random(),
            StoryFeaturedImageMother::random(),
            ...StoryMother::createRandomPages()
        );
    }
}
