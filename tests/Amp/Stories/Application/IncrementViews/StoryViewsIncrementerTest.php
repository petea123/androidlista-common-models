<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Application\IncrementViews;

use App\Amp\Stories\Application\IncrementViews\StoryViewsIncrementCommandHandler;
use App\Amp\Stories\Application\IncrementViews\StoryViewsIncrementer;
use App\Amp\Stories\Domain\ValueObject\StoryViews;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Stories\Domain\StoryMother;
use App\Tests\Amp\Stories\StoryModuleUnitTestCase;
use App\Tests\Shared\Domain\DuplicatorMother;

final class StoryViewsIncrementerTest extends StoryModuleUnitTestCase
{
    /** @var StoryViewsIncrementCommandHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $incrementer = new StoryViewsIncrementer($this->repository());

        $this->handler = new StoryViewsIncrementCommandHandler($incrementer);
    }

    /** @test */
    public function itShouldIncrementViews(): void
    {
        $command = StoryViewsIncrementCommandMother::random();

        $story = StoryMother::withId(StoryIdMother::create($command->storyId()));

        $incremented = DuplicatorMother::with(
            $story,
            [
                'views' => new StoryViews($story->views()->value() + 1)
            ]
        );

        $this->shouldSearch($story);
        $this->shouldSaveStory($incremented);
        $this->dispatch($command, $this->handler);
    }
}
