<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Application\IncrementViews;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Application\IncrementViews\StoryViewsIncrementCommand;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;

final class StoryViewsIncrementCommandMother
{

    public static function create(StoryId $storyId): StoryViewsIncrementCommand
    {
        return new StoryViewsIncrementCommand($storyId->value());
    }

    public static function random(): StoryViewsIncrementCommand
    {
        return self::create(StoryIdMother::random());
    }
}
