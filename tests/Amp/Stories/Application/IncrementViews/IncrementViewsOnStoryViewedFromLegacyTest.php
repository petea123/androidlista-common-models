<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Application\IncrementViews;

use App\Amp\Stories\Application\IncrementViews\IncrementViewsOnStoryViewedFromLegacy;
use App\Amp\Stories\Application\IncrementViews\StoryViewsIncrementCommand;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Stories\Domain\Event\StoryWasViewedFromLegacyMother;
use App\Tests\Amp\Stories\StoryModuleUnitTestCase;

final class IncrementViewsOnStoryViewedFromLegacyTest extends StoryModuleUnitTestCase
{
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new IncrementViewsOnStoryViewedFromLegacy($this->commandBus());
    }

    /** @test */
    public function itShouldIncrementViewsOnStoryViewedFromLegacy(): void
    {
        $event = StoryWasViewedFromLegacyMother::random();

        $command = StoryViewsIncrementCommandMother::create(StoryIdMother::create($event->aggregateId()));

        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldHandle($command);
        $this->notify($event, $this->subscriber);
    }
}
