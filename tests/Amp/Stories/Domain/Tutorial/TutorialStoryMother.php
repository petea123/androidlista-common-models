<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\Tutorial;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Tutorial\TutorialStory;
use App\Amp\Stories\Domain\ValueObject\StoryCreatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryLikes;
use App\Amp\Stories\Domain\ValueObject\StoryUpdatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryViews;
use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Tests\Al\Apks\Domain\ValueObject\File\ExternalUrlMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryFeaturedImageMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryLanguageMother;
use App\Tests\Amp\Stories\Domain\ValueObject\Page\StoryPageMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryCategoryMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryCreatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryGroupIdMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryLikesMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryUpdatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryViewsMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserMother;
use App\Tests\Shared\Domain\AggregateVersionMother;
use App\Tests\Shared\Domain\MotherCreator;
use App\Tests\Shared\Domain\NumberMother;

final class TutorialStoryMother
{
    public static function withId(StoryId $id): TutorialStory
    {
        return self::create(
            $id,
            StoryGroupIdMother::random(),
            ExternalUrlMother::random()->value(),
            self::randomCategories(),
            StoryUserMother::random(),
            StoryLanguageMother::random(),
            MotherCreator::random()->boolean,
            StoryTitleMother::random(),
            StoryDescriptionMother::random(),
            StoryFeaturedImageMother::random(),
            StoryViewsMother::random(),
            StoryLikesMother::random(),
            StoryCreatedAtMother::random(),
            StoryUpdatedAtMother::random(),
            ...self::createRandomPages()
        );
    }

    public static function create(
        StoryId $id,
        StoryGroupId $groupId,
        string $postHref,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        bool $validated,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryViews $views,
        StoryLikes $likes,
        StoryCreatedAt $createdAt,
        StoryUpdatedAt $updatedAt,
        StoryPage ...$pages
    ): TutorialStory {
        return new TutorialStory(
            $id,
            $groupId,
            $postHref,
            $categories,
            $user,
            $language,
            $validated,
            $title,
            $description,
            $featuredImage,
            $views,
            $likes,
            $createdAt,
            $updatedAt,
            AggregateVersionMother::create(1),
            ...$pages
        );
    }

    public static function random(): TutorialStory
    {
        return self::create(
            StoryIdMother::random(),
            StoryGroupIdMother::random(),
            ExternalUrlMother::random()->value(),
            self::randomCategories(),
            StoryUserMother::random(),
            StoryLanguageMother::random(),
            MotherCreator::random()->boolean,
            StoryTitleMother::random(),
            StoryDescriptionMother::random(),
            StoryFeaturedImageMother::random(),
            StoryViewsMother::random(),
            StoryLikesMother::random(),
            StoryCreatedAtMother::random(),
            StoryUpdatedAtMother::random(),
            ...self::createRandomPages()
        );
    }

    public static function createRandomPages(): array
    {
        $pages = [];

        for ($i= 0; $i < NumberMother::lessThan(10); $i++) {
            $pages[] = StoryPageMother::withPageNumber($i);
        }

        return $pages;
    }

    public static function randomCategories(): array
    {
        $categories = [];
        for ($i = 0; $i < NumberMother::between(0, 10); ++$i) {
            $categories[] = StoryCategoryMother::random();
        }

        return $categories;
    }
}
