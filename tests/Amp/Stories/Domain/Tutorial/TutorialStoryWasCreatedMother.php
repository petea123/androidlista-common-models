<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\Tutorial;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Event\TutorialStoryWasCreated;
use App\Amp\Stories\Domain\ValueObject\StoryCreatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryLikes;
use App\Amp\Stories\Domain\ValueObject\StoryViews;
use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\ValueObject\AggregateVersion;
use App\Tests\Amp\Shared\Domain\Stories\StoryFeaturedImageMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryLanguageMother;
use App\Tests\Amp\Stories\Domain\StoryMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryCreatedAtMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryGroupIdMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryLikesMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryViewsMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserMother;
use App\Tests\Shared\Domain\AggregateVersionMother;
use function Lambdish\Phunctional\map;

final class TutorialStoryWasCreatedMother
{
    public static function create(
        StoryId $id,
        StoryGroupId $groupId,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryViews $views,
        StoryLikes $likes,
        StoryCreatedAt $createdAt,
        AggregateVersion $aggregateVersion,
        StoryPage ...$pages
    ): TutorialStoryWasCreated {
        return new TutorialStoryWasCreated(
            $id->value(),
            $groupId->value(),
            map(Story::categoryToValues(), $categories),
            $user->toValues(),
            $language->value(),
            $title->value(),
            $description->value(),
            $featuredImage->value(),
            $views->value(),
            $likes->value(),
            $createdAt->value(),
            map(Story::pageToValues(), $pages),
            $aggregateVersion->value()
        );
    }

    public static function random(): TutorialStoryWasCreated
    {
        return self::create(
            StoryIdMother::random(),
            StoryGroupIdMother::random(),
            StoryMother::randomCategories(),
            StoryUserMother::random(),
            StoryLanguageMother::random(),
            StoryTitleMother::random(),
            StoryDescriptionMother::random(),
            StoryFeaturedImageMother::random(),
            StoryViewsMother::random(),
            StoryLikesMother::random(),
            StoryCreatedAtMother::random(),
            AggregateVersionMother::random(),
            ...StoryMother::createRandomPages()
        );
    }
}
