<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryUpdatedAt;
use App\Tests\Shared\Domain\DateMother;
use DateTimeInterface;

final class StoryUpdatedAtMother
{
    public static function create(DateTimeInterface $dateTime): StoryUpdatedAt
    {
        return new StoryUpdatedAt($dateTime);
    }

    public static function random(): StoryUpdatedAt
    {
        return self::create(DateMother::random()->value());
    }
}
