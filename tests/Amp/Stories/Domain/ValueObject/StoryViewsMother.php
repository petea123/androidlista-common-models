<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryViews;
use App\Tests\Shared\Domain\NumberMother;

final class StoryViewsMother
{
    public static function create(int $views): StoryViews
    {
        return new StoryViews($views);
    }

    public static function random(): StoryViews
    {
        return self::create(NumberMother::between(1,100000));
    }
}
