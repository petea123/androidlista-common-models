<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryTemplate;

final class StoryTemplateMother
{
    public static function create(string $template): StoryTemplate
    {
        return new StoryTemplate($template);
    }

    public static function random(): StoryTemplate
    {
        return StoryTemplate::random();
    }
}
