<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\User;

use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Amp\Stories\Domain\ValueObject\User\StoryUserAvatar;
use App\Amp\Stories\Domain\ValueObject\User\StoryUserUsername;
use App\Shared\Domain\Users\UserId;
use App\Tests\Shared\Domain\Users\UserIdMother;

final class StoryUserMother
{
    public static function create(UserId $userId, StoryUserUsername $username, StoryUserAvatar $avatar): StoryUser
    {
        return new StoryUser($userId, $username, $avatar);
    }

    public static function withId(int $userId): StoryUser
    {
        return self::create(
            UserIdMother::create($userId),
            StoryUserUsernameMother::random(),
            StoryUserAvatarMother::random()
        );
    }

    public static function random(): StoryUser
    {
        return self::create(
            UserIdMother::random(),
            StoryUserUsernameMother::random(),
            StoryUserAvatarMother::random()
        );
    }
}
