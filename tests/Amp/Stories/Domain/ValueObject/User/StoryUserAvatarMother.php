<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\User;

use App\Amp\Stories\Domain\ValueObject\User\StoryUserAvatar;

final class StoryUserAvatarMother
{

    public static function create(string $avatar): StoryUserAvatar
    {
        return new StoryUserAvatar($avatar);
    }

    public static function random(): StoryUserAvatar
    {
        return self::create('http://lorempixel.com/640/480/dafd.jpg');
    }
}
