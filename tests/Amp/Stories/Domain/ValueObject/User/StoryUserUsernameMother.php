<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\User;

use App\Amp\Stories\Domain\ValueObject\User\StoryUserUsername;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryUserUsernameMother
{
    public static function create(string $username): StoryUserUsername
    {
        return new StoryUserUsername($username);
    }

    public static function random(): StoryUserUsername
    {
        return self::create(MotherCreator::random()->word.' ');
    }
}
