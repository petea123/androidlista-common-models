<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryCreatedAt;
use App\Tests\Shared\Domain\DateMother;
use DateTimeInterface;

final class StoryCreatedAtMother
{
    public static function create(DateTimeInterface $dateTime): StoryCreatedAt
    {
        return new StoryCreatedAt($dateTime);
    }

    public static function random(): StoryCreatedAt
    {
        return self::create(DateMother::random()->value());
    }
}
