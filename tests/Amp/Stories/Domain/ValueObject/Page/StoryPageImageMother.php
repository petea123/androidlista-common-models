<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\Page;

use App\Amp\Stories\Domain\ValueObject\Page\StoryPageImage;

final class StoryPageImageMother
{
    public static function create(string $video): StoryPageImage
    {
        return new StoryPageImage($video);
    }

    public static function random(): StoryPageImage
    {
        return self::create('http://lorempixel.com/640/480/dafd.jpg');
    }
}
