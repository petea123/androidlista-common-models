<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\Page;

use App\Amp\Stories\Domain\ValueObject\Page\StoryPageTitle;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryPageTitleMother
{
    public static function create(string $title): StoryPageTitle
    {
        return new StoryPageTitle($title);
    }

    public static function random(): StoryPageTitle
    {
        return self::create(MotherCreator::random()->sentence(3));
    }
}
