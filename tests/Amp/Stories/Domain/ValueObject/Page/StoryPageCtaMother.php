<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\Page;

use App\Amp\Stories\Domain\ValueObject\Page\StoryPageCta;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryPageCtaMother
{
    public static function create(bool $value, string $href): StoryPageCta
    {
        return new StoryPageCta($value, $href);
    }

    public static function random(): StoryPageCta
    {
        return self::create(MotherCreator::random()->boolean,MotherCreator::random()->url);
    }
}
