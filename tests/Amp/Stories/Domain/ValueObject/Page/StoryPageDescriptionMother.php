<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\Page;

use App\Amp\Stories\Domain\ValueObject\Page\StoryPageDescription;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryPageDescriptionMother
{
    public static function create(string $description): StoryPageDescription
    {
        return new StoryPageDescription($description);
    }

    public static function random(): StoryPageDescription
    {
        return self::create(MotherCreator::random()->sentence(3));
    }
}
