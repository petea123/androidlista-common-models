<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\Page;

use App\Al\Shared\Domain\Items\ItemId;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPageCta;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPageDescription;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPageImage;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPageTitle;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPageVideo;
use App\Tests\Al\Shared\Domain\Items\ItemIdMother;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryPageMother
{

    public static function create(
        StoryPageImage $image,
        StoryPageVideo $video,
        StoryPageTitle $title,
        StoryPageDescription $description,
        StoryPageCta $cta,
        ItemId $itemId = null
    ): StoryPage {

        return StoryPage::create($image, $video, $title, $description, $cta, $itemId);
    }

    public static function withPageNumber(int $pageNumber)
    {
        $ctaValue = ($pageNumber === 0) ? false : MotherCreator::random()->boolean;
        return self::create(
            StoryPageImageMother::random(),
            StoryPageVideoMother::random(),
            StoryPageTitleMother::random(),
            StoryPageDescriptionMother::random(),
            StoryPageCtaMother::create($ctaValue, MotherCreator::random()->url),
            MotherCreator::random()->randomElement([null,ItemIdMother::random()])
        );
    }

    public static function random(): StoryPage
    {

        return self::create(
            StoryPageImageMother::random(),
            StoryPageVideoMother::random(),
            StoryPageTitleMother::random(),
            StoryPageDescriptionMother::random(),
            StoryPageCtaMother::random(),
            MotherCreator::random()->randomElement([null,ItemIdMother::random()])
        );
    }
}
