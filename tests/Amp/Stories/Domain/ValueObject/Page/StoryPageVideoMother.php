<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject\Page;

use App\Amp\Stories\Domain\ValueObject\Page\StoryPageVideo;
use App\Tests\Shared\Domain\WordMother;

final class StoryPageVideoMother
{
    public static function create(string $video): StoryPageVideo
    {
        return new StoryPageVideo($video);
    }

    public static function random(): StoryPageVideo
    {
        $path = [
            '',
            '/tmp/video'.'/'.WordMother::random().'.mp4'
        ];

        return self::create(array_rand(array_flip($path)));
    }
}
