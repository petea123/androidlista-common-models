<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Tests\Shared\Domain\UuidMother;

final class StoryGroupIdMother
{
    public static function create(string $id): StoryGroupId
    {
        return new StoryGroupId($id);
    }

    public static function random(): StoryGroupId
    {
        return self::create(UuidMother::random());
    }
}
