<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryTitleMother
{
    public static function create(string $title): StoryTitle
    {
        return new StoryTitle($title);
    }

    public static function random(): StoryTitle
    {
        return self::create(MotherCreator::random()->sentence(3));
    }

}
