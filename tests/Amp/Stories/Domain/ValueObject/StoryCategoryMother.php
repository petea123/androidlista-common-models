<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryCategory;
use App\Tests\Shared\Domain\NumberMother;

final class StoryCategoryMother
{
    public static function create(int $category): StoryCategory
    {
        return new StoryCategory($category);
    }

    public static function random(): StoryCategory
    {
        return self::create(NumberMother::between(1,100000));
    }
}
