<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Tests\Shared\Domain\MotherCreator;

final class StoryDescriptionMother
{
    public static function create(string $description): StoryDescription
    {
        return new StoryDescription($description);
    }

    public static function random(): StoryDescription
    {
        return self::create(MotherCreator::random()->sentence(3));
    }
}
