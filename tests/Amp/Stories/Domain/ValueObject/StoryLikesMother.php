<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\ValueObject;

use App\Amp\Stories\Domain\ValueObject\StoryLikes;
use App\Tests\Shared\Domain\NumberMother;

final class StoryLikesMother
{
    public static function create(int $views): StoryLikes
    {
        return new StoryLikes($views);
    }

    public static function random(): StoryLikes
    {
        return self::create(NumberMother::between(1,100000));
    }
}
