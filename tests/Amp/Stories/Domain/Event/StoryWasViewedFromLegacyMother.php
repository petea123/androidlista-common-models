<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\Event;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Event\StoryWasViewedFromLegacy;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;

final class StoryWasViewedFromLegacyMother
{
    public static function create(StoryId $storyId): StoryWasViewedFromLegacy
    {
        return new StoryWasViewedFromLegacy($storyId->value());
    }

    public static function random(): StoryWasViewedFromLegacy
    {
        return self::create(StoryIdMother::random());
    }
}
