<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories\Domain\Event;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Event\StoryWasScheduledForTranslate;
use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\ValueObject\AggregateVersion;
use App\Tests\Amp\Shared\Domain\Stories\StoryFeaturedImageMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryIdMother;
use App\Tests\Amp\Shared\Domain\Stories\StoryLanguageMother;
use App\Tests\Amp\Stories\Domain\StoryMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryGroupIdMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use App\Tests\Amp\Stories\Domain\ValueObject\User\StoryUserMother;
use App\Tests\Shared\Domain\AggregateVersionMother;
use App\Tests\Shared\Domain\LocaleMother;
use function Lambdish\Phunctional\map;

final class StoryWasScheduledForTranslateMother
{
    public static function create(
        StoryId $id,
        StoryGroupId $groupId,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        StoryLanguage $toLanguage,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        AggregateVersion $aggregateVersion,
        StoryPage ...$pages
    ): StoryWasScheduledForTranslate {
        return new StoryWasScheduledForTranslate(
            $id->value(),
            $groupId->value(),
            map(Story::categoryToValues(), $categories),
            $user->toValues(),
            $language->value(),
            $toLanguage->value(),
            $title->value(),
            $description->value(),
            $featuredImage->value(),
            map(Story::pageToValues(), $pages),
            $aggregateVersion->value()
        );
    }

    public static function random(): StoryWasScheduledForTranslate
    {
        $language = StoryLanguageMother::random();
        return self::create(
            StoryIdMother::random(),
            StoryGroupIdMother::random(),
            StoryMother::randomCategories(),
            StoryUserMother::random(),
            $language,
            StoryLanguageMother::create(LocaleMother::randomDifferent($language)->value()),
            StoryTitleMother::random(),
            StoryDescriptionMother::random(),
            StoryFeaturedImageMother::random(),
            AggregateVersionMother::random(),
            ...StoryMother::createRandomPages()
        );
    }
}
