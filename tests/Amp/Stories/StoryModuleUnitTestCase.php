<?php

declare(strict_types=1);

namespace App\Tests\Amp\Stories;

use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\Tutorial\TutorialStory;
use App\Amp\Stories\Domain\Tutorial\TutorialStoryRepository;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

class StoryModuleUnitTestCase extends ModuleUnitTestCase
{
    private $repository;
    private $tutorialRepository;

    /** @return StoryRepositoryInterface|MockInterface */
    protected function repository()
    {
        return $this->repository = $this->repository ?: $this->mock(StoryRepositoryInterface::class);
    }

    /** @return TutorialStoryRepository|MockInterface */
    protected function tutorialRepository()
    {
        return $this->tutorialRepository = $this->tutorialRepository ?: $this->mock(TutorialStoryRepository::class);
    }

    protected function shouldSearch(?Story $story): void
    {
        $this->repository()
            ->shouldReceive('search')
            ->once()
            ->andReturn($story);
    }

    protected function shouldSaveStory(Story $story): void
    {
        $this->repository()
            ->shouldReceive('save')
            ->with($this->similarTo($story))
            ->once()
            ->andReturnNull();
    }

    protected function shouldSaveTutorialStory(TutorialStory $story): void
    {
        $this->tutorialRepository()
            ->shouldReceive('save')
            ->with($this->similarTo($story))
            ->once()
            ->andReturnNull();
    }
}
