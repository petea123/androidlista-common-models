<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Command;

use App\Shared\Domain\Bus\Command\Command;
use App\Shared\Infrastructure\Bus\Command\TacticianCommandBus;
use App\Tests\Shared\Infrastructure\PHPUnit\UnitTestCase;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\Mapping\ClassName\Suffix;
use League\Tactician\Handler\Mapping\MapByNamingConvention;
use League\Tactician\Handler\Mapping\MethodName\Invoke;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

final class TacticianCommandBusTest extends UnitTestCase
{
    /** @var TacticianCommandBus */
    private $commandBus;

    protected function setUp()
    {
        parent::setUp();

        $container = new ContainerBuilder();
        $container->set(FakeCommandHandler::class, new FakeCommandHandler());


        $handlerMiddleware = new CommandHandlerMiddleware(
            $container,
            new MapByNamingConvention(
                new Suffix('Handler'),
                new Invoke()
            )
        );

        $this->commandBus = new CommandBus($handlerMiddleware);
    }

    /**
     * @test
     * @expectedException RuntimeException
     */
    public function itShouldBeAbleToHandleACommand(): void
    {
        $this->commandBus->handle(new FakeCommand());
    }

    /** @test */
    public function itShouldRaiseAnExceptionDispatchingANonRegisteredCommand(): void
    {
        $this->expectException(ServiceNotFoundException::class);

        $this->commandBus->handle(new CommandException());
    }
}

class CommandException extends Command
{}
