<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Command;

use App\Shared\Domain\Bus\Command\CommandHandler;
use RuntimeException;

final class FakeCommandHandler implements CommandHandler
{
    public function __invoke(FakeCommand $command)
    {
        throw new RuntimeException('This works fine!');
    }
}
