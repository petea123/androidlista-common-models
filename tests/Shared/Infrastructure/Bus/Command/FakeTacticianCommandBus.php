<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Command;

use App\Shared\Domain\Bus\Command\Command;
use App\Shared\Domain\Bus\Command\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\InvokeInflector;
use League\Tactician\CommandBus as TacticianCommandBus;

final class FakeTacticianCommandBus implements CommandBus
{

    /** @var TacticianCommandBus */
    private $bus;

    public function handle(Command $command): void
    {
        $this->bus->handle($command);
    }

    public function setHandler($command, $handle)
    {
        $locator = new InMemoryLocator();
        $locator->addHandler($handle, (new \ReflectionClass($command))->getNamespaceName());

        $commandHandlerMiddleware = new CommandHandlerMiddleware(new ClassNameExtractor(), $locator, new InvokeInflector());

        $this->bus = new TacticianCommandBus([$commandHandlerMiddleware]);
    }
}