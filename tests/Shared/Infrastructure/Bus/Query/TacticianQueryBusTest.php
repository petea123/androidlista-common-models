<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Query;

use App\Shared\Domain\Bus\Query\Query;
use App\Shared\Infrastructure\Bus\Query\TacticianQueryBus;
use App\Tests\Shared\Infrastructure\PHPUnit\UnitTestCase;
use IteratorAggregate;
use League\Container\Exception\NotFoundException;
use RuntimeException;
use Traversable;

final class TacticianQueryBusTest extends UnitTestCase
{
    /** @var TacticianQueryBus */
    private $queryBus;

    protected function setUp()
    {
        parent::setUp();

        $this->queryBus = new TacticianQueryBus([new FakeQueryHandler()]);
    }

    /**
     * @test
     * @expectedException RuntimeException
     */
    public function itShouldReturnAResponseSuccessfully(): void
    {
        $this->queryBus->ask(new FakeQuery());
    }
}
