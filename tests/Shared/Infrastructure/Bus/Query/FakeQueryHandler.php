<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Query;

use App\Shared\Domain\Bus\Query\QueryHandler;
use RuntimeException;

final class FakeQueryHandler implements QueryHandler
{
    public function __invoke(FakeQuery $query)
    {
        throw new RuntimeException('This works fine!');
    }
}
