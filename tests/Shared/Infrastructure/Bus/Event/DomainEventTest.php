<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Event;

use App\Tests\Shared\Domain\UuidMother;
use App\Tests\Shared\Infrastructure\PHPUnit\UnitTestCase;
use ArgumentCountError;
use DomainException;
use TypeError;

final class DomainEventTest extends UnitTestCase
{
    /** @test */
    public function itShouldRetrieveTheEventDataAsAttributes(): void
    {
        $aggregateId    = UuidMother::random();
        $someIdentifier = UuidMother::random();
        $event          = new ConstructionTestDomainEvent($aggregateId, $someIdentifier);

        $this->assertEquals($aggregateId, $event->aggregateId());
        $this->assertEquals($someIdentifier, $event->someIdentifier());
    }

    /** @test */
    public function itShouldThrowAnExceptionConstructingAnEventWithoutARequiredParameter(): void
    {
        $this->expectException(ArgumentCountError::class);

        new ConstructionTestDomainEvent(UuidMother::random());
    }

    /** @test */
    public function itShouldThrowAnExceptionConstructingAnEventHavingAParameterWithAnIncorrectType(): void
    {
        $this->expectException(TypeError::class);

        new ConstructionTestDomainEvent(UuidMother::random(), ['this is an array']);
    }
}
