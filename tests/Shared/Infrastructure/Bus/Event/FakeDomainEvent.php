<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Event;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class FakeDomainEvent extends DomainEvent
{

    public static function eventName(): string
    {
        return 'fake_event';
    }

    public function toPrimitives(): array
    {
        return [];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return '';
    }
}
