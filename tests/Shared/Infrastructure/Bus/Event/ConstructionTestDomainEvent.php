<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Bus\Event;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class ConstructionTestDomainEvent extends DomainEvent
{
    /** @var string */
    private $someIdentifier;

    public function __construct(string $aggregateId, string $someIdentifier, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->someIdentifier = $someIdentifier;
    }

    public static function eventName(): string
    {
        return 'construction_test';
    }

    public function toPrimitives(): array
    {
        return [
            'someIdentifier' => $this->someIdentifier
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['someIdentifier'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return string
     */
    public function someIdentifier(): string
    {
        return $this->someIdentifier;
    }

    public static function stream(): string
    {
        return '';
    }
}
