<?php

declare(strict_types = 1);

namespace App\Tests\Shared\Infrastructure\Mockery;

use App\Tests\Shared\Infrastructure\PHPUnit\Constraint\ConstraintIsSimilar;
use Mockery\Matcher\MatcherAbstract;

final class MatcherIsSimilar extends MatcherAbstract
{
    private $constraint;

    public function __construct($value, $delta = 0.0)
    {
        parent::__construct($value);

        $this->constraint = new ConstraintIsSimilar($value, $delta);
    }

    public function match(&$actual)
    {
        return $this->constraint->evaluate($actual, '', true);
    }

    public function __toString()
    {
        return 'Is similar';
    }
}
