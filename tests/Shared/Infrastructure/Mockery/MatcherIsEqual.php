<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Mockery;

use App\Tests\Shared\Infrastructure\PHPUnit\Constraint\ConstraintIsEqual;
use Mockery\Matcher\MatcherAbstract;

final class MatcherIsEqual extends MatcherAbstract
{
    private $constraint;

    public function __construct($value, $delta = 0.0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
    {
        parent::__construct($value);

        $this->constraint = new ConstraintIsEqual($value, $delta, $maxDepth, $canonicalize, $ignoreCase);
    }

    public static function equalTo($value, $delta = 0.0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
    {
        return new static($value, $delta, $maxDepth, $canonicalize, $ignoreCase);
    }

    public function match(&$actual)
    {
        return $this->constraint->evaluate($actual, '', true);
    }

    public function __toString()
    {
        return 'Is equal';
    }
}