<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\PHPUnit\Module;

use App\Al\Apks\Domain\ApkWebParser;
use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Shared\Domain\Bus\Command\Command;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\Query;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\DownloaderManager;
use App\Shared\Domain\FilesystemInterface;
use App\Shared\Domain\UuidGenerator;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Domain\ValueObject\PackageValueObject;
use App\Shared\Domain\ValueObject\PathValueObject;
use App\Tests\Shared\Infrastructure\PHPUnit\UnitTestCase;
use SplFileInfo;
use function Lambdish\Phunctional\map;
use Mockery\MockInterface;

abstract class ModuleUnitTestCase extends UnitTestCase
{

    private $queryBus;
    private $commandBus;
    private $publisher;
    private $uuidGenerator;
    private $downloader;
    private $filesystem;
    private $file;
    private $webParserFactory;
    private $webParser;

    /** @return QueryBus|MockInterface */
    protected function queryBus()
    {
        return $this->queryBus = $this->queryBus ?: $this->mock(QueryBus::class);
    }

    /** @return CommandBus|MockInterface */
    protected function commandBus()
    {
        return $this->commandBus = $this->commandBus ?: $this->mock(CommandBus::class);
    }

    /** @return DomainEventPublisher|MockInterface */
    protected function publisher()
    {
        return $this->publisher = $this->publisher ?: $this->mock(DomainEventPublisher::class);
    }

    /** @return UuidGenerator|MockInterface */
    protected function uuidGenerator()
    {
        return $this->uuidGenerator = $this->uuidGenerator ?: $this->mock(UuidGenerator::class);
    }

    /** @return DownloaderManager|MockInterface */
    protected function downloader()
    {
        return $this->downloader = $this->downloader ?: $this->mock(DownloaderManager::class);
    }

    /** @return FilesystemInterface|MockInterface */
    protected function filesystem()
    {
        return $this->filesystem = $this->filesystem ?: $this->mock(FilesystemInterface::class);
    }

    /** @return SplFileInfo|MockInterface */
    protected function file()
    {
        return $this->file = $this->file ?: $this->mock(\SplFileInfo::class);
    }

    /** @return WebParserFactory|MockInterface */
    protected function webParserFactory()
    {
        return $this->webParserFactory = $this->webParserFactory ?: $this->mock(WebParserFactory::class);
    }

    /** @return ApkWebParser|MockInterface */
    protected function webParser()
    {
        return $this->webParser = $this->webParser ?: $this->mock(ApkWebParser::class);
    }

    protected function dispatch(Command $command, callable $handler): void
    {
        $handler($command);
    }

    protected function assertAskResponse(Response $expected, Query $query, callable $queryHandler): void
    {
        $actual = $queryHandler($query);

        $this->assertEquals($expected, $actual);
    }

    protected function notify(DomainEvent $event, callable $subscriber): void
    {
        $subscriber($event);
    }

    protected function shouldAsk(Query $query, Response $response = null): void
    {
        $this->queryBus()
            ->shouldReceive('ask')
            ->once()
            //->with($this->similarTo($query))
            ->andReturn($response);
    }

    protected function shouldAskThrowingException(Query $query, $exception): void
    {
        $this->queryBus()
            ->shouldReceive('ask')
            ->once()
            ->with($this->equalTo($query))
            ->andThrow($exception);
    }

    protected function shouldHandle(Command $command): void
    {

        $this->commandBus()
            ->shouldReceive('handle')
            ->once()
            ->with($this->similarTo($command))
            ->andReturnNull();
    }

    protected function shouldGenerateUuid(string $uuid): void
    {
        $this->uuidGenerator()
            ->shouldReceive('next')
            ->once()
            ->withNoArgs()
            ->andReturn($uuid);
    }

    protected function shouldDownload(ExternalUrl $url, SplFileInfo $resource = null): void
    {
        $this->downloader()
            ->shouldReceive('proxyDownload')
            ->once()
            ->with($this->similarTo($url))
            ->andReturn($resource);
    }

    /** @param DomainEvent[] $events */
    protected function shouldRecordDomainEvents(DomainEvent ...$events): void
    {
        $this->publisher()
            ->shouldReceive('record')
            ->once()
            ->with(...map($this->addSimilarTo(), $events))
            ->andReturnNull();
    }

    protected function shouldPutStream(
        PathValueObject $path,
        SplFileInfo $resource = null,
        bool $response = false
    ): void {
        $this->filesystem()
            ->shouldReceive('putStream')
            ->once()
            ->with($this->similarTo($path), $this->similarTo($resource))
            ->andReturn($response);
    }

    protected function shouldMoveFile(PathValueObject $from, PathValueObject $to, bool $response): void
    {
        $this->filesystem()
            ->shouldReceive('move')
            ->once()
            ->with($this->similarTo($from), $this->similarTo($to))
            ->andReturn($response);
    }

    protected function shouldDelete(PathValueObject $path, bool $response): void
    {
        $this->filesystem()
            ->shouldReceive('delete')
            ->once()
            ->with($this->similarTo($path))
            ->andReturn($response);
    }

    protected function shouldInstanceWebParser(ApkSource $source, PackageValueObject $google, bool $webParser = false): void
    {
        $this->webParserFactory()
            ->shouldReceive('instance')
            ->with($this->similarTo($source), $this->similarTo($google))
            ->andReturn($webParser ? $this->webParser() : null);
    }

    protected function shouldGetWebParserApp(PackageValueObject $google, bool $webParser = false): void
    {
        $this->webParser()
            ->shouldReceive('getApp')
            ->with($google)
            ->andReturn($webParser ? $this->webParser() : null);
    }

    protected function shouldGetWebParserAppNames(ItemName $names = null): void
    {
        $this->webParser()
            ->shouldReceive('names')
            ->andReturn($names);
    }

    protected function shouldGetWebParserAppVotes(int $votes = 0): void
    {
        $this->webParser()
            ->shouldReceive('votes')
            ->andReturn($votes);
    }

    protected function shouldGetWebParserAppRating(float $rating = 0): void
    {
        $this->webParser()
            ->shouldReceive('rating')
            ->andReturn($rating);
    }

    protected function shouldGetWebParserAppPrice(float $price = 0): void
    {
        $this->webParser()
            ->shouldReceive('price')
            ->andReturn($price);
    }

    protected function shouldGetWebParserAppSize(string $value = ''): void
    {
        $this->webParser()
            ->shouldReceive('size')
            ->andReturn($value);
    }

    protected function shouldGetWebParserAppMinOsVersion(string $value = ''): void
    {
        $this->webParser()
            ->shouldReceive('osVersion')
            ->andReturn($value);
    }

    protected function shouldGetWebParserAppProvider(string $value = ''): void
    {
        $this->webParser()
            ->shouldReceive('provider')
            ->andReturn($value);
    }

    protected function shouldGetWebParserAppOfficialUrl(string $value = ''): void
    {
        $this->webParser()
            ->shouldReceive('officialUrl')
            ->andReturn($value);
    }

    protected function shouldGetWebParserAppVersion(string $value = ''): void
    {
        $this->webParser()
            ->shouldReceive('lastVersion')
            ->andReturn($value);
    }

    protected function shouldGetWebParserAppCategories(array $categories = []): void
    {
        $this->webParser()
            ->shouldReceive('category')
            ->andReturn($categories);
    }

    protected function shouldGetWebParserItemValues(array $values): void
    {
        $this->webParser()
            ->shouldReceive('toValues')
            ->andReturn($values);
    }

    protected function shouldGetWebParserApkDownloadUrl(ExternalUrl $url = null): void
    {
        $this->webParser()
            ->shouldReceive('apkDownload')
            ->andReturn($url);
    }

    protected function shouldGetWebParserApkUrl(ExternalUrl $url = null): void
    {
        $this->webParser()
            ->shouldReceive('apkUrl')
            ->andReturn($url);
    }

    protected function shouldGetUptodownId(UptodownId $id = null): void
    {
        $this->webParser()
            ->shouldReceive('uptodownId')
            ->andReturn($id);
    }

    private function addSimilarTo(): callable
    {
        return function (DomainEvent $event) {
            return $this->similarTo($event);
        };
    }
}
