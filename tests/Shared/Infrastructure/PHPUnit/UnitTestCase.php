<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\PHPUnit;

use App\Tests\Shared\Domain\TestUtils;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery\Matcher\MatcherAbstract;
use Mockery\MockInterface;

abstract class UnitTestCase extends MockeryTestCase
{
    protected function mock($className): MockInterface
    {
        return Mockery::mock($className);
    }

    protected function namedMock($name, $className): MockInterface
    {
        return Mockery::namedMock($name, $className);
    }

    protected function similarTo($value, $delta = 0.0): MatcherAbstract
    {
        return TestUtils::similarTo($value, $delta);
    }
}
