<?php

declare(strict_types=1);

namespace App\Tests\Shared\Domain;

use App\Shared\Domain\ValueObject\DateValueObject;

final class DateMother
{
    public static function create(\DateTimeInterface $date): DateValueObject
    {
        return new DateValueObject($date);
    }

    public static function random(): DateValueObject
    {
        return self::create(DateTimeMother::random());
    }

    public static function createFromTimestamp(int $timestamp): DateValueObject
    {
        return DateValueObject::createFromTimestamp($timestamp);
    }
}
