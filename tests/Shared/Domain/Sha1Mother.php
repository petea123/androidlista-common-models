<?php

declare(strict_types = 1);

namespace App\Tests\Shared\Domain;

final class Sha1Mother
{
    public static function random(): string
    {
        return MotherCreator::random()->sha1;
    }
}
