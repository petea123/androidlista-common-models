<?php

declare(strict_types=1);

namespace App\Tests\Shared\Domain;

use App\Shared\Domain\ValueObject\LocaleValueObject;

final class LocaleMother
{
    public static function create(string $locale): LocaleValueObject
    {
        return new LocaleValueObject($locale);
    }
    public static function random(): LocaleValueObject
    {
        return LocaleValueObject::random();
    }

    public static function randomDifferent(LocaleValueObject $value): LocaleValueObject
    {
        $locale = LocaleValueObject::random();

        while ($locale->equals($value)) {
            $locale = LocaleValueObject::random();
        }

        return $locale;
    }
}
