<?php

declare(strict_types=1);

namespace App\Tests\Shared\Domain;

use App\Shared\Domain\ValueObject\AggregateVersion;

final class AggregateVersionMother
{
    public static function create(int $aggregateVersion): AggregateVersion
    {
        return new AggregateVersion($aggregateVersion);
    }

    public static function random(): AggregateVersion
    {
        return self::create(IntegerMother::random());
    }
}
