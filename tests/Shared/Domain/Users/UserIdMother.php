<?php

declare(strict_types=1);

namespace App\Tests\Shared\Domain\Users;

use App\Shared\Domain\Users\UserId;
use App\Tests\Shared\Domain\NumberMother;

final class UserIdMother
{
    public static function create(int $id): UserId
    {
        return new UserId($id);
    }

    public static function random(): UserId
    {
        return self::create(NumberMother::random());
    }
}
