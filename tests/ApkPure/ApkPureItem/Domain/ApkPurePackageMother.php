<?php

declare(strict_types=1);

namespace App\Tests\ApkPure\ApkPureItem\Domain;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Tests\Shared\Domain\WordMother;

final class ApkPurePackageMother
{
    public static function create(string $package): ItemApkPurePackage
    {
        return new ItemApkPurePackage($package);
    }

    public static function createFromPackage(string $package): ItemApkPurePackage
    {
        return ItemApkPurePackage::createFromPackage($package);
    }

    public static function random(): ItemApkPurePackage
    {
        return self::create(
            'https://apkpure.com/br/black-desert-mobile/'
            .WordMother::random().'.'
            .WordMother::random().'.'
            .WordMother::random()
        );
    }

    public static function withPackage(string $package): ItemApkPurePackage
    {
        return ItemApkPurePackage::createFromPackage($package);
    }
}
