<?php

declare(strict_types=1);

namespace App\Tests\ApkPure\ApkPureItem\Application\Parse\ByPackageId;

use App\Al\Shared\Domain\Apks\ApkSource;
use App\ApkPure\ApkPureItem\Application\Parse\ByPackageId\ParseApkPureItemByPackageIdQueryHandler;
use App\ApkPure\ApkPureItem\Application\Parse\ByPackageId\Parser;
use App\Tests\Al\Shared\Domain\Apks\ApkSourceMother;
use App\Tests\ApkPure\ApkPureModuleUnitTestCase;
use App\Tests\ApkPure\ApkPureItem\Application\Parse\ApkPureItemResponseMother;
use App\Tests\ApkPure\ApkPureItem\Domain\ApkPurePackageMother;

final class ParserTest extends ApkPureModuleUnitTestCase
{
    /** @var ParseApkPureItemByPackageIdQueryHandler */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $parser = new Parser($this->webParserFactory());
        $this->handler = new ParseApkPureItemByPackageIdQueryHandler($parser);
    }

    /** @test */
    public function itShouldReturnParsedItem(): void
    {
        $query = ParseApkPureItemByPackageIdQueryMother::random();
        $package = ApkPurePackageMother::createFromPackage($query->packageId());
        $response = ApkPureItemResponseMother::withPackage($package);

        $source = ApkSourceMother::create(ApkSource::APKPURE);
        $this->shouldInstanceWebParser($source, $package, true);
        $this->shouldGetWebParserApp($package, true);
        $this->shouldGetWebParserItemValues($response->toArray());
        $this->assertAskResponse($response, $query, $this->handler);
    }
}
