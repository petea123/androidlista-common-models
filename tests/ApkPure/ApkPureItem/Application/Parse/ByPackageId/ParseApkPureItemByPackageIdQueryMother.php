<?php

declare(strict_types=1);

namespace App\Tests\ApkPure\ApkPureItem\Application\Parse\ByPackageId;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\ApkPure\ApkPureItem\Application\Parse\ByPackageId\ParseApkPureItemByPackageIdQuery;
use App\Tests\ApkPure\ApkPureItem\Domain\ApkPurePackageMother;

final class ParseApkPureItemByPackageIdQueryMother
{
    public static function create(ItemApkPurePackage $package): ParseApkPureItemByPackageIdQuery
    {
        return new ParseApkPureItemByPackageIdQuery($package->id());
    }

    public static function random(): ParseApkPureItemByPackageIdQuery
    {
        return self::create(ApkPurePackageMother::random());
    }
}
