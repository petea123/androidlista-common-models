<?php

declare(strict_types=1);

namespace App\Tests\ApkPure\ApkPureItem\Application\Parse;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\ApkPure\ApkPureItem\Application\Parse\ApkPureItemResponse;
use App\Shared\Domain\Utils;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Tests\Shared\Domain\NumberMother;
use App\Tests\Shared\Domain\UrlMother;
use App\Tests\Shared\Domain\WordMother;

final class ApkPureItemResponseMother
{
    public static function create(
        string $packageId,
        array $names,
        int $votes,
        float $rating,
        string $size,
        float $price,
        string $web,
        string $minAndroidVersion,
        string $developer,
        array $category,
        string $icon,
        string $officialUrl,
        string $lastVersion
    ): ApkPureItemResponse {
        return new ApkPureItemResponse(
            $packageId,
            $names,
            $votes,
            $rating,
            $size,
            $price,
            $web,
            $minAndroidVersion,
            $developer,
            $category,
            $icon,
            $officialUrl,
            $lastVersion
        );
    }

    public static function random(): ApkPureItemResponse
    {
        return self::create(
            sprintf('%s.%s.%s', WordMother::random(), WordMother::random(), WordMother::random()),
            self::randomNames(),
            NumberMother::random(),
            NumberMother::floatBetween(1, 10),
            NumberMother::random().' MB',
            NumberMother::float(),
            UrlMother::random(),
            'Android 5 ou superior',
            WordMother::random(),
            [1, 297],
            UrlMother::random(),
            UrlMother::random(),
            WordMother::random()
        );
    }

    public static function withPackage(ItemApkPurePackage $package): ApkPureItemResponse
    {
        return self::create(
            $package->id(),
            self::randomNames(),
            NumberMother::random(),
            NumberMother::floatBetween(1, 10),
            NumberMother::random().' MB',
            NumberMother::float(),
            UrlMother::random(),
            'Android 5 ou superior',
            WordMother::random(),
            [1, 297],
            UrlMother::random(),
            UrlMother::random(),
            WordMother::random()
        );
    }

    private static function randomNames(): array
    {
        $names = [];
        foreach (LocaleValueObject::values() as $locale) {
            $name = WordMother::random().' '.WordMother::random();
            $names[] = [
                'name'.ucfirst($locale) => $name,
                'dirify' => Utils::myDirify($name)
            ];
        }

        return $names;
    }
}
