<?php

declare(strict_types=1);

namespace App\Tests\Backoffice\Stories\Application\SearchByCriteria;

use App\Backoffice\Stories\Application\SearchByCriteria\SearchBackofficeStoriesByCriteriaQuery;

final class SearchBackofficeStoriesByCriteriaQueryMother
{
    public static function create(array $filters): SearchBackofficeStoriesByCriteriaQuery
    {
        return new SearchBackofficeStoriesByCriteriaQuery($filters);
    }
}
