<?php

declare(strict_types=1);

namespace App\Tests\Backoffice\Stories\Application;

use App\Backoffice\Stories\Application\BackofficeStoriesResponse;
use App\Backoffice\Stories\Application\BackofficeStoryResponse;

final class BackofficeStoriesResponseMother
{
    public static function create(BackofficeStoryResponse ...$apksResponse): BackofficeStoriesResponse
    {
        return new BackofficeStoriesResponse(...$apksResponse);
    }

    public static function empty(): BackofficeStoriesResponse
    {
        return self::create(...[]);
    }
}
