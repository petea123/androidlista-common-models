<?php

declare(strict_types=1);

namespace App\Tests\Backoffice\StoriesAdaptation\Application\Create;

use App\Backoffice\StoriesAdaptation\Application\Create\BackofficeStoryAdaptationCreator;
use App\Backoffice\StoriesAdaptation\Application\Create\CreateAdaptationOnScheduledForTranslate;
use App\Tests\Amp\Stories\Domain\Event\StoryWasScheduledForTranslateMother;
use App\Tests\Backoffice\Stories\Application\BackofficeStoriesResponseMother;
use App\Tests\Backoffice\Stories\Application\SearchByCriteria\SearchBackofficeStoriesByCriteriaQueryMother;
use App\Tests\Backoffice\StoriesAdaptation\BackofficeStoryAdaptationModuleUnitTestCase;
use App\Tests\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationMother;
use App\Tests\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationSentencesMother;
use function Lambdish\Phunctional\flatten;
use function Lambdish\Phunctional\map;

final class CreateBackofficeStoryAdaptationOnStoryWasScheduledForTranslateTest extends BackofficeStoryAdaptationModuleUnitTestCase
{
    private $subscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subscriber = new CreateAdaptationOnScheduledForTranslate(new BackofficeStoryAdaptationCreator(
            $this->repository(),
            $this->uuidGenerator(),
            $this->translator(),
            $this->queryBus()
        ));
    }

    /** @test */
    public function itShouldCreateBackofficeStoryAdaptation(): void
    {
        $event = StoryWasScheduledForTranslateMother::random();

        $adaptation = BackofficeStoryAdaptationMother::createWithStoryWasScheduledForTranslate($event);

        $sentences = flatten([$event->title(), $event->description(), map(function($page) {
            return [$page['title'], $page['description']];}, $event->pages())]);

        $this->assertSame(get_class($event), $this->subscriber::subscribedTo()[0]);
        $this->shouldSearchByOriginalStoryIdAndLanguage($adaptation->originalStoryId(), $adaptation->toLanguage(), null);
        $this->shouldAsk(SearchBackofficeStoriesByCriteriaQueryMother::create([
            [
                'field' => 'groupId',
                'operator' => '=',
                'value' => $adaptation->groupId()
            ],
            [
                'field' => 'language',
                'operator' => '=',
                'value' => $adaptation->toLanguage()
            ]
        ]), BackofficeStoriesResponseMother::empty());
        $this->shouldGenerateUuid($adaptation->id());
        $this->shouldBatchTranslate($sentences, $adaptation->fromLanguage(), $adaptation->toLanguage(), $adaptation->sentences());
        $this->shouldBatchTranslate($sentences, $adaptation->fromLanguage(), 'en', $adaptation->references());
        $this->shouldSaveBackofficeStoryAdaptation($adaptation);
        $this->notify($event, $this->subscriber);
    }
}
