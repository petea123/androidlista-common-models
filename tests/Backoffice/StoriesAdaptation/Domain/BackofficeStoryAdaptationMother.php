<?php

declare(strict_types=1);

namespace App\Tests\Backoffice\StoriesAdaptation\Domain;

use App\Amp\Stories\Domain\Event\StoryWasScheduledForTranslate;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptation;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationSentences;
use App\Tests\Amp\Stories\Domain\StoryMother;
use App\Tests\Shared\Domain\LocaleMother;
use App\Tests\Shared\Domain\UuidMother;

final class BackofficeStoryAdaptationMother
{
    public static function create(
        string $id,
        string $groupId,
        string $originalStoryId,
        string $fromLanguage,
        string $toLanguage,
        BackofficeStoryAdaptationSentences $sentences,
        BackofficeStoryAdaptationSentences $references
    ): BackofficeStoryAdaptation {
        return new BackofficeStoryAdaptation(
            $id,
            $groupId,
            $originalStoryId,
            $fromLanguage,
            $toLanguage,
            $sentences,
            $references
        );
    }

    public static function createWithStoryWasScheduledForTranslate(StoryWasScheduledForTranslate $event): BackofficeStoryAdaptation
    {
        return self::create(
            UuidMother::random(),
            $event->groupId(),
            $event->aggregateId(),
            $event->language(),
            $event->toLanguage(),
            BackofficeStoryAdaptationSentencesMother::createWithPages($event->pages()),
            BackofficeStoryAdaptationSentencesMother::createWithPages($event->pages())
        );
    }

    public static function random(): BackofficeStoryAdaptation
    {
        $story = StoryMother::random();
        return self::create(
            UuidMother::random(),
            $story->groupId()->value(),
            $story->id()->value(),
            $story->language()->value(),
            LocaleMother::randomDifferent(LocaleMother::create($story->language()->value()))->value(),
            BackofficeStoryAdaptationSentencesMother::createWithPages($story->pages()),
            BackofficeStoryAdaptationSentencesMother::createWithPages($story->pages())
        );
    }
}
