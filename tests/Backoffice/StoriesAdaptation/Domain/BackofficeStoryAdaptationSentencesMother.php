<?php

declare(strict_types=1);

namespace App\Tests\Backoffice\StoriesAdaptation\Domain;

use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationSentences;
use App\Tests\Amp\Stories\Domain\ValueObject\Page\StoryPageDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\Page\StoryPageTitleMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryDescriptionMother;
use App\Tests\Amp\Stories\Domain\ValueObject\StoryTitleMother;
use function Lambdish\Phunctional\map;

final class BackofficeStoryAdaptationSentencesMother
{

    public static function create(string $title, string $description, array $pages): BackofficeStoryAdaptationSentences
    {
        return new BackofficeStoryAdaptationSentences($title, $description, $pages);
    }

    public static function createWithPages(array $pages): BackofficeStoryAdaptationSentences
    {
        return self::create(
            StoryTitleMother::random()->value(),
            StoryDescriptionMother::random()->value(),
            self::addProposedToPages($pages)
        );
    }

    private static function addProposedToPages(array $pages): array
    {
        return map(function($page) {
            return [
                'proposedTitle' => StoryPageTitleMother::random()->value(),
                'proposedDescription' => StoryPageDescriptionMother::random()->value()
                ];
        }, $pages);
    }
}
