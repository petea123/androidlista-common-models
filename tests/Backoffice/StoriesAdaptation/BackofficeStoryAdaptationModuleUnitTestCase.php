<?php

declare(strict_types=1);

namespace App\Tests\Backoffice\StoriesAdaptation;

use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptation;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationRepository;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationSentences;
use App\Backoffice\StoriesAdaptation\Domain\Translator;
use App\Tests\Shared\Infrastructure\PHPUnit\Module\ModuleUnitTestCase;
use Mockery\MockInterface;

class BackofficeStoryAdaptationModuleUnitTestCase extends ModuleUnitTestCase
{
    private $repository;
    private $translator;

    /** @return BackofficeStoryAdaptationRepository|MockInterface */
    protected function repository()
    {
        return $this->repository = $this->repository ?: $this->mock(BackofficeStoryAdaptationRepository::class);
    }

    /** @return Translator|MockInterface */
    protected function translator()
    {
        return $this->translator = $this->translator ?: $this->mock(Translator::class);
    }

    protected function shouldSearch(?BackofficeStoryAdaptation $story): void
    {
        $this->repository()
            ->shouldReceive('search')
            ->once()
            ->andReturn($story);
    }

    protected function shouldSearchByOriginalStoryIdAndLanguage(string $originalId, string $toLanguage, BackofficeStoryAdaptation $adaptation = null): void
    {
        $this->repository()
            ->shouldReceive('searchByOriginalStoryIdAndLanguage')
            ->with($originalId, $toLanguage)
            ->once()
            ->andReturn($adaptation);
    }

    protected function shouldSaveBackofficeStoryAdaptation(BackofficeStoryAdaptation $story): void
    {
        $this->repository()
            ->shouldReceive('save')
            ->with($this->similarTo($story))
            ->once()
            ->andReturnNull();
    }

    protected function shouldBatchTranslate(array $sentences, string $from, string $to, BackofficeStoryAdaptationSentences $proposed = null): void
    {
        $this->translator()
            ->shouldReceive('batchTranslate')
            ->with($this->similarTo($sentences), $this->similarTo($from), $this->similarTo($to))
            ->once()
            ->andReturn($proposed);
    }
}
