<?php


namespace App\Shared\Domain;

use App\Shared\Domain\ValueObject\ExternalUrl;
use SplFileInfo;

interface DownloaderManager
{
    public function proxyDownload(ExternalUrl $url): ?SplFileInfo;
}
