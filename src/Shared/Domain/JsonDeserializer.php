<?php


namespace App\Shared\Domain;


interface JsonDeserializer
{
    public function deserialize(string $domainEvent);
}
