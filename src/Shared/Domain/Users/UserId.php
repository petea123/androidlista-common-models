<?php

declare(strict_types=1);

namespace App\Shared\Domain\Users;

use App\Shared\Domain\ValueObject\IntValueObject;
use InvalidArgumentException;

final class UserId extends IntValueObject
{
    public function __construct(int $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(int $id)
    {
        if (empty($id)) {
            throw new InvalidArgumentException(sprintf('The user id <%s> is empty', $id));
        }
    }
}
