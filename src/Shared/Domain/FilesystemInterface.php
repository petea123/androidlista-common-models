<?php

declare(strict_types=1);

namespace App\Shared\Domain;

use App\Shared\Domain\ValueObject\PathValueObject;
use SplFileInfo;

interface FilesystemInterface
{
    public function read(PathValueObject $path): ?string;
    public function readStream(PathValueObject $path): ?SplFileInfo;
    public function putStream(PathValueObject $path, SplFileInfo $resource): bool;
    public function move(PathValueObject $from, PathValueObject $to): bool;
    public function delete(PathValueObject $path): bool;
}
