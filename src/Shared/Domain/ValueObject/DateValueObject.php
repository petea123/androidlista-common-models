<?php

namespace App\Shared\Domain\ValueObject;

use DateTimeImmutable;

class DateValueObject
{
    protected $value;

    public function __construct(\DateTimeInterface $value)
    {
        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    public static function now(): self
    {
        return new static(new DateTimeImmutable('now'));
    }

    public static function createFromString(string $date, string $format): self
    {
        return new static(DateTimeImmutable::createFromFormat($format, $date));
    }

    public static function createFromTimestamp(int $timestamp): self
    {
        return new static(DateTimeImmutable::createFromFormat('U', (string)$timestamp));
    }

    public function timestamp(): int
    {
        return $this->value->getTimestamp();
    }

    public function __toString()
    {
        return (string)$this->value->getTimestamp();
    }

    public function isAfter(DateValueObject $dateValueObject): bool
    {
        return $this->value > $dateValueObject->value;
    }
}
