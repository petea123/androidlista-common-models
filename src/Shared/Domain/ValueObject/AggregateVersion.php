<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

final class AggregateVersion extends IntValueObject
{

    public function increment(): self
    {
        return new self($this->value + 1);
    }

    public static function new(): self
    {
        return new self(1);
    }

    public function isNext(AggregateVersion $version): bool
    {
        return (new self($version->value - 1))->equalsTo($this);
    }
}
