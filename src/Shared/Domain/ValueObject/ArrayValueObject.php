<?php

namespace App\Shared\Domain\ValueObject;


abstract class ArrayValueObject
{
    protected $value;

    public function __construct(array $value)
    {
        $this->value = $value;
    }

    public function value(): array
    {
        return $this->value;
    }
}