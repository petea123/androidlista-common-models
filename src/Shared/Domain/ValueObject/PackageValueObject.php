<?php

namespace App\Shared\Domain\ValueObject;

use App\Shared\Domain\Utils;
use InvalidArgumentException;
use RuntimeException;

abstract class PackageValueObject extends StringValueObject
{
    /** @var string  */
    protected $package;

    /** @var string  */
    protected $dirify;

    public function __construct(string $value)
    {
        $value = str_replace(' ', '', $value);
        $this->guardValidUrl($value);
        $id = $this->extractId($value);

        parent::__construct($value);

        $this->package = $id;
        $this->dirify = Utils::myDirify($id);
    }

    private function guardValidUrl(string $url)
    {
        if (false === filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException(sprintf('The url <%s> is not well formatted', $url));
        }
    }

    abstract protected function regex(): string;

    abstract public static function createFromPackage(string $package);

    protected function extractId(string $value): string
    {
        if (false === preg_match_all($this->regex(), $value, $matches) || empty($matches[1][0])) {
            throw new RuntimeException(sprintf('The package id not found for <%s>', $this->value));
        }

        return $matches[1][0];
    }

    public function id(): string
    {
        return $this->package;
    }

    public function dirify(): string
    {
        return $this->dirify;
    }
}
