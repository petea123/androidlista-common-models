<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class LocaleValueObject extends Enum
{

    public const ES = 'es';
    public const BR = 'br';
    public const FR = 'fr';
    public const EN = 'en';
    public const RU = 'ru';
    public const TR = 'tr';
    public const ID = 'id';
    public const IT = 'it';
    public const DE = 'de';
    public const KO = 'ko';
    public const JA = 'ja';
    public const PL = 'pl';
    public const RO = 'ro';
    public const VN = 'vn';
    public const TH = 'th';
    public const NL = 'nl';
    public const GR = 'gr';

    private $DOMAIN_LIST = [
        'es' => 'androidlista.com',
        'br' => 'androidlista.com.br',
        'fr' => 'androidlista.fr',
        'en' => 'androidout.com',
        'ru' => 'androidlist-russia.com',
        'tr' => 'androidliste-tr.com',
        'gr' => 'androidlist.gr',
        'id' => 'androidout.co.id',
        'it' => 'androidlista.it',
        'de' => 'androidliste.de',
        'ko' => 'androidlist.co.kr',
        'ja' => 'androidlist.jp',
        'pl' => 'androidlista.pl',
        'ro' => 'androidliste.ro',
        'vn' => 'androidout.vn',
        'th' => 'androidlista-th.com',
        'nl' => 'androidout.nl',
    ];

    protected function throwExceptionForInvalidValue($value)
    {
        // TODO: Implement throwExceptionForInvalidValue() method.
    }

    public function domain(): string
    {
        return 'https://www.'.$this->DOMAIN_LIST[$this->value];
    }
}
