<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

abstract class EmailValueObject
{
    protected $value;

    public function __construct(string $value)
    {
        $this->guard($value);
        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value();
    }

    public function equals(EmailValueObject $other): bool
    {
        return $other->value === $this->value;
    }

    private function guard(string $value)
    {
        if (false === filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException(sprintf('Email %s is not valid', $value));
        }
    }
}
