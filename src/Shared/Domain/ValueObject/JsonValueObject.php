<?php

namespace App\Shared\Domain\ValueObject;


abstract class JsonValueObject
{
    protected $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function toArray(): array
    {
        return json_decode($this->value, true);
    }
}