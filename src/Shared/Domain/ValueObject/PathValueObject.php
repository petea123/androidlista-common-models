<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class PathValueObject extends StringValueObject
{

    protected const REGEXP_INVALID_URI_CHARS = '/[\x00-\x1f\x7f]/';

    public function __construct(string $value)
    {
        parent::__construct($value);

        $this->guard($value);
    }

    public static function create(string $value): self
    {
        return new self($value);
    }

    private function guard(string $value)
    {
        if (false === preg_match(self::REGEXP_INVALID_URI_CHARS, $value)) {
            throw new \DomainException($value." is not a valid PATH");
        }
    }
}
