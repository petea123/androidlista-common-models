<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

final class DomainEventTracking
{

    /** @var string */
    private $stream;
    /** @var AggregateVersion */
    private $version;

    public function __construct(string $stream, AggregateVersion $version)
    {
        $this->stream = $stream;
        $this->version = $version;
    }

    public static function fromPrimitives(string $stream, int $version): self
    {
        return new self($stream, new AggregateVersion($version));
    }

    public function isValid(AggregateVersion $version)
    {
        if ($version->isNext($this->version)) {
            return true;
        } else if ($this->version->isBiggerThan($version) || $this->version->equalsTo($version)) {
            return false;
        } else if ($version->isBiggerThan(new AggregateVersion($this->version->value() + 1))) {
            throw new \DomainException('Cant process the event');
        }

        return true;
    }

    public function equals(DomainEventTracking $tracking)
    {
        return $this->stream === $tracking->stream;
    }

    /**
     * @return string
     */
    public function stream(): string
    {
        return $this->stream;
    }

    /**
     * @return AggregateVersion
     */
    public function version(): AggregateVersion
    {
        return $this->version;
    }

    public function increment(): void
    {
        $this->version = $this->version->increment();
    }
}
