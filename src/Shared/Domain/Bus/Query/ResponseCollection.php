<?php

declare(strict_types = 1);

namespace App\Shared\Domain\Bus\Query;

use App\Shared\Domain\Collection;

abstract class ResponseCollection extends Collection implements Response
{
}
