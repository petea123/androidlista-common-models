<?php

declare(strict_types = 1);

namespace App\Shared\Domain\Bus\Command;

interface AsyncCommandHandler extends CommandHandler
{
    public static function command(): array;
}
