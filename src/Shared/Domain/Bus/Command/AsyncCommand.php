<?php

declare(strict_types=1);

namespace App\Shared\Domain\Bus\Command;

abstract class AsyncCommand extends Command
{
    abstract public static function commandName(): string;

    abstract public static function fromPrimitives(array $body, string $eventId, string $occurredOn): self;
}
