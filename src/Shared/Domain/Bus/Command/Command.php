<?php

declare(strict_types = 1);

namespace App\Shared\Domain\Bus\Command;

abstract class Command
{

    public function messageType(): string
    {
        return 'command';
    }
}
