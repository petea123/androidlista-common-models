<?php

declare(strict_types = 1);

namespace App\Shared\Domain\Bus\Command;

interface CommandBus
{
    public function __invoke(Command $command): void;

    public function handle(Command $command): void;
}
