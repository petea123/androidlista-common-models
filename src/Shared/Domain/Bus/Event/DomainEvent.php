<?php

declare(strict_types=1);

namespace App\Shared\Domain\Bus\Event;

use App\Shared\Domain\ValueObject\Uuid;
use DateTimeImmutable;
use DateTimeZone;

abstract class DomainEvent
{
    private $aggregateId;
    private $eventId;
    private $occurredOn;
    private $aggregateVersion;

    public function __construct(string $aggregateId, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        $this->aggregateId = $aggregateId;
        $this->eventId     = $eventId ?: Uuid::random()->value();
        $this->occurredOn = $occurredOn ?: (string)(new DateTimeImmutable('now',
            new DateTimeZone('UTC')))->getTimestamp();
        $this->aggregateVersion = $aggregateVersion ?: 0;
    }

    public static function eventName(): string
    {
        return 'DomainEvent';
    }

    abstract public static function stream(): string;

    public function aggregateVersion(): int
    {
        return $this->aggregateVersion;
    }

    abstract public function toPrimitives(): array;

    abstract public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): self;

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function occurredOn(): string
    {
        return $this->occurredOn;
    }
}
