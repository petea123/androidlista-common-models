<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure;

use App\Al\Shared\Infrastructure\ProxyBonanza\ProxyBonanza;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Domain\DownloaderManager;
use SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;

final class PhpDownloader implements DownloaderManager
{
    private const PREFIX = 'download_';

    /**
     * @var Filesystem
     */
    private $fs;

    public function __construct(Filesystem $fs)
    {
        $this->fs = $fs;
    }

    public function proxyDownload(ExternalUrl $url): ?SplFileInfo
    {
        $response = fopen($url->value(), 'r', false, ProxyBonanza::sharedStreamContext());
        if (false === $response || !is_resource($response)) {
            return null;
        }

        $path = $this->fs->tempnam(sys_get_temp_dir(), self::PREFIX);
        $this->fs->dumpFile($path, $response);
        fclose($response);

        register_shutdown_function(function () use ($path) {
            if ($this->fs->exists($path)) {
                $this->fs->remove($path);
            }
        });

        return new SplFileInfo($path);
    }
}
