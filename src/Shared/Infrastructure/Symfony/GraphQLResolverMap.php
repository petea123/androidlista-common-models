<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Symfony;

use App\Shared\Domain\Bus\Command\Command;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Query\Query;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\UuidGenerator;
use Overblog\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Resolver\ResolverMap;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class GraphQLResolverMap extends ResolverMap
{
    /** @var QueryBus */
    private $queryBus;

    /** @var CommandBus */
    private $commandBus;

    /** @var Security */
    private $security;
    /** @var AuthorizationCheckerInterface */
    private $checker;
    /** @var UuidGenerator */
    private $generator;

    public function __construct(
        QueryBus $queryBus,
        CommandBus $commandBus,
        Security $security,
        AuthorizationCheckerInterface $checker,
        UuidGenerator $generator
    ) {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->security = $security;
        $this->checker = $checker;
        $this->generator = $generator;
    }

    protected function handle(Command $command): void
    {
        try {

            $this->commandBus->handle($command);
        } catch (\Exception $e) {
            throw new UserError($e->getMessage());
        }
    }

    protected function ask(Query $query): ?Response
    {
        return $this->queryBus->ask($query);
    }

    protected function user(): ?UserInterface
    {
        return $this->security->getUser();
    }

    protected function isGranted(string $role): bool
    {
        if (!$this->checker->isGranted($role)) {
            throw new AuthenticationException('No grants');
        }

        return true;
    }

    protected function nextUuid(): string
    {
        return $this->generator->next();
    }
}
