<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Solr;

use App\Shared\Infrastructure\Solr\SolrClient;

class SolrRepository
{
    /**
     * @var SolrClient
     */
    private $client;

    public function __construct(SolrClient $client)
    {
        $this->client = $client;
    }

    protected function persist(int $id, array $body): void
    {
        $this->client->persist($id, $body);
    }

    protected function search(int $id): ?array
    {
        return $this->client->search($id);
    }
}
