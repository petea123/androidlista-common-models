<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Redis;

use App\Shared\Infrastructure\Redis\RedisClient;

abstract class RedisRepository
{

    /** @var RedisClient */
    private $client;

    public function __construct(RedisClient $client)
    {
        $this->client = $client;
    }

    protected function persist(string $id, array $plainBody): void
    {
        $this->client->persist($this->indexName($id), $plainBody);
    }

    protected function addToOrderedList(string $key, string $id, int $rank): void
    {
        $this->client->addToOrderedList($this->indexName($key.':list'), $this->indexName($id), $rank);
    }

    protected function removeInOrderedList(string $key, string $id): void
    {
        $this->client->removeInOrderedList($this->indexName($key.':list'), $this->indexName($id));
    }

    abstract protected function aggregateName(): string;

    protected function indexName(string $id): string
    {
        return sprintf('%s:%s', $this->aggregateName(), $id);
    }

    protected function remove(string $id): void
    {
        $this->client->remove($this->indexName($id));
    }
}
