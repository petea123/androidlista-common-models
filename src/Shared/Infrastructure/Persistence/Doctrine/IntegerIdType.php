<?php

declare(strict_types = 1);

namespace App\Shared\Infrastructure\Persistence\Doctrine;

use App\Shared\Domain\ValueObject\IntValueObject;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use function Lambdish\Phunctional\instance_of;

abstract class IntegerIdType extends IntegerType implements DoctrineCustomType
{
    public function getName(): string
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $className = $this->typeClassName();

        return new $className((int)$value);
    }

    /** @var IntValueObject $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof IntValueObject ? $value->value() : (int)$value;
    }

    abstract protected function typeClassName(): string;
}
