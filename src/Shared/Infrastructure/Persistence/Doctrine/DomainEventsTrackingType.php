<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Doctrine;

use App\Shared\Domain\ValueObject\AggregateVersion;
use App\Shared\Domain\ValueObject\DomainEventTracking;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use function Lambdish\Phunctional\map;

final class DomainEventsTrackingType extends JsonType implements DoctrineCustomType
{

    public static function customTypeName(): string
    {
        return 'domain_events_tracking';
    }

    public function getName(): string
    {
        return self::customTypeName();
    }

    /** @var DomainEventTracking[] $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue(map($this->values(), $value), $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $scalars = parent::convertToPHPValue($value, $platform);

        return map($this->toDomainEventTracking(), $scalars);
    }

    private function values()
    {
        return static function (DomainEventTracking $tracking) {
            return ['stream' => $tracking->stream(), 'version' => $tracking->version()->value()];
        };
    }

    private function toDomainEventTracking()
    {
        return static function (array $value) {
            return new DomainEventTracking($value['stream'], new AggregateVersion($value['version']));
        };
    }
}
