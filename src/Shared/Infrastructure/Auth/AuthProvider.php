<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Auth;

use App\Al\Users\Domain\Service\UserViewRepositoryInterface;
use App\Al\Users\Domain\ValueObject\UserUsername;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class AuthProvider implements UserProviderInterface
{

    /** @var UserViewRepositoryInterface */
    private $repository;

    public function __construct(UserViewRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $user = $this->repository->credentialsByUsername(new UserUsername($username));

        if(null === $user) {
            throw new UsernameNotFoundException();
        }

        return Auth::create(
            $user->id(),
            $user->username(),
            $user->hashedPassword(),
            $user->role(),
            $user->locale(),
            $user->salt()
        );
    }

    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException  if the user is not supported
     * @throws UsernameNotFoundException if the user is not found
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return Auth::class === $class;
    }
}
