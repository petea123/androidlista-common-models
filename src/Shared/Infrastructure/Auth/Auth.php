<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Auth;

use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class Auth implements UserInterface, EncoderAwareInterface
{

    private const ROLES = [
        1 => [
            'ROLE_ADMIN',
            'ROLE_CONTENIDO',
            'ROLE_GESTOR_CONTENIDO'
        ],
        2 => ['ROLE_CONTENIDO'],
        4 => [
            'ROLE_GESTOR_CONTENIDO',
            'ROLE_CONTENIDO'
        ]
    ];

    /** @var int */
    private $id;
    /** @var string */
    private $username;
    /** @var string */
    private $hashedPassword;
    /** @var int */
    private $roles;
    /** @var string */
    private $salt;
    /** @var string */
    private $locale;

    private function __construct(int $id, string $username, string $hashedPassword, int $roles, string $locale, string $salt)
    {
        $this->id = $id;
        $this->username = $username;
        $this->hashedPassword = $hashedPassword;
        $this->roles = $roles;
        $this->salt = $salt;
        $this->locale = $locale;
    }

    public static function create(int $id, string $username, string $hashedPassword, int $roles, string $locale, string $salt)
    {
        return new self($id, $username, $hashedPassword, $roles, $locale, $salt);
    }
    /**
     * Gets the name of the encoder used to encode the password.
     *
     * If the method returns null, the standard way to retrieve the encoder
     * will be used instead.
     *
     * @return string
     */
    public function getEncoderName()
    {
        return 'sha1';
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return self::ROLES[$this->roles];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->hashedPassword;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }
}
