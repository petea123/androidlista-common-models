<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Auth\Lexik;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\ValidationData;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Signature\LoadedJWS;

final class LcobucciJWT implements JWTEncoderInterface
{

    private const TTL = 43200;
    /** @var string */
    private $publicKey;
    /** @var string */
    private $privateKey;
    /** @var string */
    private $passphrase;

    public function __construct(string $publicKey, string $privateKey, string $passphrase)
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
        $this->passphrase = $passphrase;
    }

    /**
     * @param array $data
     *
     * @return string the encoded token string
     *
     * @throws JWTEncodeFailureException If an error occurred while trying to create
     *                                   the token (invalid crypto key, invalid payload...)
     */
    public function encode(array $data)
    {
        $signer = new Sha256();
        $privateKey = new Key('file://'.$this->privateKey, $this->passphrase);

        $time = time();
        $token = (new Builder())
            ->issuedAt($time)
            ->expiresAt($data['remember'] ? strtotime('+1 year') : $time + self::TTL)
            ->withClaim('id', $data['id'])
            ->withClaim('username', $data['username'])
            ->withClaim('roles', $data['roles'])
            ->withClaim('locale', $data['locale'])
            ->getToken($signer, $privateKey);

        return (string)$token;

    }

    /**
     * @param string $token
     *
     * @return array
     *
     * @throws JWTDecodeFailureException If an error occurred while trying to load the token
     *                                   (invalid signature, invalid crypto key, expired token...)
     */
    public function decode($token)
    {
        try {
            $token = (new Parser())->parse((string) $token);
        } catch (\Exception $e) {
            throw new JWTDecodeFailureException(JWTDecodeFailureException::INVALID_TOKEN, 'Invalid JWT Token', $e);
        }

        if (!$token->validate(new ValidationData())) {
            throw new JWTDecodeFailureException(JWTDecodeFailureException::INVALID_TOKEN, 'Invalid JWT Token');
        }

        if ($token->isExpired()) {
            throw new JWTDecodeFailureException(JWTDecodeFailureException::EXPIRED_TOKEN, 'Expired JWT Token');
        }

        if (!$token->verify(new Sha256(), new Key('file://'.$this->publicKey, $this->passphrase))) {
            throw new JWTDecodeFailureException(JWTDecodeFailureException::UNVERIFIED_TOKEN, 'Unable to verify the given JWT through the given configuration. If the "lexik_jwt_authentication.encoder" encryption options have been changed since your last authentication, please renew the token. If the problem persists, verify that the configured keys/passphrase are valid.');
        }

        $payload = [];
        foreach ($token->getClaims() as $claim) {
            $payload[$claim->getName()] = $claim->getValue();
        }

        $jws = (new LoadedJWS($payload, true, null !== self::TTL, $token->getHeaders()));
        return $jws->getPayload();
    }
}
