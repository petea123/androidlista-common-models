<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Memcached;

use Symfony\Component\Cache\Adapter\MemcachedAdapter;

final class MemcachedClient
{
    /** @var MemcachedAdapter */
    private static $connection;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    public function connection(): MemcachedAdapter
    {
        self::$connection = self::$connection ?: new MemcachedAdapter(
            MemcachedAdapter::createConnection($this->configuration['dsn']),
            $this->configuration['prefix'],
            $this->configuration['ttl']
        );;

        return self::$connection;
    }
}
