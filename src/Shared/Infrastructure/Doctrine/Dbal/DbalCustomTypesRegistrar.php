<?php

declare(strict_types = 1);

namespace App\Shared\Infrastructure\Doctrine\Dbal;

use App\Shared\Infrastructure\Persistence\Doctrine\DomainEventsTrackingType;
use Doctrine\DBAL\Types\Type;
use function Lambdish\Phunctional\each;

final class DbalCustomTypesRegistrar
{
    private static $initialized = false;

    public static function register(array $dbalCustomTypesClasses): void
    {
        $dbalCustomTypesClasses = array_merge($dbalCustomTypesClasses, [DomainEventsTrackingType::class]);
        if (!self::$initialized) {
            each(self::registerType(), $dbalCustomTypesClasses);

            self::$initialized = true;
        }
    }

    private static function registerType(): callable
    {
        return static function ($dbalCustomTypesClasses): void {
            Type::addType($dbalCustomTypesClasses::customTypeName(), $dbalCustomTypesClasses);
        };
    }
}
