<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Amazon\Sqs;

final class SqsMessage
{

    private $url;

    private $id;

    private $body;

    private $receiptHandle;

    /**
     * SQSMessage constructor.
     * @param $url
     * @param $id
     * @param $body
     * @param $receiptHandle
     */
    public function __construct($url, $id, $body, $receiptHandle)
    {
        $this->url = $url;
        $this->id = $id;
        $this->body = $body;
        $this->receiptHandle = $receiptHandle;
    }

    /**
     * @return mixed
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function body()
    {
        return json_decode($this->body, true);
    }

    public function message()
    {
        return json_decode($this->body, true)['Message'];
    }

    /**
     * @return mixed
     */
    public function receiptHandle()
    {
        return $this->receiptHandle;
    }
}
