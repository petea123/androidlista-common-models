<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Amazon\Sqs;

use Aws\Sqs\SqsClient;

final class SqsConnection
{
    /** @var SqsClient */
    private static $connection;

    /** @var String[] */
    private static $queues = [];

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $name
     * @return String
     */
    public function queue(string $name): String
    {
        if (!array_key_exists($name, self::$queues)) {

            $queue = $this->connection()->getQueueUrl([
                'QueueName' => $name
            ]);

            self::$queues[$name] = $queue->get('QueueUrl');
        }

        return self::$queues[$name];
    }

    private function hasMessages(string $queueUrl): bool
    {
        $response = $this->connection()->getQueueAttributes([
            'AttributeNames' => ['ApproximateNumberOfMessages'],
            'QueueUrl' => $queueUrl, // REQUIRED
        ]);

        $approxMessages = $response->get('Attributes')['ApproximateNumberOfMessages'];

        return $approxMessages >= 1;

    }
    /**
     * @param string $queueUrl
     * @return array
     */
    public function receiveMessage(string $queueUrl): array
    {

        if (!$this->hasMessages($queueUrl)) {
            return [];
        }

        $response = $this->connection()->receiveMessage([
            'QueueUrl' => $queueUrl,
            'MaxNumberOfMessages' => 10,
            'WaitTimeSeconds' => getenv('APP_ENV') === 'dev' ? 0 : 20
        ]);

        $result = $response->get('Messages');
        $messages = [];

        if (is_array($result)) {
            foreach ($result as $item) {
                $messages[] = new SqsMessage(
                    $queueUrl,
                    $item['MessageId'],
                    $item['Body'],
                    $item['ReceiptHandle']
                );
            }
        }

        return $messages;
    }

    public function deleteMessage(SQSMessage $message): void
    {
        $this->connection()->deleteMessage([
            'QueueUrl' => $message->url(),
            'ReceiptHandle' => $message->receiptHandle()
        ]);
    }

    public function deleteMessagesBatch(array $entries, string $queue): void
    {
        $this->connection()->deleteMessageBatch([
            'Entries' => $entries,
            'QueueUrl' => $queue
        ]);
    }

    /**
     * @return SqsClient
     */
    private function connection(): SqsClient
    {
        self::$connection = self::$connection ?: new SqsClient($this->configuration);

        return self::$connection;
    }

    public function consume(string $queueUrl, callable $consumer)
    {
        $messages = $this->receiveMessage($queueUrl);

        if (!empty($messages)) {
            foreach ($messages as $message) {
                $consumer($message);
            }
        }
    }
}
