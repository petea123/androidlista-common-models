<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Amazon\Sqs;

use App\Shared\Domain\JsonDeserializer;
use App\Shared\Infrastructure\Memcached\MemcachedClient;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Cache\CacheItem;
use Throwable;

final class SqsConsumer
{
    /** @var SqsConnection */
    private $connection;
    /** @var JsonDeserializer */
    private $deserializer;
    /** @var MemcachedClient */
    private $memcached;

    private static $PREFIX = 'event.processed.';
    private static $EXPIRES_AFTER = 7200;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        SqsConnection $connection,
        JsonDeserializer $deserializer,
        MemcachedClient $memcached,
        LoggerInterface $logger
    ) {
        $this->connection = $connection;
        $this->deserializer = $deserializer;
        $this->memcached = $memcached;
        $this->logger = $logger;
    }

    public function __invoke(callable $subscriber, string $queueName)
    {
        $queueName .= getenv('APP_ENV') === 'dev' ? '_dev' : '';
        try {
            $this->connection->consume($this->connection->queue($queueName), $this->consumer($subscriber, $queueName));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    private function consumer(callable $subscriber, string $queueName): callable
    {
        return function (SqsMessage $message) use ($subscriber, $queueName) {

            $event = $this->deserializer->deserialize($message->message());

            if ($this->isProcessed($message)) {
                return;
            }

            try {
                $subscriber($event);
                $this->processed($message);
            } catch (Throwable $error) {
                $this->log($error->getMessage(), $message, $queueName, LogLevel::ERROR, $error);
                echo $error->getMessage()."\n";
            }
        };
    }

    private function isProcessed(SqsMessage $message): bool
    {
        if ($this->memcachedEvent($message)->isHit()) {
            echo "Message duplicated\n";
            $this->connection->deleteMessage($message);
            return true;
        }

        return false;
    }

    private function processed(SqsMessage $message): void
    {
        echo "Message consumed\n";
        $this->connection->deleteMessage($message);

        $memcachedEvent = $this->memcachedEvent($message);
        $memcachedEvent->expiresAfter(self::$EXPIRES_AFTER);
        $this->memcached->connection()->save($memcachedEvent->set($message->id()));
    }

    private function memcachedEvent(SqsMessage $message): CacheItem
    {
        return $this->memcached->connection()->getItem(self::$PREFIX.$message->id());
    }

    private function log(
        string $message,
        SqsMessage $envelope,
        string $queueName,
        string $level = LogLevel::ERROR,
        Throwable $exception = null
    ) {
        $body = $envelope->body();
        $event = json_decode($envelope->message(), true);
        $this->logger->log(
            $level,
            $message,
            [
                'data' => $event['data'],
                'topic_arn' => $body['TopicArn'],
                'message_id' => $body['MessageId'],
                'queue'     => $queueName,
                'exception' => $exception ?
                    [
                        'error' => $exception->getMessage(),
                        'file'  => $exception->getFile(),
                        'line'  => $exception->getLine(),
                        'trace' => $exception->getTraceAsString(),
                    ] :
                    null,
            ]
        );
    }
}
