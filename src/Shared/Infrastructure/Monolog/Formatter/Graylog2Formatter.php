<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Monolog\Formatter;

use Exception;
use Gelf\Message;
use Monolog\Formatter\NormalizerFormatter;
use Monolog\Logger;

final class Graylog2Formatter extends NormalizerFormatter
{

    /**
     * Translates Monolog log levels to Graylog2 log priorities.
     */
    private $logLevels = [
        Logger::DEBUG     => 7,
        Logger::INFO      => 6,
        Logger::NOTICE    => 5,
        Logger::WARNING   => 4,
        Logger::ERROR     => 3,
        Logger::CRITICAL  => 2,
        Logger::ALERT     => 1,
        Logger::EMERGENCY => 0,
    ];

    public function __construct()
    {
        parent::__construct('U.u');
    }

    public function format(array $record)
    {
        return $this->formatGraylog(parent::format($record));
    }

    private function formatGraylog(array $record): Message
    {
        $message = new Message();
        $message->setVersion(1)
            ->setLevel($this->logLevels[$record['level']])
            ->setShortMessage($record['message'])
            ->setFacility(['al', $record['channel']])
            ->setHost(gethostname())
            ->setTimestamp($record['datetime'])
            ->setFile($record['extra']['file'] ?? null)
            ->setLine($record['extra']['line'] ?? null);
        unset($record['extra']['line']);
        unset($record['extra']['file']);

        if (!empty($record['extra'])) {
            foreach ($record['extra'] as $key => $val) {
                $message->setAdditional($key, $val);
            }
        }

        if (!empty($record['context'])) {
            foreach ($record['context'] as $key => $val) {
                $message->setAdditional($key, $val);
            }
        }

        return $message;
    }

    /** @param Exception $exception */
    protected function normalizeException($exception): array
    {
        return [
            'class'   => get_class($exception),
            'file'    => $exception->getFile(),
            'line'    => $exception->getLine(),
            'code'    => method_exists($exception, 'errorCode') ? $exception->errorCode() : $exception->getCode(),
            'trace'   => $exception->getTraceAsString(),
            'message' => $exception->getMessage(),
        ];
    }
}