<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Solr;

use Solarium\Client;
use Solarium\Core\Query\QueryInterface;
use Solarium\QueryType\Select\Result\Result as SelectResult;
use Solarium\QueryType\Select\Query\Query as SelectQuery;

class SolrClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * SolrClient constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function persist(int $identifier, array $body): void
    {
        $update = $this->client->createUpdate();
        $doc = $update->createDocument();
        $doc->setKey('id', $identifier);

        foreach($body as $key => $item) {
            $doc->setField($key, $item);
            $doc->setFieldModifier($key, 'set');
        }

        $update->addDocument($doc)->addCommit();
        $this->client->update($update);
    }

    public function search(int $id): ?array
    {
        $query = $this->client->createSelect();
        $query->setQuery("id:{$id}");
        $result = $this->client->select($query);

        return 0 === $result->getNumFound() ? null : $result->getData();
    }

    public function createSelect(array $options = null): SelectQuery
    {
        return $this->client->createSelect($options);
    }

    public function select(QueryInterface $query, $endpoint = null): SelectResult
    {
        return $this->client->select($query, $endpoint);
    }
}
