<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Solr;

use Solarium\Client;

final class SolrClientFactory
{
    public function __invoke(array $config, string $path)
    {
        $solariumClient = new Client([
            'endpoint' => [
                'apps' => [
                    'host' => $config['host'],
                    'port' => $config['port'],
                    'path' => '/',
                    'collection' => $path
                ]
            ]
        ]);

        return new SolrClient($solariumClient);
    }
}

