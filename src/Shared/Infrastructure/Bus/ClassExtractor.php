<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus;

use ReflectionClass;

final class ClassExtractor
{
    public static function extract($class): string
    {
        $reflector = new ReflectionClass($class);

        return $reflector->getName();
    }
}
