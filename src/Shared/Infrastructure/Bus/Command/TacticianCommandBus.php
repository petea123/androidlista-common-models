<?php

namespace App\Shared\Infrastructure\Bus\Command;

use App\Shared\Domain\Bus\Command\Command;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Infrastructure\Bus\ClassExtractor;
use App\Shared\Infrastructure\Bus\Middleware\PublishRecordedEventsMiddleware;
use App\Shared\Infrastructure\Bus\Middleware\RollbackOnlyTransactionMiddleware;
use Doctrine\ORM\EntityManagerInterface;
use League\Tactician\CommandBus as TacticianBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\Mapping\MapByNamingConvention;
use League\Tactician\Handler\Mapping\MethodName\Invoke;
use League\Tactician\Handler\Mapping\ClassName\Suffix;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class TacticianCommandBus implements CommandBus
{
    /** @var TacticianBus  */
    private $bus;

    public function __construct(iterable $handlers, DomainEventPublisher $eventPublisher, EntityManagerInterface $entityManager)
    {

        $container = new ContainerBuilder();
        foreach ($handlers as $handler) {
            $container->set(ClassExtractor::extract($handler), $handler);
        }

        $handlerMiddleware = new CommandHandlerMiddleware(
            $container,
            new MapByNamingConvention(
                new Suffix('Handler'),
                new Invoke()
            )
        );

        $this->bus = new TacticianBus(
            new PublishRecordedEventsMiddleware($eventPublisher),
            new RollbackOnlyTransactionMiddleware($entityManager),
            $handlerMiddleware
        );
    }

    public function __invoke(Command $command): void
    {
        $this->bus->handle($command);
    }

    public function handle(Command $command): void
    {
        $this->bus->handle($command);
    }
}
