<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus\Command;

use App\Shared\Domain\Bus\Command\AsyncCommand;
use App\Shared\Domain\Bus\Command\AsyncCommandHandler;
use RuntimeException;
use Traversable;
use function Lambdish\Phunctional\reduce;
use function Lambdish\Phunctional\reindex;

final class AsyncCommandMapping
{
    private $mapping = [];

    public function __construct(Traversable $mapping)
    {
        $this->mapping = reduce($this->commandsExtractor(), $mapping, []);
    }

    public function for(string $name)
    {
        if (!isset($this->mapping[$name])) {
            throw new RuntimeException("The Async Command Class for <$name> doesn't exists or have no subscribers");
        }

        return $this->mapping[$name];
    }

    public function all()
    {
        return $this->mapping;
    }

    private function commandsExtractor(): callable
    {
        return function (array $mapping, AsyncCommandHandler $command) {
            return array_merge($mapping, reindex($this->commandNameExtractor(), $command::command()));
        };
    }

    private function commandNameExtractor(): callable
    {
        return static function (string $eventClass): string {
            return $eventClass::commandName();
        };
    }
}
