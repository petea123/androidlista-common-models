<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus\Command;

use App\Shared\Domain\Bus\Command\Command;
use App\Shared\Domain\JsonDeserializer;
use App\Shared\Domain\Utils;
use RuntimeException;

final class AsyncCommandJsonDeserializer implements JsonDeserializer
{
    private $mapping;

    public function __construct(AsyncCommandMapping $mapping)
    {
        $this->mapping = $mapping;
    }

    public function deserialize(string $command): Command
    {
        $eventData  = Utils::jsonDecode($command);
        $commandName  = $eventData['data']['command_name'];
        $commandClass = $this->mapping->for($commandName);

        if (null === $commandClass) {
            throw new RuntimeException("The event <$commandName> doesn't exist or has no subscribers");
        }

        return $commandClass::fromPrimitives(
            $eventData['data']['attributes'],
            $eventData['data']['id'],
            (string)$eventData['data']['occurred_on']
        );
    }
}
