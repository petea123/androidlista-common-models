<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus\Event;

use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Infrastructure\Bus\Event\Amazon\SqsQueueNameFormatter;
use RuntimeException;
use Traversable;
use function Lambdish\Phunctional\search;

final class DomainEventSubscriberLocator
{
    private $mapping;

    public function __construct(Traversable $mapping)
    {
        $this->mapping = iterator_to_array($mapping);
    }

    public function withSqsQueueNamed(string $queueName): DomainEventSubscriber
    {
        $subscriber = search(
            static function (DomainEventSubscriber $subscriber) use ($queueName) {
                return SqsQueueNameFormatter::format($subscriber) === str_replace('_deadletter', '',$queueName);
            },
            $this->mapping
        );

        if (null === $subscriber) {
            throw new RuntimeException("There are no subscribers for the <$queueName> queue");
        }

        return $subscriber;
    }
}
