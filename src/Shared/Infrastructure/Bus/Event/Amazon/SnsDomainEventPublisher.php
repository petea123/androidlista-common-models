<?php

namespace App\Shared\Infrastructure\Bus\Event\Amazon;

use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Event\EventBus;
use App\Shared\Infrastructure\Bus\Event\DomainEventJsonSerializer;
use Aws\Sns\SnsClient;
use function Lambdish\Phunctional\each;

class SnsDomainEventPublisher implements EventBus, DomainEventPublisher
{

    private $events = [];

    /**
     * @var SnsClient
     */
    private $client;

    public function __construct(SnsClient $client)
    {
        $this->client = $client;
    }

    /**
     * Immediately publishes the received events
     */
    public function publish(DomainEvent ...$domainEvents)
    {
        each($this->eventPublisher(), $domainEvents);
    }

    private function eventPublisher()
    {
        return function (DomainEvent $event) {

            $result = $this->client->publish(
                [
                    'TopicArn' => getenv('SNS_TOPIC_ARN'),
                    'Message' => DomainEventJsonSerializer::serialize($event),
                    'MessageAttributes' => [
                        'event_name' => [
                            'DataType' => 'String',
                            'StringValue' => $event::eventName()
                        ]
                    ]
                ]
            );
        };
    }

    public function record(DomainEvent ...$domainEvents): void
    {
        $this->events = array_merge($this->events, $domainEvents);
    }

    /**
     * Publishes previously recorded events
     */
    public function publishRecorded(): void
    {
        each($this->eventPublisher(), $this->popEvents());
    }

    private function popEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }

    public function eraseEvents(): void
    {
        $this->events = [];
    }
}
