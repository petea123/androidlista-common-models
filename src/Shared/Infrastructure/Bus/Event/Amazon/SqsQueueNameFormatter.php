<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus\Event\Amazon;

use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Utils;
use function Lambdish\Phunctional\last;

final class SqsQueueNameFormatter
{
    public static function format(DomainEventSubscriber $subscriber): string
    {

        $subscriberClassPaths = explode('\\', get_class($subscriber));

        $queueNameParts = [
            $subscriberClassPaths[1],
            Utils::toSnakeCase($subscriberClassPaths[2]),
            Utils::toSnakeCase((string)last($subscriberClassPaths))
        ];

        return strtolower(implode('-', $queueNameParts));
    }

}
