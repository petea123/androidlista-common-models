<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus\Event;

use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\JsonDeserializer;
use App\Shared\Domain\Utils;
use RuntimeException;

final class DomainEventJsonDeserializer implements JsonDeserializer
{
    private $mapping;

    public function __construct(DomainEventMapping $mapping)
    {
        $this->mapping = $mapping;
    }

    public function deserialize(string $domainEvent): DomainEvent
    {
        $eventData  = Utils::jsonDecode($domainEvent);
        $eventName  = $eventData['data']['event_name'];
        $eventClass = $this->mapping->for($eventName);

        if (null === $eventClass) {
            throw new RuntimeException("The event <$eventName> doesn't exist or has no subscribers");
        }

        return $eventClass::fromPrimitives(
            (string)$eventData['data']['attributes']['id'],
            $eventData['data']['attributes'],
            $eventData['data']['attributes']['aggregate_version'] ?? 0,
            $eventData['data']['id'],
            (string)$eventData['data']['occurred_on']
        );
    }
}
