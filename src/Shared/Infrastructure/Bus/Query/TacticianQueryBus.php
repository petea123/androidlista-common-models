<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus\Query;

use App\Shared\Domain\Bus\Query\Query;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Infrastructure\Bus\ClassExtractor;
use League\Tactician\CommandBus as TacticianBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\Mapping\ClassName\Suffix;
use League\Tactician\Handler\Mapping\MapByNamingConvention;
use League\Tactician\Handler\Mapping\MethodName\Invoke;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Traversable;

final class TacticianQueryBus implements QueryBus
{

    /** @var TacticianBus  */
    private $bus;

    public function __construct(iterable $queryHandlers)
    {
        $container = new ContainerBuilder();
        foreach ($queryHandlers as $handler) {
            $container->set(ClassExtractor::extract($handler), $handler);
        }

        $handlerMiddleware = new CommandHandlerMiddleware(
            $container,
            new MapByNamingConvention(
                new Suffix('Handler'),
                new Invoke()
            )
        );

        $this->bus = new TacticianBus($handlerMiddleware);
    }

    public function ask(Query $query): ?Response
    {
        return $this->bus->handle($query);
    }
}
