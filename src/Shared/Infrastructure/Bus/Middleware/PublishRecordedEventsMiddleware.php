<?php

declare(strict_types=1);


namespace App\Shared\Infrastructure\Bus\Middleware;

use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use Exception;
use League\Tactician\Middleware;

class PublishRecordedEventsMiddleware implements Middleware
{

    /**
     * @var DomainEventPublisher
     */
    private $eventPublisher;

    /**
     * ReleaseRecorderEventsMiddleware constructor.
     *
     */
    public function __construct(DomainEventPublisher $eventPublisher)
    {
        $this->eventPublisher = $eventPublisher;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function execute(object $command, callable $next)
    {
        try {
            $returnValue = $next($command);
        } catch (Exception $exception) {
            $this->eventPublisher->eraseEvents();

            throw $exception;
        }

        $this->eventPublisher->publishRecorded();

        return $returnValue;
    }
}
