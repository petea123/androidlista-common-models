<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Flysystem;

use App\Shared\Domain\FilesystemInterface;
use App\Shared\Domain\ValueObject\PathValueObject;
use League\Flysystem\FilesystemInterface as FlysystemInterface;
use League\Flysystem\MountManager;
use SplFileInfo;

final class FlysystemClient implements FilesystemInterface
{

    /**
     * @var FlysystemInterface
     */
    private $fs;

    /**
     * FlysystemClient constructor.
     * @param FlysystemInterface $fs
     */
    public function __construct(FlysystemInterface $fs)
    {
        $this->fs = $fs;
    }

    public function putStream(PathValueObject $path, SplFileInfo $resource): bool
    {
        return $this->fs->putStream($path->value(), fopen($resource->getRealPath(), 'r'));
    }

    public function read(PathValueObject $path): ?string
    {
        return $this->fs->read($path->value()) ?: null;
    }

    public function move(PathValueObject $from, PathValueObject $to): bool
    {
        if ($this->fs instanceof MountManager) {
            return $this->fs->move($from->value(), $to->value());
        }

        return $this->fs->rename($from->value(), $to->value());
    }

    public function delete(PathValueObject $path): bool
    {
        return $this->fs->delete($path->value());
    }

    public function readStream(PathValueObject $path): ?SplFileInfo
    {
        $resource = $this->fs->readStream($path->value());
        if (false === $resource) {
            return null;
        }
        $metadata = stream_get_meta_data($resource);
        return new SplFileInfo($metadata['uri']);
    }
}
