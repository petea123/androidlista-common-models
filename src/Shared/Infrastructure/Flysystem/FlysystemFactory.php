<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Flysystem;

use League\Flysystem\FilesystemInterface;

final class FlysystemFactory
{
    public function __invoke(FilesystemInterface $fs, array $plugins = [])
    {
        foreach ($plugins as $plugin) {
            $fs->addPlugin($plugin);
        }

        return new FlysystemClient($fs);
    }
}
