<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Redis;

use App\Shared\Domain\Utils;
use Predis\Client;

final class RedisClient
{

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function persist(string $index, array $plainBody): void
    {
        $this->client->set($index, Utils::jsonEncode($plainBody));
    }

    public function addToOrderedList(string $list, string $index, int $rank): void
    {
        $this->client->zadd($list, [$index => $rank]);
    }

    public function removeInOrderedList(string $list, string $index): void
    {
        $this->client->zrem($list, $index);
    }

    public function remove(string $index): void
    {
        $this->client->del($index);
    }

    /**
     * @return Client
     */
    public function client(): Client
    {
        return $this->client;
    }
}
