<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Redis;

use Predis\Client;

final class RedisClientFactory
{
    public function __invoke(string $host): RedisClient
    {
        return new RedisClient(new Client($host));
    }
}
