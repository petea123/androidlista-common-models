<?php

declare(strict_types=1);

namespace App\ApkPure\ApkPureItem\Application\Parse;

use App\Shared\Domain\Bus\Query\Response;

final class ApkPureItemResponse implements Response
{
    /**
     * @var array
     */
    private $names;
    /**
     * @var int
     */
    private $votes;
    /**
     * @var float
     */
    private $rating;
    /**
     * @var string
     */
    private $size;
    /**
     * @var float
     */
    private $price;
    /**
     * @var string
     */
    private $web;
    /**
     * @var string
     */
    private $minAndroidVersion;
    /**
     * @var string
     */
    private $developer;
    /**
     * @var array
     */
    private $category;
    /**
     * @var string
     */
    private $packageId;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $officialUrl;
    /**
     * @var string
     */
    private $lastVersion;

    public function __construct(
        string $packageId,
        array $names,
        int $votes,
        float $rating,
        string $size,
        float $price,
        string $web,
        string $minAndroidVersion,
        string $developer,
        array $category,
        string $icon,
        string $officialUrl,
        string $lastVersion
    ) {
        $this->names = $names;
        $this->votes = $votes;
        $this->rating = $rating;
        $this->size = $size;
        $this->price = $price;
        $this->web = $web;
        $this->minAndroidVersion = $minAndroidVersion;
        $this->developer = $developer;
        $this->category = $category;
        $this->packageId = $packageId;
        $this->icon = $icon;
        $this->officialUrl = $officialUrl;
        $this->lastVersion = $lastVersion;
    }

    /**
     * @return array
     */
    public function names(): array
    {
        return $this->names;
    }

    /**
     * @return int
     */
    public function votes(): int
    {
        return $this->votes;
    }

    /**
     * @return float
     */
    public function rating(): float
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function size(): string
    {
        return $this->size;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function web(): string
    {
        return $this->web;
    }

    /**
     * @return string
     */
    public function minAndroidVersion(): string
    {
        return $this->minAndroidVersion;
    }

    /**
     * @return string
     */
    public function developer(): string
    {
        return $this->developer;
    }

    /**
     * @return array
     */
    public function category(): array
    {
        return $this->category;
    }

    public function toArray(): array
    {
        return [
            'packageId' => $this->packageId(),
            'names' => $this->names(),
            'votes' => $this->votes(),
            'rating' => $this->rating(),
            'size' => $this->size(),
            'price' => $this->price(),
            'web' => $this->web(),
            'minAndroidVersion' => $this->minAndroidVersion(),
            'developer' => $this->developer(),
            'category' => $this->category(),
            'icon' => $this->icon(),
            'officialUrl' => $this->officialUrl(),
            'lastVersion' => $this->lastVersion()
        ];
    }

    /**
     * @return string
     */
    public function packageId(): string
    {
        return $this->packageId;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function officialUrl(): string
    {
        return $this->officialUrl;
    }

    /**
     * @return string
     */
    public function lastVersion(): string
    {
        return $this->lastVersion;
    }
}
