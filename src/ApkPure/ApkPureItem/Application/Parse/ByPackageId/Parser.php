<?php

declare(strict_types=1);

namespace App\ApkPure\ApkPureItem\Application\Parse\ByPackageId;

use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Al\Shared\Domain\Apks\ApkSource;

final class Parser
{
    /**
     * @var WebParserFactory
     */
    private $webParserFactory;

    public function __construct(WebParserFactory $webParserFactory)
    {

        $this->webParserFactory = $webParserFactory;
    }

    public function __invoke(ItemApkPurePackage $package): ?array
    {
        $parser = $this->webParserFactory->instance(ApkSource::apkpure(), $package);

         return null === $parser ? null : $parser->toValues();
    }
}
