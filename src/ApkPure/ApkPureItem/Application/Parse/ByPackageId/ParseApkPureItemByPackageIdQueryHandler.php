<?php

declare(strict_types=1);

namespace App\ApkPure\ApkPureItem\Application\Parse\ByPackageId;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\ApkPure\ApkPureItem\Application\Parse\ApkPureItemResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;

final class ParseApkPureItemByPackageIdQueryHandler implements QueryHandler
{
    /**
     * @var Parser
     */
    private $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    public function __invoke(ParseApkPureItemByPackageIdQuery $command): ?ApkPureItemResponse
    {
        $package = ItemApkPurePackage::createFromPackage($command->packageId());

        $values = apply($this->parser, [$package]);

        if (null === $values) {
            return null;
        }

        return new ApkPureItemResponse(
            $values['packageId'],
            $values['names'],
            $values['votes'],
            $values['rating'],
            $values['size'],
            $values['price'],
            $values['web'],
            $values['minAndroidVersion'],
            $values['developer'],
            $values['category'],
            $values['icon'],
            $values['officialUrl'],
            $values['lastVersion']
        );
    }
}
