<?php

declare(strict_types=1);

namespace App\ApkPure\ApkPureItem\Application\Parse\ByPackageId;

use App\Shared\Domain\Bus\Query\Query;

final class ParseApkPureItemByPackageIdQuery implements Query
{
    /**
     * @var string
     */
    private $packageId;

    public function __construct(string $packageId)
    {
        $this->packageId = $packageId;
    }

    /**
     * @return string
     */
    public function packageId(): string
    {
        return $this->packageId;
    }
}
