<?php

declare(strict_types=1);

namespace App\Amp\Shared\Domain\Stories;

use App\Shared\Domain\ValueObject\LocaleValueObject;

final class StoryLanguage extends LocaleValueObject
{

    public static function forAdapt(): array
    {
        return [
            self::ES,
            self::BR,
            self::FR,
            self::EN,
            self::TR,
            self::RU
        ];
    }
}
