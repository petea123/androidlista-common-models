<?php

declare(strict_types=1);

namespace App\Amp\Shared\Domain\Stories;

use App\Shared\Domain\ValueObject\Uuid;

final class StoryId extends Uuid
{

}
