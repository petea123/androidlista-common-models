<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Application\Rank;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRanking;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingId;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingRepository;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingViews;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingWeek;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\UuidGenerator;

final class StoryWeeklyRanker
{

    /** @var StoryRepositoryInterface */
    private $repository;
    /** @var QueryBus */
    private $queryBus;
    /** @var UuidGenerator */
    private $generator;

    public function __construct(StoryWeeklyRankingRepository $repository, UuidGenerator $generator, QueryBus $queryBus)
    {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
        $this->generator = $generator;
    }

    public function __invoke(StoryId $id, StoryWeeklyRankingViews $views, StoryWeeklyRankingWeek $week)
    {
        $story = $this->queryBus->ask(new SearchStoryQuery($id->value()));

        if (null !== $story && $story->validated()) {

            $ranking = StoryWeeklyRanking::create(
                new StoryWeeklyRankingId($this->generator->next()),
                $id,
                $views,
                $week,
                new StoryLanguage($story->language())
            );

            $this->repository->save($ranking);
        }
    }
}
