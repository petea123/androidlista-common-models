<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Application\Rank;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingViews;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingWeek;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class StoryWeeklyRankingCommandHandler implements CommandHandler
{

    /** @var StoryWeeklyRanker */
    private $ranker;

    public function __construct(StoryWeeklyRanker $ranker)
    {
        $this->ranker = $ranker;
    }

    public function __invoke(StoryWeeklyRankingCommand $command)
    {
        $storyId = new StoryId($command->storyId());
        $views = new StoryWeeklyRankingViews($command->views());
        $week = StoryWeeklyRankingWeek::createFromTimestamp($command->timestamp());

        apply($this->ranker, [$storyId, $views, $week]);
    }
}
