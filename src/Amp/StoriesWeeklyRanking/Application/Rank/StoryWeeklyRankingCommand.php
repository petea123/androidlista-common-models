<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Application\Rank;

use App\Shared\Domain\Bus\Command\Command;

final class StoryWeeklyRankingCommand extends Command
{

    /** @var string */
    private $storyId;
    /** @var int */
    private $views;
    /** @var int */
    private $timestamp;

    public function __construct(string $storyId, int $views, int $timestamp)
    {
        $this->storyId = $storyId;
        $this->views = $views;
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function storyId(): string
    {
        return $this->storyId;
    }

    /**
     * @return int
     */
    public function views(): int
    {
        return $this->views;
    }

    /**
     * @return int
     */
    public function timestamp(): int
    {
        return $this->timestamp;
    }
}
