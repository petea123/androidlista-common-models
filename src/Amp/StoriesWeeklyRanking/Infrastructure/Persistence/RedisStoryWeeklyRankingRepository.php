<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Infrastructure\Persistence;

use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRanking;
use App\Amp\StoriesWeeklyRanking\Domain\StoryWeeklyRankingRepository;
use App\Shared\Infrastructure\Persistence\Redis\RedisRepository;

final class RedisStoryWeeklyRankingRepository extends RedisRepository implements StoryWeeklyRankingRepository
{

    protected function aggregateName(): string
    {
        return 'amp_stories_weekly_ranking';
    }

    public function save(StoryWeeklyRanking $ranking): void
    {
        $this->addToOrderedList($ranking->language()->value().':week:'.$ranking->week()->week(), $ranking->storyId()->value(), $ranking->views()->value());
    }
}
