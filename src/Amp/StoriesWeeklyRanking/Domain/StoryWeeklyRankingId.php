<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Domain;

use App\Shared\Domain\ValueObject\Uuid;

final class StoryWeeklyRankingId extends Uuid
{

}
