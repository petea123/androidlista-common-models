<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Domain;

use App\Shared\Domain\ValueObject\DateValueObject;

final class StoryWeeklyRankingWeek extends DateValueObject
{
    public function week()
    {
        return $this->value->format('W');
    }
}
