<?php

declare(strict_types=1);

namespace App\Amp\StoriesWeeklyRanking\Domain;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Shared\Domain\Aggregate\AggregateRoot;

final class StoryWeeklyRanking extends AggregateRoot
{

    /** @var StoryWeeklyRankingId */
    private $id;
    /** @var StoryId */
    private $storyId;
    /** @var StoryWeeklyRankingViews */
    private $views;
    /** @var StoryWeeklyRankingWeek */
    private $week;
    /** @var StoryLanguage */
    private $language;

    public function __construct(StoryWeeklyRankingId $id, StoryId $storyId, StoryWeeklyRankingViews $views, StoryWeeklyRankingWeek $week, StoryLanguage $language)
    {
        $this->id = $id;
        $this->storyId = $storyId;
        $this->views = $views;
        $this->week = $week;
        $this->language = $language;
    }

    public static function create(StoryWeeklyRankingId $id, StoryId $storyId, StoryWeeklyRankingViews $views, StoryWeeklyRankingWeek $week, StoryLanguage $language): self
    {
        return new self($id, $storyId, $views, $week, $language);
    }

    /**
     * @return StoryWeeklyRankingId
     */
    public function id(): StoryWeeklyRankingId
    {
        return $this->id;
    }

    /**
     * @return StoryId
     */
    public function storyId(): StoryId
    {
        return $this->storyId;
    }

    /**
     * @return StoryWeeklyRankingViews
     */
    public function views(): StoryWeeklyRankingViews
    {
        return $this->views;
    }

    /**
     * @return StoryWeeklyRankingWeek
     */
    public function week(): StoryWeeklyRankingWeek
    {
        return $this->week;
    }

    /**
     * @return StoryLanguage
     */
    public function language(): StoryLanguage
    {
        return $this->language;
    }
}
