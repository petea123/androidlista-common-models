<?php


namespace App\Amp\StoriesWeeklyRanking\Domain;


interface StoryWeeklyRankingRepository
{
    public function save(StoryWeeklyRanking $ranking): void;
}
