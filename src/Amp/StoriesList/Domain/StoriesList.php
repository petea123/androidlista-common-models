<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Domain;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Aggregate\AggregateRoot;
use function Lambdish\Phunctional\map;
use function Lambdish\Phunctional\search;

final class StoriesList extends AggregateRoot
{

    /** @var StoriesListId */
    private $id;

    /** @var CollectionId */
    private $collectionId;

    /** @var StoriesListLanguage */
    private $language;

    /** @var StoriesListTitle */
    private $title;

    /** @var StoriesListDescription */
    private $description;

    /** @var StoriesListCreatedAt */
    private $createdAt;

    /** @var StoriesListUpdatedAt */
    private $updatedAt;

    /** @var StoryId[]*/
    private $stories;

    /**
     * StoriesList constructor.
     * @param StoriesListId $id
     * @param CollectionId $collectionId
     * @param StoriesListLanguage $language
     * @param StoriesListTitle $title
     * @param StoriesListDescription $description
     * @param StoriesListCreatedAt $createdAt
     * @param StoriesListUpdatedAt $updatedAt
     * @param StoryId[] $stories
     */
    public function __construct(
        StoriesListId $id,
        CollectionId $collectionId,
        StoriesListLanguage $language,
        StoriesListTitle $title,
        StoriesListDescription $description,
        StoriesListCreatedAt $createdAt,
        StoriesListUpdatedAt $updatedAt,
        StoryId ...$stories
    ) {
        $this->id = $id;
        $this->collectionId = $collectionId;
        $this->language = $language;
        $this->title = $title;
        $this->description = $description;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->stories = $stories;
    }

    public static function create(
        StoriesListId $id,
        CollectionId $collectionId,
        StoriesListLanguage $language,
        StoriesListTitle $title,
        StoriesListDescription $description,
        StoryId ...$stories
    ): self {
        return new self($id, $collectionId, $language, $title, $description, StoriesListCreatedAt::now(), StoriesListUpdatedAt::now(), ...$stories);
    }

    public function toPrimitives(): array
    {
        return [
            'id' => $this->id->value(),
            'collectionId' => $this->collectionId->value(),
            'language' => $this->language->value(),
            'title' => $this->title->value(),
            'description' => $this->description->value(),
            'createdAt' => $this->createdAt->value(),
            'updatedAt' => $this->updatedAt->value(),
            'stories' => map(function (StoryId $storyId) { return $storyId->value(); }, $this->stories())
        ];
    }

    public function addStory(StoryId $storyId): void
    {
        array_unshift($this->stories, $storyId);
        $this->updatedAt = StoriesListUpdatedAt::now();
    }

    public function removeStory(StoryId $storyId): void
    {
        $this->stories = array_values(array_filter($this->stories, function($story) use ($storyId) {
            return !$this->storyIdComparator($story)($storyId);
        }));
        $this->updatedAt = StoriesListUpdatedAt::now();
    }

    public function hasStory(StoryId $storyId): bool
    {
        return null !== search($this->storyIdComparator($storyId), $this->stories());
    }

    private function storyIdComparator(StoryId $storyId): callable
    {
        return static function (StoryId $other) use ($storyId) {
            return $storyId->equals($other);
        };
    }

    /**
     * @return StoriesListId
     */
    public function id(): StoriesListId
    {
        return $this->id;
    }

    /**
     * @return CollectionId
     */
    public function collectionId(): CollectionId
    {
        return $this->collectionId;
    }

    /**
     * @return StoriesListLanguage
     */
    public function language(): StoriesListLanguage
    {
        return $this->language;
    }

    /**
     * @return StoriesListTitle
     */
    public function title(): StoriesListTitle
    {
        return $this->title;
    }

    /**
     * @return StoriesListDescription
     */
    public function description(): StoriesListDescription
    {
        return $this->description;
    }

    /**
     * @return StoriesListCreatedAt
     */
    public function createdAt(): StoriesListCreatedAt
    {
        return $this->createdAt;
    }

    /**
     * @return StoriesListUpdatedAt
     */
    public function updatedAt(): StoriesListUpdatedAt
    {
        return $this->updatedAt;
    }

    /**
     * @return StoryId[]
     */
    public function stories(): array
    {
        return $this->stories;
    }
}
