<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Domain\Service;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Amp\StoriesList\Domain\StoriesListLanguage;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use DomainException;

final class StoriesListFinder
{
    /** @var StoriesListRepository */
    private $repository;

    public function __construct(StoriesListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findByCategoryAndLanguage(CollectionId $collectionId, StoriesListLanguage $language): StoriesList
    {
        $filters = Filters::fromValues([
            [
                'field' => 'collectionId',
                'operator' => '=',
                'value' => (string)$collectionId->value()
            ],
            [
                'field' => 'language.value',
                'operator' => '=',
                'value' => $language->value()
            ]
        ]);
        $order   = Order::fromValues(null, null);

        $criteria = new Criteria($filters, $order, null, null);

        $list = $this->repository->matching($criteria);

        if (empty($list)) {
            throw new DomainException('StoryCard doesnt exists');
        }

        return $list[0];
    }

}
