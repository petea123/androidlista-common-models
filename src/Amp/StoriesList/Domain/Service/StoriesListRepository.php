<?php


namespace App\Amp\StoriesList\Domain\Service;


use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Amp\StoriesList\Domain\StoriesListId;
use App\Shared\Domain\Criteria\Criteria;

interface StoriesListRepository
{
    public function save(StoriesList $list): void;

    public function matching(Criteria $criteria): array;

    public function search(StoriesListId $list): ?StoriesList;

    public function delete(StoriesList $list): void;

    public function searchByStoryId(StoryId $storyId): array;

    public function searchByStoryIdAndCollectionId(StoryId $storyId, CollectionId $collectionId): ?StoriesList;
}
