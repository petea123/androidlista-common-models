<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Domain;

use App\Shared\Domain\ValueObject\LocaleValueObject;

final class StoriesListLanguage extends LocaleValueObject
{

}
