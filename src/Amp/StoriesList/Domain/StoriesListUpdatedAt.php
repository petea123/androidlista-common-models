<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Domain;

use App\Shared\Domain\ValueObject\DateValueObject;

final class StoriesListUpdatedAt extends DateValueObject
{

}
