<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInList;

use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Event\StoryCategoryWasRemoved;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Bus\Query\QueryBus;

final class RemoveStoryOnStoryCategoryRemoved implements DomainEventSubscriber
{

    /** @var CommandBus */
    private $bus;
    /** @var QueryBus */
    private $queryBus;

    public function __construct(CommandBus $bus, QueryBus $queryBus)
    {
        $this->bus = $bus;
        $this->queryBus = $queryBus;
    }

    public function __invoke(StoryCategoryWasRemoved $event)
    {

        /** @var StoryResponse $story */
        $story = $this->queryBus->ask(new SearchStoryQuery($event->aggregateId()));

        if (null !== $story && !$story->hasCategory($event->category())) {
            $this->bus->handle(new RemoveStoryInListCommand(
                $event->aggregateId(),
                $event->category()
            ));
        }
    }

    public static function subscribedTo(): array
    {
        return [StoryCategoryWasRemoved::class];
    }
}
