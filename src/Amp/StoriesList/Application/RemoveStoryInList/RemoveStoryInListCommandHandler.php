<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInList;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Bus\Command\CommandHandler;

final class RemoveStoryInListCommandHandler implements CommandHandler
{
    /** @var StoryInListRemover */
    private $remover;

    public function __construct(StoryInListRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveStoryInListCommand $command)
    {
        $id = new StoryId($command->id());
        $category = new CollectionId($command->category());
        $this->remover->remove($id, $category);
    }
}
