<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInList;

use App\Shared\Domain\Bus\Command\Command;

final class RemoveStoryInListCommand extends Command
{

    /** @var string */
    private $id;
    /** @var int */
    private $category;

    public function __construct(string $id, int $category)
    {
        $this->id = $id;
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function category(): int
    {
        return $this->category;
    }
}
