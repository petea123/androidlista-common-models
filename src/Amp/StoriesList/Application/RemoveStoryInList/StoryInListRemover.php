<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInList;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\Service\StoriesListRepository;

final class StoryInListRemover
{

    /** @var StoriesListRepository */
    private $repository;

    public function __construct(StoriesListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function remove(StoryId $storyId, CollectionId $category)
    {
        $list = $this->repository->searchByStoryIdAndCollectionId($storyId, $category);

        if (empty($list) || !$list->hasStory($storyId)) {
            return;
        }

        $list->removeStory($storyId);

        if (empty($list->stories())) {
            $this->repository->delete($list);
        } else {
            $this->repository->save($list);
        }
    }
}
