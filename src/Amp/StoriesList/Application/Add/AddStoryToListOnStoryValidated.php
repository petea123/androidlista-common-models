<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\Add;

use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Event\StoryWasValidated;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Bus\Query\QueryBus;

final class AddStoryToListOnStoryValidated implements DomainEventSubscriber
{
    /** @var CommandBus */
    private $bus;
    /** @var QueryBus */
    private $queryBus;

    public function __construct(CommandBus $bus, QueryBus $queryBus)
    {
        $this->bus = $bus;
        $this->queryBus = $queryBus;
    }

    public function __invoke(StoryWasValidated $event): void
    {
        /** @var StoryResponse $story */
        $story = $this->queryBus->ask(new SearchStoryQuery($event->aggregateId()));

        if (null === $story || $story->validated() === false) {
            return;
        }

        foreach ($story->categories() as $collectionId) {
            $this->bus->handle(new AddStoryToListCommand(
                $collectionId,
                $event->aggregateId(),
                $story->language()
            ));
        }
    }

    public static function subscribedTo(): array
    {
        return [StoryWasValidated::class];
    }
}
