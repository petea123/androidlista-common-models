<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\Add;

use App\Shared\Domain\Bus\Command\Command;

final class AddStoryToListCommand extends Command
{

    /** @var int */
    private $collectionId;
    /** @var string */
    private $storyId;
    /** @var string */
    private $language;

    public function __construct(
        int $collectionId,
        string $storyId,
        string $language
    ) {
        $this->collectionId = $collectionId;
        $this->storyId = $storyId;
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function collectionId(): int
    {
        return $this->collectionId;
    }

    /**
     * @return string
     */
    public function storyId(): string
    {
        return $this->storyId;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
