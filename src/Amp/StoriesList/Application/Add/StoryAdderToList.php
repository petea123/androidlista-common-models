<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\Add;

use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Application\CollectionsResponse;
use App\Al\Collections\Application\Search\ByCriteria\SearchCollectionsByCriteriaQuery;
use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\Service\StoriesListRepository;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Amp\StoriesList\Domain\StoriesListDescription;
use App\Amp\StoriesList\Domain\StoriesListId;
use App\Amp\StoriesList\Domain\StoriesListLanguage;
use App\Amp\StoriesList\Domain\StoriesListTitle;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use App\Shared\Domain\UuidGenerator;

final class StoryAdderToList
{
    /** @var StoriesListRepository */
    private $repository;
    /** @var QueryBus */
    private $queryBus;
    /** @var UuidGenerator */
    private $generator;

    public function __construct(StoriesListRepository $repository, QueryBus $queryBus, UuidGenerator $generator)
    {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
        $this->generator = $generator;
    }

    public function __invoke(CollectionId $collectionId, StoryId $storyId, StoriesListLanguage $language)
    {

        $list = $this->searchOrCreateList($collectionId, $language);

        if (!$list->hasStory($storyId)) {
            $list->addStory($storyId);

            $this->repository->save($list);
        }
    }

    public function searchOrCreateList(CollectionId $collectionId, StoriesListLanguage $language): ?StoriesList
    {
        $criteria = new Criteria($this->filter($collectionId, $language), Order::none(), null, null);
        $list = $this->repository->matching($criteria);

        return $list[0] ?? $this->createList($collectionId, $language);
    }

    private function createList(CollectionId $collectionId, StoriesListLanguage $language)
    {
        /** @var CollectionsResponse $collection */
        $collection = $this->queryBus->ask(new SearchCollectionsByCriteriaQuery([
            [
                'field' => 'collectionGroup',
                'operator' => '=',
                'value' => (string)$collectionId->value()
            ],
            [
                'field' => 'language',
                'operator' => '=',
                'value' => $language->value()
            ]
        ]))->collections();

        if (empty($collection[0])) {
            throw new \DomainException(sprintf('Collection %s with language %s doesnt exists', $collectionId->value(), $language->value()));
        }

        /** @var CollectionResponse $collection */
        $collection = $collection[0];

        return StoriesList::create(
            new StoriesListId($this->generator->next()),
            $collectionId,
            $language,
            new StoriesListTitle($collection->shortname()),
            new StoriesListDescription(strip_tags(html_entity_decode($collection->description())))
        );
    }

    private function filter(CollectionId $collectionId, StoriesListLanguage $language): Filters
    {
        return Filters::fromValues([
            [
                'field' => 'collectionId',
                'operator' => '=',
                'value' => (string)$collectionId->value()
            ],
            [
                'field' => 'language.value',
                'operator' => '=',
                'value' => $language->value()
            ],
        ]);
    }
}
