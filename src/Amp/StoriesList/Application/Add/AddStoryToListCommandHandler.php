<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\Add;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\StoriesListLanguage;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class AddStoryToListCommandHandler implements CommandHandler
{
    /** @var StoryAdderToList */
    private $adder;

    public function __construct(StoryAdderToList $adder)
    {
        $this->adder = $adder;
    }

    public function __invoke(AddStoryToListCommand $command)
    {
        $collectionId = new CollectionId($command->collectionId());
        $language = new StoriesListLanguage($command->language());
        $storyId = new StoryId($command->storyId());

        apply($this->adder, [$collectionId, $storyId, $language]);
    }

}
