<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application;

use App\Shared\Domain\Bus\Query\Response;

final class StoriesListResponse implements Response
{

    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
