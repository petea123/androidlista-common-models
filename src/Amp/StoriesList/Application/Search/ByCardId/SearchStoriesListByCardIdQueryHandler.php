<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\Search\ByCardId;

use App\Amp\Shared\Domain\StoriesCard\StoryCardId;
use App\Amp\StoriesList\Application\StoriesListResponse;
use App\Amp\StoriesList\Application\StoriesListsResponse;
use App\Amp\StoriesList\Domain\Service\StoriesListRepository;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\map;

final class SearchStoriesListByCardIdQueryHandler implements QueryHandler
{

    /** @var StoriesListRepository */
    private $repository;

    public function __construct(StoriesListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(SearchStoriesListByCardIdQuery $query): StoriesListsResponse
    {
        $lists = $this->repository->searchByStoryCardId(new StoryCardId($query->id()));

        return new StoriesListsResponse(...map($this->toResponse(), $lists));
    }


    private function toResponse(): callable
    {
        return static function (StoriesList $list) {
            return new StoriesListResponse(
                $list->id()->value()
            );
        };
    }
}
