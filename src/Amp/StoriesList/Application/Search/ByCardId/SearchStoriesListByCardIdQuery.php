<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\Search\ByCardId;

use App\Shared\Domain\Bus\Query\Query;

final class SearchStoriesListByCardIdQuery implements Query
{

    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
