<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application;

use App\Shared\Domain\Bus\Query\Response;

final class StoriesListsResponse implements Response
{
    /** @var StoriesListResponse[] */
    private $lists;

    public function __construct(StoriesListResponse ...$lists)
    {
        $this->lists = $lists;
    }

    public function lists(): array
    {
        return $this->lists;
    }
}
