<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInAllLists;

use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Domain\Event\StoryWasDeleted;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Bus\Query\QueryBus;

final class RemoveStoryInAllListsOnStoryDeleted implements DomainEventSubscriber
{

    /** @var CommandBus */
    private $bus;
    /** @var QueryBus */
    private $queryBus;

    public function __construct(CommandBus $bus, QueryBus $queryBus)
    {
        $this->bus = $bus;
        $this->queryBus = $queryBus;
    }

    public function __invoke(StoryWasDeleted $event)
    {
        $story = $this->queryBus->ask(new SearchStoryQuery($event->aggregateId()));

        if (null === $story) {
            $this->bus->handle(new RemoveStoryInAllListsCommand($event->aggregateId()));
        }
    }

    public static function subscribedTo(): array
    {
        return [StoryWasDeleted::class];
    }
}
