<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInAllLists;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\Service\StoriesListRepository;
use App\Amp\StoriesList\Domain\StoriesList;

final class StoryRemoverInAllLists
{

    /** @var StoriesListRepository */
    private $repository;

    public function __construct(StoriesListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(StoryId $storyId)
    {
        $lists = $this->repository->searchByStoryId($storyId);

        /** @var StoriesList $list */
        foreach ($lists as $list) {
            if ($list->hasStory($storyId)) {
                $list->removeStory($storyId);
                if (empty($list->stories())) {
                    $this->repository->delete($list);
                }
                $this->repository->save($list);
            }
        }
    }
}
