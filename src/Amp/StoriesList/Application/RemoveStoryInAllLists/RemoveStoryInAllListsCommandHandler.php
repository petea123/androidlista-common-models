<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Application\RemoveStoryInAllLists;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class RemoveStoryInAllListsCommandHandler implements CommandHandler
{

    /** @var StoryRemoverInAllLists */
    private $remover;

    public function __construct(StoryRemoverInAllLists $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveStoryInAllListsCommand $command)
    {
        $storyId = new StoryId($command->id());

        apply($this->remover, [$storyId]);
    }
}
