<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Infrastructure\Persistence\Doctrine;

use App\Amp\StoriesList\Domain\StoriesListId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class StoriesListIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'stories_list_id';
    }

    protected function typeClassName(): string
    {
        return StoriesListId::class;
    }
}
