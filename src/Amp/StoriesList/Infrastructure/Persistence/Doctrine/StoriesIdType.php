<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Infrastructure\Persistence\Doctrine;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use function Lambdish\Phunctional\map;

final class StoriesIdType extends JsonType implements DoctrineCustomType
{

    public static function customTypeName(): string
    {
        return 'stories_id';
    }

    public function getName(): string
    {
        return self::customTypeName();
    }

    /** @var StoryId[] $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue(map($this->values(), $value), $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $scalars = parent::convertToPHPValue($value, $platform);

        return map($this->toStoryId(), $scalars);
    }

    private function values()
    {
        return static function (StoryId $id) {
            return $id->value();
        };
    }

    private function toStoryId()
    {
        return static function (string $value) {
            return new StoryId($value);
        };
    }
}
