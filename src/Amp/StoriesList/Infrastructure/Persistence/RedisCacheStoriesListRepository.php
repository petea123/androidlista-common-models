<?php

declare(strict_types=1);

namespace App\Amp\StoriesList\Infrastructure\Persistence;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\Service\StoriesListRepository;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Amp\StoriesList\Domain\StoriesListId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Redis\RedisRepository;
use App\Shared\Infrastructure\Redis\RedisClient;

final class RedisCacheStoriesListRepository extends RedisRepository implements StoriesListRepository
{

    /** @var MySqlStoriesListRepository */
    private $repository;

    public function __construct(RedisClient $client, MySqlStoriesListRepository $repository)
    {
        parent::__construct($client);
        $this->repository = $repository;
    }

    public function save(StoriesList $list): void
    {
        $this->repository->save($list);

        $this->persist($list->id()->value(), $list->toPrimitives());
        $this->addToOrderedList($list->language()->value(), $list->id()->value(), $list->updatedAt()->timestamp());
    }

    public function matching(Criteria $criteria): array
    {
        return $this->repository->matching($criteria);
    }

    public function search(StoriesListId $list): ?StoriesList
    {
        return $this->repository->search($list);
    }

    protected function aggregateName(): string
    {
        return 'amp_stories_list';
    }

    public function delete(StoriesList $list): void
    {
        $this->repository->delete($list);
        $this->remove($list->id()->value());
        $this->removeInOrderedList($list->language()->value(), $list->id()->value());
    }

    public function searchByStoryId(StoryId $storyId): array
    {
        return $this->repository->searchByStoryId($storyId);
    }

    public function searchAll(): array
    {
        return $this->repository->searchAll();
    }

    public function searchByStoryIdAndCollectionId(StoryId $storyId, CollectionId $collectionId): ?StoriesList
    {
        return $this->repository->searchByStoryIdAndCollectionId($storyId, $collectionId);
    }
}
