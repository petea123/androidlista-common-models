<?php

declare(strict_types = 1);

namespace App\Amp\StoriesList\Infrastructure\Persistence;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\StoriesList\Domain\Service\StoriesListRepository;
use App\Amp\StoriesList\Domain\StoriesList;
use App\Amp\StoriesList\Domain\StoriesListId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlStoriesListRepository extends DoctrineRepository implements StoriesListRepository
{

    public function save(StoriesList $list): void
    {
        $this->persist($list);
        $this->entityManager()->flush($list);
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        return $this->repository(StoriesList::class)->matching($doctrineCriteria)->toArray();
    }

    public function search(StoriesListId $id): ?StoriesList
    {
        return $this->repository(StoriesList::class)->find($id);
    }

    public function delete(StoriesList $list): void
    {
        $this->remove($list);
        $this->entityManager()->flush($list);
    }

    public function searchByStoryId(StoryId $storyId): array
    {
        $lists = $this->repository(StoriesList::class)
            ->createQueryBuilder('l')
            ->where('l.stories LIKE :story')
            ->setParameter('story', '%'.$storyId->value().'%')
            ->getQuery()
            ->getResult();

        return $lists;
    }

    public function searchByStoryIdAndCollectionId(StoryId $storyId, CollectionId $collectionId): ?StoriesList
    {
        $list = $this->repository(StoriesList::class)
            ->createQueryBuilder('l')
            ->where('l.stories LIKE :story')
            ->andWhere('l.collectionId = :collectionId')
            ->setParameter('collectionId', $collectionId)
            ->setParameter('story', '%'.$storyId->value().'%')
            ->getQuery()
            ->getOneOrNullResult();

        return $list;
    }

    public function searchAll(): array
    {
        return $this->repository(StoriesList::class)->findAll();
    }
}
