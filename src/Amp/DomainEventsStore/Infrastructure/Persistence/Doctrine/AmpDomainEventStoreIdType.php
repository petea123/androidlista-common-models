<?php

declare(strict_types=1);

namespace App\Amp\DomainEventsStore\Infrastructure\Persistence\Doctrine;

use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class AmpDomainEventStoreIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'amp_domain_event_store_id';
    }

    protected function typeClassName(): string
    {
        return AmpDomainEventStoreId::class;
    }
}
