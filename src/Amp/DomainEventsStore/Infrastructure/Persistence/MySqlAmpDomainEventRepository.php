<?php

declare(strict_types=1);

namespace App\Amp\DomainEventsStore\Infrastructure\Persistence;

use App\Amp\DomainEventsStore\Domain\AmpDomainEventStore;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreRepository;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use DateTimeInterface;

final class MySqlAmpDomainEventRepository extends DoctrineRepository implements AmpDomainEventStoreRepository
{
    public function save(AmpDomainEventStore $event): void
    {
        $this->persist($event);
        $this->entityManager()->flush($event);
    }

    public function searchAndCountWeeklyStoryWasViewedFromLegacy(DateTimeInterface $date): array
    {
        $startDate = $date->setISODate((int)$date->format('Y'), (int)$date->format('W'));
        $endDate = $date->setISODate((int)$date->format('Y'), (int)$date->format('W'), 7);

        echo $startDate->format('Y-m-d H:i:s');
        echo $endDate->format('Y-m-d H:i:s');

        $qb = $this->queryBuilder()
            ->select('COUNT(e.aggregateId.value) as views, e.aggregateId.value as aggregateId')
            ->from(AmpDomainEventStore::class, 'e')
            ->where('e.name.value = :eventName')
            ->andWhere('e.occurredOn.value >= :startDate')
            ->andWhere('e.occurredOn.value <= :endDate')
            ->groupBy('aggregateId')
            ->setParameter('eventName', 'StoryWasViewedFromLegacy')
            ->setParameter('startDate', $startDate->format('Y-m-d').' 00:00:00')
            ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
            ->getQuery();

        echo $qb->getSQL();

        return $qb->getResult();
    }
}
