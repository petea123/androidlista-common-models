<?php

declare(strict_types=1);

namespace App\Amp\DomainEventsStore\Application\Store;

use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreAggregateId;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreAggregateVersion;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreBody;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreId;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreName;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreOccurredOn;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreRepository;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStore;

final class AmpDomainEventStorer
{
    private $repository;

    public function __construct(AmpDomainEventStoreRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(
        AmpDomainEventStoreId $id,
        AmpDomainEventStoreAggregateId $aggregateId,
        AmpDomainEventStoreAggregateVersion $aggregateVersion,
        AmpDomainEventStoreName $name,
        AmpDomainEventStoreBody $body,
        AmpDomainEventStoreOccurredOn $occurredOn
    ): void {
        $domainEvent = new AmpDomainEventStore($id, $aggregateId, $aggregateVersion, $name, $body, $occurredOn);

        $this->repository->save($domainEvent);
    }
}
