<?php

declare(strict_types=1);

namespace App\Amp\DomainEventsStore\Application\Store;

use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreAggregateId;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreAggregateVersion;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreBody;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreId;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreName;
use App\Amp\DomainEventsStore\Domain\AmpDomainEventStoreOccurredOn;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class AmpStoreDomainEventOnOccurred implements DomainEventSubscriber
{
    private $storer;

    public function __construct(AmpDomainEventStorer $storer)
    {
        $this->storer = $storer;
    }

    public static function subscribedTo(): array
    {
        return [DomainEvent::class];
    }

    public function __invoke(DomainEvent $event)
    {
        $id          = new AmpDomainEventStoreId($event->eventId());
        $aggregateId = new AmpDomainEventStoreAggregateId($event->aggregateId());
        $aggregateVersion = new AmpDomainEventStoreAggregateVersion($event->aggregateVersion());
        $name        = new AmpDomainEventStoreName($event::eventName());
        $body        = new AmpDomainEventStoreBody($event->toPrimitives());
        $occurredOn = AmpDomainEventStoreOccurredOn::createFromTimestamp((int)$event->occurredOn());
        $this->storer->store($id, $aggregateId, $aggregateVersion, $name, $body, $occurredOn);
    }
}
