<?php

declare(strict_types = 1);

namespace App\Amp\DomainEventsStore\Domain;

final class AmpDomainEventStoreAggregateId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }
}
