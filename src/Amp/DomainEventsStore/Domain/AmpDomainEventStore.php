<?php

declare(strict_types=1);

namespace App\Amp\DomainEventsStore\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;

final class AmpDomainEventStore extends AggregateRoot
{
    private $id;
    private $aggregateId;
    private $name;
    private $body;
    private $occurredOn;
    private $aggregateVersion;

    public function __construct(
        AmpDomainEventStoreId $id,
        AmpDomainEventStoreAggregateId $aggregateId,
        AmpDomainEventStoreAggregateVersion $aggregateVersion,
        AmpDomainEventStoreName $name,
        AmpDomainEventStoreBody $body,
        AmpDomainEventStoreOccurredOn $occurredOn
    ) {
        $this->id          = $id;
        $this->aggregateId = $aggregateId;
        $this->name        = $name;
        $this->body        = $body;
        $this->occurredOn = $occurredOn;
        $this->aggregateVersion = $aggregateVersion;
    }

    public function id(): AmpDomainEventStoreId
    {
        return $this->id;
    }

    public function aggregateId(): AmpDomainEventStoreAggregateId
    {
        return $this->aggregateId;
    }

    public function name(): AmpDomainEventStoreName
    {
        return $this->name;
    }

    public function body(): AmpDomainEventStoreBody
    {
        return $this->body;
    }

    public function occurredOn(): AmpDomainEventStoreOccurredOn
    {
        return $this->occurredOn;
    }

    /**
     * @return AmpDomainEventStoreAggregateVersion
     */
    public function aggregateVersion(): AmpDomainEventStoreAggregateVersion
    {
        return $this->aggregateVersion;
    }
}
