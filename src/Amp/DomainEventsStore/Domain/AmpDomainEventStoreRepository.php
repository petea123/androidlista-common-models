<?php

declare(strict_types=1);

namespace App\Amp\DomainEventsStore\Domain;

use DateTimeInterface;

interface AmpDomainEventStoreRepository
{
    public function save(AmpDomainEventStore $event): void;

    public function searchAndCountWeeklyStoryWasViewedFromLegacy(DateTimeInterface $date): array;
}
