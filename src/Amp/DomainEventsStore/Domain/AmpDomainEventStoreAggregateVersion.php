<?php

declare(strict_types = 1);

namespace App\Amp\DomainEventsStore\Domain;


use App\Shared\Domain\ValueObject\IntValueObject;

final class AmpDomainEventStoreAggregateVersion extends IntValueObject
{
}
