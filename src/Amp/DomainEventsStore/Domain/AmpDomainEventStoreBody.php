<?php

declare(strict_types = 1);

namespace App\Amp\DomainEventsStore\Domain;

final class AmpDomainEventStoreBody
{
    private $value;

    public function __construct(array $value)
    {
        $this->value = $value;
    }

    public function value(): array
    {
        return $this->value;
    }
}
