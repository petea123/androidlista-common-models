<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence\Doctrine;

use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class StoryLanguageType extends StringType implements DoctrineCustomType
{

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new StoryLanguage($value);
    }

    /** @var StoryLanguage $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof LocaleValueObject ? $value->value() : (string)$value;
    }

    public static function customTypeName(): string
    {
        return 'story_language';
    }
}
