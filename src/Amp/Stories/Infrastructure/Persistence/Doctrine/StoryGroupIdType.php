<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence\Doctrine;

use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class StoryGroupIdType extends UuidType
{

    protected function typeClassName(): string
    {
        return StoryGroupId::class;
    }

    public static function customTypeName(): string
    {
        return 'story_group_id';
    }
}
