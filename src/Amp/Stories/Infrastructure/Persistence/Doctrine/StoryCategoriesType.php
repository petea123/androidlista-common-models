<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence\Doctrine;

use App\Amp\Stories\Domain\ValueObject\StoryCategory;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use function Lambdish\Phunctional\map;

final class StoryCategoriesType extends JsonType implements DoctrineCustomType
{

    public function getName(): string
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $scalars = parent::convertToPHPValue($value, $platform);
        return map($this->toStoryCategory(), $scalars);
    }

    /** @var StoryCategory[] $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue(map($this->values(), $value), $platform);
    }

    public static function customTypeName(): string
    {
        return 'story_categories';
    }

    private function values()
    {
        return static function (StoryCategory $category) {
            return $category->value();
        };
    }

    private function toStoryCategory()
    {
        return static function (int $value) {
            return new StoryCategory($value);
        };
    }

}
