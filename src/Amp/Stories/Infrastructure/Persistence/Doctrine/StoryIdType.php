<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence\Doctrine;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class StoryIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'story_id';
    }

    protected function typeClassName(): string
    {
        return StoryId::class;
    }
}
