<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence\Doctrine;

use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

final class StoryUserType extends JsonType implements DoctrineCustomType
{

    public static function customTypeName(): string
    {
        return 'story_user';
    }

    public function getName(): string
    {
        return self::customTypeName();
    }

    /** @var StoryUser $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue($value->toValues(), $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $scalars = parent::convertToPHPValue($value, $platform);

        return StoryUser::fromValues($scalars);
    }
}
