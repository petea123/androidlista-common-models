<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Redis\RedisRepository;
use App\Shared\Infrastructure\Redis\RedisClient;

final class RedisCacheStoryRepository extends RedisRepository implements StoryRepositoryInterface
{

    /** @var MySqlStoryRepository */
    private $repository;

    public function __construct(RedisClient $client, MySqlStoryRepository $repository)
    {
        parent::__construct($client);
        $this->repository = $repository;
    }

    protected function aggregateName(): string
    {
        return 'amp_stories';
    }

    public function save(Story $story): void
    {
        $this->repository->save($story);

        $this->persist($story->id()->value(), $story->toPrimitives());
    }

    public function addToRank(Story $story): void
    {
        $this->addToOrderedList($story->language()->value(), $story->id()->value(), $story->updatedAt()->timestamp());
    }

    public function removeInRank(Story $story): void
    {
        $this->removeInOrderedList($story->language()->value(), $story->id()->value());
    }

    public function addViewsRank(Story $story): void
    {
        $this->addToOrderedList($story->language()->value().':views', $story->id()->value(), $story->views()->value());
    }

    public function removeViewsRank(Story $story): void
    {
        $this->removeInOrderedList($story->language()->value().':views', $story->id()->value());
    }

    public function search(StoryId $id): ?Story
    {
        return $this->repository->search($id);
    }

    public function delete(Story $story): void
    {
        $this->repository->delete($story);
        $this->remove($story->id()->value());
    }

    public function searchAll(): array
    {
        return $this->repository->searchAll();
    }

    public function searchAllValidated(): array
    {
        return $this->repository->searchAllValidated();
    }

    public function searchByGroupId(StoryGroupId $groupId): array
    {
        return $this->repository->searchByGroupId($groupId);
    }

    public function searchAllByLanguage(StoryLanguage $language): array
    {
        return $this->repository->searchAllByLanguage($language);
    }

    public function addTemplateList(Story $story): void
    {
        $template = strtolower($story::template());
        $this->addToOrderedList(
            "{$story->language()->value()}:{$template}",
            $story->id()->value(),
            $story->updatedAt()->timestamp()
        );
    }

    public function matching(Criteria $criteria): array
    {
        return $this->repository->matching($criteria);
    }
}
