<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Backoffice\Stories\Domain\BackofficeStory;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlStoryRepository extends DoctrineRepository implements StoryRepositoryInterface
{
    public function save(Story $story): void
    {
        $this->persist($story);
        $this->entityManager()->flush($story);
    }

    public function search(StoryId $id): ?Story
    {
        return $this->repository(Story::class)->find($id);
    }

    public function delete(Story $story): void
    {
        $this->remove($story);
        $this->entityManager()->flush();
    }

    public function searchAll(): array
    {
        return $this->repository(Story::class)->findAll();
    }

    public function searchAllValidated(): array
    {
        return $this->repository(Story::class)->findBy(['validated' => true]);
    }

    public function searchByGroupId(StoryGroupId $groupId): array
    {
        return $this->repository(Story::class)->findBy(['groupId' => $groupId]);
    }

    public function searchAllByLanguage(StoryLanguage $language): array
    {
        return $this->repository(Story::class)->findBy(['language' => $language], ['createdAt.value' => 'DESC']);
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        return $this->repository(Story::class)->matching($doctrineCriteria)->toArray();
    }
}
