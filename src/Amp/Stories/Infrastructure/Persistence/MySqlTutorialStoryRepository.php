<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Tutorial\TutorialStory;
use App\Amp\Stories\Domain\Tutorial\TutorialStoryRepository;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlTutorialStoryRepository extends DoctrineRepository implements TutorialStoryRepository
{
    public function save(TutorialStory $story): void
    {
        $this->persist($story);
        $this->entityManager()->flush($story);
    }

    public function search(StoryId $id): ?TutorialStory
    {
        return $this->repository(TutorialStory::class)->find($id);
    }

    public function delete(TutorialStory $story): void
    {
        $this->remove($story);
        $this->entityManager()->flush();
    }

    public function searchAll(): array
    {
        return $this->repository(TutorialStory::class)->findAll();
    }

    public function searchAllValidated(): array
    {
        return $this->repository(TutorialStory::class)->findBy(['validated' => true]);
    }

    public function searchByGroupId(StoryGroupId $groupId): array
    {
        return $this->repository(TutorialStory::class)->findBy(['groupId' => $groupId]);
    }
}
