<?php

declare(strict_types=1);

namespace App\Amp\Stories\Infrastructure\Persistence;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Tutorial\TutorialStory;
use App\Amp\Stories\Domain\Tutorial\TutorialStoryRepository;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Shared\Infrastructure\Persistence\Redis\RedisRepository;
use App\Shared\Infrastructure\Redis\RedisClient;

final class RedisCacheTutorialStoryRepository extends RedisRepository implements TutorialStoryRepository
{

    /** @var MySqlTutorialStoryRepository */
    private $repository;

    public function __construct(RedisClient $client, MySqlTutorialStoryRepository $repository)
    {
        parent::__construct($client);
        $this->repository = $repository;
    }

    protected function aggregateName(): string
    {
        return 'amp_stories';
    }

    public function save(TutorialStory $story): void
    {
        $this->repository->save($story);

        $this->persist($story->id()->value(), $story->toPrimitives());
    }

    public function search(StoryId $id): ?TutorialStory
    {
        return $this->repository->search($id);
    }

    public function delete(TutorialStory $story): void
    {
        $this->repository->delete($story);
        $this->remove($story->id()->value());
    }

    public function searchAll(): array
    {
        return $this->repository->searchAll();
    }

    public function searchAllValidated(): array
    {
        return $this->repository->searchAllValidated();
    }

    public function searchByGroupId(StoryGroupId $groupId): array
    {
        return $this->repository->searchByGroupId($groupId);
    }

    public function addTemplateList(TutorialStory $story): void
    {
        $template = strtolower($story::template()->value());
        $this->addToOrderedList(
            "{$story->language()->value()}:{$template}",
            $story->id()->value(),
            $story->updatedAt()->timestamp()
        );
    }
}
