<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Event\StoryCategoryWasAdded;
use App\Amp\Stories\Domain\Event\StoryCategoryWasRemoved;
use App\Amp\Stories\Domain\Event\StoryDescriptionWasChanged;
use App\Amp\Stories\Domain\Event\StoryFeaturedImageWasChanged;
use App\Amp\Stories\Domain\Event\StoryPageWasChanged;
use App\Amp\Stories\Domain\Event\StoryTitleWasChanged;
use App\Amp\Stories\Domain\Event\StoryWasDeleted;
use App\Amp\Stories\Domain\Event\StoryWasScheduledForTranslate;
use App\Amp\Stories\Domain\ValueObject\StoryCreatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryLikes;
use App\Amp\Stories\Domain\ValueObject\StoryUpdatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryViews;
use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Amp\Stories\Domain\Event\StoryWasCreated;
use App\Amp\Stories\Domain\Event\StoryWasInvalidated;
use App\Amp\Stories\Domain\Event\StoryWasValidated;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryCategory;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Domain\ValueObject\AggregateVersion;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\map;
use function Lambdish\Phunctional\search;

class Story extends AggregateRoot
{

    /** @var StoryId */
    protected $id;

    /** @var StoryGroupId */
    protected $groupId;

    /** @var StoryCategory[] */
    private $categories;

    /** @var StoryUser */
    protected $user;

    /** @var StoryLanguage */
    protected $language;

    /** @var bool */
    protected $validated;

    /** @var StoryTitle */
    protected $title;

    /** @var StoryDescription */
    protected $description;

    /** @var StoryPage[] */
    private $pages;

    /** @var StoryFeaturedImage */
    protected $featuredImage;

    /** @var AggregateVersion */
    private $aggregateVersion;
    /** @var StoryViews */
    protected $views;
    /** @var StoryLikes */
    protected $likes;
    /** @var StoryCreatedAt */
    protected $createdAt;
    /** @var StoryUpdatedAt */
    protected $updatedAt;

    /**
     * Story constructor.
     * @param StoryId $id
     * @param StoryGroupId $groupId
     * @param StoryCategory[] $categories
     * @param StoryUser $user
     * @param StoryLanguage $language
     * @param bool $validated
     * @param StoryTitle $title
     * @param StoryDescription $description
     * @param StoryFeaturedImage $featuredImage
     * @param StoryViews $views
     * @param StoryLikes $likes
     * @param StoryCreatedAt $createdAt
     * @param StoryUpdatedAt $updatedAt
     * @param AggregateVersion $aggregateVersion
     * @param StoryPage[] $pages
     */
    public function __construct(
        StoryId $id,
        StoryGroupId $groupId,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        bool $validated,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryViews $views,
        StoryLikes $likes,
        StoryCreatedAt $createdAt,
        StoryUpdatedAt $updatedAt,
        AggregateVersion $aggregateVersion,
        StoryPage ...$pages
    ) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->categories = $categories;
        $this->user = $user;
        $this->language = $language;
        $this->validated = $validated;
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
        $this->featuredImage = $featuredImage;
        $this->aggregateVersion = $aggregateVersion;
        $this->views = $views;
        $this->likes = $likes;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }


    public static function create(
        StoryId $id,
        StoryGroupId $groupId,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryPage ...$pages
    ) {

        $story = new self(
            $id,
            $groupId,
            $categories,
            $user,
            $language,
            false,
            $title,
            $description,
            $featuredImage,
            new StoryViews(0),
            new StoryLikes(0),
            StoryCreatedAt::now(),
            StoryUpdatedAt::now(),
            AggregateVersion::new(),
            ...$pages
        );

        $story->record(new StoryWasCreated(
            $id->value(),
            $story->groupId()->value(),
            $story->categoriesToValues(),
            $story->user()->toValues(),
            $story->language()->value(),
            $story->title()->value(),
            $story->description()->value(),
            $story->featuredImage()->value(),
            $story->views()->value(),
            $story->likes()->value(),
            $story->createdAt()->value(),
            $story->pagesToValues(),
            $story->aggregateVersion()->value()
        ));

        return $story;
    }

    /**
     * @return StoryId
     */
    public function id(): StoryId
    {
        return $this->id;
    }

    /**
     * @return StoryGroupId
     */
    public function groupId(): StoryGroupId
    {
        return $this->groupId;
    }

    /**
     * @return StoryCategory[]
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return StoryUser
     */
    public function user(): StoryUser
    {
        return $this->user;
    }

    /**
     * @return StoryLanguage
     */
    public function language(): StoryLanguage
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function validated(): bool
    {
        return $this->validated;
    }

    /**
     * @return StoryTitle
     */
    public function title(): StoryTitle
    {
        return $this->title;
    }

    /**
     * @return StoryDescription
     */
    public function description(): StoryDescription
    {
        return $this->description;
    }

    /**
     * @return StoryPage[]
     */
    public function pages(): array
    {
        return $this->pages;
    }

    public function totalPages(): int
    {
        return count($this->pages);
    }

    /**
     * @return StoryFeaturedImage
     */
    public function featuredImage(): StoryFeaturedImage
    {
        return $this->featuredImage;
    }

    /**
     * @return StoryViews
     */
    public function views(): StoryViews
    {
        return $this->views;
    }

    /**
     * @return StoryLikes
     */
    public function likes(): StoryLikes
    {
        return $this->likes;
    }

    /**
     * @return StoryCreatedAt
     */
    public function createdAt(): StoryCreatedAt
    {
        return $this->createdAt;
    }

    /**
     * @return StoryUpdatedAt
     */
    public function updatedAt(): StoryUpdatedAt
    {
        return $this->updatedAt;
    }

    /**
     * @return AggregateVersion
     */
    public function aggregateVersion(): AggregateVersion
    {
        return $this->aggregateVersion;
    }

    public function toPrimitives(): array
    {
        return [
            'id' => $this->id->value(),
            'groupId' => $this->groupId->value(),
            'categories' => $this->categoriesToValues(),
            'user' => $this->user->toValues(),
            'language' => $this->language->value(),
            'validated' => $this->validated,
            'title' => $this->title->value(),
            'description' => $this->description->value(),
            'featuredImage' => $this->featuredImage->value(),
            'views' => $this->views->value(),
            'likes' => $this->likes->value(),
            'createdAt' => $this->createdAt->timestamp(),
            'updatedAt' => $this->updatedAt->timestamp(),
            'pages' => $this->pagesToValues(),
            'aggregateVersion' => $this->aggregateVersion()->value(),
            'template' => self::template()->value()
        ];
    }

    public function categoriesToValues(): array
    {
        return map(self::categoryToValues(), $this->categories());
    }

    public static function categoryToValues(): callable
    {
        return static function (StoryCategory $category): int {
            return $category->value();
        };
    }

    public static function categoryFromValue(): callable
    {
        return static function (int $value): StoryCategory {
            return new StoryCategory($value);
        };
    }

    public function pagesToValues(): array
    {
        return map(Story::pageToValues(), $this->pages());
    }

    public static function pageFromValues(): callable
    {
        return static function (array $values): StoryPage {
            return StoryPage::fromValues($values);
        };
    }

    public static function pageToValues(): callable
    {
        return static function (StoryPage $page): array {
            return $page->toValues();
        };
    }

    public function addCategory(StoryCategory $category)
    {
        $this->categories[] = $category;
        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryCategoryWasAdded(
            $this->id->value(),
            $category->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function removeCategory(StoryCategory $category)
    {
        $this->categories = array_values(array_filter($this->categories, function ($other) use ($category) {
            return !$this->categoryComparator($other)($category);
        }));

        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryCategoryWasRemoved(
            $this->id->value(),
            $category->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function hasCategory(StoryCategory $category): bool
    {
        return null !== search($this->categoryComparator($category), $this->categories());
    }

    private function categoryComparator(StoryCategory $category): callable
    {
        return static function (StoryCategory $other) use ($category) {
            return $category->equalsTo($other);
        };
    }

    public function changeTitle(StoryTitle $title)
    {
        $this->title = $title;
        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryTitleWasChanged(
            $this->id->value(),
            $title->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function changeDescription(StoryDescription $description)
    {
        $this->description = $description;
        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryDescriptionWasChanged(
            $this->id->value(),
            $description->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function changePages(StoryPage ...$pages)
    {
        $this->pages = $pages;
        $this->updatedAt = StoryUpdatedAt::now();

        foreach ($pages as $page) {
            $this->aggregateVersion = $this->aggregateVersion()->increment();

            $this->record(new StoryPageWasChanged(
                $this->id->value(),
                $page->toValues(),
                $this->aggregateVersion->value()
            ));
        }
    }

    public function validate()
    {
        $this->validated = true;
        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryWasValidated(
            $this->id()->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function invalidate()
    {
        $this->validated = false;
        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryWasInvalidated(
            $this->id()->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function remove(): void
    {
        $this->aggregateVersion = $this->aggregateVersion()->increment();

        $this->record(new StoryWasDeleted(
            $this->id->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function changeFeaturedImage(StoryFeaturedImage $featuredImage)
    {
        $this->featuredImage = $featuredImage;
        $this->aggregateVersion = $this->aggregateVersion()->increment();
        $this->updatedAt = StoryUpdatedAt::now();
        $this->record(new StoryFeaturedImageWasChanged(
            $this->id->value(),
            $featuredImage->value(),
            $this->aggregateVersion->value()
        ));
    }

    public function incrementViews(): void
    {
        $this->views = $this->views->increment();
        $this->updatedAt = StoryUpdatedAt::now();
    }

    public function scheduleForTranslate(): void
    {
        foreach (StoryLanguage::forAdapt() as $value) {
            if (!$this->language->equals(StoryLanguage::fromString($value))) {
                $this->record(new StoryWasScheduledForTranslate(
                    $this->id->value(),
                    $this->groupId()->value(),
                    $this->categoriesToValues(),
                    $this->user()->toValues(),
                    $this->language()->value(),
                    $value,
                    $this->title()->value(),
                    $this->description()->value(),
                    $this->featuredImage()->value(),
                    $this->pagesToValues(),
                    $this->aggregateVersion()->value()
                ));
            }
        }
    }

    public static function template(): StoryTemplate
    {
        return StoryTemplate::games();
    }
}
