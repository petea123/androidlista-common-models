<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Service;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Exception\StoryNotFound;
use App\Amp\Stories\Domain\Story;

final class StoryFinder
{

    /** @var StoryRepositoryInterface */
    private $repository;

    public function __construct(StoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search(StoryId $id): Story
    {
        $story = $this->repository->search($id);
        $this->guard($id, $story);

        return $story;
    }

    private function guard(StoryId $id, Story $story = null): void
    {
        if (null === $story) {
            throw new StoryNotFound($id);
        }
    }
}
