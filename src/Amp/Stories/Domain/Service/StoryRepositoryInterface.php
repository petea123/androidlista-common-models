<?php


namespace App\Amp\Stories\Domain\Service;


use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Shared\Domain\Criteria\Criteria;

interface StoryRepositoryInterface
{
    public function save(Story $story): void;

    public function search(StoryId $id): ?Story;

    public function searchByGroupId(StoryGroupId $groupId): array;

    public function searchAll(): array;
    public function searchAllByLanguage(StoryLanguage $language): array;
    public function searchAllValidated(): array;
    public function matching(Criteria $criteria): array;
    public function delete(Story $story): void;
}
