<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Exception;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\DomainError;

final class StoryNotFound extends DomainError
{

    private $id;

    public function __construct(StoryId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'story_not_found';
    }

    protected function errorMessage(): string
    {
        return sprintf('The story <%s> has not been found', $this->id->value());
    }
}
