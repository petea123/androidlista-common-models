<?php

declare(strict_types=1);


namespace App\Amp\Stories\Domain\Exception;

use App\Al\StoryVideos\Domain\ValueObject\StoryVideoPath;
use App\Shared\Domain\DomainError;

final class StoryVideoNotUploaded extends DomainError
{

    public function __construct()
    {

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'story_video_not_uploaded';
    }

    protected function errorMessage(): string
    {
        return sprintf('The video has not been uploaded');
    }
}
