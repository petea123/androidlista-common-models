<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject;

use App\Shared\Domain\ValueObject\Uuid;

final class StoryGroupId extends Uuid
{

}
