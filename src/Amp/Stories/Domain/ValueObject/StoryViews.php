<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject;

use App\Shared\Domain\ValueObject\IntValueObject;

final class StoryViews extends IntValueObject
{

    public function increment()
    {
        return new self($this->value + 1);
    }
}
