<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject;

use App\Shared\Domain\ValueObject\Enum;
use DomainException;
/**
 * @method static StoryTemplate games()
 * @method static StoryTemplate tutorials()
 */
final class StoryTemplate extends Enum
{
    public const APP = 'APP';
    public const GAMES = 'GAMES';
    public const TUTORIALS = 'TUTORIALS';

    protected function throwExceptionForInvalidValue($value)
    {
        throw new DomainException('Invalid Template '.$value);
    }
}
