<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject;

use App\Shared\Domain\ValueObject\StringValueObject;
use DomainException;

final class StoryDescription extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->guardLength($value);

        parent::__construct($value);
    }

    private function guardLength(string $value)
    {
        $length = mb_strlen($value, 'UTF-8');

        if (4 > $length) {
            throw new DomainException('Story description is too short. It should have 4 characters or more.');
        }

        if (125 < $length) {
            throw new DomainException('Story description is too long. It should have 125 characters or less.');
        }
    }
}
