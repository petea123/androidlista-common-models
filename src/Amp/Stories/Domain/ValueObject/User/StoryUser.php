<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\User;

use App\Shared\Domain\Users\UserId;

final class StoryUser
{

    /** @var UserId */
    private $id;
    /** @var StoryUserUsername */
    private $username;
    /** @var StoryUserAvatar */
    private $avatar;

    public function __construct(UserId $id, StoryUserUsername $username, StoryUserAvatar $avatar)
    {
        $this->userId = $id;
        $this->username = $username;
        $this->avatar = $avatar;
    }

    public function toValues(): array
    {
        return [
            'id' => $this->userId->value(),
            'username' => $this->username->value(),
            'avatar' => $this->avatar->value()
        ];
    }

    public static function fromValues(array $user)
    {
        return new self(
            new UserId($user['id']),
            new StoryUserUsername($user['username']),
            new StoryUserAvatar($user['avatar'])
        );
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return StoryUserUsername
     */
    public function username(): StoryUserUsername
    {
        return $this->username;
    }

    /**
     * @return StoryUserAvatar
     */
    public function avatar(): StoryUserAvatar
    {
        return $this->avatar;
    }
}
