<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\User;

use App\Shared\Domain\ValueObject\StringValueObject;

final class StoryUserUsername extends StringValueObject
{

}
