<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject;

use App\Shared\Domain\ValueObject\DateValueObject;

final class StoryCreatedAt extends DateValueObject
{

}
