<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\Page;

use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class StoryPageVideo extends StringValueObject
{
    private const VALID_EXTENSIONS = ['mp4'];

    private const CDN_PATH = 'https://media.cdnandroid.com/story_videos';

    public function __construct(string $value)
    {
        $this->guardExtension($value);
        parent::__construct($value);
    }

    private function guardExtension(string $path)
    {
        if (!empty($path) && !in_array(pathinfo($path)['extension'], self::VALID_EXTENSIONS)) {
            throw new InvalidArgumentException('Invalid extension');
        }
    }
}
