<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\Page;

use App\Al\Shared\Domain\Items\ItemId;

final class StoryPage
{

    /** @var StoryPageTitle */
    private $title;
    /** @var StoryPageDescription */
    private $description;

    /** @var StoryPageCta */
    private $cta;
    /** @var StoryPageImage */
    private $image;
    /** @var StoryPageVideo */
    private $video;

    /** @var ?ItemId */
    private $itemId;

    public function __construct(
        StoryPageImage $image,
        StoryPageVideo $video,
        StoryPageTitle $title,
        StoryPageDescription $description,
        StoryPageCta $cta,
        ItemId $itemId = null
    ) {

        $this->title = $title;
        $this->description = $description;
        $this->cta = $cta;
        $this->image = $image;
        $this->video = $video;
        $this->itemId = $itemId;
    }

    public static function create(
        StoryPageImage $image,
        StoryPageVideo $video,
        StoryPageTitle $title,
        StoryPageDescription $description,
        StoryPageCta $cta,
        ItemId $itemId = null
    ) {
        return new self($image, $video, $title, $description, $cta, $itemId);
    }

    /**
     * @return StoryPageTitle
     */
    public function title(): StoryPageTitle
    {
        return $this->title;
    }

    /**
     * @return StoryPageDescription
     */
    public function description(): StoryPageDescription
    {
        return $this->description;
    }

    /**
     * @return StoryPageCta
     */
    public function cta(): StoryPageCta
    {
        return $this->cta;
    }

    /**
     * @return StoryPageImage
     */
    public function image(): StoryPageImage
    {
        return $this->image;
    }

    /**
     * @return StoryPageVideo
     */
    public function video(): StoryPageVideo
    {
        return $this->video;
    }

    /**
     * @return ItemId|null
     */
    public function itemId(): ?ItemId
    {
        return $this->itemId;
    }

    public function toValues(): array
    {
        return [
            'title'     => $this->title()->value(),
            'description' => $this->description()->value(),
            'cta' => $this->cta()->toValues(),
            'image' => $this->image()->value(),
            'video' => $this->video()->value(),
            'itemId' => $this->itemId() !== null ? $this->itemId()->value() : null
        ];
    }

    public static function fromValues(array $values): self
    {
        return new self(
            new StoryPageImage($values['image']),
            new StoryPageVideo($values['video'] ?? ''),
            new StoryPageTitle($values['title']),
            new StoryPageDescription($values['description']),
            new StoryPageCta($values['cta']['value'] ?? false, $values['cta']['href'] ?? ''),
            isset($values['itemId']) ? new ItemId($values['itemId']) : null
        );
    }

    public function equals(StoryPage $page): bool
    {
        return $this->image->equals($page->image()) && $this->video->equals($page->video()) && $this->title->equals($page->title())
            && $this->description->equals($page->description()) && $this->cta->equals($page->cta()) && $this->itemId->equalsTo($page->itemId());
    }
}
