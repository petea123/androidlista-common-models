<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\Page;

use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class StoryPageImage extends StringValueObject
{
    private const VALID_EXTENSIONS = ['jpg', 'jpeg', 'png'];

    public function __construct(string $value)
    {
        $this->guardExtension($value);
        parent::__construct($value);
    }

    private function guardExtension(string $path)
    {
        if (empty($path)) {
            throw new InvalidArgumentException('Empty path to image');
        }

        if (!in_array(pathinfo($path)['extension'], self::VALID_EXTENSIONS)) {
            throw new InvalidArgumentException('Invalid extension');
        }
    }
}
