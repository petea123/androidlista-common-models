<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\Page;

use App\Shared\Domain\ValueObject\StringValueObject;
use DomainException;

final class StoryPageTitle extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->guardLength($value);

        parent::__construct($value);
    }

    private function guardLength(string $value)
    {
        $length = mb_strlen($value, 'UTF-8');

        if (4 > $length) {
            throw new DomainException('Story page title is too short. It should have 4 characters or more.');
        }

        if (50 < $length) {
            throw new DomainException('Story page title is too long. It should have 50 characters or less.');
        }
    }
}
