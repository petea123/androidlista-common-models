<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\ValueObject\Page;

use DomainException;

final class StoryPageCta
{

    private $value;
    private $href;

    public function __construct(bool $value, string $href = '')
    {
        if (true === $value) {
            $this->guardHref($href);
        }

        $this->value = $value;
        $this->href = $href;
    }

    private function guardHref(string $href)
    {
        if (false === filter_var($href, FILTER_VALIDATE_URL)) {
            throw new DomainException(sprintf('Url %s not valid', $href));
        }
    }

    /**
     * @return bool
     */
    public function value(): bool
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    public function toValues(): array
    {
        return [
            'value' => $this->value,
            'href' => $this->href
        ];
    }
    
    public function equals(StoryPageCta $cta): bool 
    {
        return $this->value === $cta->value && $this->href === $cta->href;
    }
}
