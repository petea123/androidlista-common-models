<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Tutorial;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Event\TutorialStoryWasCreated;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryCreatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryLikes;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Amp\Stories\Domain\ValueObject\StoryUpdatedAt;
use App\Amp\Stories\Domain\ValueObject\StoryViews;
use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Shared\Domain\ValueObject\AggregateVersion;

final class TutorialStory extends Story
{
    /**
     * @var string
     */
    private $postHref;

    public function __construct(
        StoryId $id,
        StoryGroupId $groupId,
        string $postHref,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        bool $validated,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryViews $views,
        StoryLikes $likes,
        StoryCreatedAt $createdAt,
        StoryUpdatedAt $updatedAt,
        AggregateVersion $aggregateVersion,
        StoryPage ...$pages
    ) {
        parent::__construct(
            $id,
            $groupId,
            $categories,
            $user,
            $language,
            $validated,
            $title,
            $description,
            $featuredImage,
            $views,
            $likes,
            $createdAt,
            $updatedAt,
            $aggregateVersion,
            ...$pages
        );
        $this->postHref = $postHref;
    }

    public static function createTutorial(
        StoryId $id,
        StoryGroupId $groupId,
        string $postHref,
        array $categories,
        StoryUser $user,
        StoryLanguage $language,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryPage ...$pages
    ): self {
        $story = new self(
            $id,
            $groupId,
            $postHref,
            $categories,
            $user,
            $language,
            false,
            $title,
            $description,
            $featuredImage,
            new StoryViews(0),
            new StoryLikes(0),
            StoryCreatedAt::now(),
            StoryUpdatedAt::now(),
            AggregateVersion::new(),
            ...$pages
        );

        $story->record(new TutorialStoryWasCreated(
            $id->value(),
            $story->groupId()->value(),
            $story->categoriesToValues(),
            $story->user()->toValues(),
            $story->language()->value(),
            $story->title()->value(),
            $story->description()->value(),
            $story->featuredImage()->value(),
            $story->views()->value(),
            $story->likes()->value(),
            $story->createdAt()->value(),
            $story->pagesToValues(),
            $story->aggregateVersion()->value()
        ));

        return $story;
    }

    public static function template(): StoryTemplate
    {
        return StoryTemplate::tutorials();
    }

    public function toPrimitives(): array
    {
        return [
            'id' => $this->id->value(),
            'groupId' => $this->groupId->value(),
            'categories' => $this->categoriesToValues(),
            'user' => $this->user->toValues(),
            'language' => $this->language->value(),
            'validated' => $this->validated,
            'title' => $this->title->value(),
            'description' => $this->description->value(),
            'featuredImage' => $this->featuredImage->value(),
            'views' => $this->views->value(),
            'likes' => $this->likes->value(),
            'createdAt' => $this->createdAt->timestamp(),
            'updatedAt' => $this->updatedAt->timestamp(),
            'pages' => $this->pagesToValues(),
            'aggregateVersion' => $this->aggregateVersion()->value(),
            'postHref' => $this->postHref,
            'template' => self::template()->value()
        ];
    }

    /**
     * @return string
     */
    public function postHref(): string
    {
        return $this->postHref ?? '';
    }

    public function changePostHref($postHref): void
    {
        $this->postHref = $postHref;
    }
}
