<?php


namespace App\Amp\Stories\Domain\Tutorial;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;

interface TutorialStoryRepository
{
    public function save(TutorialStory $story): void;
    public function search(StoryId $id): ?TutorialStory;
    public function searchByGroupId(StoryGroupId $groupId): array;
    public function searchAll(): array;
    public function searchAllValidated(): array;
    public function delete(TutorialStory $story): void;
}
