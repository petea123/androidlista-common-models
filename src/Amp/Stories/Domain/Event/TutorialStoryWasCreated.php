<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Utils;
use DateTimeInterface;

final class TutorialStoryWasCreated extends DomainEvent
{
    /** @var string */
    private $groupId;

    /** @var array */
    private $categories;
    /** @var array */
    private $user;
    /** @var string */
    private $language;
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var array */
    private $pages;
    /** @var string */
    private $featuredImage;
    /** @var int */
    private $views;
    /** @var int */
    private $likes;
    /** @var int */
    private $createdAt;

    public function __construct(
        string $aggregateId,
        string $groupId,
        array $categories,
        array $user,
        string $language,
        string $title,
        string $description,
        string $featuredImage,
        int $views,
        int $likes,
        DateTimeInterface $createdAt,
        array $pages,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->groupId = $groupId;
        $this->categories = $categories;
        $this->user = $user;
        $this->language = $language;
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
        $this->featuredImage = $featuredImage;
        $this->views = $views;
        $this->likes = $likes;
        $this->createdAt = $createdAt;
    }

    public static function eventName(): string
    {
        return 'StoryWasCreated';
    }

    public function toPrimitives(): array
    {
        return [
            'groupId' => $this->groupId,
            'categories' => $this->categories,
            'user' => $this->user,
            'language' => $this->language,
            'title' => $this->title,
            'description' => $this->description,
            'featuredImage' => $this->featuredImage,
            'views' => $this->views,
            'likes' => $this->likes,
            'createdAt' => $this->createdAt->getTimestamp(),
            'pages' => $this->pages,
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['groupId'], $body['categories'], $body['user'],
            $body['language'], $body['title'], $body['description'], $body['featuredImage'], $body['views'],
            $body['likes'], Utils::timestampToDate($body['createdAt']), $body['pages'], $aggregateVersion, $eventId,
            $occurredOn);
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return array
     */
    public function user(): array
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function pages(): array
    {
        return $this->pages;
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage;
    }

    public static function stream(): string
    {
        return Story::class;
    }

    /**
     * @return int
     */
    public function views(): int
    {
        return $this->views;
    }

    /**
     * @return int
     */
    public function likes(): int
    {
        return $this->likes;
    }

    /**
     * @return DateTimeInterface
     */
    public function createdAt(): DateTimeInterface
    {
        return $this->createdAt;
    }
}
