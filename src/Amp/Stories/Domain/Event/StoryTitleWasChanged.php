<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryTitleWasChanged extends DomainEvent
{

    /** @var string */
    private $title;

    public function __construct(string $aggregateId, string $title, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->title = $title;
    }

    public static function eventName(): string
    {
        return 'StoryTitleWasChanged';
    }

    public function toPrimitives(): array
    {
        return [
            'title' => $this->title
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['title'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
