<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryFeaturedImageWasChanged extends DomainEvent
{

    /** @var string */
    private $featuredImage;

    public function __construct(string $aggregateId, string $featuredImage, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->featuredImage = $featuredImage;
    }

    public static function eventName(): string
    {
        return 'StoryFeaturedImageWasChanged';
    }

    public function toPrimitives(): array
    {
        return [
            'featuredImage' => $this->featuredImage
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['featuredImage'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage;
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
