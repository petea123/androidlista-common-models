<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryWasValidated extends DomainEvent
{

    public static function eventName(): string
    {
        return 'StoryWasValidated';
    }

    public function toPrimitives(): array
    {
        return [];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
