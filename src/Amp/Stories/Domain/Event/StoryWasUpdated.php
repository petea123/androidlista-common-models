<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryWasUpdated extends DomainEvent
{

    /** @var string */
    private $template;
    /** @var array */
    private $categories;
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var array */
    private $pages;

    public function __construct(string $aggregateId, string $template, array $categories, string $title, string $description, array $pages, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->template = $template;
        $this->categories = $categories;
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
    }

    public static function eventName(): string
    {
        return 'StoryWasUpdated';
    }

    public function toPrimitives(): array
    {
        return [
            'template' => $this->template,
            'categories' => $this->categories,
            'title' => $this->title,
            'description' => $this->description,
            'pages' => $this->pages
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['template'], $body['categories'], $body['title'], $body['description'], $body['pages'], $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
