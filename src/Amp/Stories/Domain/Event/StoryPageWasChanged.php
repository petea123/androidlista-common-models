<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryPageWasChanged extends DomainEvent
{

    /** @var array */
    private $page;

    public function __construct(string $aggregateId, array $page, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->page = $page;
    }

    public static function eventName(): string
    {
        return 'StoryPageWasChanged';
    }

    public function toPrimitives(): array
    {
        return [
            'page' => $this->page
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['page'], $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
