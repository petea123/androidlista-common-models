<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryCategoryWasRemoved extends DomainEvent
{

    /** @var int */
    private $category;

    public function __construct(string $aggregateId, int $category, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->category = $category;
    }

    public static function eventName(): string
    {
        return 'StoryCategoryWasRemoved';
    }

    public function toPrimitives(): array
    {
        return [
            'category' => $this->category
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['category'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function category(): int
    {
        return $this->category;
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
