<?php

declare(strict_types=1);

namespace App\Amp\Stories\Domain\Event;

use App\Amp\Stories\Domain\Story;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryDescriptionWasChanged extends DomainEvent
{

    /** @var string */
    private $description;

    public function __construct(string $aggregateId, string $description, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->description = $description;
    }

    public static function eventName(): string
    {
        return 'StoryDescriptionWasChanged';
    }

    public function toPrimitives(): array
    {
        return [
            'description' => $this->description
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['description'], $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return Story::class;
    }
}
