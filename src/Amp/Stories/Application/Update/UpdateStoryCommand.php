<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Update;

use App\Shared\Domain\Bus\Command\Command;

final class UpdateStoryCommand extends Command
{

    /** @var string */
    private $id;

    /** @var array */
    private $categories;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var array */
    private $pages;
    /** @var string */
    private $featuredImage;
    /**
     * @var string
     */
    private $postHref;

    /**
     * CreateStoryCommand constructor.
     * @param string $id
     * @param array $categories
     * @param string $title
     * @param string $description
     * @param string $featuredImage
     * @param array $pages
     */
    public function __construct(
        string $id,
        array $categories,
        string $title,
        string $description,
        string $featuredImage,
        string $postHref,
        array $pages
    ) {
        $this->id = $id;
        $this->categories = $categories;
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
        $this->featuredImage = $featuredImage;
        $this->postHref = $postHref;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function pages(): array
    {
        return $this->pages;
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage;
    }

    /**
     * @return string
     */
    public function postHref(): string
    {
        return $this->postHref;
    }
}
