<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Update;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Service\StoryFinder;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryCategory;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;

final class StoryUpdater
{
    /** @var StoryRepositoryInterface */
    private $repository;

    /** @var DomainEventPublisher */
    private $publisher;
    /** @var StoryFinder */
    private $finder;

    public function __construct(StoryRepositoryInterface $repository, DomainEventPublisher $publisher)
    {
        $this->repository = $repository;
        $this->publisher = $publisher;
        $this->finder = new StoryFinder($repository);
    }

    public function __invoke(
        StoryId $id,
        array $categories,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        string $postHref,
        StoryPage ...$pages
    ) {
        $story = $this->finder->search($id);

        if ($story::template()->equals(StoryTemplate::tutorials())
            && !empty($postHref) && $story->postHref() != $postHref) {
            $story->changePostHref($postHref);
        }

        $this->removeCategories($story, ...$categories);
        $this->addCategories($story, ...$categories);

        if (!$story->title()->equals($title)) {
            $story->changeTitle($title);
        }

        if (!$story->description()->equals($description)) {
            $story->changeDescription($description);
        }

        if (!$story->featuredImage()->equals($featuredImage)) {
            $story->changeFeaturedImage($featuredImage);
        }

        $story->changePages(...$pages);

        $this->repository->save($story);

        $this->publisher->record(...$story->pullDomainEvents());
    }

    private function removeCategories(Story $story, StoryCategory ...$categories)
    {
        $removed = array_udiff($story->categories(), $categories, [$this, 'categoryCompare']);
        foreach ($removed as $category) {
            if ($story->hasCategory($category)) {
                $story->removeCategory($category);
            }
        }
    }

    private function addCategories(Story $story, StoryCategory ...$categories)
    {
        foreach ($categories as $category) {
            if (!$story->hasCategory($category)) {
                $story->addCategory($category);
            }
        }
    }

    private function categoryCompare(StoryCategory $a, StoryCategory $b): int
    {
        if ($a->isBiggerThan($b)) {
            return 1;
        } elseif ($b->isBiggerThan($a)) {
            return -1;
        }

        return 0;
    }
}
