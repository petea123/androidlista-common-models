<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Update;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\map;

final class UpdateStoryCommandHandler implements CommandHandler
{

    /** @var StoryUpdater */
    private $updater;

    public function __construct(StoryUpdater $updater)
    {
        $this->updater = $updater;
    }

    public function __invoke(UpdateStoryCommand $command)
    {
        $id = new StoryId($command->id());
        $categories = map(Story::categoryFromValue(),$command->categories());
        $title = new StoryTitle($command->title());
        $description = new StoryDescription($command->description());
        $featuredImage = new StoryFeaturedImage($command->featuredImage());
        $pages = map(Story::pageFromValues(), $command->pages());

        $this->updater->__invoke(
            $id,
            $categories,
            $title,
            $description,
            $featuredImage,
            $command->postHref(),
            ...$pages
        );
    }
}
