<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application;

use App\Shared\Domain\Bus\Query\Response;

final class StoryResponse implements Response
{

    /** @var string */
    private $id;
    /** @var string */
    private $groupId;
    /** @var array */
    private $categories;
    /** @var array */
    private $user;
    /** @var string */
    private $language;
    /** @var bool */
    private $validated;
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var array[] */
    private $pages;
    /** @var string */
    private $featuredImage;
    /** @var int */
    private $views;
    /** @var int */
    private $likes;
    /** @var int */
    private $createdAt;
    /** @var int */
    private $updatedAt;
    /**
     * @var string
     */
    private $template;
    /**
     * @var string
     */
    private $postHref;

    public function __construct(
        string $id,
        string $groupId,
        string $template,
        array $categories,
        array $user,
        string $language,
        bool $validated,
        string $title,
        string $description,
        string $featuredImage,
        int $views,
        int $likes,
        int $createdAt,
        int $updatedAt,
        string $postHref = '',
        array ...$pages
    ) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->categories = $categories;
        $this->user = $user;
        $this->language = $language;
        $this->validated = $validated;
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
        $this->featuredImage = $featuredImage;
        $this->views = $views;
        $this->likes = $likes;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->template = $template;
        $this->postHref = $postHref;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return array[]
     */
    public function pages(): array
    {
        return $this->pages;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function user(): array
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function validated(): bool
    {
        return $this->validated;
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage;
    }

    public function hasCategory(int $category): bool
    {
        return in_array($category, $this->categories);
    }

    /**
     * @return int
     */
    public function views(): int
    {
        return $this->views;
    }

    /**
     * @return int
     */
    public function likes(): int
    {
        return $this->likes;
    }

    /**
     * @return int
     */
    public function createdAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function updatedAt(): int
    {
        return $this->updatedAt;
    }

    public function totalPages(): int
    {
        return count($this->pages);
    }

    /**
     * @return string
     */
    public function template(): string
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function postHref(): string
    {
        return $this->postHref;
    }
}
