<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\SearchAll;

use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Application\StoriesResponse;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Bus\Query\Response;
use function Lambdish\Phunctional\map;

final class SearchAllStoriesQueryHandler implements QueryHandler
{

    /**
     * @var StoryRepositoryInterface
     */
    private $repository;

    public function __construct(StoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(SearchAllStoriesQuery $query): ?Response
    {
        return new StoriesResponse(
            ...map(
                $this->toResponse(),
                $this->repository->searchAllByLanguage(StoryLanguage::fromString($query->language()))
            )
        );
    }

    private function toResponse(): callable
    {
        return static function (Story $story) {
            return new StoryResponse(
                $story->id()->value(),
                $story->groupId()->value(),
                $story::template()->value(),
                $story->categories(),
                $story->user()->toValues(),
                $story->language()->value(),
                $story->validated(),
                $story->title()->value(),
                $story->description()->value(),
                $story->featuredImage()->value(),
                $story->views()->value(),
                $story->likes()->value(),
                $story->createdAt()->timestamp(),
                $story->updatedAt()->timestamp(),
                $story::template()->equals(StoryTemplate::tutorials()) ? $story->postHref() : '',
                ...$story->pagesToValues()
            );
        };
    }
}
