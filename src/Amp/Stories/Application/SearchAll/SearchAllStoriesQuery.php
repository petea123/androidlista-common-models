<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\SearchAll;

use App\Shared\Domain\Bus\Query\Query;

final class SearchAllStoriesQuery implements Query
{

    /**
     * @var string
     */
    private $language;

    public function __construct(string $language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
