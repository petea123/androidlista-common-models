<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Validate;

use App\Shared\Domain\Bus\Command\Command;

final class ValidateStoryCommand extends Command
{

    /** @var string */
    private $id;
    /** @var bool */
    private $state;

    public function __construct(string $id, bool $state)
    {
        $this->id = $id;
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function state(): bool
    {
        return $this->state;
    }
}
