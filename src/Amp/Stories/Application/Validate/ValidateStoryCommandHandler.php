<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Validate;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class ValidateStoryCommandHandler implements CommandHandler
{

    /** @var StoryValidator */
    private $validator;

    public function __construct(StoryValidator $validator)
    {
        $this->validator = $validator;
    }

    public function __invoke(ValidateStoryCommand $command)
    {
        $id = new StoryId($command->id());

        apply($this->validator, [$id, $command->state()]);
    }

}
