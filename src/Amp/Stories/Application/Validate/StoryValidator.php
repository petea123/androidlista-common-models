<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Validate;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Service\StoryFinder;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;

final class StoryValidator
{

    /** @var StoryRepositoryInterface */
    private $repository;
    /** @var StoryFinder */
    private $finder;
    /** @var DomainEventPublisher */
    private $publisher;

    public function __construct(StoryRepositoryInterface $repository, DomainEventPublisher $publisher)
    {
        $this->repository = $repository;
        $this->finder = new StoryFinder($repository);
        $this->publisher = $publisher;
    }

    public function __invoke(StoryId $id, bool $state)
    {
        $story = $this->finder->search($id);

        $state ? $story->validate() : $story->invalidate();

        $this->repository->save($story);

        if ($story->validated()) {
            $this->scheduleForTranslate($story);
            $this->repository->addToRank($story);
        } else {
            $this->repository->removeInRank($story);
            $this->repository->removeViewsRank($story);
        }

        $this->publisher->record(...$story->pullDomainEvents());
    }

    private function scheduleForTranslate(Story $story): void
    {
        if (count($this->repository->searchByGroupId($story->groupId())) === 1) {
            $story->scheduleForTranslate();
        }
    }
}
