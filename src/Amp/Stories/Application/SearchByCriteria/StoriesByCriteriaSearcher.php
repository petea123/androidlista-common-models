<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\SearchByCriteria;

use App\Amp\Stories\Application\StoriesResponse;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class StoriesByCriteriaSearcher
{

    /** @var StoryRepositoryInterface */
    private $repository;

    public function __construct(StoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search(Filters $filters, Order $order, ?int $limit, ?int $offset): StoriesResponse
    {
        $criteria = new Criteria($filters, $order, $offset, $limit);

        return new StoriesResponse(...map($this->toResponse(), $this->repository->matching($criteria)));
    }

    private function toResponse(): callable
    {
        return static function (Story $story) {
            return new StoryResponse(
                $story->id()->value(),
                $story->groupId()->value(),
                $story::template()->value(),
                $story->categoriesToValues(),
                $story->user()->toValues(),
                $story->language()->value(),
                $story->validated(),
                $story->title()->value(),
                $story->description()->value(),
                $story->featuredImage()->value(),
                $story->views()->value(),
                $story->likes()->value(),
                $story->createdAt()->timestamp(),
                $story->updatedAt()->timestamp(),
                $story::template()->equals(StoryTemplate::tutorials()) ? $story->postHref() : '',
                ...$story->pagesToValues()
            );
        };
    }
}
