<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\SearchByCriteria;

use App\Amp\Stories\Application\StoriesResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchStoriesByCriteriaQueryHandler implements QueryHandler
{
    /** @var StoriesByCriteriaSearcher */
    private $searcher;

    public function __construct(StoriesByCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchStoriesByCriteriaQuery $query): StoriesResponse
    {
        $filters = Filters::fromValues($query->filters());
        $order   = Order::fromValues($query->orderBy(), $query->order());

        return $this->searcher->search($filters, $order, $query->limit(), $query->offset());
    }
}
