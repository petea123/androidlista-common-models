<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Upload\Video;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Exception\StoryVideoNotUploaded;
use App\Shared\Domain\FilesystemInterface;
use App\Shared\Domain\ValueObject\PathValueObject;

final class Uploader
{
    /** @var FilesystemInterface */
    private $filesystem;

    public function __construct(FilesystemInterface $storyVideosStorageMount)
    {
        $this->filesystem = $storyVideosStorageMount;
    }

    public function __invoke(StoryId $id, string $filename, string $path): void
    {
        $resource = new \SplFileInfo($path);

        if (false === $this->filesystem->putStream(PathValueObject::create('s3://'.$filename), $resource)) {
            throw new StoryVideoNotUploaded();
        }
    }
}
