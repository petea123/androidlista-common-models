<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Upload\Video;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class UploadStoryVideoCommandHandler implements CommandHandler
{
    /** @var Uploader */
    private $uploader;

    public function __construct(Uploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function __invoke(UploadStoryVideoCommand $command)
    {
        $id = new StoryId($command->id());

        apply($this->uploader, [$id, $command->filename(), $command->path()]);
    }
}
