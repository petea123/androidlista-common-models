<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Upload\Video;

use App\Shared\Domain\Bus\Command\Command;

final class UploadStoryVideoCommand extends Command
{

    /** @var string */
    private $path;
    /** @var string */
    private $filename;
    /** @var string */
    private $id;

    public function __construct(string $id, string $filename, string $path)
    {
        $this->path = $path;
        $this->filename = $filename;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function path(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function filename(): string
    {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
