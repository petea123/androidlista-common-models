<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Remove;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;

final class StoryRemover
{

    /** @var StoryRepositoryInterface */
    private $repository;

    /** @var DomainEventPublisher */
    private $publisher;

    public function __construct(StoryRepositoryInterface $repository, DomainEventPublisher $publisher)
    {
        $this->repository = $repository;
        $this->publisher = $publisher;
    }

    public function remove(StoryId $id)
    {
        $story = $this->repository->search($id);

        if (null === $story) {
            return;
        }

        $story->remove();

        $this->repository->delete($story);
        $this->repository->removeInRank($story);
        $this->repository->removeViewsRank($story);
        $this->publisher->record(...$story->pullDomainEvents());
    }
}
