<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Remove;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Bus\Command\CommandHandler;

final class RemoveStoryCommandHandler implements CommandHandler
{

    /** @var StoryRemover */
    private $remover;

    public function __construct(StoryRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveStoryCommand $command)
    {
        $this->remover->remove(new StoryId($command->id()));
    }
}
