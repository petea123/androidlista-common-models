<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Find;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Service\StoryFinder;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Bus\Query\Response;
use function Lambdish\Phunctional\map;

final class FindStoryQueryHandler implements QueryHandler
{
    /** @var StoryFinder */
    private $finder;

    public function __construct(StoryFinder $finder)
    {
        $this->finder = $finder;
    }

    public function __invoke(FindStoryQuery $query): ?Response
    {
        $story = $this->finder->search(new StoryId($query->id()));

        return new StoryResponse(
            $story->id()->value(),
            $story->groupId()->value(),
            $story->template()->value(),
            $story->categoriesToValues(),
            $story->user()->toValues(),
            $story->language()->value(),
            $story->validated(),
            $story->title()->value(),
            $story->description()->value(),
            $story->featuredImage()->value(),
            $story->views()->value(),
            $story->likes()->value(),
            $story->createdAt()->timestamp(),
            $story->updatedAt()->timestamp(),
            $story::template()->equals(StoryTemplate::tutorials()) ? $story->postHref() : '',
            ...$story->pagesToValues()
        );
    }
}
