<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Search;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;
use App\Amp\Stories\Domain\ValueObject\StoryTemplate;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Bus\Query\Response;

final class SearchStoryQueryHandler implements QueryHandler
{
    /** @var StoryRepositoryInterface */
    private $repository;

    public function __construct(StoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(SearchStoryQuery $query): ?Response
    {
        $story = $this->repository->search(new StoryId($query->id()));

        if (null === $story) {
            return null;
        }

        return new StoryResponse(
            $story->id()->value(),
            $story->groupId()->value(),
            $story::template()->value(),
            $story->categoriesToValues(),
            $story->user()->toValues(),
            $story->language()->value(),
            $story->validated(),
            $story->title()->value(),
            $story->description()->value(),
            $story->featuredImage()->value(),
            $story->views()->value(),
            $story->likes()->value(),
            $story->createdAt()->timestamp(),
            $story->updatedAt()->timestamp(),
            $story::template()->equals(StoryTemplate::tutorials()) ? $story->postHref() : '',
            ...$story->pagesToValues()
        );
    }
}
