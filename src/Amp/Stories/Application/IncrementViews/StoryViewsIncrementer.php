<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\IncrementViews;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Service\StoryRepositoryInterface;

final class StoryViewsIncrementer
{
    /** @var StoryRepositoryInterface */
    private $repository;

    public function __construct(StoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(StoryId $id)
    {

        $story = $this->repository->search($id);

        if (null === $story) {
            return;
        }

        $story->incrementViews();
        $this->repository->save($story);
    }
}
