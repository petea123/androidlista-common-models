<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\IncrementViews;

use App\Amp\Stories\Domain\Event\StoryWasViewedFromLegacy;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class IncrementViewsOnStoryViewedFromLegacy implements DomainEventSubscriber
{

    /** @var CommandBus */
    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function __invoke(StoryWasViewedFromLegacy $event)
    {
        $this->bus->handle(new StoryViewsIncrementCommand($event->aggregateId()));
    }

    public static function subscribedTo(): array
    {
        return [StoryWasViewedFromLegacy::class];
    }
}
