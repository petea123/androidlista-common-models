<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\IncrementViews;

use App\Shared\Domain\Bus\Command\Command;

final class StoryViewsIncrementCommand extends Command
{

    /** @var string */
    private $storyId;

    public function __construct(string $storyId)
    {
        $this->storyId = $storyId;
    }

    /**
     * @return string
     */
    public function storyId(): string
    {
        return $this->storyId;
    }
}
