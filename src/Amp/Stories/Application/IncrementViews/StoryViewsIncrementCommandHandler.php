<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\IncrementViews;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class StoryViewsIncrementCommandHandler implements CommandHandler
{
    /** @var StoryViewsIncrementer */
    private $incrementer;

    public function __construct(StoryViewsIncrementer $incrementer)
    {
        $this->incrementer = $incrementer;
    }

    public function __invoke(StoryViewsIncrementCommand $command)
    {
        $storyId = new StoryId($command->storyId());

        apply($this->incrementer, [$storyId]);
    }
}
