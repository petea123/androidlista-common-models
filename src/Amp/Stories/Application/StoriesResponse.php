<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application;

use App\Shared\Domain\Bus\Query\Response;

final class StoriesResponse implements Response
{
    private $stories;

    public function __construct(StoryResponse ...$stories)
    {
        $this->stories = $stories;
    }

    /**
     * @return StoryResponse[]
     */
    public function stories(): array
    {
        return $this->stories;
    }
}
