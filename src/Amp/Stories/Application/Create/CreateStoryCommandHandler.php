<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Create;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Shared\Domain\Users\UserId;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\map;

final class CreateStoryCommandHandler implements CommandHandler
{

    /** @var StoryCreator */
    private $creator;

    public function __construct(StoryCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(CreateStoryCommand $command)
    {
        $id = new StoryId($command->id());
        $groupId = new StoryGroupId($command->groupId());
        $categories = map(Story::categoryFromValue(), $command->categories());
        $user = new UserId($command->user());
        $language = StoryLanguage::fromString($command->language());
        $title = new StoryTitle($command->title());
        $description = new StoryDescription($command->description());
        $featuredImage = new StoryFeaturedImage($command->featuredImage());

        $pages = $command->pages();
        $pages[0]['cta']['value'] = false;

        $pages = map(Story::pageFromValues(), $pages);

        $this->creator->__invoke(
            $id,
            $groupId,
            $categories,
            $user,
            $language,
            $title,
            $description,
            $featuredImage,
            ...$pages
        );
    }
}
