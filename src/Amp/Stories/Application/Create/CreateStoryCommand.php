<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Create;

use App\Shared\Domain\Bus\Command\Command;

class CreateStoryCommand extends Command
{

    /** @var string */
    private $id;

    /** @var string */
    private $groupId;

    /** @var array */
    private $categories;

    /** @var int */
    private $user;

    /** @var string */
    private $language;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var array */
    private $pages;
    /** @var string */
    private $featuredImage;

    /**
     * CreateStoryCommand constructor.
     * @param string $id
     * @param string $groupId
     * @param array $categories
     * @param int $user
     * @param string $language
     * @param string $title
     * @param string $description
     * @param string $featuredImage
     * @param array $pages
     */
    public function __construct(
        string $id,
        string $groupId,
        array $categories,
        int $user,
        string $language,
        string $title,
        string $description,
        string $featuredImage,
        array $pages
    ) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->categories = $categories;
        $this->user = $user;
        $this->language = $language;
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
        $this->featuredImage = $featuredImage;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return int
     */
    public function user(): int
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function pages(): array
    {
        return $this->pages;
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage;
    }
}
