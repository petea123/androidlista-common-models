<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Create\Tutorial;

use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Story;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Users\UserId;
use function Lambdish\Phunctional\map;

final class CreateTutorialStoryCommandHandler implements CommandHandler
{

    /** @var TutorialStoryCreator */
    private $creator;

    public function __construct(TutorialStoryCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(CreateTutorialStoryCommand $command)
    {
        $id = new StoryId($command->id());
        $groupId = new StoryGroupId($command->groupId());
        $categories = map(Story::categoryFromValue(), $command->categories());
        $user = new UserId($command->user());
        $language = StoryLanguage::fromString($command->language());
        $title = new StoryTitle($command->title());
        $description = new StoryDescription($command->description());
        $featuredImage = new StoryFeaturedImage($command->featuredImage());

        $pages = $command->pages();
        $pages[0]['cta']['value'] = false;

        $pages = map(Story::pageFromValues(), $pages);

        $this->creator->__invoke(
            $id,
            $groupId,
            $command->postHref(),
            $categories,
            $user,
            $language,
            $title,
            $description,
            $featuredImage,
            ...$pages
        );
    }
}
