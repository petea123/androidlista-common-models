<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Create\Tutorial;

use App\Al\UsersProfile\Application\Find\FindUserProfileQuery;
use App\Al\UsersProfile\Application\UserProfileResponse;
use App\Amp\Shared\Domain\Stories\StoryFeaturedImage;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Shared\Domain\Stories\StoryLanguage;
use App\Amp\Stories\Domain\Tutorial\TutorialStory;
use App\Amp\Stories\Domain\Tutorial\TutorialStoryRepository;
use App\Amp\Stories\Domain\ValueObject\Page\StoryPage;
use App\Amp\Stories\Domain\ValueObject\StoryDescription;
use App\Amp\Stories\Domain\ValueObject\StoryGroupId;
use App\Amp\Stories\Domain\ValueObject\StoryTitle;
use App\Amp\Stories\Domain\ValueObject\User\StoryUser;
use App\Amp\Stories\Domain\ValueObject\User\StoryUserAvatar;
use App\Amp\Stories\Domain\ValueObject\User\StoryUserUsername;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Users\UserId;

final class TutorialStoryCreator
{
    /** @var TutorialStoryRepository */
    private $repository;
    /** @var DomainEventPublisher */
    private $publisher;
    /** @var QueryBus */
    private $queryBus;

    public function __construct(
        TutorialStoryRepository $repository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher
    ) {
        $this->repository = $repository;
        $this->publisher = $publisher;
        $this->queryBus = $queryBus;
    }

    public function __invoke(
        StoryId $id,
        StoryGroupId $groupId,
        string $postHref,
        array $categories,
        UserId $user,
        StoryLanguage $language,
        StoryTitle $title,
        StoryDescription $description,
        StoryFeaturedImage $featuredImage,
        StoryPage ...$pages
    ) {
        $story = TutorialStory::createTutorial(
            $id,
            $groupId,
            $postHref,
            $categories,
            $this->createUser($user),
            $language,
            $title,
            $description,
            $featuredImage,
            ...$pages
        );

        $this->repository->save($story);

        $this->publisher->record(...$story->pullDomainEvents());
    }

    private function createUser(UserId $id): StoryUser
    {
        /** @var UserProfileResponse $user */
        $user = $this->queryBus->ask(new FindUserProfileQuery($id->value()));

        return new StoryUser(
            new UserId($id->value()),
            new StoryUserUsername($user->username()),
            new StoryUserAvatar($user->avatar())
        );
    }
}
