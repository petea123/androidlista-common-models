<?php

declare(strict_types=1);

namespace App\Amp\Stories\Application\Create\Tutorial;

use App\Amp\Stories\Application\Create\CreateStoryCommand;

final class CreateTutorialStoryCommand extends CreateStoryCommand
{
    /**
     * @var string
     */
    private $postHref;

    public function __construct(
        string $id,
        string $groupId,
        string $postHref,
        array $categories,
        int $user,
        string $language,
        string $title,
        string $description,
        string $featuredImage,
        array $pages
    ) {
        parent::__construct($id, $groupId, $categories, $user, $language, $title, $description, $featuredImage, $pages);
        $this->postHref = $postHref;
    }

    /**
     * @return string
     */
    public function postHref(): string
    {
        return $this->postHref;
    }
}
