<?php

declare(strict_types=1);

namespace App\Wordpress\Post\Domain;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class WpPostPublished extends DomainEvent
{

    /**
     * @var int
     */
    private $publishedOn;
    /**
     * @var string
     */
    private $title;
    /**
     * @var array
     */
    private $tags;
    /**
     * @var int
     */
    private $language;
    /**
     * @var string
     */
    private $href;
    /**
     * @var array
     */
    private $images;
    /**
     * @var string
     */
    private $description;

    public function __construct(
        string $aggregateId,
        int $publishedOn,
        string $title,
        string $description,
        array $tags,
        int $language,
        string $href,
        array $images,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->publishedOn = $publishedOn;
        $this->title = $title;
        $this->tags = $tags;
        $this->language = $language;
        $this->href = $href;
        $this->images = $images;
        $this->description = $description;
    }

    public static function eventName(): string
    {
        return 'WpPostPublished';
    }

    public static function stream(): string
    {
        return WpPost::class;
    }

    public function toPrimitives(): array
    {
        return [
            'publishedOn' => $this->publishedOn,
            'title' => $this->title,
            'description' => $this->description,
            'tags' => $this->tags,
            'language' => $this->language,
            'href' => $this->href,
            'images' => $this->images
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['publishedOn'], $body['title'], $body['description'], $body['tags'], $body['language'], $body['href'], $body['images'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function publishedOn(): int
    {
        return $this->publishedOn;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function tags(): array
    {
        return $this->tags;
    }

    /**
     * @return int
     */
    public function language(): int
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return array
     */
    public function images(): array
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}
