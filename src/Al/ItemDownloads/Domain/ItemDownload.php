<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Domain;

use App\Al\ItemDownloads\Domain\ValueObject\ItemDownloadId;
use App\Al\Shared\Domain\Items\ItemId;

class ItemDownload
{

    /** @var ItemDownloadId */
    private $id;

    /** @var ItemId */
    private $itemId;

    private function __construct(ItemDownloadId $id, ItemId $itemId)
    {
        $this->id = $id;
        $this->itemId = $itemId;
    }

    /**
     * @return ItemDownloadId
     */
    public function id(): ItemDownloadId
    {
        return $this->id;
    }

    /**
     * @return ItemId
     */
    public function itemId(): ItemId
    {
        return $this->itemId;
    }
}
