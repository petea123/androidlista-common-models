<?php


namespace App\Al\ItemDownloads\Domain\Service;

use App\Al\ItemDownloads\Domain\ItemDownloads;

interface ItemDownloadsRepositoryInterface
{
    public function mostDownloaded(int $limit, int $offset = 0): ItemDownloads;
}
