<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Domain;

use App\Shared\Domain\Collection;

final class ItemDownloads extends Collection
{
    protected function type(): string
    {
        return ItemDownload::class;
    }
}
