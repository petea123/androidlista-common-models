<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Domain\ValueObject;

use App\Shared\Domain\ValueObject\IntValueObject;
use InvalidArgumentException;

final class ItemDownloadId extends IntValueObject
{
    public function __construct(int $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(int $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException(sprintf('The item id <%s> is empty', $value));
        }
    }
}
