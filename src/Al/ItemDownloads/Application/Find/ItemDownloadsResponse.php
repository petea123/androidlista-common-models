<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find;

use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\Collection;

final class ItemDownloadsResponse extends Collection implements Response
{
    protected function type(): string
    {
        return ItemDownloadResponse::class;
    }
}
