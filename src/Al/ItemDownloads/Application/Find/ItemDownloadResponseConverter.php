<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find;

use App\Al\ItemDownloads\Domain\ItemDownload;

final class ItemDownloadResponseConverter
{

    public function __invoke(ItemDownload $item): ItemDownloadResponse
    {
        return new ItemDownloadResponse(
            $item->id()->value(),
            $item->itemId()->value()
        );
    }
}
