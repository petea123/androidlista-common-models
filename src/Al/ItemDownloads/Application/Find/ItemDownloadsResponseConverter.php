<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find;

use App\Al\ItemDownloads\Domain\ItemDownloads;
use function Lambdish\Phunctional\apply;

final class ItemDownloadsResponseConverter
{

    public function __invoke(ItemDownloads $items): ItemDownloadsResponse
    {
        $array = [];

        foreach ($items as $item) {
            $array[] = apply(new ItemDownloadResponseConverter(), [$item]);
        }

        return new ItemDownloadsResponse($array);
    }
}
