<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find;

use App\Shared\Domain\Bus\Query\Response;

final class ItemDownloadResponse implements Response
{

    /** @var int */
    private $id;

    /** @var int */
    private $itemId;

    public function __construct(int $id, int $itemId)
    {
        $this->id = $id;
        $this->itemId = $itemId;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }
}
