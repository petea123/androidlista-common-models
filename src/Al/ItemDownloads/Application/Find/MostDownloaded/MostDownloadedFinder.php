<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find\MostDownloaded;

use App\Al\ItemDownloads\Domain\ItemDownloads;
use App\Al\ItemDownloads\Domain\Service\ItemDownloadsRepositoryInterface;

final class MostDownloadedFinder
{

    /** @var ItemDownloadsRepositoryInterface */
    private $downloadsRepository;

    public function __construct(ItemDownloadsRepositoryInterface $downloadsRepository)
    {
        $this->downloadsRepository = $downloadsRepository;
    }

    public function __invoke(int $limit, int $offset): ItemDownloads
    {
        return $this->downloadsRepository->mostDownloaded($limit, $offset);
    }

}