<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find\MostDownloaded;

use App\Al\ItemDownloads\Application\Find\ItemDownloadsResponse;
use App\Al\ItemDownloads\Application\Find\ItemDownloadsResponseConverter;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\pipe;

class FindMostDownloadedItemsQueryHandler implements QueryHandler
{

    /**
     * @var MostDownloadedFinder
     */
    private $finder;

    public function __construct(MostDownloadedFinder $finder)
    {
        $this->finder = pipe($finder, new ItemDownloadsResponseConverter());
    }

    public function __invoke(FindMostDownloadedItemsQuery $query): ItemDownloadsResponse
    {
        return apply($this->finder,[$query->limit(), $query->offset()]);
    }
}
