<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Application\Find\MostDownloaded;

use App\Shared\Domain\Bus\Query\Query;

class FindMostDownloadedItemsQuery implements Query
{

    /** @var int */
    private $limit;

    /** @var int */
    private $offset;

    public function __construct(int $limit, int $offset = 0)
    {

        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function offset(): int
    {
        return $this->offset;
    }
}
