<?php

declare(strict_types=1);


namespace App\Al\ItemDownloads\Infrastructure\Persistence\Doctrine;

use App\Al\ItemDownloads\Domain\ValueObject\ItemDownloadId;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

final class ItemDownloadIdType extends IntegerType implements DoctrineCustomType
{
    public function getName(): string
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new ItemDownloadId((int)$value);
    }

    /** @var ItemDownloadId $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->value();
    }

    public static function customTypeName(): string
    {
        return 'item_download_id';
    }
}
