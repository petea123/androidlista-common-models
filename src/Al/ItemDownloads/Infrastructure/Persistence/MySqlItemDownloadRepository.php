<?php

declare(strict_types=1);

namespace App\Al\ItemDownloads\Infrastructure\Persistence;

use App\Al\ItemDownloads\Domain\ItemDownload;
use App\Al\ItemDownloads\Domain\ItemDownloads;
use App\Al\ItemDownloads\Domain\Service\ItemDownloadsRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlItemDownloadRepository extends DoctrineRepository implements ItemDownloadsRepositoryInterface
{

    public function mostDownloaded(int $limit, int $offset = 0): ItemDownloads
    {
        $qb = $this->repository(ItemDownload::class)->createQueryBuilder('i');

        $result = $qb
            ->select('i, COUNT(i.itemId) as count')
            ->where($qb->expr()->notIn('i.itemId', ItemId::BANNED_ITEMS))
            ->groupBy('i.itemId')
            ->orderBy('count', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();

        return new ItemDownloads(array_column($result, 0));
    }
}
