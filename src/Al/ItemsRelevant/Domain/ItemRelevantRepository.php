<?php


namespace App\Al\ItemsRelevant\Domain;

use App\Al\Shared\Domain\Items\ItemId;

interface ItemRelevantRepository
{
    public function save(ItemRelevant $relevant): void;

    public function searchByItemId(ItemId $itemId): ?ItemRelevant;
}
