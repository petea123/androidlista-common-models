<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Domain;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\search;

//TODO TESTING ItemRelevant
final class ItemRelevant extends AggregateRoot
{

    /**
     * @var ItemRelevantId
     */
    private $id;
    /**
     * @var ItemId
     */
    private $itemId;
    /**
     * @var ItemRelevantLocale[]
     */
    private $locales;

    public function __construct(ItemRelevantId $id, ItemId $itemId, ItemRelevantLocale  ...$locales)
    {
        $this->id = $id;
        $this->itemId = $itemId;
        $this->locales = $locales;
    }

    public static function create(ItemRelevantId $id, ItemId $itemId, ItemRelevantLocale  ...$locales)
    {
        return new self($id, $itemId, ...$locales);
    }

    public function addRelevant(ItemRelevantLocale $locale): void
    {
        if (null === search($this->localeComparator($locale->locale()), $this->locales)) {
            $this->locales[] = $locale;
        }
    }

    public function removeRelevant(LocaleValueObject $locale)
    {
        $this->locales = array_values(array_filter($this->locales, function (ItemRelevantLocale $other) use ($locale) {
            return !$this->localeComparator($locale)($other);
        }));
    }

    private function localeComparator(LocaleValueObject $locale): callable
    {
        return static function (ItemRelevantLocale $other) use ($locale) {
            return $locale->equals($other->locale());
        };
    }
}
