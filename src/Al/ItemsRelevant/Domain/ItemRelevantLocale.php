<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Domain;

use App\Shared\Domain\Utils;
use App\Shared\Domain\ValueObject\LocaleValueObject;

final class ItemRelevantLocale
{
    /**
     * @var LocaleValueObject
     */
    private $locale;
    /**
     * @var \DateTimeImmutable
     */
    private $markedAt;

    public function __construct(LocaleValueObject $locale, \DateTimeImmutable $markedAt)
    {
        $this->locale = $locale;
        $this->markedAt = $markedAt;
    }

    /**
     * @return LocaleValueObject
     */
    public function locale(): LocaleValueObject
    {
        return $this->locale;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function markedAt(): \DateTimeImmutable
    {
        return $this->markedAt;
    }

    public static function fromValues(array $values): self
    {
        return new self(
            LocaleValueObject::fromString($values['locale']),
            Utils::stringToDate($values['marked_at'])
        );
    }

    public function toValues(): array
    {
        return [
            'locale' => $this->locale->value(),
            'marked_at' => $this->markedAt()->format('Y-m-d H:i:s')
        ];
    }
}
