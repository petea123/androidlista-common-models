<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\AddLocale;

use App\Shared\Domain\Bus\Command\Command;

final class AddItemRelevantLocaleCommand extends Command
{
    /**
     * @var int
     */
    private $itemId;
    /**
     * @var string
     */
    private $locale;
    /**
     * @var \DateTimeImmutable
     */
    private $markedAt;

    public function __construct(int $itemId, string $locale, \DateTimeImmutable $markedAt)
    {
        $this->itemId = $itemId;
        $this->locale = $locale;
        $this->markedAt = $markedAt;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function markedAt(): \DateTimeImmutable
    {
        return $this->markedAt;
    }
}
