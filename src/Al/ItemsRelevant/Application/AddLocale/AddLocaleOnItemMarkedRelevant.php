<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\AddLocale;

use App\Al\Items\Domain\Event\ItemMarkedAsRelevant;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Utils;

final class AddLocaleOnItemMarkedRelevant implements DomainEventSubscriber
{
    /**
     * @var CommandBus
     */
    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function __invoke(ItemMarkedAsRelevant $event): void
    {
        $this->bus->handle(new AddItemRelevantLocaleCommand(
            (int)$event->aggregateId(),
            $event->locale(),
            Utils::timestampToDate((int)$event->occurredOn())
        ));
    }

    public static function subscribedTo(): array
    {
        return [ItemMarkedAsRelevant::class];
    }
}
