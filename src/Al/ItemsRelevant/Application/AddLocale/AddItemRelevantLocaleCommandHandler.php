<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\AddLocale;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\apply;

final class AddItemRelevantLocaleCommandHandler implements CommandHandler
{
    /**
     * @var ItemRelevantLocaleAdder
     */
    private $adder;

    public function __construct(ItemRelevantLocaleAdder $adder)
    {
        $this->adder = $adder;
    }

    public function __invoke(AddItemRelevantLocaleCommand $command): void
    {
        $itemId = new ItemId($command->itemId());
        $locale = LocaleValueObject::fromString($command->locale());

        apply($this->adder, [$itemId, $locale, $command->markedAt()]);
    }
}
