<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\AddLocale;

use App\Al\ItemsRelevant\Domain\ItemRelevant;
use App\Al\ItemsRelevant\Domain\ItemRelevantId;
use App\Al\ItemsRelevant\Domain\ItemRelevantLocale;
use App\Al\ItemsRelevant\Domain\ItemRelevantRepository;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\Uuid;

final class ItemRelevantLocaleAdder
{
    /**
     * @var ItemRelevantRepository
     */
    private $repository;

    public function __construct(ItemRelevantRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ItemId $itemId, LocaleValueObject $locale, \DateTimeImmutable $markedAt): void
    {
        $item = $this->repository->searchByItemId($itemId);

        if (null === $item) {
            $item = ItemRelevant::create(
                new ItemRelevantId(Uuid::random()->value()),
                $itemId
            );
        }

        $item->addRelevant(new ItemRelevantLocale($locale, $markedAt));

        $this->repository->save($item);
    }
}
