<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\RemoveLocale;

use App\Al\Items\Domain\Event\ItemUnmarkedAsRelevant;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class RemoveLocaleOnItemUnmarkedRelevant implements DomainEventSubscriber
{
    /**
     * @var CommandBus
     */
    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function __invoke(ItemUnmarkedAsRelevant $event): void
    {
        $this->bus->handle(new RemoveItemRelevantLocaleCommand(
            (int)$event->aggregateId(),
            $event->locale()
        ));
    }

    public static function subscribedTo(): array
    {
        return [ItemUnmarkedAsRelevant::class];
    }
}
