<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\RemoveLocale;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\apply;

final class RemoveItemRelevantLocaleCommandHandler implements CommandHandler
{
    /**
     * @var ItemRelevantLocaleRemover
     */
    private $remover;

    public function __construct(ItemRelevantLocaleRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveItemRelevantLocaleCommand $command): void
    {
        $itemId = new ItemId($command->itemId());
        $locale = LocaleValueObject::fromString($command->locale());

        apply($this->remover, [$itemId, $locale]);
    }
}
