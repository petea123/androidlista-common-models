<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\RemoveLocale;

use App\Shared\Domain\Bus\Command\Command;

final class RemoveItemRelevantLocaleCommand extends Command
{
    /**
     * @var int
     */
    private $itemId;
    /**
     * @var string
     */
    private $locale;

    public function __construct(int $itemId, string $locale)
    {
        $this->itemId = $itemId;
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }
}
