<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Application\RemoveLocale;

use App\Al\ItemsRelevant\Domain\ItemRelevantRepository;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\LocaleValueObject;

final class ItemRelevantLocaleRemover
{
    /**
     * @var ItemRelevantRepository
     */
    private $repository;

    public function __construct(ItemRelevantRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ItemId $itemId, LocaleValueObject $locale): void
    {
        $item = $this->repository->searchByItemId($itemId);

        if (null === $item) {
            return;
        }

        $item->removeRelevant($locale);
        $this->repository->save($item);
    }
}
