<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Infrastructure\Persistence\Doctrine;

use App\Al\ItemsRelevant\Domain\ItemRelevantId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class ItemRelevantIdType extends UuidType
{
    public static function customTypeName(): string
    {
        return 'item_relevant_id';
    }

    protected function typeClassName(): string
    {
        return ItemRelevantId::class;
    }
}
