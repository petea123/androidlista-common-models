<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Infrastructure\Persistence\Doctrine;

use App\Al\ItemsRelevant\Domain\ItemRelevantLocale;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use function Lambdish\Phunctional\map;

final class ItemRelevantLocalesType extends JsonType implements DoctrineCustomType
{
    public function getName(): string
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return map(self::fromValues(), parent::convertToPHPValue($value, $platform));
    }

    /** @var ItemRelevantLocale[] $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue(map(self::toValues(), $value), $platform);
    }

    public static function customTypeName(): string
    {
        return 'item_relevant_locales';
    }

    public static function fromValues(): callable
    {
        return static function (array $values): ItemRelevantLocale {
            return ItemRelevantLocale::fromValues($values);
        };
    }

    public static function toValues(): callable
    {
        return static function (ItemRelevantLocale $file): array {
            return $file->toValues();
        };
    }
}
