<?php

declare(strict_types=1);

namespace App\Al\ItemsRelevant\Infrastructure\Persistence;

use App\Al\ItemsRelevant\Domain\ItemRelevant;
use App\Al\ItemsRelevant\Domain\ItemRelevantRepository;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlItemRelevantRepository extends DoctrineRepository implements ItemRelevantRepository
{

    public function save(ItemRelevant $relevant): void
    {
        $this->persist($relevant);
        $this->entityManager()->flush();
    }

    public function searchByItemId(ItemId $itemId): ?ItemRelevant
    {
        return $this->repository(ItemRelevant::class)->findOneBy(['itemId' => $itemId]);
    }
}
