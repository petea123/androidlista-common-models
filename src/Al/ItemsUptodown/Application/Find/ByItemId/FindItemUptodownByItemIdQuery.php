<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find\ByItemId;

use App\Shared\Domain\Bus\Query\Query;

final class FindItemUptodownByItemIdQuery implements Query
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
