<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find\ByItemId;

use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\Service\ItemUptodownRepositoryInterface;
use App\Al\ItemsUptodown\Domain\Service\Finder as DomainFinder;
use App\Al\Shared\Domain\Items\ItemId;

final class Finder
{
    /** @var DomainFinder  */
    private $finder;

    public function __construct(ItemUptodownRepositoryInterface $repository)
    {
        $this->finder = new DomainFinder($repository);
    }

    public function __invoke(ItemId $id): ItemUptodown
    {
        return $this->finder->searchByItemId($id);
    }
}