<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find\ByItemId;

use App\Al\ItemsUptodown\Application\Find\ItemUptodownResponse;
use App\Al\ItemsUptodown\Application\Find\ItemUptodownResponseConverter;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\pipe;

final class FindItemUptodownByItemIdQueryHandler implements QueryHandler
{

    /**
     * @var Finder
     */
    private $uptodownFinder;

    public function __construct(Finder $uptodownFinder)
    {
        $this->uptodownFinder = pipe($uptodownFinder, new ItemUptodownResponseConverter());
    }

    public function __invoke(FindItemUptodownByItemIdQuery $query): ItemUptodownResponse
    {
        $itemId = new ItemId($query->id());

        return apply($this->uptodownFinder, [$itemId]);
    }
}
