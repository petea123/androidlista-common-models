<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find;

use App\Shared\Domain\Bus\Query\Response;

final class ItemUptodownResponse implements Response
{

    /** @var string */
    private $id;
    /** @var int */
    private $itemId;
    /** @var string */
    private $externalUrl;
    /** @var int */
    private $uptodownId;

    public function __construct(string $id, int $itemId, string $externalUrl, int $uptodownId)
    {
        $this->id = $id;
        $this->itemId = $itemId;
        $this->externalUrl = $externalUrl;
        $this->uptodownId = $uptodownId;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function externalUrl(): string
    {
        return $this->externalUrl;
    }

    /**
     * @return int
     */
    public function uptodownId(): int
    {
        return $this->uptodownId;
    }


}
