<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find;

use App\Al\ItemsUptodown\Domain\ItemUptodown;

final class ItemUptodownResponseConverter
{

    public function __invoke(ItemUptodown $item): ItemUptodownResponse
    {
        return new ItemUptodownResponse(
            $item->id()->value(),
            $item->itemId()->value(),
            $item->externalUrl()->value(),
            $item->uptodownId()->value()
        );
    }
}