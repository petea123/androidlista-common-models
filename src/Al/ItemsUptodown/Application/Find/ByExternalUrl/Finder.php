<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find\ByExternalUrl;

use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\Service\ItemUptodownRepositoryInterface;
use App\Al\ItemsUptodown\Domain\Service\Finder as DomainFinder;
use App\Shared\Domain\ValueObject\ExternalUrl;

final class Finder
{
    /** @var DomainFinder  */
    private $finder;

    public function __construct(ItemUptodownRepositoryInterface $repository)
    {
        $this->finder = new DomainFinder($repository);
    }

    public function __invoke(ExternalUrl $url): ItemUptodown
    {
        return $this->finder->searchByExternalUrl($url);
    }
}