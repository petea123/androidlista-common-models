<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find\ByExternalUrl;

use App\Shared\Domain\Bus\Query\Query;

final class FindItemByUptodownUrlQuery implements Query
{

    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function url(): string
    {
        return $this->url;
    }
}
