<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Find\ByExternalUrl;

use App\Al\Items\Application\Find\ItemFinder;
use App\Al\Items\Application\Find\ItemResponse;
use App\Al\Items\Application\Find\ItemResponseConverter;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;

final class FindItemByUptodownUrlQueryHandler implements QueryHandler
{

    /**
     * @var Finder
     */
    private $uptodownFinder;

    /** @var ItemFinder */
    private $finder;

    public function __construct(Finder $uptodownFinder, ItemFinder $finder)
    {
        $this->uptodownFinder = $uptodownFinder;
        $this->finder = $finder;
    }

    public function __invoke(FindItemByUptodownUrlQuery $query): ItemResponse
    {
        $uptodown = apply($this->uptodownFinder, [$query->url()]);

        $itemId = new ItemId($uptodown->itemId());

        $item = apply($this->finder, [$itemId]);

        return apply(new ItemResponseConverter(), [$item, $uptodown]);
    }
}
