<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Mutation;

use App\Al\ItemsUptodown\Application\Create\ByItemId\CreateUptodownRelationByItemIdCommand;
use App\Shared\Domain\Bus\Command\CommandBus;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Error\UserError;

final class ItemUptodownMutation implements MutationInterface, AliasedInterface
{

    /** @var CommandBus */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function createUptodownRelationWithItemId(string $id, int $itemId): string
    {

        try {
            $this->commandBus->handle(new CreateUptodownRelationByItemIdCommand(
                $id,
                $itemId
            ));
        } catch (\Exception $e) {
            throw new UserError($e->getMessage());
        }

        return $id;
    }

    /**
     * Returns methods aliases.
     * @return array
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'ItemUptodownMutation'
        ];
    }
}
