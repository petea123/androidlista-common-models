<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Create;

use App\Shared\Domain\Bus\Command\Command;

final class CreateUptodownRelationCommand extends Command
{

    /** @var string */
    private $id;

    /**
     * @var string
     */
    private $packageName;

    public function __construct(string $id, string $packageName)
    {
        $this->id = $id;
        $this->packageName = $packageName;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function packageName(): string
    {
        return $this->packageName;
    }
}
