<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Create;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class CreateUptodownRelationCommandHandler implements CommandHandler
{
    /** @var Creator */
    private $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(CreateUptodownRelationCommand $command)
    {
        $id = new ItemUptodownId($command->id());
        $packageName = new ItemGoogle($command->packageName());

        apply($this->creator, [$id, $packageName]);
    }
}
