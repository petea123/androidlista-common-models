<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Create\ByItemId;

use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Items\Application\Find\FindItemQuery;
use App\Al\Items\Application\Find\ItemResponse;
use App\Al\ItemsUptodown\Domain\Exception\ItemUptodownRelationExist;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\Service\ItemUptodownRepositoryInterface;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;

final class Creator
{


    /** @var ItemUptodownRepositoryInterface */
    private $repository;

    /** @var QueryBus */
    private $queryBus;

    /** @var DomainEventPublisher */
    private $publisher;
    /**
     * @var WebParserFactory
     */
    private $webParserFactory;

    public function __construct(
        ItemUptodownRepositoryInterface $repository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher,
        WebParserFactory $webParserFactory
    ) {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
        $this->publisher = $publisher;
        $this->webParserFactory = $webParserFactory;
    }

    public function __invoke(ItemUptodownId $id, ItemId $itemId): void
    {

        $this->guard($itemId);

        /** @var ItemResponse $item */
        $item = $this->queryBus->ask(new FindItemQuery($itemId->value()));

        if (null === $app = $this->webParserFactory->instance(
            ApkSource::uptodown(),
            ItemGoogle::createFromPackage($item->googleId())
        )
        ) {
            return;
        }

        $itemUptodown = ItemUptodown::create(
            $id,
            $itemId,
            $app->apkUrl(),
            $app->uptodownId()
        );

        $this->repository->save($itemUptodown);

        $this->publisher->record(...$itemUptodown->pullDomainEvents());
    }

    private function guard(ItemId $id): void
    {
        $item = $this->repository->searchByItemId($id);

        if (null !== $item) {
            throw new ItemUptodownRelationExist($id);
        }
    }
}
