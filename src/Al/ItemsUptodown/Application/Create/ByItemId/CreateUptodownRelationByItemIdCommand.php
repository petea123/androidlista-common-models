<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Create\ByItemId;

use App\Shared\Domain\Bus\Command\Command;

final class CreateUptodownRelationByItemIdCommand extends Command
{

    /** @var string */
    private $id;

    /** @var int */
    private $itemId;

    public function __construct(string $id, int $itemId)
    {
        $this->id = $id;
        $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }
}
