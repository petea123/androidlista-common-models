<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Create\ByItemId;

use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class CreateUptodownRelationByItemIdCommandHandler implements CommandHandler
{
    /** @var Creator */
    private $creator;

    public function __construct(Creator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(CreateUptodownRelationByItemIdCommand $command)
    {
        $id = new ItemUptodownId($command->id());
        $itemId = new ItemId($command->itemId());

        apply($this->creator, [$id, $itemId]);
    }
}
