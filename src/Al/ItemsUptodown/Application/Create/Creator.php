<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Application\Create;

use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Application\Find\ByPackageName\FindItemByPackageNameQuery;
use App\Al\ItemsUptodown\Domain\Exception\ItemUptodownRelationExist;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\Service\ItemUptodownRepositoryInterface;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;

final class Creator
{

    /** @var ItemUptodownRepositoryInterface */
    private $repository;

    /** @var QueryBus */
    private $queryBus;

    /** @var DomainEventPublisher */
    private $publisher;
    /**
     * @var WebParserFactory
     */
    private $webParserFactory;

    public function __construct(
        ItemUptodownRepositoryInterface $repository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher,
        WebParserFactory $webParserFactory
    ) {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
        $this->publisher = $publisher;
        $this->webParserFactory = $webParserFactory;
    }

    public function __invoke(ItemUptodownId $id, ItemGoogle $package): void
    {

        if (null === $app = $this->webParserFactory->instance(ApkSource::uptodown(), $package)) {
            throw new \Exception('not found in Uptodown');
        }

        $item = $this->queryBus->ask(new FindItemByPackageNameQuery($package->id()));

        $itemId = new ItemId($item->id());
        $this->guard($itemId);


        $itemUptodown = ItemUptodown::create(
            $id,
            $itemId,
            $app->apkUrl(),
            $app->uptodownId()
        );

        $this->repository->save($itemUptodown);

        $this->publisher->record(...$itemUptodown->pullDomainEvents());
    }

    private function guard(ItemId $id): void
    {
        $item = $this->repository->searchByItemId($id);

        if (null !== $item) {
            throw new ItemUptodownRelationExist($id);
        }
    }
}
