<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Domain\Service;

use App\Al\Apks\Application\Search\ApksResponse;
use App\Al\Apks\Application\Search\ByCriteria\SearchApksByCriteriaQuery;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Items\Application\Find\Price\FindItemPriceQuery;
use App\Al\ItemsUptodown\Domain\Exception\ItemUptodownNoVersionsToImport;
use App\Al\ItemsUptodown\Domain\Exception\ItemWithPrice;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Al\Shared\Domain\WebParserInterface;
use App\Shared\Domain\Bus\Query\QueryBus;

final class Importer
{

    /** @var QueryBus */
    private $queryBus;

    /** @var WebParserInterface */
    private $webParser;

    public function __construct(QueryBus $queryBus, WebParserInterface $uptodownParser)
    {
        $this->queryBus = $queryBus;
        $this->webParser = $uptodownParser;
    }

    public function import(ExternalUrl $url, ItemId $itemId, UptodownId $uptodownId, int $total): array
    {

        $this->guardPrice($itemId);

        /** @var ApksResponse $apks */
        $apks = $this->queryBus->ask(new SearchApksByCriteriaQuery(
            [
                [
                    'field' => 'itemId' ,
                    'operator' => '=',
                    'value' => (string)$itemId->value()
                ]
            ]
        ));

        $actualVersion = $this->webParser->infoFor($url);

        $externalVersions = $this->webParser->versions($uptodownId->value());
        $externalVersions[$actualVersion['version']] = $url.'/download';

        foreach ($externalVersions as $key => $externalVersion) {

            if (!$this->hasApkVersion($apks, $key)) {
                $versions[] = $url.'/'.explode('/', $externalVersion, 5)[4];
                --$total;
            }

            if (0 >= $total) {
                return $versions;
            }
        }

        if (empty($versions)) {
            throw new ItemUptodownNoVersionsToImport($itemId);
        }

        return $versions;
    }

    private function hasApkVersion(ApksResponse $apks, $version): bool
    {

        foreach ($apks as $apk) {
            if (false !== strpos($apk->version(), $version)) {
                return true;
            }
        }

        return false;
    }

    private function guardPrice(ItemId $itemId)
    {
        $price = $this->queryBus->ask(new FindItemPriceQuery($itemId->value()));

        if (0 < $price->price()) {
            throw new ItemWithPrice($itemId);
        }
    }
}
