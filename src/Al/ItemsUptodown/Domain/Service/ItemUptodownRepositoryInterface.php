<?php


namespace App\Al\ItemsUptodown\Domain\Service;


use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\Shared\Domain\Items\ItemId;

interface ItemUptodownRepositoryInterface
{
    public function save(ItemUptodown $itemUptodown): void;

    public function search(ItemUptodownId $id): ?ItemUptodown;

    public function searchByExternalUrl(ExternalUrl $externalUrl): ?ItemUptodown;

    public function searchByItemId(ItemId $id): ?ItemUptodown;
}
