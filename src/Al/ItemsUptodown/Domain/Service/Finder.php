<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Domain\Service;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\ItemsUptodown\Domain\Exception\ItemUptodownByItemIdNotFound;
use App\Al\ItemsUptodown\Domain\Exception\ItemUptodownExternalUrlWithoutRelation;
use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\Shared\Domain\Items\ItemId;

final class Finder
{
    /**
     * @var ItemUptodownRepositoryInterface
     */
    private $repository;

    public function __construct(ItemUptodownRepositoryInterface $repository)
    {

        $this->repository = $repository;
    }

    public function searchByExternalUrl(ExternalUrl $url): ItemUptodown
    {
        $item = $this->repository->searchByExternalUrl($url);

        if (null === $item) {
            throw new ItemUptodownExternalUrlWithoutRelation($url);
        }

        return $item;
    }

    public function searchByItemId(ItemId $id): ItemUptodown
    {
        $item = $this->repository->searchByItemId($id);

        if (null === $item) {
            throw new ItemUptodownByItemIdNotFound($id);
        }

        return $item;
    }
}
