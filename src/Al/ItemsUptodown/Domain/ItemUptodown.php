<?php

declare(strict_types=1);

namespace App\Al\ItemsUptodown\Domain;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Items\Domain\Event\ItemApkImportStarted;
use App\Al\ItemsUptodown\Domain\Event\ItemUptodownCreated;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Aggregate\AggregateRoot;

class ItemUptodown extends AggregateRoot
{

    /** @var ItemUptodownId */
    private $id;

    /** @var ItemId */
    private $itemId;

    /** @var ExternalUrl */
    private $externalUrl;

    /** @var UptodownId */
    private $uptodownId;

    private function __construct(ItemUptodownId $id, ItemId $itemId, ExternalUrl $externalUrl, UptodownId $uptodownId)
    {
        $this->id = $id;
        $this->itemId = $itemId;
        $this->externalUrl = $externalUrl;
        $this->uptodownId = $uptodownId;
    }

    public static function create(ItemUptodownId $id, ItemId $itemId, ExternalUrl $externalUrl, UptodownId $uptodownId)
    {
        $item = new self($id, $itemId, $externalUrl, $uptodownId);

        $item->record(new ItemUptodownCreated($id->value(), $itemId->value(), $externalUrl->value()));

        return $item;
    }

    public function importVersion(ApkId $apkId, ExternalUrl $url)
    {
        $this->record(new ItemApkImportStarted(
            (string)$this->itemId->value(), $apkId->value(), $url->value(), ApkSource::uptodown()->value()
        ));
    }

    /**
     * @return ItemUptodownId
     */
    public function id(): ItemUptodownId
    {
        return $this->id;
    }

    /**
     * @return ItemId
     */
    public function itemId(): ItemId
    {
        return $this->itemId;
    }

    /**
     * @return ExternalUrl
     */
    public function externalUrl(): ExternalUrl
    {
        return $this->externalUrl;
    }

    /**
     * @return UptodownId
     */
    public function uptodownId(): UptodownId
    {
        return $this->uptodownId;
    }
}
