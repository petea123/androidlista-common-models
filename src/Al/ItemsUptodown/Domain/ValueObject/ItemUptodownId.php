<?php

namespace App\Al\ItemsUptodown\Domain\ValueObject;

use App\Shared\Domain\ValueObject\Uuid;

final class ItemUptodownId extends Uuid
{
}