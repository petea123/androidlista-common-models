<?php

namespace App\Al\ItemsUptodown\Domain\ValueObject;

use App\Shared\Domain\ValueObject\IntValueObject;
use InvalidArgumentException;

final class UptodownId extends IntValueObject
{

    public function __construct(int $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(int $id)
    {
        if (empty($id)) {
            throw new InvalidArgumentException(sprintf('The uptodown id <%s> is empty', $id));
        }
    }
}
