<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Domain\Exception;

use App\Shared\Domain\DomainError;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemWithPrice extends DomainError
{

    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_with_price';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item <%s> has price', $this->id->value());
    }
}