<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Domain\Exception;

use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Domain\DomainError;

final class ItemUptodownExternalUrlWithoutRelation extends DomainError
{

    /**
     * @var ExternalUrl
     */
    private $url;

    public function __construct(ExternalUrl $url)
    {
        $this->url = $url;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'uptodown_url_without_relation';
    }

    protected function errorMessage(): string
    {
        return sprintf('The url <%s> dont have relation', $this->url->value());
    }
}
