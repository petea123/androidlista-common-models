<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Domain\Exception;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\DomainError;

final class ItemUptodownNoVersionsToImport extends DomainError
{

    /**
     * @var ItemId
     */
    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'uptodown_no_versions_to_import';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item id <%s> dont have versions pending to import', $this->id->value());
    }
}
