<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Domain\Exception;

use App\Shared\Domain\DomainError;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemUptodownRelationExist extends DomainError
{

    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_with_relation_exist';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item <%s> has relation to create new', $this->id->value());
    }
}