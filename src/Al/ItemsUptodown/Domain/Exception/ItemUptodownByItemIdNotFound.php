<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Domain\Exception;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\DomainError;

final class ItemUptodownByItemIdNotFound extends DomainError
{

    /**
     * @var ItemId
     */
    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'uptodown_with_item_id_not_found';
    }

    protected function errorMessage(): string
    {
        return sprintf('The itemId <%s> dont have relation', $this->id->value());
    }
}
