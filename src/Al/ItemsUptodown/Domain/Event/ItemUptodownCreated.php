<?php

namespace App\Al\ItemsUptodown\Domain\Event;

use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ItemUptodownCreated extends DomainEvent
{

    /** @var int */
    private $itemId;
    /** @var string */
    private $externalUrl;

    public function __construct(string $aggregateId, int $itemId, string $externalUrl, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->itemId = $itemId;
        $this->externalUrl = $externalUrl;
    }

    static function eventName(): string
    {
        return 'ItemUptodownCreated';
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId,
            'externalUrl' => $this->externalUrl
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['itemId'], $body['externalUrl'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function externalUrl(): string
    {
        return $this->externalUrl;
    }

    public static function stream(): string
    {
        return ItemUptodown::class;
    }
}
