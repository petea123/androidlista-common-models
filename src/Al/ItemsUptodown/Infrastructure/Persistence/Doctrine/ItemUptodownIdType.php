<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Infrastructure\Persistence\Doctrine;

use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class ItemUptodownIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'item_uptodown_id';
    }

    protected function typeClassName(): string
    {
        return ItemUptodownId::class;
    }
}
