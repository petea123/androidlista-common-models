<?php

declare(strict_types=1);


namespace App\Al\ItemsUptodown\Infrastructure\Persistence;

use App\Al\ItemsUptodown\Domain\ItemUptodown;
use App\Al\ItemsUptodown\Domain\Service\ItemUptodownRepositoryInterface;
use App\Al\ItemsUptodown\Domain\ValueObject\ItemUptodownId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlItemUptodownRepository extends DoctrineRepository implements ItemUptodownRepositoryInterface
{

    public function save(ItemUptodown $itemUptodown): void
    {
        $this->persist($itemUptodown);
    }

    public function search(ItemUptodownId $id): ?ItemUptodown
    {
        // TODO: Implement search() method.
    }

    public function searchByExternalUrl(ExternalUrl $externalUrl): ?ItemUptodown
    {
        return $this->repository(ItemUptodown::class)->findOneBy(['externalUrl.value' => $externalUrl->value()]);
    }

    public function searchByItemId(ItemId $id): ?ItemUptodown
    {
        return $this->repository(ItemUptodown::class)->findOneBy(['itemId' => $id]);
    }
}
