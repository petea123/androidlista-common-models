<?php declare(strict_types=1);

namespace App\Al\ItemsDescriptions\Domain;

use App\Al\Items\Domain\Item;

final class ItemDescription
{
    /** @var int */
    private $id;
    /** @var Item */
    private $item;
    /** @var int */
    private $descriptionTypeId;
    /** @var string */
    private $languageCode;
    /** @var int */
    private $typeId;
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var int */
    private $userId;
    /** @var \DateTime */
    private $createdAt;
    /** @var \DateTime */
    private $updatedAt;
    /** @var int */
    private $estPublicacion = 0;
    /** @var bool */
    private $isRejected = false;
    /** @var bool */
    private $isPropagated = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescriptionTypeId(): ?int
    {
        return $this->descriptionTypeId;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function setDescriptionTypeId(?int $descriptionTypeId): self
    {
        $this->descriptionTypeId = $descriptionTypeId;

        return $this;
    }

    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?string $languageCode): self
    {
        $this->languageCode = $languageCode;

        return $this;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function setTypeId(?int $typeId): self
    {
        $this->typeId = $typeId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getEstPublicacion(): ?int
    {
        return $this->estPublicacion;
    }

    public function setEstPublicacion(?int $estPublicacion): self
    {
        $this->estPublicacion = $estPublicacion;

        return $this;
    }

    public function isRejected(): ?bool
    {
        return $this->isRejected;
    }

    public function setIsRejected(?bool $isRejected): self
    {
        $this->isRejected = $isRejected;

        return $this;
    }

    public function isPropagated(): ?bool
    {
        return $this->isPropagated;
    }

    public function setIsPropagated(?bool $isPropagated): self
    {
        $this->isPropagated = $isPropagated;

        return $this;
    }
}
