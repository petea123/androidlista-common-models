<?php

declare(strict_types=1);

namespace App\Al\ItemsDescriptions\Domain\Provider;

use App\Al\ItemsDescriptions\Domain\ItemDescription;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ItemDescriptionProviderWasValidated extends DomainEvent
{

    /**
     * @var int
     */
    private $itemId;
    /**
     * @var string
     */
    private $language;

    public function __construct(
        string $aggregateId,
        int $itemId,
        string $language,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->itemId = $itemId;
        $this->language = $language;
    }

    public static function eventName(): string
    {
        return 'ItemDescriptionProviderWasValidated';
    }

    public static function stream(): string
    {
        return ItemDescription::class;
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId(),
            'language' => $this->language()
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['itemId'], $body['language'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
