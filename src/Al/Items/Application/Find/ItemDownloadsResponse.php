<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Shared\Domain\Bus\Query\Response;

final class ItemDownloadsResponse implements Response
{
    /** @var int */
    private $downloads;

    /**
     * ItemResponse constructor.
     * @param int $downloads
     */
    public function __construct(int $downloads)
    {
        $this->downloads = $downloads;
    }

    /**
     * @return int
     */
    public function downloads(): int
    {
        return $this->downloads;
    }

}
