<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\pipe;

class FindItemQueryHandler implements QueryHandler
{

    /**
     * @var ItemFinder
     */
    private $finder;

    public function __construct(ItemFinder $finder)
    {
        $this->finder = pipe($finder, new ItemResponseConverter());
    }

    public function __invoke(FindItemQuery $query): ItemResponse
    {
        $itemId = new ItemId($query->id());

        return apply($this->finder, [$itemId]);
    }
}
