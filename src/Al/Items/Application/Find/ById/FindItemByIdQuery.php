<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\ById;

use App\Shared\Domain\Bus\Query\Query;

final class FindItemByIdQuery implements Query
{
    private $id;

    /** @var string */
    private $language;

    public function __construct(int $id, string $language = 'en')
    {
        $this->id = $id;
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
