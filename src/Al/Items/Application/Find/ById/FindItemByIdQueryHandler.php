<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\ById;

use App\Al\Items\Infrastructure\Persistence\MySqlItemReadModelRepository;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Shared\Domain\Bus\Query\QueryHandler;

final class FindItemByIdQueryHandler implements QueryHandler
{
    /**
     * @var MySqlItemReadModelRepository
     */
    private $repository;

    public function __construct(MySqlItemReadModelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(FindItemByIdQuery $query): ?ItemView
    {
        return $this->repository->searchByLanguage($query->id(), $query->language());
    }
}
