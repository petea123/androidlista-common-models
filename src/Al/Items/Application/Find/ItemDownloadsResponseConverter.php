<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Al\Items\Domain\ValueObject\ItemDownloads;

final class ItemDownloadsResponseConverter
{

    public function __invoke(ItemDownloads $item): ItemDownloadsResponse
    {
        return new ItemDownloadsResponse(
            $item->value()
        );
    }
}