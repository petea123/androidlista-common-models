<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\ByPackageName;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\Service\ItemFinder as DomainItemFinder;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Items\Domain\ValueObject\ItemGoogle;

final class ItemGoogleFinder
{
    /** @var DomainItemFinder  */
    private $finder;

    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->finder = new DomainItemFinder($repository);
    }

    public function __invoke(ItemGoogle $package): Item
    {
        return $this->finder->searchByPackage($package);
    }
}