<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\ByPackageName;

use App\Shared\Domain\Bus\Query\Query;

final class FindItemByPackageNameQuery implements Query
{

    private $package;

    public function __construct(string $package)
    {
        $this->package = $package;
    }

    /**
     * @return string
     */
    public function package(): string
    {
        return $this->package;
    }
}