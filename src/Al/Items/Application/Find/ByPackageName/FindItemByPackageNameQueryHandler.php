<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\ByPackageName;

use App\Al\Items\Application\Find\ItemResponseConverter;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\pipe;

final class FindItemByPackageNameQueryHandler implements QueryHandler
{

    private $finder;

    public function __construct(ItemGoogleFinder $finder)
    {

        $this->finder = pipe($finder, new ItemResponseConverter());
    }

    public function __invoke(FindItemByPackageNameQuery $query)
    {
        $itemGoogle = ItemGoogle::createFromPackage($query->package());
        return apply($this->finder, [$itemGoogle]);
    }
}
