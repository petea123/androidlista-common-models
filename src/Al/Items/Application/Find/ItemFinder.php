<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\Service\ItemFinder as DomainItemFinder;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemFinder
{
    /** @var DomainItemFinder  */
    private $finder;

    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->finder = new DomainItemFinder($repository);
    }

    public function __invoke(ItemId $itemId): Item
    {
        return $this->finder->search($itemId);
    }
}