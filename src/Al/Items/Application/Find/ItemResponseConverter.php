<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\ValueObject\PackageValueObject;

final class ItemResponseConverter
{

    public function __invoke(Item $item): ItemResponse
    {
        $package = new ItemGoogle($item->google()->value());
        return new ItemResponse(
            $item->id()->value(),
            $package->id(),
            $item->google()->dirify(),
            $item->name()->dirify()
        );
    }
}
