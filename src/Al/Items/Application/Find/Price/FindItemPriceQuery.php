<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\Price;

use App\Shared\Domain\Bus\Query\Query;

class FindItemPriceQuery implements Query
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}