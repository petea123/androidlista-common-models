<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find\Price;

use App\Shared\Domain\Bus\Query\Response;

final class ItemPriceResponse implements Response
{
    /** @var float */
    private $price;

    public function __construct(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

}
