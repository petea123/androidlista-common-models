<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Shared\Domain\Bus\Query\Response;

final class ItemResponse implements Response
{
    private $id;
    private $googleIdDirify;
    /**
     * @var string
     */
    private $nameDirify;
    /** @var string */
    private $googleId;

    /**
     * ItemResponse constructor.
     * @param int $id
     * @param string $googleIdDirify
     * @param string $nameDirify
     */
    public function __construct(int $id, string $googleId, string $googleIdDirify, string $nameDirify)
    {
        $this->id = $id;
        $this->googleIdDirify = $googleIdDirify;
        $this->nameDirify = $nameDirify;
        $this->googleId = $googleId;
    }

    /**
     * @return string
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function googleIdDirify(): string
    {
        return $this->googleIdDirify;
    }

    /**
     * @return string
     */
    public function nameDirify(): string
    {
        return $this->nameDirify;
    }

    /**
     * @return string
     */
    public function googleId(): string
    {
        return $this->googleId;
    }
}
