<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Find;

use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;

class FindItemDownloadsQueryHandler implements QueryHandler
{

    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(FindItemDownloadsQuery $query): ItemDownloadsResponse
    {
        $itemId = new ItemId($query->id());

        return apply(new ItemDownloadsResponseConverter(),[$this->itemRepository->totalDownloads($itemId)]);
    }
}
