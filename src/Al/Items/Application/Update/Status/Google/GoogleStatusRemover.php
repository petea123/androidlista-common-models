<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Google;

use App\Al\Items\Domain\Service\ItemFinder;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;

final class GoogleStatusRemover
{

    /**
     * @var ItemFinder
     */
    private $finder;
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    public function __construct(ItemRepositoryInterface $repository, DomainEventPublisher $publisher)
    {
        $this->finder = new ItemFinder($repository);
        $this->repository = $repository;
        $this->publisher = $publisher;
    }

    public function __invoke(ItemId $id)
    {
        $item = $this->finder->search($id);
        if ($item->isPublishedGoogle()) {
            $item->googlePlayRemoved();
            $this->repository->save($item);
            $this->repository->forFree($id);
            $this->publisher->record(...$item->pullDomainEvents());
        }
    }
}
