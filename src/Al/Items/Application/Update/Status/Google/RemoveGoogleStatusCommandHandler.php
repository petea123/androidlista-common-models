<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Google;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class RemoveGoogleStatusCommandHandler implements CommandHandler
{
    /**
     * @var GoogleStatusRemover
     */
    private $remover;

    public function __construct(GoogleStatusRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveGoogleStatusCommand $command)
    {
        $id = new ItemId($command->id());
        apply($this->remover, [$id]);
    }
}
