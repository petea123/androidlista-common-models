<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Google;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class PublishGoogleStatusCommandHandler implements CommandHandler
{
    /**
     * @var GoogleStatusPublisher
     */
    private $publisher;

    public function __construct(GoogleStatusPublisher $publisher)
    {
        $this->publisher = $publisher;
    }

    public function __invoke(PublishGoogleStatusCommand $command)
    {
        $id = new ItemId($command->id());
        apply($this->publisher, [$id]);
    }
}
