<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Google;

use App\Shared\Domain\Bus\Command\Command;

final class RemoveGoogleStatusCommand extends Command
{

    /**
     * @var int
     */
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
