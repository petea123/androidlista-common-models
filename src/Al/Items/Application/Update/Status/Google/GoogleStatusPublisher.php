<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Google;

use App\Al\Items\Domain\Service\ItemFinder;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;

final class GoogleStatusPublisher
{

    /**
     * @var ItemFinder
     */
    private $finder;
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;

    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->finder = new ItemFinder($repository);
        $this->repository = $repository;
    }

    public function __invoke(ItemId $id)
    {
        $item = $this->finder->search($id);
        if (!$item->isPublishedGoogle()) {
            $item->publishedGooglePlay();
            $this->repository->save($item);
        }
    }
}
