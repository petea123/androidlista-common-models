<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Relevant\ByPackageId;

use App\Al\Items\Domain\Service\ItemFinder;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\ValueObject\LocaleValueObject;

final class RelevantMarkerByPackageIdAndLanguage
{
    /**
     * @var ItemFinder
     */
    private $finder;
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    public function __construct(ItemRepositoryInterface $repository, DomainEventPublisher $publisher)
    {
        $this->finder = new ItemFinder($repository);
        $this->repository = $repository;
        $this->publisher = $publisher;
    }

    public function __invoke(ItemGoogle $google, LocaleValueObject $locale): void
    {
        $item = $this->finder->searchByPackage($google);
        if (!$item->is404($locale)) {
            $item->markRelevant($locale);
            $item->updateItem();

            $this->repository->save($item);
            $this->publisher->record(...$item->pullDomainEvents());
        }
    }
}
