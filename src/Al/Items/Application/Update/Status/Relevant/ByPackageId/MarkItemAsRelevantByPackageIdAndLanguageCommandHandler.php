<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Relevant\ByPackageId;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\Bus\Command\AsyncCommandHandler;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\apply;

final class MarkItemAsRelevantByPackageIdAndLanguageCommandHandler implements AsyncCommandHandler
{
    /**
     * @var RelevantMarkerByPackageIdAndLanguage
     */
    private $marker;

    public function __construct(RelevantMarkerByPackageIdAndLanguage $marker)
    {
        $this->marker = $marker;
    }

    public function __invoke(MarkItemAsRelevantByPackageIdAndLanguageCommand $command): void
    {
        $package = ItemGoogle::createFromPackage($command->packageId());
        $locale = LocaleValueObject::fromString($command->locale());

        apply($this->marker, [$package, $locale]);
    }

    public static function command(): array
    {
        return [MarkItemAsRelevantByPackageIdAndLanguageCommand::class];
    }
}
