<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Relevant\ByPackageId;

use App\Shared\Domain\Bus\Command\AsyncCommand;

final class MarkItemAsRelevantByPackageIdAndLanguageCommand extends AsyncCommand
{

    /**
     * @var string
     */
    private $packageId;
    /**
     * @var string
     */
    private $locale;

    public function __construct(string $packageId, string $locale)
    {
        $this->packageId = $packageId;
        $this->locale = $locale;
    }

    public static function commandName(): string
    {
        return 'MarkItemAsRelevantByPackageIdAndLanguageCommand';
    }

    public static function fromPrimitives(array $body, string $eventId, string $occurredOn): AsyncCommand
    {
        return new self($body['packageId'], $body['language']);
    }

    /**
     * @return string
     */
    public function packageId(): string
    {
        return $this->packageId;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }
}
