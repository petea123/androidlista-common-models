<?php


namespace App\Al\Items\Application\Update\Status\Apk;

use App\Al\Apks\Domain\Event\ApkWasUploaded;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class PublishApkStatusOnApkUploaded implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasUploaded $event)
    {
        $this->commandBus->handle(new PublishApkStatusCommand(
            $event->itemId()
        ));
    }

    public static function subscribedTo(): array
    {
        return [ApkWasUploaded::class];
    }
}
