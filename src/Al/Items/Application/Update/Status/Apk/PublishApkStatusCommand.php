<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Apk;

use App\Shared\Domain\Bus\Command\Command;

final class PublishApkStatusCommand extends Command
{

    /**
     * @var int
     */
    private $itemId;

    public function __construct(int $itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }
}
