<?php


namespace App\Al\Items\Application\Update\Status\Apk;

use App\Al\Apks\Domain\Event\ApkWasDangerous;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class UnpublishApkStatusOnApkDangerous implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {

        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasDangerous $event)
    {
        $this->commandBus->handle(new UnpublishApkStatusCommand(
            $event->itemId()
        ));
    }

    public static function subscribedTo(): array
    {
        return [
            ApkWasDangerous::class
        ];
    }
}
