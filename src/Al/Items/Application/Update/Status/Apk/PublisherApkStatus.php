<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Apk;

use App\Al\Items\Domain\Service\ItemFinder;
use App\Al\Items\Domain\Service\ItemPublisherStatusValidator;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Al\Shared\Domain\Items\ItemId;

final class PublisherApkStatus
{

    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var ItemFinder
     */
    private $finder;
    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    /** @var ItemPublisherStatusValidator */
    private $publishValidator;

    /** @var DomainEventPublisher */
    private $publisher;

    public function __construct(
        ItemRepositoryInterface $itemRepository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher
    ) {
        $this->queryBus = $queryBus;
        $this->finder = new ItemFinder($itemRepository);
        $this->publishValidator = new ItemPublisherStatusValidator($queryBus);
        $this->itemRepository = $itemRepository;
        $this->publisher = $publisher;
    }

    public function __invoke(ItemId $id)
    {
        $item = $this->finder->search($id);

        $this->publishValidator->ensureHaveProcessedApks($id);

        $item->publishApk();

        $this->itemRepository->save($item);

        $this->publisher->record(...$item->pullDomainEvents());
    }
}
