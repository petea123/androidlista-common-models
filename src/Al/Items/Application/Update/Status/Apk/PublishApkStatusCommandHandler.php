<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\Status\Apk;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Al\Shared\Domain\Items\ItemId;
use function Lambdish\Phunctional\apply;

final class PublishApkStatusCommandHandler implements CommandHandler
{

    /**
     * @var PublisherApkStatus
     */
    private $publisher;

    public function __construct(PublisherApkStatus $publisher)
    {
        $this->publisher = $publisher;
    }

    public function __invoke(PublishApkStatusCommand $command)
    {
        $id = new ItemId($command->itemId());

        apply($this->publisher, [$id]);
    }
}
