<?php

declare(strict_types=1);


namespace App\Al\Items\Application\Update\Status\Apk;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Al\Shared\Domain\Items\ItemId;
use function Lambdish\Phunctional\apply;

final class UnpublishApkStatusCommandHandler implements CommandHandler
{

    /**
     * @var UnpublisherApkStatus
     */
    private $unpublisher;

    public function __construct(UnpublisherApkStatus $unpublisher)
    {
        $this->unpublisher = $unpublisher;
    }

    public function __invoke(UnpublishApkStatusCommand $command)
    {
        $id = new ItemId($command->itemId());

        apply($this->unpublisher, [$id]);
    }

}
