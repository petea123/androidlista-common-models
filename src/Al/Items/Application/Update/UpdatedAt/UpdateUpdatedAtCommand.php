<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\UpdatedAt;

use App\Shared\Domain\Bus\Command\Command;

final class UpdateUpdatedAtCommand extends Command
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $updatedAt;

    public function __construct(int $id, int $updatedAt)
    {
        $this->id = $id;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function updatedAt(): int
    {
        return $this->updatedAt;
    }
}
