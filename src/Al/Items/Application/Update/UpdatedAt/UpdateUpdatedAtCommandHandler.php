<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\UpdatedAt;

use App\Al\Items\Domain\ItemUpdatedAt;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class UpdateUpdatedAtCommandHandler implements CommandHandler
{
    /**
     * @var UpdatedAtUpdater
     */
    private $updater;

    public function __construct(UpdatedAtUpdater $updater)
    {
        $this->updater = $updater;
    }

    public function __invoke(UpdateUpdatedAtCommand $command): void
    {
        $id = new ItemId($command->id());
        $updatedAt = ItemUpdatedAt::createFromTimestamp($command->updatedAt());

        apply($this->updater, [$id, $updatedAt]);
    }

}
