<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\UpdatedAt;

use App\Al\Items\Domain\ItemUpdatedAt;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Shared\Domain\Items\ItemId;

final class UpdatedAtUpdater
{
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;

    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ItemId $id, ItemUpdatedAt $updatedAt): void
    {
        $item = $this->repository->search($id);

        if (null !== $item && $updatedAt->isAfter($item->updatedAt())) {
            $item->updateItem($updatedAt);
            $this->repository->save($item);
        }
    }
}
