<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Update\UpdatedAt;

use App\Al\Apks\Domain\Event\ApkWasUploaded;
use App\Al\Items\Domain\Event\ItemMarkedAsRelevant;
use App\Al\Items\Domain\Event\ItemMarkedAsTopDownload;
use App\Al\ItemsDescriptions\Domain\AuthorPhrase\ItemDescriptionAuthorPhraseWasValidated;
use App\Al\ItemsDescriptions\Domain\MainBullets\ItemDescriptionMainBulletWasValidated;
use App\Al\ItemsDescriptions\Domain\NegativePoints\ItemDescriptionNegativePointWasValidated;
use App\Al\ItemsDescriptions\Domain\Permissions\ItemDescriptionPermissionsWasValidated;
use App\Al\ItemsDescriptions\Domain\PositivePoints\ItemDescriptionPositivePointWasValidated;
use App\Al\ItemsDescriptions\Domain\Provider\ItemDescriptionProviderWasValidated;
use App\Al\ItemsDescriptions\Domain\Specialist\ItemDescriptionSpecialistWasValidated;
use App\Al\ItemsDescriptions\Domain\UsefulTips\ItemDescriptionUsefulTipWasValidated;
use App\Al\ItemsGoogle\Domain\ItemGoogleAppVersionWasUpdated;
use App\Al\ItemsGoogle\Domain\ItemGoogleDateWasUpdated;
use App\Al\ItemsImages\Domain\Icon\ItemImageIconWasUpdated;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotWasAdded;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotWasRemoved;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class UpdateUpdatedAtOnDomainEvent implements DomainEventSubscriber
{
    /**
     * @var CommandBus
     */
    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function __invoke(DomainEvent $event): void
    {
        $this->bus->handle(new UpdateUpdatedAtCommand($this->itemId($event), (int)$event->occurredOn()));
    }

    public static function subscribedTo(): array
    {
        return [
            ItemImageIconWasUpdated::class,
            ItemImageScreenshotWasAdded::class,
            ItemImageScreenshotWasRemoved::class,
            ApkWasUploaded::class,
            ItemMarkedAsRelevant::class,
            ItemMarkedAsTopDownload::class,
            ItemDescriptionMainBulletWasValidated::class,
            ItemDescriptionPositivePointWasValidated::class,
            ItemDescriptionNegativePointWasValidated::class,
            ItemDescriptionProviderWasValidated::class,
            ItemDescriptionSpecialistWasValidated::class,
            ItemDescriptionPermissionsWasValidated::class,
            ItemDescriptionAuthorPhraseWasValidated::class,
            ItemDescriptionUsefulTipWasValidated::class,
            ItemGoogleDateWasUpdated::class,
            ItemGoogleAppVersionWasUpdated::class
        ];
    }

    private function itemId(DomainEvent $event): int
    {
        switch ($event::eventName()) {
            case 'ItemMarkedAsRelevant':
            case 'ItemMarkedAsTopDownload':
                return (int)$event->aggregateId();
            case 'ItemDescriptionMainBulletWasValidated':
            case 'ItemDescriptionPositivePointWasValidated':
            case 'ItemDescriptionNegativePointWasValidated':
            case 'ItemDescriptionProviderWasValidated':
            case 'ItemDescriptionSpecialistWasValidated':
            case 'ItemDescriptionPermissionsWasValidated':
            case 'ItemDescriptionAuthorPhraseWasValidated':
            case 'ItemDescriptionUsefulTipWasValidated':
            case 'ItemGoogleDateWasUpdated':
            case 'ItemGoogleAppVersionWasUpdated':
            default:
                return $event->itemId();
        }
    }
}
