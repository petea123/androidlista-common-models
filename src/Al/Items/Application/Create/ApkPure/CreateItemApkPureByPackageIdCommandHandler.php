<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Create\ApkPure;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Shared\Domain\Bus\Command\AsyncCommandHandler;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class CreateItemApkPureByPackageIdCommandHandler implements CommandHandler, AsyncCommandHandler
{
    /**
     * @var ItemApkPureCreator
     */
    private $creator;

    public function __construct(ItemApkPureCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(CreateItemApkPureByPackageIdCommand $command): void
    {
        $package = ItemApkPurePackage::createFromPackage($command->packageId());
        apply($this->creator, [$package]);
    }

    public static function command(): array
    {
        return [CreateItemApkPureByPackageIdCommand::class];
    }
}
