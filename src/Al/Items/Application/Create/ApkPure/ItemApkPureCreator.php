<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Create\ApkPure;

use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Apks\Infrastructure\WebParser\ApkPureWebParser;
use App\Al\Items\Domain\ApkPure\ItemApkPure;
use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;

final class ItemApkPureCreator
{

    /**
     * @var WebParserFactory
     */
    private $webParserFactory;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;
    /**
     * @var ItemRepositoryInterface
     */
    private $repository;

    public function __construct(
        ItemRepositoryInterface $repository,
        WebParserFactory $webParserFactory,
        DomainEventPublisher $publisher
    ) {
        $this->webParserFactory = $webParserFactory;
        $this->publisher = $publisher;
        $this->repository = $repository;
    }

    public function __invoke(ItemApkPurePackage $package): void
    {
        if ($item = $this->repository->searchByPackage($package)) {
            echo $package->id()."\n";
            return;
        }

        /** @var ApkPureWebParser $parser */
        $parser = $this->webParserFactory->instance(ApkSource::apkpure(), $package);
        if (null === $parser) {
            echo 'null parser'."\n";
            return;
        }

        $names = $parser->names();
        $votes = $parser->votes();
        $note = $parser->rating();
        $price = $parser->price();
        $size = $parser->size();
        $minOsVersion = $parser->osVersion();
        $provider = $parser->provider();
        $categories = $parser->category();
        $officialUrl = $parser->officialUrl();
        $version = $parser->lastVersion();

        $item = ItemApkPure::create(
            $this->repository->nextId(),
            ItemGoogle::createFromPackage($package->id()),
            $names,
            $package,
            $votes,
            $note,
            $price,
            $size,
            $minOsVersion,
            $provider,
            $officialUrl,
            $version,
            $categories
        );

        $this->repository->save($item);
        $this->publisher->record(...$item->pullDomainEvents());
    }
}
