<?php

declare(strict_types=1);

namespace App\Al\Items\Application\Create\ApkPure;

use App\Shared\Domain\Bus\Command\AsyncCommand;

final class CreateItemApkPureByPackageIdCommand extends AsyncCommand
{
    /**
     * @var string
     */
    private $packageId;

    public function __construct(string $packageId)
    {
        $this->packageId = $packageId;
    }

    /**
     * @return string
     */
    public function packageId(): string
    {
        return $this->packageId;
    }

    public static function commandName(): string
    {
        return 'CreateItemApkPureByPackageIdCommand';
    }

    public static function fromPrimitives(array $body, string $eventId, string $occurredOn): AsyncCommand
    {
        return new self($body['packageId']);
    }
}
