<?php

declare(strict_types=1);

namespace App\Al\Items\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;

final class ItemCategory extends AggregateRoot
{

    private $id;
    /**
     * @var Item
     */
    private $item;
    /**
     * @var int
     */
    private $categoryId;


    public function __construct(Item $item, int $categoryId)
    {
        $this->item = $item;
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function categoryId(): int
    {
        return $this->categoryId;
    }
}
