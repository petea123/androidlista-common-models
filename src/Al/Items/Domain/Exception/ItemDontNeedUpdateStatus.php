<?php

declare(strict_types=1);


namespace App\Al\Items\Domain\Exception;

use App\Shared\Domain\DomainError;

final class ItemDontNeedUpdateStatus extends DomainError
{

    public function __construct()
    {
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_dont_need_update_status';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item dont need update status');
    }
}
