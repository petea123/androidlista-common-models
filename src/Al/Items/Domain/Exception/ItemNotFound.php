<?php

declare(strict_types=1);


namespace App\Al\Items\Domain\Exception;

use App\Shared\Domain\DomainError;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemNotFound extends DomainError
{

    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_not_found';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item <%s> has not been found', $this->id->value());
    }
}