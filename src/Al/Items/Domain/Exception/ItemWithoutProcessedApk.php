<?php

declare(strict_types=1);


namespace App\Al\Items\Domain\Exception;

use App\Shared\Domain\DomainError;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemWithoutProcessedApk extends DomainError
{

    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_have_not_processed_apks';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item <%s> has not processed apks', $this->id->value());
    }
}