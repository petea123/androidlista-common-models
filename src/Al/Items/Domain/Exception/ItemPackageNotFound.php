<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\Exception;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\DomainError;

final class ItemPackageNotFound extends DomainError
{

    private $package;

    public function __construct(ItemGoogle $package)
    {
        $this->package = $package;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_package_not_found';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item with package <%s> and url <%s> has not been found', $this->package->id(),
            $this->package->value());
    }
}
