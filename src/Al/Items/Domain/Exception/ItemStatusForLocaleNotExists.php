<?php

declare(strict_types=1);


namespace App\Al\Items\Domain\Exception;

use App\Shared\Domain\DomainError;

final class ItemStatusForLocaleNotExists extends DomainError
{

    /** @var string */
    private $locale;

    public function __construct(string $locale)
    {
        $this->locale = $locale;
        parent::__construct();

    }

    public function errorCode(): string
    {
        return 'item_status_for_locale_not_exists';
    }

    protected function errorMessage(): string
    {
        return sprintf('The status for locale <%s>', $this->locale);
    }
}
