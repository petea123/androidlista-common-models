<?php

declare(strict_types=1);


namespace App\Al\Items\Domain\Exception;

use App\Shared\Domain\DomainError;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemWithProcessedApk extends DomainError
{

    private $id;

    public function __construct(ItemId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'item_have_processed_apks';
    }

    protected function errorMessage(): string
    {
        return sprintf('The item <%s> has processed apks', $this->id->value());
    }
}