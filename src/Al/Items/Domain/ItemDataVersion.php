<?php

declare(strict_types=1);

namespace App\Al\Items\Domain;

use App\Shared\Domain\Utils;

final class ItemDataVersion extends ItemData
{
    /**
     * @var string
     */
    private $value;

    public function __construct(Item $item, string $value)
    {
        parent::__construct(
            $item,
            hash('md5', $value),
            Utils::mydirify($value)
        );
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}
