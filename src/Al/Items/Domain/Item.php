<?php

declare(strict_types=1);

namespace App\Al\Items\Domain;

use App\Al\Items\Domain\Event\ItemApkStatusWasPublished;
use App\Al\Items\Domain\Event\ItemApkStatusWasUpdated;
use App\Al\Items\Domain\Event\ItemGooglePlayWasRemoved;
use App\Al\Items\Domain\Event\ItemMarkedAsRelevant;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use Doctrine\Common\Collections\ArrayCollection;

// TODO Crear ItemApkPure heredando
class Item extends AggregateRoot
{

    /** @var ItemId */
    protected $id;

    /** @var ItemStatus */
    protected $statuses;

    /** @var ItemGoogle */
    private $google;

    /** @var ItemName */
    private $name;

    /** @var ItemUpdatedAt */
    private $updatedAt;
    /**
     * @var ItemCreatedAt
     */
    private $createdAt;
    /**
     * @var int
     */
    private $votes;
    /**
     * @var float
     */
    private $note;

    private $project_id = 1;
    private $type_id = 1;
    private $nr_comments = 0;
    private $nr_views = 0;
    private $ranking = 0;
    private $parent_id = 0;
    private $country_code = '';
    private $poblation_id = 0;
    private $latitude = 0;
    private $longitude = 0;

    private $itemData;

    private $categories;

    public function __construct(
        ItemId $id,
        ItemStatus $statuses,
        ItemGoogle $google,
        ItemName $name,
        ItemUpdatedAt $updatedAt,
        ItemCreatedAt $createdAt,
        int $votes,
        float $note
    ) {
        $this->id = $id;
        $this->statuses = $statuses;
        $this->google = $google;
        $this->name = $name;
        $this->updatedAt = $updatedAt;
        $this->createdAt = $createdAt;
        $this->votes = $votes;
        $this->note = $note;
        $this->itemData = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return ItemId
     */
    public function id(): ItemId
    {
        return $this->id;
    }

    public function unpublishApk(): void
    {
        $this->statuses = $this->statuses->unpublishApk();
        $this->updateItem();

        $this->record(
            new ItemApkStatusWasUpdated(
                (string)$this->id->value(),
                'unpublish'
            )
        );
    }

    public function publishApk(): void
    {

        $this->statuses = $this->statuses->publishApk();
        $this->updateItem();

        $this->record(
            new ItemApkStatusWasPublished((string)$this->id->value())
        );
    }

    public function googlePlayRemoved(): void
    {
        $this->statuses = $this->statuses->googlePlayRemoved();
        $this->updateItem();

        $this->record(
            new ItemGooglePlayWasRemoved((string)$this->id->value())
        );
    }

    public function publishedGooglePlay(): void
    {
        $this->statuses = $this->statuses->publishedGooglePlay();
        $this->updateItem();
    }

    public function isPublishedGoogle(): bool
    {
        return $this->statuses->isPublishedGoogle();
    }

    /**
     * @return ItemGoogle
     */
    public function google(): ItemGoogle
    {
        return $this->google;
    }

    public function packageId(): string
    {
        return $this->google->id();
    }

    /**
     * @return ItemName
     */
    public function name(): ItemName
    {
        return $this->name;
    }

    /**
     * @return ItemStatus
     */
    public function statuses(): ItemStatus
    {
        return $this->statuses;
    }

    /**
     * @return ItemUpdatedAt
     */
    public function updatedAt(): ItemUpdatedAt
    {
        return $this->updatedAt;
    }

    public function updateItem(ItemUpdatedAt $updatedAt = null): void
    {
        $this->updatedAt = $updatedAt ?: ItemUpdatedAt::now();
    }

    /**
     * @return int
     */
    public function votes(): int
    {
        return $this->votes;
    }

    /**
     * @return float
     */
    public function note(): float
    {
        return $this->note;
    }

    public function addData(ItemData ...$datas): self
    {
        foreach ($datas as $val) {
            if (!$this->itemData->contains($val)) {
                $this->itemData[] = $val;
            }
        }

        return $this;
    }

    public function addCategory(ItemCategory ...$categories): self
    {
        foreach ($categories as $category) {
            if (!$this->categories->contains($category)) {
                $this->categories[] = $category;
            }
        }

        return $this;
    }

    public function price(): ?ItemDataPrice
    {
        foreach ($this->itemData as $data) {
            if ($data instanceof ItemDataPrice) {
                return $data;
            }
        }

        return null;
    }

    public function size(): ?ItemDataSize
    {
        foreach ($this->itemData as $data) {
            if ($data instanceof ItemDataSize) {
                return $data;
            }
        }

        return null;
    }

    public function minOsVersion(): ?ItemDataMinOsVersion
    {
        foreach ($this->itemData as $data) {
            if ($data instanceof ItemDataMinOsVersion) {
                return $data;
            }
        }

        return null;
    }

    public function provider(): ?ItemDataProvider
    {
        foreach ($this->itemData as $data) {
            if ($data instanceof ItemDataProvider) {
                return $data;
            }
        }

        return null;
    }

    public function officialUrl(): ?ItemDataOfficialUrl
    {
        foreach ($this->itemData as $data) {
            if ($data instanceof ItemDataOfficialUrl) {
                return $data;
            }
        }

        return null;
    }

    public function version(): ?ItemDataVersion
    {
        foreach ($this->itemData as $data) {
            if ($data instanceof ItemDataVersion) {
                return $data;
            }
        }

        return null;
    }

    public function categories(): array
    {
        return $this->categories->toArray();
    }

    public function markRelevant(LocaleValueObject $locale): void
    {
        $this->statuses = $this->statuses->markRelevant($locale);
        $this->record(
            new ItemMarkedAsRelevant(
                (string)$this->id->value(),
                $locale->value()
            )
        );
    }

    public function is404(LocaleValueObject $locale): bool
    {
        return $this->statuses->is404($locale);
    }
}
