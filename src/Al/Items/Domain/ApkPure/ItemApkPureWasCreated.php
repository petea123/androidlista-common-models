<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\ApkPure;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class ItemApkPureWasCreated extends DomainEvent
{

    public function __construct(
        string $aggregateId,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
    }

    public static function eventName(): string
    {
        return 'ItemApkPureWasCreated';
    }

    public function toPrimitives(): array
    {
        return [];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return ItemApkPure::class;
    }
}
