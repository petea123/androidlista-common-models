<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\ApkPure;

use App\Al\Items\Domain\Event\ItemMarkedAsRelevant;
use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ItemCategory;
use App\Al\Items\Domain\ItemCreatedAt;
use App\Al\Items\Domain\ItemDataMinOsVersion;
use App\Al\Items\Domain\ItemDataOfficialUrl;
use App\Al\Items\Domain\ItemDataPrice;
use App\Al\Items\Domain\ItemDataProvider;
use App\Al\Items\Domain\ItemDataSize;
use App\Al\Items\Domain\ItemDataVersion;
use App\Al\Items\Domain\ItemUpdatedAt;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\Items\Domain\ValueObject\ItemStatus;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\map;

final class ItemApkPure extends Item
{
    /**
     * @var ItemApkPurePackage
     */
    private $package;

    public function __construct(
        ItemId $id,
        ItemStatus $statuses,
        ItemGoogle $google,
        ItemName $name,
        ItemUpdatedAt $updatedAt,
        ItemCreatedAt $createdAt,
        int $votes,
        float $note,
        ItemApkPurePackage $package
    ) {
        parent::__construct($id, $statuses, $google, $name, $updatedAt, $createdAt, $votes, $note);
        $this->package = $package;
    }

    public static function create(
        ItemId $id,
        ItemGoogle $google,
        ItemName $name,
        ItemApkPurePackage $package,
        int $votes,
        float $note,
        float $price,
        string $size,
        string $minOsVersion,
        string $provider,
        string $officialUrl,
        string $version,
        array $categories
    ): ItemApkPure {
        $item = new self(
            $id,
            ItemStatus::createFromApkPure(),
            $google,
            $name,
            ItemUpdatedAt::now(),
            ItemCreatedAt::now(),
            $votes === 0 ? 2000 : $votes,
            $note === floor(0) ? 8 : $note,
            $package
        );

        $price = new ItemDataPrice($item, $price);
        $size = new ItemDataSize($item, $size);
        $minOsVersion = new ItemDataMinOsVersion($item, $minOsVersion);
        $provider = new ItemDataProvider($item, $provider);
        $officialUrl = new ItemDataOfficialUrl($item, $officialUrl);
        $version = new ItemDataVersion($item, $version);
        $categories = map(function ($category) use ($item) {
            return new ItemCategory($item, $category);
        }, $categories);

        $item->addData($price, $size, $minOsVersion, $provider, $officialUrl, $version);
        $item->addCategory(...$categories);

        $item->record(new ItemApkPureWasCreated((string)$id->value()));

        return $item;
    }

    public function packageId(): string
    {
        return $this->package->id();
    }

    public function markRelevant(LocaleValueObject $locale): void
    {
        if (!$this->statuses->isApkPublished()) {
            throw new \DomainException(
                sprintf(
                    'No marked as relevant, no apk published for package: %s',
                    $this->package->id()
                )
            );
        }

        parent::markRelevant($locale);
    }
}
