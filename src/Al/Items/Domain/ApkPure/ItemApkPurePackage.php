<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\ApkPure;

use App\Shared\Domain\ValueObject\PackageValueObject;

final class ItemApkPurePackage extends PackageValueObject
{

    public static function createFromPackage(string $package): self
    {
        return new self('https://apkpure.com/br/black-desert-mobile/'.$package);
    }

    public static function extractPackage(string $value): string
    {
        return self::extractId($value);
    }

    protected function regex(): string
    {
        return "/black-desert-mobile\/(\d+|[A-Za-z0-9_\.]+)/i";
    }
}
