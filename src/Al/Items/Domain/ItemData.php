<?php

declare(strict_types=1);

namespace App\Al\Items\Domain;

use App\Al\Shared\Domain\Items\ItemId;

abstract class ItemData
{
    private $id;

    /**
     * @var Item
     */
    protected $item;

    private $type_id = 1;
    /**
     * @var string
     */
    private $hash;
    /**
     * @var string
     */
    private $valueDirify;

    public function __construct(Item $item, string $hash, string $valueDirify)
    {
        $this->item = $item;
        $this->hash = $hash;
        $this->valueDirify = $valueDirify;
    }

}
