<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\Service;

use App\Al\Items\Domain\Exception\ItemNotFound;
use App\Al\Items\Domain\Exception\ItemPackageNotFound;
use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemFinder
{


    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;

    public function __construct(ItemRepositoryInterface $itemRepository)
    {

        $this->itemRepository = $itemRepository;
    }

    public function search(ItemId $id): Item
    {
        $item = $this->itemRepository->search($id);
        $this->guard($id, $item);

        return $item;
    }

    public function searchByPackage(ItemGoogle $package): Item
    {
        $item = $this->itemRepository->searchByPackage($package);

        if (null === $item) {
            throw new ItemPackageNotFound($package);
        }

        return $item;
    }

    /**
     * @param ItemId $id
     * @param Item|null $item
     */
    private function guard(ItemId $id, Item $item = null): void
    {
        if (null === $item) {
            throw new ItemNotFound($id);
        }
    }
}