<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\Service;

use App\Al\Items\Infrastructure\Projections\ItemView;

interface ItemReadModelRepositoryInterface
{
    public function searchByLanguage(int $id, string $language): ?ItemView;
}