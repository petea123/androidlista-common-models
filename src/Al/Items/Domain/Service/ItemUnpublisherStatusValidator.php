<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\Service;

use App\Al\Apks\Application\Search\ApksResponse;
use App\Al\Apks\Application\Search\ByCriteria\SearchApksByCriteriaQuery;
use App\Al\Apks\Domain\ValueObject\State;
use App\Al\Items\Domain\Exception\ItemWithProcessedApk;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemUnpublisherStatusValidator
{

    private $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function ensureHaveNotProcessedApks(ItemId $id): void
    {
        /** @var ApksResponse $apks */
        $apks = $this->queryBus->ask(new SearchApksByCriteriaQuery(
            [
                [
                    'field' => 'itemId' ,
                    'operator' => '=',
                    'value' => (string)$id->value()
                ],
                [
                    'field' => 'state.value',
                    'operator' => '=',
                    'value' => (string)State::processed()->value()
                ]
            ]
        ));

        if (!empty($apks->apks())) {
            throw new ItemWithProcessedApk($id);
        }
    }
}
