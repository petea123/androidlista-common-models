<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\Service;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\ValueObject\ItemDownloads;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\PackageValueObject;

interface ItemRepositoryInterface
{
    public function search(ItemId $id): ?Item;

    public function save(Item $item): void;

    public function totalDownloads(ItemId $id): ItemDownloads;

    public function searchByPackage(PackageValueObject $package): ?Item;

    public function searchPrice(ItemId $id): float;

    public function forFree(ItemId $id): void;

    public function searchValid(int $from = 1, int $to = 1, ItemId $id = null): array;

    public function nextId(): ItemId;
}
