<?php

declare(strict_types=1);

namespace App\Al\Items\Domain;

final class ItemDataPrice extends ItemData
{
    /**
     * @var float
     */
    private $value;

    public function __construct(Item $item, float $value)
    {
        $string = number_format($value, 2);
        parent::__construct(
            $item,
            hash('md5', $string),
            $string
        );
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function value(): float
    {
        return $this->value;
    }
}
