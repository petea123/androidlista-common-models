<?php

namespace App\Al\Items\Domain\Event;

use App\Al\Items\Domain\Item;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ItemMarkedAsTopDownload extends DomainEvent
{

    public function __construct(
        string $aggregateId,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
    }

    public static function eventName(): string
    {
        return 'ItemMarkedAsTopDownload';
    }

    public function toPrimitives(): array
    {
        return [];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return Item::class;
    }
}
