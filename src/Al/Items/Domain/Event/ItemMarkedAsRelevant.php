<?php

namespace App\Al\Items\Domain\Event;

use App\Al\Items\Domain\Item;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ItemMarkedAsRelevant extends DomainEvent
{

    /**
     * @var string
     */
    private $locale;

    public function __construct(
        string $aggregateId,
        string $locale,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->locale = $locale;
    }

    public static function eventName(): string
    {
        return 'ItemMarkedAsRelevant';
    }

    public function toPrimitives(): array
    {
        return [
            'locale' => $this->locale
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['locale'], $aggregateVersion, $eventId, $occurredOn);
    }

    public static function stream(): string
    {
        return Item::class;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }
}
