<?php

namespace App\Al\Items\Domain\Event;

use App\Al\Items\Domain\Item;
use App\Shared\Domain\Bus\Event\DomainEvent;


final class ItemApkImportStarted extends DomainEvent
{

    /** @var string */
    private $apkId;
    /** @var string */
    private $externalUrl;
    /** @var string */
    private $source;

    public function __construct(
        string $aggregateId,
        string $apkId,
        string $externalUrl,
        string $source,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);

        $this->apkId = $apkId;
        $this->externalUrl = $externalUrl;
        $this->source = $source;
    }

    public static function eventName(): string
    {
        return 'ItemApkImportStarted';
    }

    public function toPrimitives(): array
    {
        return [
            'apkId' => $this->apkId,
            'externalUrl' => $this->externalUrl,
            'source' => $this->source
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['apkId'], $body['externalUrl'], $body['source'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return string
     */
    public function apkId(): string
    {
        return $this->apkId;
    }

    /**
     * @return string
     */
    public function externalUrl(): string
    {
        return $this->externalUrl;
    }

    /**
     * @return string
     */
    public function source(): string
    {
        return $this->source;
    }

    public static function stream(): string
    {
        return Item::class;
    }
}
