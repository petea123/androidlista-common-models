<?php

namespace App\Al\Items\Domain\Event;

use App\Al\Items\Domain\Item;
use App\Shared\Domain\Bus\Event\DomainEvent;


final class ItemApkStatusWasUpdated extends DomainEvent
{

    /** @var string */
    private $change;

    public function __construct(string $aggregateId, string $change, int $aggregateVersion = null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->change = $change;
    }

    public static function eventName(): string
    {
        return 'ItemApkStatusWasUpdated';
    }

    public function toPrimitives(): array
    {
        return ['change' => $this->change];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['change'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return string
     */
    public function change(): string
    {
        return $this->change;
    }

    public static function stream(): string
    {
        return Item::class;
    }
}
