<?php

declare(strict_types=1);

namespace App\Al\Items\Domain;

use App\Shared\Domain\ValueObject\DateValueObject;

final class ItemCreatedAt extends DateValueObject
{

}
