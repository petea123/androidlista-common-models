<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\ValueObject;

use App\Al\Items\Domain\Exception\ItemDontNeedUpdateStatus;
use App\Al\Items\Domain\Exception\ItemStatusForLocaleNotExists;
use App\Al\Items\Domain\Item;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use League\Flysystem\Adapter\Local;

final class ItemStatus
{
    const PUBLISHED = 1;
    const REMOVED = 4;
    const NO_INDEX = 512;
    const ADSENSE = 256;
    const APK_PUBLISH = 128;
    const GOOGLE_PLAY_REMOVED = 64;
    const APK_PURE = 1024;
    const RELEVANT = 16384;
    const REDIRECT_404 = 2048;

    /** @var int */
    private $statusBr;

    /** @var int */
    private $statusEn;

    /** @var int */
    private $statusFr;

    /** @var int */
    private $statusIt;

    /** @var int */
    private $statusDe;

    /** @var int */
    private $statusTr;

    /** @var int */
    private $statusNl;

    /** @var int */
    private $statusVn;

    /** @var int */
    private $statusPl;

    /** @var int */
    private $statusGr;

    /** @var int */
    private $statusTh;

    /** @var int */
    private $statusRo;

    /** @var int */
    private $statusJa;

    /** @var int */
    private $statusKo;

    /** @var int */
    private $statusRu;

    /** @var int */
    private $statusId;

    /** @var int */
    private $statusEs;

    /**
     * ItemStatus constructor.
     * @param int $statusBr
     * @param int $statusEn
     * @param int $statusFr
     * @param int $statusIt
     * @param int $statusDe
     * @param int $statusTr
     * @param int $statusNl
     * @param int $statusVn
     * @param int $statusPl
     * @param int $statusGr
     * @param int $statusTh
     * @param int $statusRo
     * @param int $statusJa
     * @param int $statusKo
     * @param int $statusRu
     * @param int $statusId
     * @param int $statusEs
     */
    public function __construct(
        int $statusBr,
        int $statusEn,
        int $statusFr,
        int $statusIt,
        int $statusDe,
        int $statusTr,
        int $statusNl,
        int $statusVn,
        int $statusPl,
        int $statusGr,
        int $statusTh,
        int $statusRo,
        int $statusJa,
        int $statusKo,
        int $statusRu,
        int $statusId,
        int $statusEs
    ) {
        $this->statusBr = $statusBr;
        $this->statusEn = $statusEn;
        $this->statusFr = $statusFr;
        $this->statusIt = $statusIt;
        $this->statusDe = $statusDe;
        $this->statusTr = $statusTr;
        $this->statusNl = $statusNl;
        $this->statusVn = $statusVn;
        $this->statusPl = $statusPl;
        $this->statusGr = $statusGr;
        $this->statusTh = $statusTh;
        $this->statusRo = $statusRo;
        $this->statusJa = $statusJa;
        $this->statusKo = $statusKo;
        $this->statusRu = $statusRu;
        $this->statusId = $statusId;
        $this->statusEs = $statusEs;
    }

    public static function createFromApkPure(): self
    {
        $status = new self(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        $status->addStatus(self::APK_PURE + self::GOOGLE_PLAY_REMOVED);
        return $status;
    }

    public function publishApk(): self
    {
        $updated = false;
            foreach (LocaleValueObject::values() as $locale) {
            $attribute = 'status'.ucfirst($locale);
            if (($this->$attribute&self::APK_PUBLISH) === 0) {
                $this->$attribute = $this->$attribute + self::APK_PUBLISH;
                $updated = true;
            }
        }

        $this->guardUpdated($updated);

        return new self(
            $this->statusBr,
            $this->statusEn,
            $this->statusFr,
            $this->statusIt,
            $this->statusDe,
            $this->statusTr,
            $this->statusNl,
            $this->statusVn,
            $this->statusPl,
            $this->statusGr,
            $this->statusTh,
            $this->statusRo,
            $this->statusJa,
            $this->statusKo,
            $this->statusRu,
            $this->statusId,
            $this->statusEs
        );
    }

    public function unpublishApk(): self
    {
        $updated = false;
        foreach (LocaleValueObject::values() as $locale) {
            $attribute = 'status'.ucfirst($locale);
            if(($this->$attribute&self::APK_PUBLISH) === self::APK_PUBLISH) {
                $this->$attribute = $this->$attribute - self::APK_PUBLISH;
                $updated = true;
            }
        }

        $this->guardUpdated($updated);

        return new self(
            $this->statusBr,
            $this->statusEn,
            $this->statusFr,
            $this->statusIt,
            $this->statusDe,
            $this->statusTr,
            $this->statusNl,
            $this->statusVn,
            $this->statusPl,
            $this->statusGr,
            $this->statusTh,
            $this->statusRo,
            $this->statusJa,
            $this->statusKo,
            $this->statusRu,
            $this->statusId,
            $this->statusEs
        );
    }

    public function googlePlayRemoved(): self
    {
        $this->addStatus(self::GOOGLE_PLAY_REMOVED);

        return new self(
            $this->statusBr,
            $this->statusEn,
            $this->statusFr,
            $this->statusIt,
            $this->statusDe,
            $this->statusTr,
            $this->statusNl,
            $this->statusVn,
            $this->statusPl,
            $this->statusGr,
            $this->statusTh,
            $this->statusRo,
            $this->statusJa,
            $this->statusKo,
            $this->statusRu,
            $this->statusId,
            $this->statusEs
        );
    }

    public function publishedGooglePlay(): self
    {
        $this->removeStatus(self::GOOGLE_PLAY_REMOVED);

        return new self(
            $this->statusBr,
            $this->statusEn,
            $this->statusFr,
            $this->statusIt,
            $this->statusDe,
            $this->statusTr,
            $this->statusNl,
            $this->statusVn,
            $this->statusPl,
            $this->statusGr,
            $this->statusTh,
            $this->statusRo,
            $this->statusJa,
            $this->statusKo,
            $this->statusRu,
            $this->statusId,
            $this->statusEs
        );
    }

    public function statusByLocale(string $locale): int
    {
        $attribute = 'status'.ucfirst($locale);
        return $this->$attribute;
    }

    private function guardUpdated(bool $updated)
    {
        if (!$updated) {
            throw new ItemDontNeedUpdateStatus();
        }
    }

    private function addStatus(int $status)
    {
        foreach (LocaleValueObject::values() as $locale) {
            $this->addStatusToLocale(LocaleValueObject::fromString($locale), $status);
        }
    }

    private function addStatusToLocale(LocaleValueObject $locale, int $status): void
    {
        $attribute = 'status'.ucfirst($locale->value());
        $this->$attribute = $this->$attribute|$status;
    }

    private function removeStatus(int $status)
    {
        foreach (LocaleValueObject::values() as $locale) {
            $this->removeStatusToLocale(LocaleValueObject::fromString($locale), $status);
        }
    }

    private function removeStatusToLocale(LocaleValueObject $locale, int $status): void
    {
        $attribute = 'status'.ucfirst($locale->value());
        $this->$attribute = $this->$attribute&(~$status);
    }

    public function markRelevant(LocaleValueObject $locale): self
    {
        $this->addStatusToLocale($locale, self::PUBLISHED);
        $this->addStatusToLocale($locale, self::ADSENSE);
        $this->addStatusToLocale($locale, self::RELEVANT);
        $this->removeStatusToLocale($locale, self::NO_INDEX);
        $this->removeStatusToLocale($locale, self::REMOVED);

        return new self(
            $this->statusBr,
            $this->statusEn,
            $this->statusFr,
            $this->statusIt,
            $this->statusDe,
            $this->statusTr,
            $this->statusNl,
            $this->statusVn,
            $this->statusPl,
            $this->statusGr,
            $this->statusTh,
            $this->statusRo,
            $this->statusJa,
            $this->statusKo,
            $this->statusRu,
            $this->statusId,
            $this->statusEs
        );
    }

    public function markAs404(): self
    {
        $this->addStatus(self::REDIRECT_404);

        return new self(
            $this->statusBr,
            $this->statusEn,
            $this->statusFr,
            $this->statusIt,
            $this->statusDe,
            $this->statusTr,
            $this->statusNl,
            $this->statusVn,
            $this->statusPl,
            $this->statusGr,
            $this->statusTh,
            $this->statusRo,
            $this->statusJa,
            $this->statusKo,
            $this->statusRu,
            $this->statusId,
            $this->statusEs
        );
    }

    public function isApkPublished(): bool
    {
        return $this->hasStatus(LocaleValueObject::random(), self::APK_PUBLISH);
    }

    public function isPublishedGoogle(): bool
    {
        return !$this->hasStatus(LocaleValueObject::random(), self::GOOGLE_PLAY_REMOVED);
    }

    public function is404(LocaleValueObject $locale):bool
    {
        return $this->hasStatus($locale, self::REDIRECT_404);
    }

    private function hasStatus(LocaleValueObject $locale, int $status): bool
    {
        $attribute = 'status'.ucfirst($locale->value());

        return ($this->$attribute&$status) === $status;
    }
}
