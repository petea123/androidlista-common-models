<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\ValueObject;

use App\Shared\Domain\ValueObject\PackageValueObject;

final class ItemGoogle extends PackageValueObject
{
    public static function createFromPackage(string $package): self
    {
        return new self('https://play.google.com/store/apps/details?id='.$package);
    }

    protected function regex(): string
    {
        return "/details\?id\=(\d+|[A-Za-z0-9_\.]+)/i";
    }
}
