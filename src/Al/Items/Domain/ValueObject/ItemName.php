<?php

declare(strict_types=1);

namespace App\Al\Items\Domain\ValueObject;

use App\Shared\Domain\Utils;
use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class ItemName extends StringValueObject
{
    /**
     * @var string
     */
    private $nameBr;
    /**
     * @var string
     */
    private $nameEs;
    /**
     * @var string
     */
    private $nameFr;
    /**
     * @var string
     */
    private $nameId;
    /**
     * @var string
     */
    private $nameIt;
    /**
     * @var string
     */
    private $nameDe;
    /**
     * @var string
     */
    private $nameTr;
    /**
     * @var string
     */
    private $nameRu;
    /**
     * @var string
     */
    private $nameJa;
    /**
     * @var string
     */
    private $nameKo;
    /**
     * @var string
     */
    private $namePl;
    /**
     * @var string
     */
    private $nameRo;
    /**
     * @var string
     */
    private $nameVn;
    /**
     * @var string
     */
    private $nameTh;
    /**
     * @var string
     */
    private $nameNl;
    /**
     * @var string
     */
    private $nameGr;
    /**
     * @var string
     */
    private $dirify;

    public function __construct(
        string $value,
        string $nameBr,
        string $nameEs,
        string $nameFr,
        string $nameId,
        string $nameIt,
        string $nameDe,
        string $nameTr,
        string $nameRu,
        string $nameJa,
        string $nameKo,
        string $namePl,
        string $nameRo,
        string $nameVn,
        string $nameTh,
        string $nameNl,
        string $nameGr
    ) {
        $this->guardNotEmptyName($value);

        parent::__construct($value);
        $this->nameBr = !empty($nameBr) ? $nameBr : $value;
        $this->nameEs = !empty($nameEs) ? $nameBr : $value;
        $this->nameFr = !empty($nameFr) ? $nameBr : $value;
        $this->nameId = !empty($nameId) ? $nameBr : $value;
        $this->nameIt = !empty($nameIt) ? $nameBr : $value;
        $this->nameDe = !empty($nameDe) ? $nameBr : $value;
        $this->nameTr = !empty($nameTr) ? $nameBr : $value;
        $this->nameRu = !empty($nameRu) ? $nameBr : $value;
        $this->nameJa = !empty($nameJa) ? $nameBr : $value;
        $this->nameKo = !empty($nameKo) ? $nameBr : $value;
        $this->namePl = !empty($namePl) ? $nameBr : $value;
        $this->nameRo = !empty($nameRo) ? $nameBr : $value;
        $this->nameVn = !empty($nameVn) ? $nameBr : $value;
        $this->nameTh = !empty($nameTh) ? $nameBr : $value;
        $this->nameNl = !empty($nameNl) ? $nameBr : $value;
        $this->nameGr = !empty($nameGr) ? $nameBr : $value;
        $this->dirify = Utils::myDirify($value);
    }

    private function guardNotEmptyName(string $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException(sprintf('Empty item name'));
        }
    }

    public function dirify(): string
    {
        return $this->dirify;
    }
}
