<?php

declare(strict_types=1);

namespace App\Al\Items\Infrastructure\Projections;

use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\Utils;

final class ItemView implements Response
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var array */
    private $images;

    /** @var string */
    private $href;
    /** @var string */
    private $sentence;
    /**
     * @var string
     */
    private $originalName;
    /**
     * @var int
     */
    private $price;
    /**
     * @var string
     */
    private $web;

    /**
     * ItemView constructor.
     * @param int $id
     * @param string $name
     */
    public function __construct(int $id, string $web, string $originalName, string $name, array $images, string $href, int $price, string $sentence = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->images = $images;
        $this->href = $href;
        $this->sentence = $sentence;
        $this->originalName = $originalName;
        $this->price = $price;
        $this->web = $web;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    public function dirify(): string
    {
        return Utils::myDirify($this->name);
    }

    /**
     * @return array
     */
    public function images(): array
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function sentence(): string
    {
        return $this->sentence;
    }

    /**
     * @return string
     */
    public function originalName(): string
    {
        return $this->originalName;
    }

    public function icon(): array
    {
        foreach ($this->images as $image) {
            if ('0' === $image['count']) {
                return $image;
            }
        }

        return [];
    }

    public function horizontalImg(): array
    {
        foreach ($this->images as $image) {
            if ($image['width'] > $image['height']) {
                return $image;
            }
        }

        return [];
    }

    /**
     * @return int
     */
    public function price(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function web(): string
    {
        return $this->web;
    }
}
