<?php

declare(strict_types=1);


namespace App\Al\Items\Infrastructure\Persistence\Doctrine;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class StringToFloatType extends StringType implements DoctrineCustomType
{
    public static function customTypeName(): string
    {
        return 'string_to_float';
    }

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return floatval($value);
    }

    /** @var float $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return number_format($value, 2);
    }
}
