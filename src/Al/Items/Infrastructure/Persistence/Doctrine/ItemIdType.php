<?php

declare(strict_types=1);


namespace App\Al\Items\Infrastructure\Persistence\Doctrine;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

final class ItemIdType extends IntegerType implements DoctrineCustomType
{
    public static function customTypeName(): string
    {
        return 'item_id';
    }

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new ItemId((int)$value);
    }

    /** @var ItemId $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (is_string($value) || is_int($value)) {
            return (new ItemId((int)$value))->value();
        }

        return $value->value();
    }
}
