<?php

declare(strict_types=1);

namespace App\Al\Items\Infrastructure\Persistence;

use App\Al\Items\Domain\Item;
use App\Al\Items\Domain\Service\ItemRepositoryInterface;
use App\Al\Items\Domain\ValueObject\ItemDownloads;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\PackageValueObject;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use Doctrine\DBAL\FetchMode;

final class MySqlItemRepository extends DoctrineRepository implements ItemRepositoryInterface
{

    public function search(ItemId $id): ?Item
    {
        /** @var Item $item */
        $item = $this->repository(Item::class)->find($id);

        return $item;
    }

    public function save(Item $item): void
    {
        $this->persist($item);
        $this->entityManager()->flush();
    }

    public function totalDownloads(ItemId $id): ItemDownloads
    {
        $query = "select count(*) as downloads from tbl_item_downloads where item_id=" . $id->value();
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $downloads = $stmt->fetchAll();

        return new ItemDownloads((int)$downloads[0]['downloads']);
    }

    public function searchByPackage(PackageValueObject $package): ?Item
    {
        return $this->repository(Item::class)->findOneBy(['google.dirify' => $package->dirify()]);
    }

    public function searchPrice(ItemId $id): float
    {
        $query = "select value as price from tbl_item_data where typefield_id=5 and item_id=" . $id->value();
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $price = $stmt->fetchAll();

        return (float)$price[0]['price'] ?? 0;
    }

    public function searchValid(int $from = 1, int $to = 1, ItemId $id = null): array
    {
        $query = "SELECT id, web FROM tbl_item WHERE 
                    ((est_publicacion_br&4!=4 AND est_publicacion_br&1=1 AND est_publicacion_br&512!=512 AND est_publicacion_br&256=256) ||
                    (est_publicacion_es&4!=4 AND est_publicacion_es&1=1 AND est_publicacion_es&512!=512 AND est_publicacion_es&256=256) ||
                    (est_publicacion_en&4!=4 AND est_publicacion_en&1=1 AND est_publicacion_en&512!=512 AND est_publicacion_en&256=256) ||
                    (est_publicacion_it&4!=4 AND est_publicacion_it&1=1 AND est_publicacion_it&512!=512 AND est_publicacion_it&256=256) ||
                    (est_publicacion_fr&4!=4 AND est_publicacion_fr&1=1 AND est_publicacion_fr&512!=512 AND est_publicacion_fr&256=256) ||
                    (est_publicacion_id&4!=4 AND est_publicacion_id&1=1 AND est_publicacion_id&512!=512 AND est_publicacion_id&256=256) ||
                    (est_publicacion_de&4!=4 AND est_publicacion_de&1=1 AND est_publicacion_de&512!=512 AND est_publicacion_de&256=256) ||
                    (est_publicacion_tr&4!=4 AND est_publicacion_tr&1=1 AND est_publicacion_tr&512!=512 AND est_publicacion_tr&256=256) ||
                    (est_publicacion_pl&4!=4 AND est_publicacion_pl&1=1 AND est_publicacion_pl&512!=512 AND est_publicacion_pl&256=256) ||
                    (est_publicacion_ro&4!=4 AND est_publicacion_ro&1=1 AND est_publicacion_ro&512!=512 AND est_publicacion_ro&256=256) ||
                    (est_publicacion_gr&4!=4 AND est_publicacion_gr&1=1 AND est_publicacion_gr&512!=512 AND est_publicacion_gr&256=256) ||
                    (est_publicacion_ja&4!=4 AND est_publicacion_ja&1=1 AND est_publicacion_ja&512!=512 AND est_publicacion_ja&256=256) ||
                    (est_publicacion_vn&4!=4 AND est_publicacion_vn&1=1 AND est_publicacion_vn&512!=512 AND est_publicacion_vn&256=256) ||
                    (est_publicacion_th&4!=4 AND est_publicacion_th&1=1 AND est_publicacion_th&512!=512 AND est_publicacion_th&256=256) ||
                    (est_publicacion_ru&4!=4 AND est_publicacion_ru&1=1 AND est_publicacion_ru&512!=512 AND est_publicacion_ru&256=256) ||
                    (est_publicacion_nl&4!=4 AND est_publicacion_nl&1=1 AND est_publicacion_nl&512!=512 AND est_publicacion_nl&256=256) ||
                    (est_publicacion_ko&4!=4 AND est_publicacion_ko&1=1 AND est_publicacion_ko&512!=512 AND est_publicacion_ko&256=256))";

        if (null !== $id) {
            $query .= " AND id = {$id->value()}";
        } else {
            $query .= " AND id >= {$from} AND id < {$to}";
        }

        $conn = $this->entityManager()->getConnection();

        return $conn->executeQuery($query)->fetchAll(FetchMode::ASSOCIATIVE);
    }

    public function forFree(ItemId $id): void
    {
        $this->entityManager()->getConnection()
            ->executeUpdate(
                "UPDATE tbl_item_data SET value = 0,value_dirify=0,value_extended=0 
                        WHERE (typefield_id = 5 OR typefield_id = 38) 
                          AND type_id=1 
                          AND item_id = :itemId",
                [
                    'itemId' => $id->value()
                ]
            );
    }

    public function nextId(): ItemId
    {
        $result = $this->entityManager()->getConnection()
            ->executeQuery(
                "SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
                        WHERE table_name = 'tbl_item'");

        return new ItemId(intval($result->fetchColumn()));
    }
}
