<?php

declare(strict_types=1);

namespace App\Al\Items\Infrastructure\Persistence;

use App\Al\Items\Domain\Service\ItemReadModelRepositoryInterface;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Shared\Domain\Utils;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlItemReadModelRepository extends DoctrineRepository implements ItemReadModelRepositoryInterface
{

    public function searchByLanguage(int $id, string $language): ?ItemView
    {
        $query = "SELECT id,name_en as originalName, name_".$language." as name, google_id_dirify as package, name_dirify as dirify, web FROM tbl_item WHERE id = ".$id;

        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $item = $stmt->fetchAll();

        if(null === $item) {
            return null;
        }

        $query = "SELECT id,CONCAT('https://media.cdnandroid.com',url_image) as path, width, height, count FROM tbl_item_images WHERE item_id = ".$id;

        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $images = $stmt->fetchAll();

        $href = $item[0]['dirify'];

        if (in_array($language, ['jp', 'ko' ,'th', 'vn'])) {
            $href = $item[0]['package'];
        }

        $query = "SELECT value from tbl_item_data WHERE item_id=".$id." and typefield_id=5";
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $price = (int)($result[0]['value'] ?? 0);

        $query = "SELECT * from tbl_descriptions WHERE item_id=".$id." and description_type_id=7 and est_publicacion=1 and language_code='".$language."'";
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $sentence = $result[0]['description'] ?? '';

        return new ItemView(
            (int)$item[0]['id'],
            $item[0]['web'],
            $item[0]['originalName'],
            $item[0]['name'],
            $images,
            sprintf('%s/item/android-apps/%s/%s/', Utils::domainForLanguage($language), $item[0]['id'], $href),
            $price,
            $sentence
        );
    }
}
