<?php

declare(strict_types=1);

namespace App\Al\Users\Infrastructure\Auth;

use App\Shared\Infrastructure\Auth\Auth;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

final class JwtProvider
{

    /** @var JWTTokenManagerInterface */
    private $tokenManager;

    public function __construct(JWTEncoderInterface $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    public function generateToken(int $id, string $username, string $hashedPassword, int $roles, string $locale, string $salt, bool $remember)
    {
        $auth = Auth::create($id, $username, $hashedPassword, $roles, $locale, $salt);

        return $this->tokenManager->encode([
            'remember' => $remember,
            'username' => $auth->getUsername(),
            'id' => $auth->id(),
            'roles' => $auth->getRoles(),
            'locale' => $auth->locale()
        ]);
    }
}
