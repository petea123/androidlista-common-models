<?php

declare(strict_types=1);

namespace App\Al\Users\Infrastructure\Auth\Guard;

use App\Al\Users\Domain\Service\LoginAuthenticatorInterface;
use App\Al\Users\Domain\Service\UserRepositoryInterface;
use App\Al\Users\Domain\User;
use App\Al\Users\Domain\ValueObject\UserUsername;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

final class Authenticator implements LoginAuthenticatorInterface
{

    /** @var UserRepositoryInterface */
    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function login(UserUsername $username, string $plainPassword): User
    {
        $user = $this->repository->searchByUsername($username);

        if (null === $user) {
            throw new UsernameNotFoundException($username->value());
        }

        $this->checkPassword($plainPassword, $user->hashedPassword()->value(), $user->salt()->value());

        return $user;
    }

    private function checkPassword(string $plainPassword, string $hashedPassword, string $salt)
    {
        if (!($hashedPassword === sha1($salt.$plainPassword))) {
            throw new BadCredentialsException();
        }
    }
}
