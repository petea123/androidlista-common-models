<?php

declare(strict_types=1);

namespace App\Al\Users\Infrastructure\Projections;

final class UserView
{
    /** @var string */
    private $username;
    /** @var int */
    private $id;
    /** @var string */
    private $hashedPassword;
    /** @var string */
    private $salt;
    /** @var int */
    private $role;
    /** @var string */
    private $locale;

    public function __construct(int $id, string $username, string $hashedPassword, string $salt, string $locale, int $role)
    {
        $this->username = $username;
        $this->id = $id;
        $this->hashedPassword = $hashedPassword;
        $this->salt = $salt;
        $this->role = $role;
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function hashedPassword(): string
    {
        return $this->hashedPassword;
    }

    /**
     * @return string
     */
    public function salt(): string
    {
        return $this->salt;
    }

    /**
     * @return int
     */
    public function role(): int
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }
}
