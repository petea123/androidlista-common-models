<?php

declare(strict_types=1);

namespace App\Al\Users\Infrastructure\Persistence;

use App\Al\Users\Domain\Service\UserViewRepositoryInterface;
use App\Al\Users\Domain\ValueObject\UserUsername;
use App\Al\Users\Infrastructure\Projections\UserView;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class UserViewRepository extends DoctrineRepository implements UserViewRepositoryInterface
{

    public function credentialsByUsername(UserUsername $username): ?UserView
    {
        $user = $this->entityManager()->getConnection()
            ->executeQuery("SELECT u.id, u.username, u.salt, u.password, p.permission_id as role, up.language_code as locale
                            FROM sf_guard_user u 
                            LEFT JOIN sf_guard_user_permission p ON p.user_id = u.id
                            LEFT JOIN sf_guard_user_profile up ON up.user_id=u.id
                            WHERE u.username = '".$username->value()."'")
            ->fetch();

        if(empty($user)) {
            return null;
        }

        return new UserView((int)$user['id'], $user['username'], $user['password'], $user['salt'], $user['locale'], (int)$user['role']);

    }
}
