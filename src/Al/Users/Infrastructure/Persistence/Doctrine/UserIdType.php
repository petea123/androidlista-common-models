<?php

declare(strict_types=1);

namespace App\Al\Users\Infrastructure\Persistence\Doctrine;

use App\Shared\Domain\Users\UserId;
use App\Shared\Infrastructure\Persistence\Doctrine\IntegerIdType;

final class UserIdType extends IntegerIdType
{

    public static function customTypeName(): string
    {
        return 'user_id';
    }

    protected function typeClassName(): string
    {
        return UserId::class;
    }
}
