<?php

declare(strict_types=1);

namespace App\Al\Users\Infrastructure\Persistence;

use App\Al\Users\Domain\Service\UserRepositoryInterface;
use App\Al\Users\Domain\User;
use App\Al\Users\Domain\ValueObject\UserUsername;
use App\Shared\Domain\Users\UserId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlUserRepository extends DoctrineRepository implements UserRepositoryInterface
{

    public function searchByUsername(UserUsername $username): ?User
    {
        return $this->repository(User::class)->findOneBy(['username.value' => $username->value()]);
    }

    public function search(UserId $id): ?User
    {
        // TODO: Implement search() method.
    }
}
