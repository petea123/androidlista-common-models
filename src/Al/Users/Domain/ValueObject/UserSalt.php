<?php

declare(strict_types=1);

namespace App\Al\Users\Domain\ValueObject;

use App\Shared\Domain\ValueObject\StringValueObject;

final class UserSalt extends StringValueObject
{

}