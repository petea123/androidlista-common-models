<?php

declare(strict_types=1);

namespace App\Al\Users\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ArrayValueObject;

final class UserRoles extends ArrayValueObject
{
    private const ROLES = [
        1 => [
            'ROLE_ADMIN',
            'ROLE_CONTENIDO',
            'ROLE_GESTOR_CONTENIDO'
            ],
        2 => ['ROLE_CONTENIDO'],
        4 => [
            'ROLE_GESTOR_CONTENIDO',
            'ROLE_CONTENIDO'
        ]
    ];

    public static function createFromPermission(int $permission): self
    {
        return new self(self::ROLES[$permission]);
    }
}