<?php

declare(strict_types=1);

namespace App\Al\Users\Domain;

use App\Al\Users\Domain\ValueObject\UserLocale;
use App\Shared\Domain\Users\UserId;
use App\Al\Users\Domain\ValueObject\UserHashedPassword;
use App\Al\Users\Domain\ValueObject\UserSalt;
use App\Al\Users\Domain\ValueObject\UserUsername;

class User
{

    /** @var UserId */
    private $id;
    /** @var UserUsername */
    private $username;
    /** @var UserHashedPassword */
    private $hashedPassword;
    /** @var UserSalt */
    private $salt;
    /** @var UserLocale */
    private $locale;

    public function __construct(UserId $id, UserUsername $username, UserHashedPassword $hashedPassword, UserLocale $locale, UserSalt $salt)
    {
        $this->id = $id;
        $this->username = $username;
        $this->hashedPassword = $hashedPassword;
        $this->salt = $salt;
        $this->locale = $locale;
    }

    /**
     * @return UserId
     */
    public function id(): UserId
    {
        return $this->id;
    }

    /**
     * @return UserUsername
     */
    public function username(): UserUsername
    {
        return $this->username;
    }

    /**
     * @return UserHashedPassword
     */
    public function hashedPassword(): UserHashedPassword
    {
        return $this->hashedPassword;
    }

    /**
     * @return UserSalt
     */
    public function salt(): UserSalt
    {
        return $this->salt;
    }

    /**
     * @return UserLocale
     */
    public function locale(): UserLocale
    {
        return $this->locale;
    }
}
