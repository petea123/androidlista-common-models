<?php


namespace App\Al\Users\Domain\Service;

use App\Al\Users\Domain\User;
use App\Al\Users\Domain\ValueObject\UserUsername;
use App\Shared\Domain\Users\UserId;

interface UserRepositoryInterface
{
    public function search(UserId $id): ?User;

    public function searchByUsername(UserUsername $username): ?User;
}
