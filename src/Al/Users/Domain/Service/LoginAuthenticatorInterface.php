<?php


namespace App\Al\Users\Domain\Service;


use App\Al\Users\Domain\User;
use App\Al\Users\Domain\ValueObject\UserUsername;

interface LoginAuthenticatorInterface
{
    public function login(UserUsername $username, string $plainPassword): User;
}