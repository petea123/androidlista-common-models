<?php


namespace App\Al\Users\Domain\Service;


use App\Al\Users\Domain\ValueObject\UserUsername;
use App\Al\Users\Infrastructure\Projections\UserView;

interface UserViewRepositoryInterface
{
    public function credentialsByUsername(UserUsername $username): ?UserView;
}