<?php

declare(strict_types=1);

namespace App\Al\Users\Application\SignIn;

use App\Al\Users\Domain\Service\LoginAuthenticatorInterface;
use App\Al\Users\Domain\ValueObject\UserUsername;

final class SignIn
{

    /** @var LoginAuthenticatorInterface */
    private $authenticator;

    public function __construct(LoginAuthenticatorInterface $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    public function __invoke(UserUsername $username, string $plainPassword)
    {
        $this->authenticator->login($username, $plainPassword);
    }
}