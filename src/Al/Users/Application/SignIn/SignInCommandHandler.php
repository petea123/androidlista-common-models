<?php

declare(strict_types=1);

namespace App\Al\Users\Application\SignIn;

use App\Al\Users\Domain\ValueObject\UserUsername;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class SignInCommandHandler implements CommandHandler
{

    /** @var SignIn */
    private $signIn;

    public function __construct(SignIn $signIn)
    {
        $this->signIn = $signIn;
    }

    public function __invoke(SignInCommand $command)
    {
        $username = new UserUsername($command->username());

        apply($this->signIn, [$username, $command->plainPassword()]);
    }
}
