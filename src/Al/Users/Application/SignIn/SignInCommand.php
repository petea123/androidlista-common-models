<?php

declare(strict_types=1);

namespace App\Al\Users\Application\SignIn;

use App\Shared\Domain\Bus\Command\Command;

final class SignInCommand extends Command
{
    /** @var string */
    private $username;
    /** @var string */
    private $plainPassword;

    public function __construct(string $username, string $plainPassword)
    {
        $this->username = $username;
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function plainPassword(): string
    {
        return $this->plainPassword;
    }
}
