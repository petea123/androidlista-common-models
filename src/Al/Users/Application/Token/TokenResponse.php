<?php

declare(strict_types=1);

namespace App\Al\Users\Application\Token;

use App\Shared\Domain\Bus\Query\Response;

final class TokenResponse implements Response
{

    /** @var string */
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function token(): string
    {
        return $this->token;
    }
}
