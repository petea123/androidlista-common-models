<?php

declare(strict_types=1);

namespace App\Al\Users\Application\Token;

use App\Shared\Domain\Bus\Query\Query;

final class GetTokenQuery implements Query
{

    /** @var string */
    private $username;
    /** @var bool */
    private $remember;

    public function __construct(string $username, bool $remember = false)
    {
        $this->username = $username;
        $this->remember = $remember;
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    /**
     * @return bool
     */
    public function remember(): bool
    {
        return $this->remember;
    }
}
