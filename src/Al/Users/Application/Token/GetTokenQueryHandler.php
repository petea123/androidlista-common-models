<?php

declare(strict_types=1);

namespace App\Al\Users\Application\Token;

use App\Al\Users\Domain\Service\UserViewRepositoryInterface;
use App\Al\Users\Domain\ValueObject\UserUsername;
use App\Al\Users\Infrastructure\Auth\JwtProvider;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Bus\Query\Response;

final class GetTokenQueryHandler implements QueryHandler
{
    /** @var UserViewRepositoryInterface */
    private $repository;
    /** @var JwtProvider */
    private $provider;

    public function __construct(JwtProvider $provider, UserViewRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->provider = $provider;
    }

    public function __invoke(GetTokenQuery $command): ?Response
    {
        $user = $this->repository->credentialsByUsername(new UserUsername($command->username()));

        return new TokenResponse($this->provider->generateToken(
            $user->id(),
            $user->username(),
            $user->hashedPassword(),
            $user->role(),
            $user->locale(),
            $user->salt(),
            $command->remember())
        );
    }
}
