<?php

declare(strict_types=1);

namespace App\Al\ItemsGoogle\Domain;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class ItemGoogleDateWasUpdated extends DomainEvent
{
    /**
     * @var int
     */
    private $itemId;


    public function __construct(
        string $aggregateId,
        int $itemId,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->itemId = $itemId;
    }

    public static function eventName(): string
    {
        return 'ItemGoogleDateWasUpdated';
    }

    public static function stream(): string
    {
        return ItemGoogle::class;
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId(),
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['itemId'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }
}
