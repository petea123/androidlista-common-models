<?php

declare(strict_types=1);

namespace App\Al\Shared\Domain;

final class Relation
{

    /** @var string */
    private $url;

    private $id;

    /** @var string */
    private $googlePackage;

    public function __construct(string $url, $id, string $googlePackage)
    {
        $this->url = $url;
        $this->id = $id;
        $this->googlePackage = $googlePackage;
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function googlePackage(): string
    {
        return $this->googlePackage;
    }
}
