<?php

declare(strict_types=1);

namespace App\Al\Shared\Domain;

interface UploaderInterface
{
    public function upload(string $path): bool;
}