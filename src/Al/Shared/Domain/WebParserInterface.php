<?php

namespace App\Al\Shared\Domain;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Shared\Domain\ValueObject\ExternalUrl;

interface WebParserInterface
{
    public static function getApp(ItemGoogle $google, ApkSource $source = null): ?WebParserInterface;

    public function setAdapter(ApkSource $source): void;

    public function downloadUrl(): ExternalUrl;
    public function downloadLink(ExternalUrl $url): ExternalUrl;

    public function categoryItems(string $category, int $pages = 1): array;

    public function relation(ExternalUrl $url): Relation;

    public function versions($externalId): array;

    public function infoFor(ExternalUrl $url): array;

    public function infoByPackage(string $package): Relation;
}
