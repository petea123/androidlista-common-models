<?php

declare(strict_types=1);

namespace App\Al\Shared\Domain;

use SplFileInfo;

interface HashGenerator
{
    public function hash(SplFileInfo $resource): string;
}
