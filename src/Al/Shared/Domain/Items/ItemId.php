<?php

namespace App\Al\Shared\Domain\Items;

use App\Shared\Domain\ValueObject\IntValueObject;
use InvalidArgumentException;

final class ItemId extends IntValueObject
{
    CONST BANNED_ITEMS = [
        663687,149303, 657604, 430256, 164661, 719027, 533684, 657604, 18284, 46431, 307546, 31725, 31726, 884050, 619293, 989079,
        978688, 919149, 86502, 707553, 596534, 615652, 609223, 651006, 100535, 812374, 651098, 954841, 909334, 901846, 32122, 900873,
        639032, 990045
    ];

    public function __construct(int $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(int $id)
    {
        if (empty($id)) {
            throw new InvalidArgumentException(sprintf('The item id <%s> is empty', $id));
        }
    }

    public function isBannedForDownloadApk(): bool
    {
        return in_array($this->value(), self::BANNED_ITEMS);
    }
}
