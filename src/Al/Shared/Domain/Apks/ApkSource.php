<?php

namespace App\Al\Shared\Domain\Apks;

use App\Shared\Domain\ValueObject\Enum;
use InvalidArgumentException;

//TODO move to shared or change to source value object
/**
 * @method static ApkSource uptodown()
 * @method static ApkSource apkpure()
 * @method static ApkSource apkmonk()
 * @method static ApkSource apkdescargar()
 * @method static ApkSource undefined()
 */
final class ApkSource extends Enum
{
    public const UPTODOWN = 'Uptodown';
    public const APKPURE = 'ApkPure';
    public const APKMONK = 'ApkMonk';
    public const APKDESCARGAR = 'ApkDescargar';
    public const UNDEFINED = 'Undefined';

    protected function throwExceptionForInvalidValue($value)
    {
        throw new InvalidArgumentException(sprintf('The <%s> is not a valid source string', $value));
    }
}
