<?php

declare(strict_types=1);

namespace App\Al\Shared\Domain\Categories;

use App\Shared\Domain\ValueObject\IntValueObject;
use InvalidArgumentException;

final class CategoryId extends IntValueObject
{
    public function __construct(int $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(int $id)
    {
        if (empty($id)) {
            throw new InvalidArgumentException(sprintf('The category id <%s> is empty', $id));
        }
    }
}
