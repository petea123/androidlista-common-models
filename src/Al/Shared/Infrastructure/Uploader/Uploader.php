<?php

declare(strict_types=1);

namespace App\Al\Shared\Infrastructure\Uploader;

use App\Al\Shared\Domain\FilesystemInterface;
use App\Al\Shared\Domain\UploaderInterface;

final class Uploader implements UploaderInterface
{

    /** @var FilesystemInterface */
    private $s3Storage;

    /** @var FilesystemInterface */
    private $localStorage;

    public function __construct(FilesystemInterface $localStorage, FilesystemInterface $s3Storage)
    {
        $this->s3Storage = $s3Storage;
        $this->localStorage = $localStorage;
    }

    public function upload(string $path): bool
    {
        $resource = $this->localStorage->readStream($path);
        $result = $this->s3Storage->writeStream($path, $resource);
        fclose($resource);

        return $result;
    }
}