<?php

declare(strict_types=1);

namespace App\Al\Shared\Infrastructure;

use App\Al\Shared\Domain\HashGenerator;
use SplFileInfo;

final class Md5HashGenerator implements HashGenerator
{

    public function hash(SplFileInfo $resource): string
    {
        $contents = file_get_contents($resource->getRealPath());
        $md5 = md5($contents);
        $contents = null;
        return $md5;
    }
}
