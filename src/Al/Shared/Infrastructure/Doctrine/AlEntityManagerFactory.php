<?php

declare(strict_types = 1);

namespace App\Al\Shared\Infrastructure\Doctrine;

use App\Shared\Infrastructure\Doctrine\DoctrineEntityManagerFactory;
use Doctrine\ORM\EntityManagerInterface;

final class AlEntityManagerFactory
{

    public static function create(string $url, string $environment): EntityManagerInterface
    {
        $isDevMode = 'prod' !== $environment;

        $prefixes               = array_merge(
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../../Al', 'App\Al'),
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../../Amp', 'App\Amp'),
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../../Backoffice', 'App\Backoffice'),
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../../Retention', 'App\Retention'),
        );

        $dbalCustomTypesClasses = array_merge(
            DbalTypesSearcher::inPath(__DIR__ . '/../../../../Al', 'Al'),
            DbalTypesSearcher::inPath(__DIR__ . '/../../../../Amp', 'Amp'),
            DbalTypesSearcher::inPath(__DIR__ . '/../../../../Backoffice', 'Backoffice'),
            DbalTypesSearcher::inPath(__DIR__ . '/../../../../Retention', 'Retention'),
        );

        return DoctrineEntityManagerFactory::create(
            $url,
            $prefixes,
            $isDevMode,
            $dbalCustomTypesClasses
        );
    }
}
