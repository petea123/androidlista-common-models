<?php

declare(strict_types=1);

namespace App\Al\Shared\Infrastructure\ProxyBonanza;

final class ProxyBonanza
{

    private const SHARED_PROXIES = [
        'credentials' => 'owsdj0:xlwoggen',
        'list' => [
            '192.225.163.34:60099',
            '192.225.163.40:60099',
            '192.225.163.45:60099',
            '192.225.163.58:60099',
            '192.225.163.72:60099',
            '208.100.6.134:45623',
            '208.100.6.162:45623',
            '208.100.6.220:45623',
            '208.100.6.231:45623',
            '208.100.6.234:45623',
            '216.86.159.140:45623',
            '216.86.159.166:45623',
            '216.86.159.215:45623',
            '216.86.159.216:45623',
            '216.86.159.225:45623',
            '196.19.128.22:60099',
            '196.19.128.62:60099',
            '196.19.128.84:60099',
            '196.19.128.93:60099',
            '196.19.128.158:60099',
            '185.172.173.13:60099',
            '185.172.173.14:60099',
            '185.172.173.19:60099',
            '185.172.175.42:60099',
            '196.19.128.3:60099',
            '185.172.175.53:60099',
            '185.172.175.58:60099',
            '185.172.175.60:60099',
            '185.172.173.99:60099',
            '185.172.173.102:60099',
            '77.83.44.19:60099',
            '77.83.44.90:60099',
            '77.83.44.93:60099',
            '77.83.44.183:60099',
            '77.83.44.184:60099',
            '38.109.240.7:60099',
            '155.254.19.140:60099',
            '154.16.247.130:60099',
            '154.16.247.136:60099',
            '154.16.247.155:60099',
            '104.152.223.78:60099',
            '104.152.223.85:60099',
            '104.152.223.91:60099',
            '104.255.174.2:60099',
            '104.152.223.104:60099',
            '38.145.124.7:60099',
            '38.109.240.10:60099',
            '104.255.169.13:60099',
            '104.255.169.17:60099',
            '38.109.241.2:60099'
        ]
    ];



    public static function randomSharedProxy()
    {

        $proxies = self::SHARED_PROXIES['list'];

        shuffle($proxies);

        foreach ($proxies as $proxy) {
            if(self::checkProxy($proxy)) {
                return $proxy;
            }
        }

        return false;
    }

    private static function checkProxy($ip)
    {
        $waitTimeoutInSeconds = 1;
        try {
            $fp = stream_socket_client('tcp://'.$ip, $errCode, $errStr, $waitTimeoutInSeconds);

            if (is_resource($fp)) {
                fclose($fp);
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }

        return false;
    }

    public static function sharedStreamContext(string $method = 'GET')
    {
        return stream_context_create([
            'https' => [
                'method' => $method,
                'proxy' => 'tcp://'.self::randomSharedProxy(),
                'request_fulluri' => false,
                'max_redirects' => 5,
                'follow_location' => 1,
                'header' => 'Proxy-Authorization: Basic '.base64_encode(self::SHARED_PROXIES['credentials']),
                'user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
            ]
        ]);
    }
}
