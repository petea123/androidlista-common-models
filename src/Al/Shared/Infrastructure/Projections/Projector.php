<?php

declare(strict_types=1);

namespace App\Al\Shared\Infrastructure\Projections;

use App\Shared\Domain\Bus\Event\DomainEvent;

abstract class Projector
{
    /**
     * {@inheritdoc}
     */
    public function handle(DomainEvent $event): void
    {
        $method = $this->getHandleMethod($event);
        if (!method_exists($this, $method)) {
            return;
        }
        $this->$method($event);
    }

    /**
     * @param mixed $event
     */
    private function getHandleMethod($event): string
    {
        $classParts = explode('\\', get_class($event));
        return 'apply'.end($classParts);
    }
}
