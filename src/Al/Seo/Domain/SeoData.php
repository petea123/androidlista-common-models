<?php declare(strict_types=1);

namespace App\Al\Seo\Domain;

use App\Al\Projects\Domain\Project;

class SeoData
{
    /** @var int */
    private $id;
    /** @var int */
    private $seoDataId;
    /** @var Project */
    private $project;
    /** @var int */
    private $typeId;
    /** @var string */
    private $base;
    /** @var string */
    private $type;
    /** @var string */
    private $titleTranslate;
    /** @var string */
    private $descriptionTranslate;
    /** @var bool */
    private $noindex;
    /** @var string */
    private $languageCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;
        return $this;
    }

    public function getSeoDataId(): ?int
    {
        return $this->seoDataId;
    }

    public function setSeoDataId(?int $seoDataId): self
    {
        $this->seoDataId = $seoDataId;
        return $this;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function setTypeId(?int $typeId): self
    {
        $this->typeId = $typeId;
        return $this;
    }

    public function getBase(): ?string
    {
        return $this->base;
    }

    public function setBase(?string $base): self
    {
        $this->base = $base;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getTitleTranslate(): ?string
    {
        return $this->titleTranslate;
    }

    public function setTitleTranslate(?string $titleTranslate): self
    {
        $this->titleTranslate = $titleTranslate;
        return $this;
    }

    public function getDescriptionTranslate(): ?string
    {
        return $this->descriptionTranslate;
    }

    public function setDescriptionTranslate(?string $descriptionTranslate): self
    {
        $this->descriptionTranslate = $descriptionTranslate;
        return $this;
    }

    public function isNoindex(): ?bool
    {
        return $this->noindex;
    }

    public function setNoindex(?bool $noindex): self
    {
        $this->noindex = $noindex;
        return $this;
    }

    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?string $languageCode): self
    {
        $this->languageCode = $languageCode;
        return $this;
    }
}
