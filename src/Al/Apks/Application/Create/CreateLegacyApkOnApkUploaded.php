<?php

namespace App\Al\Apks\Application\Create;

use App\Al\Apks\Domain\Event\ApkWasUploaded;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class CreateLegacyApkOnApkUploaded implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {

        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasUploaded $event)
    {
        $this->commandBus->handle(new CreateLegacyApkCommand(
            $event->aggregateId()
        ));
    }

    public static function subscribedTo(): array
    {
        return [
            ApkWasUploaded::class
        ];
    }
}
