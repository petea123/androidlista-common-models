<?php

declare(strict_types=1);


namespace App\Al\Apks\Application\Create;

use App\Al\Apks\Domain\Service\ApkFinder;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Command\CommandHandler;

final class CreateLegacyApkCommandHandler implements CommandHandler
{

    /**
     * @var ApkRepositoryInterface
     */
    private $apkRepository;

    /**
     * @var ApkRepositoryInterface
     */
    private $legacyApkRepository;

    /**
     * @var ApkFinder
     */
    private $finder;

    public function __construct(ApkRepositoryInterface $apkRepository, ApkRepositoryInterface $legacyApkRepository)
    {

        $this->apkRepository = $apkRepository;
        $this->legacyApkRepository = $legacyApkRepository;
        $this->finder = new ApkFinder($apkRepository);
    }

    public function __invoke(CreateLegacyApkCommand $command)
    {
        $apk = $this->finder->search(new ApkId($command->id()));

        $this->legacyApkRepository->save($apk);
    }
}
