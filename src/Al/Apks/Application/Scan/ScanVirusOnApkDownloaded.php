<?php


namespace App\Al\Apks\Application\Scan;

use App\Al\Apks\Domain\Event\ApkWasDownloaded;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class ScanVirusOnApkDownloaded implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {

        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasDownloaded $event)
    {
        $this->commandBus->handle(new ScanApkCommand($event->aggregateId()));
    }

    public static function subscribedTo(): array
    {
        return [
            ApkWasDownloaded::class
        ];
    }
}
