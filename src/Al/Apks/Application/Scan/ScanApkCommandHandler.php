<?php

declare(strict_types=1);


namespace App\Al\Apks\Application\Scan;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class ScanApkCommandHandler implements CommandHandler
{

    /**
     * @var ScanVirus
     */
    private $scanner;

    public function __construct(ScanVirus $scanner)
    {

        $this->scanner = $scanner;
    }

    public function __invoke(ScanApkCommand $command)
    {
        $id = new ApkId($command->id());

        apply($this->scanner, [$id]);
    }
}
