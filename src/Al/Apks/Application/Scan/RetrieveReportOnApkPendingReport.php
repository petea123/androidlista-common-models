<?php

namespace App\Al\Apks\Application\Scan;

use App\Al\Apks\Domain\Event\ApkHasPendingReport;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class RetrieveReportOnApkPendingReport implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {

        $this->commandBus = $commandBus;
    }


    public function __invoke(ApkHasPendingReport $event)
    {
        $this->commandBus->handle(new RetrieveApkReportCommand(
            $event->aggregateId()
        ));

    }

    public static function subscribedTo(): array
    {
        return [
            ApkHasPendingReport::class
        ];
    }
}
