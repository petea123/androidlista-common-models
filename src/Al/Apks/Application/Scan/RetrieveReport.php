<?php

namespace App\Al\Apks\Application\Scan;

use App\Al\Apks\Domain\Service\ApkFinder;
use App\Al\Apks\Domain\Service\ApkReportValidator;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\Service\ScannerInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\FilesystemInterface;

final class RetrieveReport
{

    private $finder;

    /**
     * @var ApkRepositoryInterface
     */
    private $apkRepository;

    /**
     * @var ApkReportValidator
     */
    private $validator;

    /**
     * @var ScannerInterface
     */
    private $scanner;

    /**
     * @var DomainEventPublisher
     */
    private $publisher;
    /**
     * @var FilesystemInterface
     */
    private $localStorage;

    public function __construct(
        ScannerInterface $scanner,
        ApkRepositoryInterface $apkRepository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher,
        FilesystemInterface $apksStorageMount
    ) {
        $this->scanner = $scanner;
        $this->finder = new ApkFinder($apkRepository);
        $this->apkRepository = $apkRepository;
        $this->validator = new ApkReportValidator($queryBus);
        $this->publisher = $publisher;
        $this->localStorage = $apksStorageMount;
    }

    public function __invoke(ApkId $id)
    {
        $apk = $this->finder->search($id);

        if (!$apk->fileIsTmp()) {
            return;
        }

        $report = $this->scanner->getReportAllInfo($apk->reportResource());

        $report = $this->validator->validateRescan($report, $apk->itemId(), $apk->id());

        $report->isDangerous() ? $apk->dangerous($report) : $apk->scanned($report);

        $this->apkRepository->save($apk);
        $this->publisher->record(...$apk->pullDomainEvents());
    }
}
