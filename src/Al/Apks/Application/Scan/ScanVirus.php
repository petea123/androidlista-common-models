<?php

namespace App\Al\Apks\Application\Scan;

use App\Al\Apks\Domain\Service\ApkFinder;
use App\Al\Apks\Domain\Service\ApkReportValidator;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\Service\ScannerInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;

final class ScanVirus
{

    private $finder;

    /**
     * @var ApkRepositoryInterface
     */
    private $apkRepository;

    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    /**
     * @var ApkReportValidator
     */
    private $validator;

    /**
     * @var ScannerInterface
     */
    private $scanner;

    public function __construct(
        ScannerInterface $scanner,
        ApkRepositoryInterface $apkRepository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher
    ) {
        $this->scanner = $scanner;
        $this->finder = new ApkFinder($apkRepository);
        $this->validator = new ApkReportValidator($queryBus);
        $this->apkRepository = $apkRepository;
        $this->publisher = $publisher;
    }

    public function __invoke(ApkId $id)
    {
        $apk = $this->finder->search($id);

        if (!$apk->fileIsTmp()) {
            return;
        }

        $report = $this->scanner->analyze($apk);

        $report = $this->validator->validate($report, $apk->itemId());
        $report->isDangerous() ? $apk->dangerous($report) : $apk->scanned($report);

        $this->apkRepository->save($apk);
        $this->publisher->record(...$apk->pullDomainEvents());
    }
}
