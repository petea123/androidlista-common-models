<?php

declare(strict_types=1);


namespace App\Al\Apks\Application\Scan;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class RetrieveApkReportCommandHandler implements CommandHandler
{

    /**
     * @var RetrieveReport
     */
    private $scanner;

    public function __construct(RetrieveReport $scanner)
    {

        $this->scanner = $scanner;
    }

    public function __invoke(RetrieveApkReportCommand $command)
    {
        $id = new ApkId($command->id());

        apply($this->scanner, [$id]);
    }

}
