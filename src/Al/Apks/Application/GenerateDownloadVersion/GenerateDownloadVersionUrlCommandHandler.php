<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadVersion;

use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\ExternalUrl;
use function Lambdish\Phunctional\apply;

final class GenerateDownloadVersionUrlCommandHandler implements CommandHandler
{
    /**
     * @var ApkDownloadVersionUrlGenerator
     */
    private $generator;

    public function __construct(ApkDownloadVersionUrlGenerator $generator)
    {
        $this->generator = $generator;
    }

    public function __invoke(GenerateDownloadVersionUrlCommand $command): void
    {
        $itemId = new ItemId($command->itemId());

        apply($this->generator, [$itemId, $command->total()]);
    }
}
