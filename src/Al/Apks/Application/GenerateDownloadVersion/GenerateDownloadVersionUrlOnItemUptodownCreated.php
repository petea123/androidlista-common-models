<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadVersion;

use App\Al\ItemsUptodown\Domain\Event\ItemUptodownCreated;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class GenerateDownloadVersionUrlOnItemUptodownCreated implements DomainEventSubscriber
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {

        $this->commandBus = $commandBus;
    }

    public function __invoke(ItemUptodownCreated $event): void
    {
        $this->commandBus->handle(new GenerateDownloadVersionUrlCommand(
            $event->itemId()
        ));
    }

    public static function subscribedTo(): array
    {
        return [ItemUptodownCreated::class];
    }
}
