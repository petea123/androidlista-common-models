<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadVersion;

use App\Shared\Domain\Bus\Command\Command;

final class GenerateDownloadVersionUrlCommand extends Command
{

    /**
     * @var string
     */
    private $source;
    /**
     * @var int
     */
    private $itemId;
    /**
     * @var int
     */
    private $total;

    public function __construct(int $itemId, int $total = 1)
    {
        $this->itemId = $itemId;
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return int
     */
    public function total(): int
    {
        return $this->total;
    }
}
