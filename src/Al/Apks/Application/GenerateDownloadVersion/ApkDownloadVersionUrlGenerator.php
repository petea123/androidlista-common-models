<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadVersion;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\ApkCreator;
use App\Al\Apks\Domain\ApkWebParser;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\UuidGenerator;
use App\Shared\Domain\ValueObject\ExternalUrl;

final class ApkDownloadVersionUrlGenerator
{
    /**
     * @var ApkRepositoryInterface
     */
    private $repository;
    /**
     * @var ApkCreator
     */
    private $creator;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;
    /**
     * @var WebParserFactory
     */
    private $webParserFactory;
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var UuidGenerator
     */
    private $generator;

    public function __construct(
        ApkRepositoryInterface $repository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher,
        WebParserFactory $webParserFactory,
        UuidGenerator $generator
    ) {
        $this->repository = $repository;
        $this->creator = new ApkCreator($repository, $webParserFactory);
        $this->publisher = $publisher;
        $this->webParserFactory = $webParserFactory;
        $this->queryBus = $queryBus;
        $this->generator = $generator;
    }

    public function __invoke(ItemId $itemId, int $total): void
    {
        /** @var ItemView $item */
        if (null === $item = $this->queryBus->ask(new FindItemByIdQuery($itemId->value()))) {
            return;
        }
        $google = new ItemGoogle($item->web());

        foreach (ApkSource::values() as $value) {
            $source = ApkSource::fromString($value);
            $app = $this->webParserFactory->instance($source, $google);
            if (null !== $app && 0 <= $total) {
                $versions = $app->versions();
                $apks = $this->repository->searchByItemId($itemId);

                foreach ($versions as $key => $url) {
                    $apk = $this->generate($apks, $url, new Version((string)$key), $item, $source);
                    if (null !== $apk) {
                        $this->repository->save($apk);
                        $this->publisher->record(...$apk->pullDomainEvents());
                        --$total;
                    }
                }
            }
        }
    }

    public function generate(array $apks, ExternalUrl $url, Version $version, ItemView $item, ApkSource $source): ?Apk
    {
        if ($this->hasVersion($apks, $version)) {
            return null;
        }

        return $this->creator->create(new ApkId($this->generator->next()), $source, $item, $url);
    }

    private function hasVersion(array $apks, Version $version): bool
    {

        /** @var Apk $apk */
        foreach ($apks as $apk) {
            if ($version->equals($apk->version())) {
                return true;
            }
        }

        return false;
    }
}
