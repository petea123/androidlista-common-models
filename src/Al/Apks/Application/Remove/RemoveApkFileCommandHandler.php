<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Remove;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class RemoveApkFileCommandHandler implements CommandHandler
{
    /**
     * @var ApkFileRemover
     */
    private $remover;

    public function __construct(ApkFileRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveApkFileCommand $command): void
    {
        $id = new ApkId($command->id());

        apply($this->remover, [$id]);
    }
}
