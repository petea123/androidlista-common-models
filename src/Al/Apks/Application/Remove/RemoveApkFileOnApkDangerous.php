<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Remove;

use App\Al\Apks\Domain\Event\ApkWasDangerous;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class RemoveApkFileOnApkDangerous implements DomainEventSubscriber
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasDangerous $event): void
    {
        $this->commandBus->handle(new RemoveApkFileCommand($event->aggregateId()));
    }

    public static function subscribedTo(): array
    {
        return [ApkWasDangerous::class];
    }
}
