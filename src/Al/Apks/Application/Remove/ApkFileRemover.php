<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Remove;

use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Shared\Domain\FilesystemInterface;

final class ApkFileRemover
{
    /**
     * @var FilesystemInterface
     */
    private $apksStorageMount;
    /**
     * @var ApkRepositoryInterface
     */
    private $repository;

    public function __construct(ApkRepositoryInterface $repository, FilesystemInterface $apksStorageMount)
    {
        $this->apksStorageMount = $apksStorageMount;
        $this->repository = $repository;
    }

    public function __invoke(ApkId $id): void
    {
        $apk = $this->repository->search($id);

        if (null !== $apk && $apk->isDangerous() && $apk->fileIsTmp()) {
            $this->apksStorageMount->delete(Url::create('local://'.$apk->url()->value()));
        }
    }
}
