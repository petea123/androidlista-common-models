<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Download;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class DownloadApkCommandHandler implements CommandHandler
{

    /**
     * @var ApkDownloader
     */
    private $downloader;

    public function __construct(ApkDownloader $downloader)
    {
        $this->downloader = $downloader;
    }

    public function __invoke(DownloadApkCommand $command)
    {
        apply($this->downloader, [new ApkId($command->id())]);
    }
}
