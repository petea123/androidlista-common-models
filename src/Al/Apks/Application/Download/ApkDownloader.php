<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Download;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\ApkParser;
use App\Al\Apks\Domain\Service\ApkFinder;
use App\Al\Apks\Domain\Service\ApkParserInterface;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\DownloaderManager;
use App\Shared\Domain\FilesystemInterface;
use SplFileInfo;

final class ApkDownloader
{
    /**
     * @var FilesystemInterface
     */
    private $apksStorageMount;
    /**
     * @var DownloaderManager
     */
    private $downloader;

    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var ApkRepositoryInterface
     */
    private $apkRepository;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    /**
     * @var ApkFinder
     */
    private $finder;

    /**
     * @var ApkParser
     */
    private $parser;
    /**
     * @var WebParserFactory
     */
    private $webParserFactory;

    public function __construct(
        DownloaderManager $downloader,
        FilesystemInterface $apksStorageMount,
        QueryBus $queryBus,
        ApkRepositoryInterface $apkRepository,
        DomainEventPublisher $publisher,
        ApkParserInterface $apkParser,
        WebParserFactory $webParserFactory
    ) {
        $this->finder = new ApkFinder($apkRepository);
        $this->parser = new ApkParser($apkRepository, $apkParser);
        $this->apksStorageMount = $apksStorageMount;
        $this->downloader = $downloader;
        $this->queryBus = $queryBus;
        $this->apkRepository = $apkRepository;
        $this->publisher = $publisher;
        $this->webParserFactory = $webParserFactory;
    }

    public function __invoke(ApkId $id)
    {
        $apk = $this->finder->search($id);

        if (null !== $apk->state()->value() || null === $tmpApk = $this->download($apk)) {
            return;
        }

        if (false === $this->parser->parseApkStream($tmpApk, $apk)) {
            return;
        }

        if ($this->apksStorageMount->putStream(Url::create('local://'.$apk->url()->value()), $tmpApk)) {
            $this->apkRepository->save($apk);
            $this->publisher->record(...$apk->pullDomainEvents());
        }
    }

    private function download(Apk $apk): ?SplFileInfo
    {
        if (null === $item = $this->queryBus->ask(new FindItemByIdQuery($apk->itemId()->value()))) {
            return null;
        }

        if (null === $parser = $this->webParserFactory->instance($apk->source(), new ItemGoogle($item->web()))) {
            return null;
        }

        return $parser->apkDownload() ? $this->downloader->proxyDownload($parser->apkDownload()) : null;
    }
}
