<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Download;

use App\Al\Apks\Domain\Event\ApkWasCreated;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class DownloadApkOnApkCreated implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasCreated $event)
    {
        $this->commandBus->handle(new DownloadApkCommand($event->aggregateId()));
    }

    public static function subscribedTo(): array
    {
        return [ApkWasCreated::class];
    }
}
