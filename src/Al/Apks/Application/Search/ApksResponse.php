<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Search;

use App\Shared\Domain\Bus\Query\Response;

final class ApksResponse implements Response
{
    /** @var ApkResponse[] */
    private $apks;

    public function __construct(ApkResponse ...$apks)
    {
        $this->apks = $apks;
    }

    public function apks(): array
    {
        return $this->apks;
    }
}
