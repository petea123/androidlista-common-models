<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Search;

use App\Shared\Domain\Bus\Query\Response;

final class ApkResponse implements Response
{
    private $id;
    private $itemId;
    private $signature;
    private $version;

    public function __construct(string $id, int $itemId, string $signature, string $version)
    {
        $this->id = $id;
        $this->itemId = $itemId;
        $this->signature = $signature;
        $this->version = $version;
    }

    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function signature(): string
    {
        return $this->signature;
    }

    /**
     * @return string
     */
    public function version(): string
    {
        return $this->version;
    }
}
