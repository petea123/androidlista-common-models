<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Search\ByCriteria;

use App\Al\Apks\Application\Search\ApkResponse;
use App\Al\Apks\Application\Search\ApksResponse;
use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class ApksByCriteriaSearcher
{
    /** @var ApkRepositoryInterface */
    private $repository;

    public function __construct(ApkRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search(Filters $filters, Order $order, ?int $limit, ?int $offset): ApksResponse
    {
        $criteria = new Criteria($filters, $order, $offset, $limit);

        return new ApksResponse(...map($this->toResponse(), $this->repository->matching($criteria)));
    }

    private function toResponse(): callable
    {
        return static function (Apk $apk) {
            return new ApkResponse($apk->id()->value(), $apk->itemId()->value(), $apk->signature()->value(), $apk->version()->value());
        };
    }
}
