<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Search\ByCriteria;

use App\Al\Apks\Application\Search\ApksResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchApksByCriteriaQueryHandler implements QueryHandler
{
    /** @var ApksByCriteriaSearcher */
    private $searcher;

    public function __construct(ApksByCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchApksByCriteriaQuery $query): ApksResponse
    {
        $filters = Filters::fromValues($query->filters());
        $order   = Order::fromValues($query->orderBy(), $query->order());

        return $this->searcher->search($filters, $order, $query->limit(), $query->offset());
    }
}
