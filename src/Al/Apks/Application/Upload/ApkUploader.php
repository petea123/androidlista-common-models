<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Upload;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\FilesystemInterface;

final class ApkUploader
{
    /**
     * @var ApkRepositoryInterface
     */
    private $apkRepository;

    /** @var DomainEventPublisher */
    private $publisher;

    /**
     * @var FilesystemInterface
     */
    private $apksStorageMount;

    /**
     * ApkUploader constructor.
     * @param ApkRepositoryInterface $apkRepository
     * @param DomainEventPublisher $publisher
     * @param FilesystemInterface $apksStorageMount
     */
    public function __construct(
        ApkRepositoryInterface $apkRepository,
        DomainEventPublisher $publisher,
        FilesystemInterface $apksStorageMount
    ) {
        $this->apkRepository = $apkRepository;
        $this->publisher = $publisher;
        $this->apksStorageMount = $apksStorageMount;
    }

    public function __invoke(ApkId $id): void
    {
        $apk = $this->apkRepository->search($id);

        if (null !== $apk && $this->upload($apk)) {
            $apk->uploaded();
            $this->apkRepository->save($apk);
            $this->publisher->record(...$apk->pullDomainEvents());
        }
    }

    private function upload(Apk $apk): bool
    {
        if (!$apk->isPendingForUpload()) {
            return false;
        }

        if (!$this->apksStorageMount->move(
            Url::create('local://'.$apk->url()->value()),
            Url::create('s3://'.$apk->url()->value())
        )) {
            $this->apksStorageMount->delete(Url::create('local://'.$apk->url()->value()));
            return false;
        }

        return true;
    }
}
