<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\Upload;

use App\Al\Apks\Domain\Event\ApkWasScanned;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class UploadApkOnApkScanned implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ApkWasScanned $event)
    {
        $this->commandBus->handle(new UploadApkCommand($event->aggregateId()));
    }

    public static function subscribedTo(): array
    {
        return [
            ApkWasScanned::class
        ];
    }
}
