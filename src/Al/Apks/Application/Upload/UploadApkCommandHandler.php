<?php

declare(strict_types=1);


namespace App\Al\Apks\Application\Upload;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class UploadApkCommandHandler implements CommandHandler
{

    /**
     * @var ApkUploader
     */
    private $uploader;

    public function __construct(ApkUploader $uploader)
    {

        $this->uploader = $uploader;
    }

    public function __invoke(UploadApkCommand $command)
    {
        $id = new ApkId($command->id());

        apply($this->uploader, [$id]);
    }
}
