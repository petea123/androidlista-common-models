<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadUrl;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;
use const Lambdish\Phunctional\apply;

final class GenerateDownloadUrlCommandHandler implements CommandHandler
{
    /**
     * @var ApkDownloadUrlGenerator
     */
    private $generator;

    public function __construct(ApkDownloadUrlGenerator $generator)
    {
        $this->generator = $generator;
    }

    public function __invoke(GenerateDownloadUrlCommand $command)
    {
        $itemId = new ItemId($command->itemId());
        $apkId = new ApkId($command->id());

        apply($this->generator, [$itemId, $apkId]);
    }
}
