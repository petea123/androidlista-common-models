<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadUrl;

use App\Al\Items\Domain\Event\ItemGooglePlayWasRemoved;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\ValueObject\Uuid;

final class GenerateDownloadUrlOnItemGooglePlayRemoved implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ItemGooglePlayWasRemoved $event): void
    {
        $this->commandBus->handle(new GenerateDownloadUrlCommand((int)$event->aggregateId(), Uuid::random()->value()));
    }

    public static function subscribedTo(): array
    {
        return [ItemGooglePlayWasRemoved::class];
    }
}
