<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadUrl;

use App\Al\Items\Domain\ApkPure\ItemApkPureWasCreated;
use App\Al\Items\Domain\Event\ItemWasCreated;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\ValueObject\Uuid;

final class GenerateDownloadUrlOnItemCreated implements DomainEventSubscriber
{

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(DomainEvent $event): void
    {
        $this->commandBus->handle(new GenerateDownloadUrlCommand((int)$event->aggregateId(), Uuid::random()->value()));
    }

    public static function subscribedTo(): array
    {
        return [
            ItemWasCreated::class,
            ItemApkPureWasCreated::class
        ];
    }
}
