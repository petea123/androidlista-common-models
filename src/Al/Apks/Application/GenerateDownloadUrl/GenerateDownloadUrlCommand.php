<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadUrl;

use App\Shared\Domain\Bus\Command\Command;

final class GenerateDownloadUrlCommand extends Command
{
    /**
     * @var int
     */
    private $itemId;
    /**
     * @var string
     */
    private $id;

    public function __construct(int $itemId, string $id)
    {
        $this->itemId = $itemId;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
