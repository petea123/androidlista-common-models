<?php

declare(strict_types=1);

namespace App\Al\Apks\Application\GenerateDownloadUrl;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\ApkCreator;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Infrastructure\WebParser\WebParserFactory;
use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;

final class ApkDownloadUrlGenerator
{
    /**
     * @var ApkRepositoryInterface
     */
    private $repository;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    /**
     * @var ApkCreator
     */
    private $creator;
    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(
        ApkRepositoryInterface $repository,
        QueryBus $queryBus,
        DomainEventPublisher $publisher,
        WebParserFactory $webParserFactory
    ) {
        $this->repository = $repository;
        $this->publisher = $publisher;
        $this->creator = new ApkCreator($repository, $webParserFactory);
        $this->queryBus = $queryBus;
    }

    public function __invoke(ItemId $itemId, ApkId $id)
    {
        $apk = $this->repository->searchByItemId($itemId, 1);

        if (!empty($apk)) {
            return;
        }

        /** @var ItemView $item */
        if (null === $item = $this->queryBus->ask(new FindItemByIdQuery($itemId->value()))) {
            return;
        }

        if (null === $apk = $this->create($id, $item)) {
            return;
        }

        $this->repository->save($apk);
        $this->publisher->record(...$apk->pullDomainEvents());
    }

    private function create(ApkId $id, ItemView $item = null): ?Apk
    {
        foreach (ApkSource::values() as $value) {
            $source = ApkSource::fromString($value);
            if (!$source->equals(ApkSource::uptodown())) {
                if (null !== $apk = $this->creator->create($id, $source, $item)) {
                    return $apk;
                }
            }

        }
        return null;
    }
}
