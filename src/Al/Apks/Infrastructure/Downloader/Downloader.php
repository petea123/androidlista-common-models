<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\Downloader;

use App\Al\Apks\Domain\Service\DownloaderInterface;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Shared\Domain\FilesystemInterface;
use App\Al\Shared\Infrastructure\ProxyBonanza\ProxyBonanza;
use ZipArchive;

final class Downloader implements DownloaderInterface
{

    /**
     * @var FilesystemInterface
     */
    private $localStorage;

    public function __construct(FilesystemInterface $localStorage)
    {
        $this->localStorage = $localStorage;
    }

    public function download(ExternalUrl $url, Url $saveTo): bool
    {
        $response = fopen($url->value(), 'r', false, ProxyBonanza::sharedStreamContext());

        if(false === $response || !is_resource($response)) {
            return false;
        }

        $result = $this->localStorage->writeStream($saveTo->value(), $response);

        fclose($response);

        return $result;
    }

    public function isXapk(Url $path): ?string
    {
        $zip = new ZipArchive();
        $res = $zip->open($this->localStorage->absolutePath($path->value()));

        if ($res !== true) {
            $this->delete($path);
            throw new \RuntimeException('Fail open apk at path: '.$this->localStorage->absolutePath($path->value()).' with error: '.$res);
        }

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $file = $zip->getNameIndex($i);
            if (preg_match('#\.(apk)$#i', $file)) {
                $zip->close();
                return $file;
            }
        }

        $zip->close();
        return null;
    }

    public function extractFromXapk(Url $path, string $file): bool
    {
        $apk = $this->localStorage->unzip($path->value(), $file);
        return $this->localStorage->update($path->value(), $apk);
    }

    public function delete(Url $path): void
    {
        $this->localStorage->delete($path->value());
    }
}
