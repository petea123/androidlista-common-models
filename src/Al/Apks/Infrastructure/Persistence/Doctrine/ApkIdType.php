<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\Persistence\Doctrine;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class ApkIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'apk_id';
    }

    protected function typeClassName(): string
    {
        return ApkId::class;
    }
}
