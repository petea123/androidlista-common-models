<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\Persistence\Doctrine;

use App\Al\Apks\Domain\ValueObject\ApkDate;
use App\Shared\Infrastructure\Persistence\Doctrine\DateType;

final class ApkDateType extends DateType
{

    public static function customTypeName(): string
    {
        return 'apk_date';
    }

    protected function typeClassName(): string
    {
        return ApkDate::class;
    }
}
