<?php

declare(strict_types=1);


namespace App\Al\Apks\Infrastructure\Persistence;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlApkRepository extends DoctrineRepository implements ApkRepositoryInterface
{

    public function save(Apk $apk): void
    {
        $this->persist($apk);
    }

    public function delete(Apk $apk): void
    {
        $this->remove($apk);
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        return $this->repository(Apk::class)->matching($doctrineCriteria)->toArray();
    }

    public function search(ApkId $apk): ?Apk
    {
        return $this->repository(Apk::class)->find($apk);
    }

    public function searchByItemId(ItemId $itemId, int $limit = null): array
    {
        return $this->repository(Apk::class)->findBy(['itemId' => $itemId], null, $limit);
    }
}
