<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\Persistence;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\State;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Utils;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlLegacyApkRepository extends DoctrineRepository implements ApkRepositoryInterface
{

    public function save(Apk $apk): void
    {

        $query = "
            insert into tbl_apk_item(item_id,apk,external_url,version,size,version_date,signature,sha1,is_processed,is_dangerous) 
            values(
                   " . $apk->itemId()->value() . ",
                   '" . $apk->url()->value() . "',
                   '" . $apk->externalUrl()->value() . "',
                   '" . $apk->version()->value() . "',
                   '" . $apk->size()->value() . "',
                   '" . $apk->date()->value()->format('d-m-Y') . "',
                   '" . $apk->signature()->value() . "',
                   '" . $apk->sha1()->value() . "',
                   1,
                   '" . $apk->state()->equals(State::dangerous()) . "'
            );
         ";

        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();

        $query = "select * from tbl_apk_item where item_id=" . $apk->itemId()->value() . " and version='" . $apk->version()->value() . "' order by id desc limit 1;";
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $apkSql = $stmt->fetchAll();

        $this->insertVirusTotal($apkSql[0]['id'], $apk);

        $this->insertVersionOnItemData($apk->itemId(), $apk->version());
        $this->insertVersionDateOnItemData($apk->itemId(), $apk->date()->value());
    }

    private function insertVirusTotal($id, Apk $apk)
    {
        $query = "
            insert into tbl_virus_total(id_apk_item,url_virus_total,data,is_dangerous) 
            values(
                   " . $id . ",
                   '" . $apk->url()->value() . "',
                   '" . base64_encode(serialize($apk->report()->value())) . "',
                   '" . $apk->state()->equals(State::dangerous()) . "'
            );
         ";

        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
    }

    private function insertVersionOnItemData(ItemId $itemId, Version $version)
    {
        $query = "delete from tbl_item_data where typefield_id=13 and item_id=" . $itemId->value() . ";";
        $query .= "insert into tbl_item_data(item_id,type_id,typefield_id,value,value_dirify,hash,language_code,value_extended,extra_information) values(" . $itemId->value() . ",1,13,'" . $version->value() . "','" . Utils::myDirify($version->value()) . "','" . hash('md5',
                $version->value()) . "','','" . $version->value() . "','" . $version->value() . "');";
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
    }

    private function insertVersionDateOnItemData(ItemId $itemId, \DateTimeInterface $date)
    {
        $date = $date->format('d-m-Y');
        $query = "delete from tbl_item_data where typefield_id=17 and item_id=" . $itemId->value() . ";";
        $query .= "insert into tbl_item_data(item_id,type_id,typefield_id,value,value_dirify,hash,language_code,value_extended,extra_information) values(" . $itemId->value() . ",1,17,'" . $date . "','" . Utils::myDirify($date) . "','" . hash('md5',
                $date) . "','','" . $date . "','" . $date . "');";
        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
    }

    public function delete(Apk $apk): void
    {
        // TODO: Implement delete() method.
    }

    public function matching(Criteria $criteria): array
    {
        // TODO: Implement matching() method.
    }

    public function search(ApkId $apk): ?Apk
    {
        // TODO: Implement search() method.
    }

    public function searchByItemId(ItemId $itemId, int $limit = null): array
    {
        // TODO: Implement searchByItemId() method.
    }
}
