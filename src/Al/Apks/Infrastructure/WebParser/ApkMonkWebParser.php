<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\WebParser;

use App\Al\Items\Domain\ValueObject\ItemName;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Shared\Infrastructure\ProxyBonanza\ProxyBonanza;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\PackageValueObject;
use Symfony\Component\DomCrawler\Crawler;

final class ApkMonkWebParser extends WebParser
{
    private const BASE = 'https://www.apkmonk.com/app/';

    public static function getApp(PackageValueObject $googleId, LocaleValueObject $locale =null): ?self
    {
        $html = @file_get_contents(
            sprintf('%s%s/', self::BASE, $googleId->id()),
            false,
            ProxyBonanza::sharedStreamContext()
        );

        if (false === $html) {
            return null;
        }

        $crawler = new Crawler($html);

        $apkUrl = $crawler->filterXPath("//a[@id='download_button']");
        if ($apkUrl->count() <= 0) {
            return null;
        }

        $apkUrl = new ExternalUrl($apkUrl->attr('href'));
        $apkDownload = self::apkDownloadUrl($apkUrl, $googleId->id());
        return new self($googleId->id(), $apkUrl, $apkDownload);
    }

    private static function apkDownloadUrl(ExternalUrl $url, string $googleId): ?ExternalUrl
    {
        $array = explode('/', $url->value());
        array_pop($array);
        $idMonk = end($array);

        $url = 'https://www.apkmonk.com/down_file/?pkg='.$googleId.'&key='.$idMonk;

        $json = @file_get_contents($url, false, ProxyBonanza::sharedStreamContext());

        if (false === $json) {
            return null;
        }

        $response = json_decode($json, true);

        if (isset($response['resp']) && $response['resp'] === 'success') {
            return new ExternalUrl($response['url']);
        }

        return null;
    }

    public function versions(): array
    {
        $html = self::getContent(sprintf('%s%s/', self::BASE, $this->packageId));
        $crawler = new Crawler($html);
        $version = $crawler->filterXPath("//td[contains(text(),'App Download Version')]/following-sibling::td/span");

        if (0 === $version->count() || null === $this->apkDownload()) {
            return [];
        }

        return [trim($version->text()) => $this->apkDownload()];
    }

    public function toValues(): array
    {
        // TODO: Implement toValues() method.
    }

    public function names(): ItemName
    {
        // TODO: Implement names() method.
    }
}
