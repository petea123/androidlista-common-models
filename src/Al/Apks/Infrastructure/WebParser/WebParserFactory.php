<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\WebParser;

use App\Al\Apks\Domain\ApkWebParser;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Apks\Domain\Service\WebParserFactory as DomainWebParserFactory;
use App\Shared\Domain\ValueObject\PackageValueObject;

final class WebParserFactory implements DomainWebParserFactory
{

    public function instance(ApkSource $source, PackageValueObject $package): ?ApkWebParser
    {
        switch ($source) {
            case ApkSource::apkmonk()->equals($source):
                $app = ApkMonkWebParser::getApp($package);
                return $app ?: null;
            case ApkSource::apkpure()->equals($source):
                $app = ApkPureWebParser::getApp($package);
                return $app ?: null;
            case ApkSource::uptodown()->equals($source):
                $app = UptodownWebParser::getApp($package);
                return $app ?: null;
            default:
                return null;
        }
    }
}
