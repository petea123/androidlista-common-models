<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\WebParser;

use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Al\Shared\Infrastructure\ProxyBonanza\ProxyBonanza;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\PackageValueObject;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

final class ApkPureWebParser extends WebParser
{
    private const BASE = 'https://apkpure.com/%sroblox/%s';
    /**
     * @var Crawler
     */
    private $crawler;

    private function __construct(string $packageId, ExternalUrl $apkUrl, ?ExternalUrl $apkDownload, string $html)
    {
        parent::__construct($packageId, $apkUrl, $apkDownload);
        $this->crawler = new Crawler($html);
    }

    public static function getApp(PackageValueObject $package, LocaleValueObject $locale = null): ?self
    {
        $lang = (null === $locale) ? 'br' : self::ALTERNATE_LANGS[$locale->value()] ?? $locale->value();

        $url = sprintf(self::BASE, $lang.('' !== $lang ? '/' : ''), $package->id());
        $html = @file_get_contents($url, false, ProxyBonanza::sharedStreamContext());

        if (false === $html) {
            return null;
        }

        $crawler = new Crawler($html);

        $download = $crawler->filterXPath('//a[@class=" da"]');

        if ($download->count() == 0)  {
            return null;
        }

        $apkUrl = new ExternalUrl($url);
        $apkDownload = self::apkDownloadUrl(new ExternalUrl('https://apkpure.com'.$download->attr('href')), $apkUrl);

        return new self($package->id(), new ExternalUrl($url), $apkDownload, $html);
    }

    private static function apkDownloadUrl(ExternalUrl $url,ExternalUrl $referer): ?ExternalUrl
    {
        $client = new Client();

        $response = $client->request('GET', $url->value(), [
            'proxy' => 'http://owsdj0:xlwoggen@'.ProxyBonanza::randomSharedProxy(),
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
            ],
            'timeout' => 3,
            'allow_redirects' => true,
        ]);

        $html = $response->getBody()->getContents();

        if (false === $html) {
            return null;
        }

        $crawler = new Crawler($html);

        $apkUrl = $crawler->filterXPath('//a[@id="download_link"]');

        return $apkUrl->count() > 0 ? new ExternalUrl($apkUrl->attr('href')) : null;
    }

    public function versions(): array
    {
        $html = self::getContent($this->apkUrl()->value());
        $crawler = new Crawler($html);
        $version = $crawler->filterXPath("//span[@itemprop='version']");

        if (0 === $version->count() || null === $this->apkDownload()) {
            return [];
        }

        return [trim($version->text()) => $this->apkDownload()];
    }

    public function lastVersion(): string
    {
        $version = $this->filter("//span[@itemprop='version']");

        return 0 === $version->count() ? '' : trim($version->text());
    }

    public function name(): string
    {
        $name = $this->filter("//span[@itemprop='name']");

        return 0 === $name->count() ? '' : trim($name->text());
    }

    public function names(): ItemName
    {
        $values = [];
        foreach (LocaleValueObject::values() as $locale) {
            $app = self::getApp(ItemApkPurePackage::createFromPackage($this->packageId), LocaleValueObject::fromString($locale));
            $values[$locale] = (null !== $app) ? $app->name() : '';
        }

        return new ItemName(
            $values['en'],
            $values['br'],
            $values['es'],
            $values['fr'],
            $values['id'],
            $values['it'],
            $values['de'],
            $values['tr'],
            $values['ru'],
            $values['ja'],
            $values['ko'],
            $values['pl'],
            $values['ro'],
            $values['vn'],
            $values['th'],
            $values['nl'],
            $values['gr']
        );
    }

    public function votes(): int
    {
        $votes = $this->filter("//meta[@itemprop='ratingCount']");

        return 0 === $votes->count() ? 0 : (int)filter_var(
            $votes->attr('content'),
            FILTER_SANITIZE_NUMBER_INT
        );
    }

    public function rating(): float
    {
        $rating = $this->filter("//span[@itemprop='ratingValue']");

        return 0 === $rating->count() ? 0 : filter_var($rating->text(), FILTER_VALIDATE_FLOAT);
    }

    public function size(): string
    {
        $size = $this->filter("//span[@class='fsize']/span");
        return 0 === $size->count() ? '' : ltrim($size->text());
    }

    public function price(): float
    {
        $price = $this->filter("//meta[@itemprop='price']");

        return 0 === $price->count() ? 0 : filter_var($price->attr('content'), FILTER_VALIDATE_FLOAT);
    }

    public function osVersion(): string
    {
        $osVersion = $this->filter("//meta[@itemprop='operatingSystem']/preceding-sibling::p[1]");
        $version = rtrim(str_replace(['Android', '+', 'and', 'up'], '', $osVersion->text()));

        return 0 === $osVersion->count() ? '' : $version.' ou superior';
    }

    public function provider(): string
    {
        $provider = $this->filter("//p[@itemprop='publisher']/a");

        return 0 == $provider->count() ? '' : $provider->text();
    }

    public function category(): array
    {
        $category = $this->filter("//meta[@itemprop='applicationSubCategory']");

        return 0 == $category->count() ? [] : self::CATEGORIES_AL[$category->attr('content')];
    }

    public function icon(): ?ExternalUrl
    {
        $icon = $this->filter("//div[@class='icon']//img");
        return 0 == $icon->count() ? null : new ExternalUrl($icon->attr('src'));
    }

    public function officialUrl(): string
    {
        $url = $this->filter("//a[@class='ga']");
        return 0 == $url->count() ? '' : $url->attr('href');
    }

    public function toValues(): array
    {
        return [
            'names' => $this->names(),
            'icon' => $this->icon(),
            'packageId' => $this->packageId,
            'votes' => $this->votes(),
            'rating' => $this->rating(),
            'size' => $this->size(),
            'price' => $this->price(),
            'web' => $this->apkUrl->value(),
            'minAndroidVersion' => $this->osVersion(),
            'developer' => $this->provider(),
            'category' => $this->category(),
            'officialUrl' => $this->officialUrl(),
            'lastVersion' => $this->lastVersion()
        ];
    }

    private function filter(string $filter): Crawler
    {
        return $this->crawler->filterXPath($filter);
    }

    private const ALTERNATE_LANGS = [
        'en' => '',
        'ja' => 'jp',
        'ko' => 'kr',
    ];

    private const CATEGORIES_AL = [
        'Ação' => [1, 2],
        'Arcade' => [1, 39],
        'Aventura' => [1, 77],
        'Corridas' => [1, 138],
        'Cartas' => [1, 263],
        'Cassino' => [1, 275],
        'Casual' => [1, 39],
        'Esportes' => [1, 169],
        'Educativo' => [1, 348],
        'Estratégia' => [1, 121],
        'Tabuleiro' => [1, 286],
        'RPG' => [1, 257],
        'Música' => [1, 354],
        'Palavras' => [1, 211],
        'Quebra-cabeças' => [1, 211],
        'Quebra-cabeça' => [1, 211],
        'Simulação' => [1, 111],
        'Entretenimento' => [732, 1734],
        'Saúde e fitness' => [732, 1574],
        'Estilo de vida' => [732, 1734],
        'Educação' => [732, 1340],
        'Social' => [732, 2872],
        'Livros e referências' => [732, 839],
        'Corporativo' => [732, 945],
        'Música e áudio' => [732, 2398],
        'Personalização' => [357, false],
        'Notícias e revistas' => [732, 1842],
        'Ferramentas' => [732, 3207],
        'Mapas e navegação' => [732, 3303],
        'Turismo e local' => [732, 3445],
        'Produtividade' => [732, 2565],
        'Compras' => [732, 2736],
        'Comunicação' => [732, 1158],
        'Finanças' => [732, 1494],
        'Clima' => [732, 3620],
        'Humor' => [732, 1095],
        'Fotografia' => [732, 2436],
        'Reproduzir e editar vídeos' => [732, 1947],
        'Bibliotecas e demos' => [3207],
        'Medicina' => [732, 2005],
        'Arte e design' => [732, 794],
        'Veículos' => [732, 3303],
        'Beleza' => [732, 1574, 1725],
        'Encontros' => [732, 2872, 2889],
        'Eventos' => [732, 1734],
        'Comer e beber' => [732, 1236],
        'Casa e decoração' => [732, 794, 820],
        'Criar os filhos' => [732, 1340, 1349]
    ];
}
