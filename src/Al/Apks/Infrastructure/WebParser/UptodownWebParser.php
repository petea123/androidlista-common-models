<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\WebParser;

use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Domain\ValueObject\ItemName;
use App\Al\ItemsUptodown\Domain\ValueObject\UptodownId;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\PackageValueObject;
use Symfony\Component\DomCrawler\Crawler;

final class UptodownWebParser extends WebParser
{

    private const API = 'http://api.uptodown.com/en/9d2b1ad5bbc16c44d49116dc213c53f2/app';
    private const WEB = 'https://en.uptodown.com';

    private const CATEGORIES = [
        ['newreleases', 'action-adventure'],
        ['top', 'arcade'],
        ['launchers', 'tv-radio'],
        ['personalization', 'video'],
        ['security-performance', 'fitness'],
        ['food-and-drink', 'strategy'],
        ['sports', 'rpg'],
        ['racing-sims', 'audio'],
        ['video-audio-downloaders', 'photography'],
        ['religion', 'kids'],
        ['networks', 'messaging'],
        ['education-languages', 'health'],
        ['emulators', 'travel'],
        ['shopping', 'platformer'],
        ['casual', 'keyboards'],
        ['file-management', 'browsing'],
        ['social', 'root'],
        ['writing-notes'],
        ['schedules-calendars'],
        ['other-tools'],
        ['communication-utilities'],
        ['weather'],
        ['design-and-fashion'],
        ['card-games'],
        ['personal'],
        ['IOT'],
        ['puzzle'],
        ['other-games'],
        ['download-managers'],
        ['books-comics-readers'],
        ['funny']
    ];
    /**
     * @var int
     */
    private $uptodownId;

    public function __construct(string $packageId, ExternalUrl $apkUrl, ExternalUrl $apkDownload, int $uptodownId = 0)
    {
        parent::__construct($packageId, $apkUrl, $apkDownload);
        $this->uptodownId = $uptodownId;
    }

    public static function getApp(PackageValueObject $packageId, LocaleValueObject $locale = null): ?WebParser
    {
        $url = sprintf('%s?packagename=%s', self::API, $packageId->id());

        $html = self::getContent($url);
        if (empty($html)) {
            return null;
        }

        $data = json_decode($html, true);

        if (empty($data['data']['url'])) {
            return null;
        }

        $uptodownId = self::getUptodownId($data['data']['url']);
        $apkUrl = new ExternalUrl($data['data']['url']);
        $apkDownload = self::apkDownloadUrl(new ExternalUrl($data['data']['url'].'/download'));
        return new self($packageId->value(), $apkUrl, $apkDownload, $uptodownId);
    }

    private static function apkDownloadUrl(ExternalUrl $url): ?ExternalUrl
    {
        $html = self::getContent($url->value());
        if (empty($html)) {
            return null;
        }

        $crawler = new Crawler($html);

        $apkUrl = $crawler->filterXPath('//a[@class="button download"]')->attr('href');

        return $apkUrl ? new ExternalUrl($apkUrl) : null;
    }

    public static function itemsInCategory(string $category, int $pages): array
    {
        $items = [];

        for ($i = 1; $i <= $pages; ++$i) {
            $data = self::getContent(self::categoryUrl($category)."?pag=".$i);
            $response = json_decode($data, true);
            if (!empty($response)) {
                $items = array_merge($items, array_column($response, 'url'));
            }
        }

        array_walk($items, function (&$url) {
            $url = new ExternalUrl($url);
        });

        return $items;
    }

    public static function categoryByDay(int $day): array
    {
        return self::CATEGORIES[$day - 1];
    }

    private static function categoryUrl(string $category): string
    {
        switch ($category) {
            case 'top':
            case 'newreleases':
                return self::WEB."/android/apps/".$category;
            default:
                return self::WEB."/android/".$category."/apps";
        }
    }

    public function versions(): array
    {
        $url = sprintf(
            '%s/apps/%s/versions?page[limit]=6&page[offset]=0',
            self::WEB,
            $this->uptodownId
        );

        $content = $this->getContent($url);
        if (empty($content)) {
            return [];
        }

        $response = json_decode($content, true)['data'] ?? [];

        $reindex = array_column($response, 'versionURL', 'version');

        array_walk($reindex, function (&$url) {
            $url = new ExternalUrl(str_replace('https://.en.uptodown.com/android', $this->apkUrl->value(), $url));
        });

        return $reindex;
    }

    private static function getUptodownId(string $url): int
    {
        $html = self::getContent($url);
        $crawler = new Crawler($html);

        $id = (int)filter_var(
            $crawler->filterXPath('//meta[@property="id_prog"]')->attr('content'),
            FILTER_SANITIZE_NUMBER_INT
        );

        if (false === $id || !is_int($id)) {
            throw new \InvalidArgumentException('No appId for uptodown relation with url: '.$url);
        }

        return $id;
    }

    public static function googlePackage(ExternalUrl $url): ?ItemGoogle
    {
        $html = self::getContent($url->value());
        if (null === $html) {
            return null;
        }
        $crawler = new Crawler($html);
        $googlePackage = filter_var(
            $crawler->filterXPath('(//div[@class="content"]/div[@class="full"]/div[2])[1]')->text(),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );

        return !empty($googlePackage) ? ItemGoogle::createFromPackage($googlePackage) : null;
    }

    /**
     * @return UptodownId
     */
    public function uptodownId(): UptodownId
    {
        return new UptodownId($this->uptodownId);
    }

    public function toValues(): array
    {
        // TODO: Implement toValues() method.
    }

    public function names(): ItemName
    {
        // TODO: Implement names() method.
    }
}
