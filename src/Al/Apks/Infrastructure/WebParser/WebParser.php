<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\WebParser;

use App\Al\Apks\Domain\ApkWebParser;
use App\Al\Shared\Infrastructure\ProxyBonanza\ProxyBonanza;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\PackageValueObject;

//TODO MOVER A SHARED
abstract class WebParser implements ApkWebParser
{

    /**
     * @var string
     */
    protected $packageId;
    /**
     * @var ExternalUrl
     */
    protected $apkUrl;
    /**
     * @var ExternalUrl
     */
    private $apkDownload;

    protected function __construct(string $packageId, ExternalUrl $apkUrl = null, ?ExternalUrl $apkDownload = null)
    {
        $this->packageId = $packageId;
        $this->apkUrl = $apkUrl;
        $this->apkDownload = $apkDownload;
    }

    abstract public static function getApp(PackageValueObject $package, LocaleValueObject $locale = null);

    /**
     * @return ExternalUrl
     */
    public function apkUrl(): ?ExternalUrl
    {
        return $this->apkUrl;
    }

    public function hasApk(): bool
    {
        return !empty($this->apkUrl);
    }

    protected static function getContent(string $url)
    {
        $handle = @fopen($url, "r", false, ProxyBonanza::sharedStreamContext());

        if (!is_resource($handle)) {
            return null;
        }

        $response = stream_get_contents($handle);
        fclose($handle);
        return $response;
    }

    /**
     * @return ExternalUrl
     */
    public function apkDownload(): ?ExternalUrl
    {
        return $this->apkDownload;
    }
}
