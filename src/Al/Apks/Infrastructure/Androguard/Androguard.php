<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\Androguard;

use App\Al\Apks\Infrastructure\ApkParser\ApkParseException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Androguard
{

    /**
     * @var string
     */
    private $binary;

    public function __construct()
    {
        $this->binary = getenv('ANDROGUARD_BIN');
    }

    public function signature(string $path): string
    {
        $process = new Process([$this->binary, 'sign', $path]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        if (preg_match('/(sha1) ([a-zA-Z0-9]+)/', $process->getOutput(), $match) !== false) {
            return $match[2];
        }

        return '';
    }

    public function displayedVersion(string $path): string
    {
        $process = new Process([$this->binary, 'apkid', $path]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $version = json_decode($process->getOutput(), true);

        if (0 !== json_last_error()) {
            throw new ApkParseException($path);
        }

        sort($version);
        $version = !empty($version[0][2]) ? $version[0][2] : $version[0][1];
        return $version;
    }
}
