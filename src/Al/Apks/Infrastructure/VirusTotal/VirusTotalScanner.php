<?php


namespace App\Al\Apks\Infrastructure\VirusTotal;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Service\ScannerInterface;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Shared\Domain\FilesystemInterface;
use GuzzleHttp\Client;

final class VirusTotalScanner implements ScannerInterface
{

    const MAX_SIZE = 31;

    private const API_ENDPOINT = 'https://www.virustotal.com/vtapi/v2/';

    /** @var FilesystemInterface */
    private $apksStorageMount;

    /** @var Client */
    private $client;

    /** @var string */
    private $apiKey;

    public function __construct(FilesystemInterface $apksStorageMount)
    {
        $this->apksStorageMount = $apksStorageMount;
        $this->client = new Client([
            'base_uri' => self::API_ENDPOINT,

        ]);
        $this->apiKey = getenv('VIRUSTOTAL_API');
    }

    public function analyze(Apk $apk): ?array
    {
        $report = $this->scan($apk);

        if (empty($report) || !isset($report['resource'])) {
            return null;
        }

        return $this->getReportAllInfo($report['resource']);
    }

    private function scan(Apk $apk): array
    {
        $file = $this->apksStorageMount->readStream(Url::create('local://'.$apk->url()->value()));

        if (null === $file) {
            return null;
        }

        $url = (self::MAX_SIZE > $apk->size()->toMB()) ? 'file/scan' :
            str_replace(self::API_ENDPOINT, '', $this->generateUploadUrl());

        $response = $this->client->post(
            $url,
            [
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents' => fopen($file->getRealPath(), 'r'),
                        'filename' => $file->getFilename()
                    ],
                    [
                        'name' => 'apikey',
                        'contents' => $this->apiKey
                    ]
                ],
                'headers' => ['Accept-Encoding' => 'gzip,deflate']
            ]
        );

        return json_decode((string)$response->getBody(), true);
    }

    public function getReportAllInfo($resource)
    {

        $data = $this->client->post(
            'file/report',
            [
                'form_params' => [
                    'resource' => $resource,
                    'allinfo' => 1,
                    'apikey' => $this->apiKey
                ]
            ]
        );

        return json_decode((string)$data->getBody(), true);
    }

    private function generateUploadUrl(): string
    {
        $response = $this->client->get(
            'file/scan/upload_url?'.http_build_query(['apikey' => $this->apiKey]),
            [
                'headers' => ['Accept-Encoding' => 'gzip,deflate']
            ]
        );

        $response = (string)$response->getBody();
        return json_decode($response, true)['upload_url'];
    }
}
