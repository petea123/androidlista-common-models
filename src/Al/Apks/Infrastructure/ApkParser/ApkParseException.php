<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\ApkParser;

use RuntimeException;

final class ApkParseException extends RuntimeException
{

    public function __construct(string $path)
    {
        parent::__construct(sprintf('ApkParser exception with path <%s>', $path));
    }
}