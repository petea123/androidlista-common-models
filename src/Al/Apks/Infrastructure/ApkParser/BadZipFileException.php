<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\ApkParser;

use RuntimeException;

final class BadZipFileException extends RuntimeException
{

    public function __construct(string $path)
    {
        parent::__construct(sprintf('BadZip File exception with path <%s>', $path));
    }
}