<?php

declare(strict_types=1);

namespace App\Al\Apks\Infrastructure\ApkParser;

use App\Al\Apks\Domain\Service\ApkParserInterface;
use App\Al\Apks\Domain\ValueObject\File\Sha1;
use App\Al\Apks\Domain\ValueObject\File\Signature;
use App\Al\Apks\Domain\ValueObject\File\Size;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Apks\Infrastructure\Androguard\Androguard;
use SplFileInfo;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use ZipArchive;

final class ApkParser implements ApkParserInterface
{

    /**
     * @var Androguard
     */
    private $androguard;
    /**
     * @var SplFileInfo
     */
    private $resource;

    public function __construct(Androguard $androguard)
    {
        $this->androguard = $androguard;
    }

    public function setApkStream(SplFileInfo $resource): void
    {
        $this->resource = $this->guardApk($resource);
    }

    public function signature(): Signature
    {
        return new Signature($this->androguard->signature($this->resource->getRealPath()));
    }

    public function displayedVersion(): Version
    {
        return new Version($this->androguard->displayedVersion($this->resource->getRealPath()));
    }

    public function size(): Size
    {
        return new Size(filesize($this->resource->getRealPath()));
    }

    public function sha1(): Sha1
    {
        $process = new Process(['sha1sum', $this->resource->getRealPath()]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return new Sha1(explode(' ', $process->getOutput())[0]);
    }


    private function guardApk(SplFileInfo $resource): SplFileInfo
    {
        $zip = new ZipArchive();
        $res = $zip->open($resource->getRealPath(), ZIPARCHIVE::CHECKCONS);
        if ($res !== true) {
            throw new BadZipFileException('Invalid apk');
        }

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $name = $zip->getNameIndex($i);
            if ('AndroidManifest.xml' === $name) {
                $zip->close();
                return $resource;
            }
            if (preg_match('#\.(apk)$#i', $name)) {
                file_put_contents($resource->getRealPath(), $zip->getFromName($name));
                $zip->close();
                return $resource;
            }
        }

        $zip->close();

        return $resource;
    }
}
