<?php

namespace App\Al\Apks\Domain\ValueObject;

use App\Al\Apks\Domain\Exception\ApkInvalidState;
use App\Shared\Domain\ValueObject\Enum;
use InvalidArgumentException;

/**
 * @method static State pending()
 * @method static State processed()
 * @method static State dangerous()
 * @method static State pendingUpload()
 * @method static State created()
 */
final class State extends Enum
{

    public const PENDING = 0;
    public const PROCESSED = 1;
    public const DANGEROUS = 2;
    public const PENDING_UPLOAD = 3;
    public const CREATED = null;


    protected function throwExceptionForInvalidValue($value)
    {
        throw new InvalidArgumentException(sprintf('The <%s> is not a valid state type', $value));
    }

    public function notEquals(State $state)
    {
        return $state->value !== $this->value;
    }

    public function scanned(): State
    {
        if ($this->notEquals(State::pending())) {
            throw new ApkInvalidState();
        }

        return State::pendingUpload();
    }

    public function uploaded(): State
    {
        if ($this->notEquals(State::pendingUpload())) {
            throw new ApkInvalidState();
        }

        return State::processed();
    }
}
