<?php

namespace App\Al\Apks\Domain\ValueObject\File;

use App\Shared\Domain\ValueObject\IntValueObject;
use InvalidArgumentException;

final class Size extends IntValueObject
{
    public function __construct(int $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(int $size)
    {
        if (empty($size)) {
            throw new InvalidArgumentException(sprintf('The size is empty'));
        }
    }

    public function toMB(): int
    {
        return (int)($this->value / (1024 ** 2));
    }
}