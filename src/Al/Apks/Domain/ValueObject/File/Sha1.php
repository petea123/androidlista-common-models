<?php

namespace App\Al\Apks\Domain\ValueObject\File;

use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class Sha1 extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->guardSha1($value);
        parent::__construct(strtoupper($value));
    }

    private function guardSha1(string $sha1)
    {
        if (false == preg_match('/[0-9a-f]{40}/i', $sha1)) {
            throw new InvalidArgumentException(sprintf('invalid sha1'));
        }
    }
}