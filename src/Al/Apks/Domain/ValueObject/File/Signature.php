<?php

namespace App\Al\Apks\Domain\ValueObject\File;

use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class Signature extends StringValueObject
{

    CONST ATOZ_SIGNATURE = 'd75a495c4d7897534cc9910a034820abd87d7f2f';

    public function __construct(string $value)
    {
        $this->guard($value);

        parent::__construct(strtoupper($value));
    }

    private function guard(string $signature)
    {

        if (strtoupper($signature) === strtoupper(self::ATOZ_SIGNATURE)) {
            throw new InvalidArgumentException(sprintf('The signature is atoz'));
        }

        if (empty($signature)) {
            throw new InvalidArgumentException(sprintf('The signature is empty'));
        }

        if (false == preg_match('/[0-9a-f]{40}/i', $signature)) {
            throw new InvalidArgumentException(sprintf('invalid signature'));
        }
    }
}