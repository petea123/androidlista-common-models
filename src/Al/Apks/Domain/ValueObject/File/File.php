<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\ValueObject\File;

use App\Al\Shared\Domain\Apks\ApkSource;
use App\Shared\Domain\ValueObject\ExternalUrl;

final class File
{

    /** @var Size */
    private $size;

    /** @var Sha1 */
    private $sha1;

    /** @var Url */
    private $url;

    /** @var ExternalUrl */
    private $externalUrl;

    /** @var Signature */
    private $signature;

    /** @var ApkSource */
    private $source;

    /**
     * ApkVersionFile constructor.
     * @param Signature|null $signature
     * @param Size $size
     * @param Sha1 $sha1
     * @param Url $url
     * @param ExternalUrl $externalUrl
     * @param ApkSource $source
     */
    private function __construct(
        ?Signature $signature,
        ?Size $size,
        ?Sha1 $sha1,
        Url $url,
        ExternalUrl $externalUrl,
        ApkSource $source
    ) {
        $this->size = $size;
        $this->sha1 = $sha1;
        $this->url = $url;
        $this->externalUrl = $externalUrl;
        $this->signature = $signature;
        $this->source = $source;
    }

    public static function create(
        string $signature,
        int $size,
        string $sha1,
        string $url,
        string $externalUrl,
        string $source
    ) {
        return new self(
            new Signature($signature),
            new Size($size),
            new Sha1($sha1),
            new Url($url),
            new ExternalUrl($externalUrl),
            new ApkSource($source)
        );
    }

    public static function emptyFile(Url $to, ExternalUrl $externalUrl, ApkSource $source)
    {
        return new self(null, null, null, $to, $externalUrl, $source);
    }

    public function publishToCdn(): void
    {
        $this->url = $this->url->toCdn();
    }

    public function isTmp(): bool
    {
        return strpos($this->url->value(), 'http') === false;
    }

    /**
     * @return Url
     */
    public function url(): Url
    {
        return $this->url;
    }

    /**
     * @return ExternalUrl
     */
    public function externalUrl(): ExternalUrl
    {
        return $this->externalUrl;
    }

    /**
     * @return Signature
     */
    public function signature(): Signature
    {
        return $this->signature;
    }

    /**
     * @return Size
     */
    public function size(): Size
    {
        return $this->size;
    }

    /**
     * @return Sha1
     */
    public function sha1(): Sha1
    {
        return $this->sha1;
    }

    public function source(): ApkSource
    {
        return $this->source;
    }

    public function downloaded(Size $size, Signature $signature, Sha1 $sha): self
    {
        return new self($signature, $size, $sha, $this->url, $this->externalUrl, $this->source);
    }
}
