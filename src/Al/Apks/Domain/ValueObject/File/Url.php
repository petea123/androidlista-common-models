<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\ValueObject\File;

use App\Shared\Domain\ValueObject\PathValueObject;
use InvalidArgumentException;

final class Url extends PathValueObject
{

    public function toCdn(): Url
    {
        if ($this->isCdn()) {
            return $this;
        }

        $url = "http://".$this->subdomain().".cdnandroid.com/apks/".$this->value;
        $this->guardValidPath($url);

        return new self($url);
    }

    public function isCdn(): bool
    {
        return strpos($this->value, 'cdnandroid.com') !== false;
    }

    private function guardValidPath(string $path)
    {
        if (false === filter_var($path, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException(sprintf('Invalid path <%s>', $path));
        }

        return $path;
    }

    private function subdomain(): string
    {
        return substr($this->getDirify(), 0, 60);
    }

    private function getDirify(): string
    {
        $explode = explode('/', $this->value);

        return str_replace('.apk','', end($explode));
    }
}
