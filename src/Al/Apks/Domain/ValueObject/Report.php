<?php

namespace App\Al\Apks\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ArrayValueObject;
use InvalidArgumentException;
use function Lambdish\Phunctional\get;
use function Lambdish\Phunctional\get_in;

final class Report extends ArrayValueObject
{

    public const MIN_POSITIVES = 0;

    public const MIN_DOWNLOADS = 19;

    private const fields = [
        'md5',
        'scans',
        'total',
        'vhash',
        'sha256',
        'ssdeep',
        'scan_id',
        'resource',
        'last_seen',
        'permalink',
        'positives',
        'scan_date',
        'first_seen',
        'response_code',
        'times_submitted',
        'community_reputation',
        'additional_info' => ['androguard'],
        'isDangerous',
        'pendingReport'
    ];

    public function __construct(?array $value = [])
    {
        if (null === $value) {
            throw new \InvalidArgumentException('Nullable report is not valid');
        }

        $this->guardValidJson($value);

        $report = self::fromRawData($value);

        parent::__construct($this->validateReport($report));
    }

    private function guardValidJson(array $value)
    {

        $json = json_encode($value);
        if (0 !== json_last_error()) {
            throw new InvalidArgumentException(sprintf('The json is not well formatted'));
        }
    }

    private function validateReport(array $report): array
    {
        if (null === $report['positives']) {
            $report['positives'] = 0;
            $report['pendingReport'] = 1;
        }

        if ($report['positives'] > self::MIN_POSITIVES) {
            $report['isDangerous'] = 1;
        }

        return $report;
    }

    public function bypassDangerousReport(int $downloads): void
    {
        if ($downloads >= self::MIN_DOWNLOADS) {
            $this->value['scans'] = [];
            $this->value['positives'] = 0;
            $this->value['permalink'] = '';
            $this->value['isDangerous'] = 0;
        }
    }

    public function resource()
    {
        return $this->value['resource'];
    }

    public function permalink()
    {
        return $this->value['permalink'] ?? '';
    }

    public function positives()
    {
        return $this->value['positives'];
    }

    /**
     * @return int
     */
    public function isPending(): int
    {
        return $this->value['pendingReport'] ?? 0;
    }

    public function isDangerous(): bool
    {
        return $this->value['isDangerous'] ?? false;
    }

    private static function fromRawData(array $report): array
    {
        $array = [];
        foreach (self::fields as $key => $field) {
            if (is_array($field)) {
                foreach ($field as $subfield) {
                    $array[$key][$subfield] = get_in([$key, $subfield], $report);
                }
            } else {
                $array[$field] = get($field, $report);
            }
        }

        return $array;
    }
}
