<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\ValueObject;

use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class Version extends StringValueObject
{

    public function __construct(string $value)
    {
        $this->guard($value);

        parent::__construct($value);
    }

    private function guard(string $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException(sprintf('The version is empty'));
        }
    }
}