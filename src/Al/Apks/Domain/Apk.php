<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain;

use App\Al\Apks\Domain\Event\ApkWasCreated;
use App\Al\Apks\Domain\Event\ApkHasPendingReport;
use App\Al\Apks\Domain\Event\ApkWasDangerous;
use App\Al\Apks\Domain\Event\ApkWasDownloaded;
use App\Al\Apks\Domain\Event\ApkWasProcessed;
use App\Al\Apks\Domain\Event\ApkWasScanned;
use App\Al\Apks\Domain\Event\ApkWasUploaded;
use App\Al\Apks\Domain\ValueObject\ApkDate;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\File;
use App\Al\Apks\Domain\ValueObject\File\Sha1;
use App\Al\Apks\Domain\ValueObject\File\Signature;
use App\Al\Apks\Domain\ValueObject\File\Size;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Apks\Domain\ValueObject\Report;
use App\Al\Apks\Domain\ValueObject\State;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Domain\ValueObject\ExternalUrl;

class Apk extends AggregateRoot
{
    /** @var ApkId */
    private $id;

    /** @var ItemId */
    private $itemId;

    /** @var Version */
    private $version;

    /** @var File */
    private $file;

    /** @var Report */
    private $report;

    /** @var State */
    private $state;

    /** @var ApkDate */
    private $date;

    /**
     * Apk constructor.
     * @param ApkId $id
     * @param ItemId $itemId
     * @param Version $version
     * @param File $file
     * @param Report $report
     * @param State $state
     * @param ApkDate $date
     */
    public function __construct(
        ApkId $id,
        ItemId $itemId,
        ?Version $version,
        File $file,
        Report $report,
        ?State $state,
        ApkDate $date
    ) {
        $this->id = $id;
        $this->itemId = $itemId;
        $this->version = $version;
        $this->file = $file;
        $this->report = $report;
        $this->state = $state;
        $this->date = $date;
    }

    public static function create(ApkId $id, ItemId $itemId, File $file)
    {
        $apk = new self($id, $itemId, null, $file, new Report(), null, ApkDate::now());

        $apk->record(
            new ApkWasCreated(
                $id->value(),
                $itemId->value(),
                $file->url()->value(),
                $file->externalUrl()->value(),
                $file->source()->value()
            )
        );

        return $apk;
    }

    public function downloaded(Version $version, Size $size, Signature $signature, Sha1 $sha): void
    {
        $this->version = $version;
        $this->file = $this->file->downloaded($size, $signature, $sha);
        $this->state = State::pending();
        $this->record(
            new ApkWasDownloaded(
                $this->id->value(),
                $this->itemId->value(),
                $this->url()->value(),
                $version->value(),
                $this->file->source()->value()
            )
        );
    }

    /**
     * @return ItemId
     */
    public function itemId(): ItemId
    {
        return $this->itemId;
    }

    /**
     * @return File
     */
    public function file(): File
    {
        return $this->file;
    }

    /**
     * @return Url
     */
    public function url(): Url
    {
        return $this->file->url();
    }

    /**
     * @return ExternalUrl
     */
    public function externalUrl(): ExternalUrl
    {
        return $this->file->externalUrl();
    }

    /**
     * @return Signature
     */
    public function signature(): Signature
    {
        return $this->file->signature();
    }

    /**
     * @return Size
     */
    public function size(): Size
    {
        return $this->file->size();
    }

    /**
     * @return Sha1
     */
    public function sha1(): Sha1
    {
        return $this->file->sha1();
    }

    /**
     * @return Version
     */
    public function version(): ?Version
    {
        return $this->version;
    }

    /**
     * @return State
     */
    public function state(): ?State
    {
        return $this->state;
    }

    public function source(): ApkSource
    {
        return $this->file->source();
    }

    /**
     * @return ApkId
     */
    public function id(): ApkId
    {
        return $this->id;
    }

    /**
     * @return ApkDate
     */
    public function date(): ApkDate
    {
        return $this->date;
    }

    /**
     * @return Report
     */
    public function report(): Report
    {
        return $this->report;
    }

    public function reportResource(): string
    {
        return $this->report->resource();
    }

    public function scanned(Report $report): void
    {
        $this->report = $report;

        if ($this->guardPendingReport()) {
            return;
        }

        $this->state = $this->state->scanned();

        $this->record(
            new ApkWasScanned(
                $this->id->value(), $this->itemId->value(), $this->report->permalink(), $this->file->source()->value(), $this->version()->value()
            )
        );
    }

    public function dangerous(Report $report): void
    {
        $this->report = $report;
        $this->state = State::dangerous();
        $this->record(
            new ApkWasDangerous(
                $this->id->value(),
                $this->itemId->value(),
                $this->file->source()->value(),
                $this->version()->value())
        );
    }

    public function uploaded(): void
    {
        $this->file->publishToCdn();

        $this->state = $this->state->uploaded();

        $this->record(
            new ApkWasUploaded(
                $this->id->value(),
                $this->itemId->value(),
                $this->url()->value(),
                $this->file->source()->value(),
                $this->version()->value()
            )
        );
    }

    private function guardPendingReport(): bool
    {
        if ($this->report->isPending()) {
            $this->record(
                new ApkHasPendingReport(
                    $this->id->value(), $this->itemId->value(), $this->report->resource(), $this->file->source()->value(), $this->version()->value())
            );

            return true;
        }

        return false;
    }

    public function isDangerous(): bool
    {
        return $this->state()->equals(State::dangerous());
    }

    public function isPendingForUpload(): bool
    {
        return $this->state()->equals(State::pendingUpload());
    }

    public function fileIsTmp(): bool
    {
        return $this->file->isTmp();
    }
}
