<?php

namespace App\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Apk;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ApkWasDownloaded extends DomainEvent
{

    /** @var int */
    private $itemId;
    /** @var string */
    private $url;
    /** @var string */
    private $version;
    /** @var string */
    private $source;

    public function __construct(
        string $aggregateId,
        int $itemId,
        string $url,
        string $version,
        string $source,
        int $aggregateVersion= null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);

        $this->itemId = $itemId;
        $this->url = $url;
        $this->version = $version;
        $this->source = $source;
    }

    public static function eventName(): string
    {
        return 'ApkWasDownloaded';
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId,
            'url' => $this->url,
            'version' => $this->version,
            'source' => $this->source
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['itemId'], $body['url'], $body['version'], $body['source'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function version(): string
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function source(): string
    {
        return $this->source;
    }

    public static function stream(): string
    {
        return Apk::class;
    }
}
