<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Apk;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ApkWasCreated extends DomainEvent
{
    /** @var int */
    private $itemId;

    /** @var string */
    private $source;
    /**
     * @var string
     */
    private $externalUrl;
    /**
     * @var string
     */
    private $saveTo;

    public function __construct(
        string $aggregateId,
        int $itemId,
        string $saveTo,
        string $externalUrl,
        string $source,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->itemId = $itemId;
        $this->source = $source;
        $this->externalUrl = $externalUrl;
        $this->saveTo = $saveTo;
    }

    public static function eventName(): string
    {
        return 'ApkWasCreated';
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId,
            'externalUrl' => $this->externalUrl,
            'source' => $this->source,
            'saveTo' => $this->saveTo,
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self(
            $aggregateId,
            $body['itemId'],
            $body['saveTo'],
            $body['externalUrl'],
            $body['source'],
            $aggregateVersion,
            $eventId,
            $occurredOn
        );
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function source(): string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function externalUrl(): string
    {
        return $this->externalUrl;
    }

    public static function stream(): string
    {
        return Apk::class;
    }

    /**
     * @return string
     */
    public function saveTo(): string
    {
        return $this->saveTo;
    }
}
