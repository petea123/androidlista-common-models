<?php

namespace App\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Apk;
use App\Shared\Domain\Bus\Event\DomainEvent;

final class ApkHasPendingReport extends DomainEvent
{
    /** @var int */
    private $itemId;
    /** @var string */
    private $resource;
    /** @var string */
    private $source;
    /** @var string */
    private $version;

    public function __construct(
        string $aggregateId,
        int $itemId,
        string $resource,
        string $source,
        string $version,
        int $aggregateVersion= null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->itemId = $itemId;
        $this->resource = $resource;
        $this->source = $source;
        $this->version = $version;
    }

    public static function eventName(): string
    {
        return 'ApkHasPendingReport';
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId,
            'resource' => $this->resource,
            'source' => $this->source,
            'version' => $this->version,
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['itemId'], $body['resource'], $body['source'], $body['version'], $aggregateVersion, $eventId,
            $occurredOn);
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function resource(): string
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function source(): string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function version(): string
    {
        return $this->version;
    }

    public static function stream(): string
    {
        return Apk::class;
    }
}
