<?php

namespace App\Al\Apks\Domain\Event;

use App\Al\Apks\Domain\Apk;
use App\Shared\Domain\Bus\Event\DomainEvent;


final class ApkWasScanned extends DomainEvent
{

    /** @var int */
    private $itemId;
    /** @var string */
    private $reportUrl;
    /** @var string */
    private $source;
    /** @var string */
    private $version;

    public function __construct(string $aggregateId, int $itemId, string $reportUrl, string $source, string $version, int $aggregateVersion= null, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->itemId = $itemId;
        $this->reportUrl = $reportUrl;
        $this->source = $source;
        $this->version = $version;
    }

    public static function eventName(): string
    {
        return 'ApkWasScanned';
    }

    public function toPrimitives(): array
    {
        return [
            'itemId' => $this->itemId,
            'reportUrl' => $this->reportUrl,
            'source' => $this->source,
            'version' => $this->version
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['itemId'], $body['reportUrl'], $body['source'], $body['version'], $aggregateVersion, $eventId, $occurredOn);
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function reportUrl(): string
    {
        return $this->reportUrl;
    }

    /**
     * @return string
     */
    public function source(): string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function version(): string
    {
        return $this->version;
    }

    public static function stream(): string
    {
        return Apk::class;
    }
}
