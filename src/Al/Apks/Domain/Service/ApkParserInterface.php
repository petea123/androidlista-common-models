<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\Service;

use App\Al\Apks\Domain\ValueObject\File\Sha1;
use App\Al\Apks\Domain\ValueObject\File\Signature;
use App\Al\Apks\Domain\ValueObject\File\Size;
use App\Al\Apks\Domain\ValueObject\Version;
use SplFileInfo;
use SplFileObject;

interface ApkParserInterface
{

    public function setApkStream(SplFileInfo $resource): void;
    public function signature(): Signature;
    public function displayedVersion(): Version;
    public function size(): Size;
    public function sha1(): Sha1;
}
