<?php


namespace App\Al\Apks\Domain\Service;

use App\Al\Apks\Domain\ApkWebParser;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Shared\Domain\ValueObject\PackageValueObject;

//TODO move to shared
interface WebParserFactory
{
    public function instance(ApkSource $source, PackageValueObject $package): ?ApkWebParser;
}
