<?php

declare(strict_types=1);


namespace App\Al\Apks\Domain\Service;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\Exception\ApkNotFound;
use App\Al\Apks\Domain\ValueObject\ApkId;

final class ApkFinder
{

    /**
     * @var ApkRepositoryInterface
     */
    protected $apkRepository;

    /**
     * ApkFinder constructor.
     * @param ApkRepositoryInterface $apkRepository
     */
    public function __construct(ApkRepositoryInterface $apkRepository)
    {
        $this->apkRepository = $apkRepository;
    }

    /**
     * @param ApkId $id
     * @return Apk
     */
    public function search(ApkId $id): Apk
    {
        $apk = $this->apkRepository->search($id);
        $this->guard($id, $apk);

        return $apk;
    }

    /**
     * @param ApkId $id
     * @param Apk|null $apk
     */
    private function guard(ApkId $id, Apk $apk = null): void
    {
        if (null === $apk) {
            throw new ApkNotFound($id);
        }
    }
}
