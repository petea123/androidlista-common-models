<?php

declare(strict_types=1);


namespace App\Al\Apks\Domain\Service;

use App\Al\Apks\Domain\Exception\ApkReportNotRetrieved;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\Report;
use App\Al\Items\Application\Find\FindItemDownloadsQuery;
use App\Al\Items\Application\Find\ItemDownloadsResponse;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Query\QueryBus;

final class ApkReportValidator
{

    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function validate(?array $report, ItemId $itemId): Report
    {
        $report = new Report($report);

        $this->guardDangerousReport($report, $itemId);

        return $report;
    }

    public function validateRescan(?array $report, ItemId $itemId, ApkId $apkId): Report
    {
        $report = new Report($report);

        if ($report->isPending()) {
            throw new ApkReportNotRetrieved($apkId);
        }

        $this->guardDangerousReport($report, $itemId);

        return $report;
    }

    private function guardDangerousReport(Report $report, ItemId $itemId): Report
    {
        if ($report->isDangerous()) {
            $report->bypassDangerousReport($this->itemTotalDownloads($itemId));
        }

        return $report;
    }

    private function itemTotalDownloads(ItemId $itemId): int
    {
        /** @var ItemDownloadsResponse $downloads */
        $downloads = $this->queryBus->ask(new FindItemDownloadsQuery($itemId->value()));

        return $downloads->downloads();
    }
}
