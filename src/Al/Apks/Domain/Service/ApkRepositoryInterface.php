<?php

declare(strict_types = 1);

namespace App\Al\Apks\Domain\Service;

use App\Al\Apks\Domain\Apk;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Criteria\Criteria;

interface ApkRepositoryInterface
{

    public function save(Apk $apk): void;

    public function delete(Apk $apk): void;

    public function search(ApkId $apk): ?Apk;

    public function matching(Criteria $criteria): array;
    public function searchByItemId(ItemId $itemId, int $limit = null): array;
}
