<?php


namespace App\Al\Apks\Domain\Service;

use App\Al\Apks\Domain\Apk;

interface ScannerInterface
{

    public function analyze(Apk $apk): ?array;

    public function getReportAllInfo($resource);
}
