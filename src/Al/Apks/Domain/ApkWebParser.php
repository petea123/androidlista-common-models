<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain;

use App\Al\Items\Domain\ValueObject\ItemName;
use App\Shared\Domain\ValueObject\ExternalUrl;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\PackageValueObject;

// TODO change name to WebParser
interface ApkWebParser
{
    public static function getApp(PackageValueObject $packageId, LocaleValueObject $locale = null);
    public function apkUrl(): ?ExternalUrl;
    public function apkDownload(): ?ExternalUrl;
    public function hasApk(): bool;
    public function versions(): array;
    public function toValues(): array;
    public function names(): ItemName;

}
