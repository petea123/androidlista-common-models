<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain;

use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Al\Apks\Domain\ValueObject\File\File;
use App\Al\Apks\Domain\ValueObject\File\Url;
use App\Al\Items\Domain\ValueObject\ItemGoogle;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\ExternalUrl;

final class ApkCreator
{
    /**
     * @var ApkRepositoryInterface
     */
    private $repository;
    /**
     * @var WebParserFactory
     */
    private $webParserFactory;

    public function __construct(ApkRepositoryInterface $repository, WebParserFactory $webParserFactory)
    {
        $this->repository = $repository;
        $this->webParserFactory = $webParserFactory;
    }

    public function create(ApkId $id, ApkSource $source, ItemView $item, ExternalUrl $externalUrl = null): ?Apk
    {
        $itemId = new ItemId($item->id());
        if (null === $this->guardItem($itemId, $item)) {
            return null;
        }
        $google = new ItemGoogle($item->web());

        $to = $this->generateUrl($id, $google, $item->dirify());

        if (null !== $externalUrl) {
            $file = File::emptyFile($to, $externalUrl, $source);
            return Apk::create($id, $itemId, $file);
        }

        if (null !== $this->repository->search($id)) {
            return null;
        }

        /** @var ApkWebParser $app */
        $app = $this->webParserFactory->instance($source, $google);

        if (null !== $app && $app->hasApk() && null !== $app->apkDownload()) {
            $file = File::emptyFile($to, $app->apkUrl(), $source);
            return Apk::create($id, $itemId, $file);
        }

        return null;
    }

    private function guardItem(ItemId $itemId, ItemView $item): ?ItemView
    {
        if ($itemId->isBannedForDownloadApk() ||  $item->price() > 0) {
            return null;
        }

        return $item;
    }

    private function generateUrl(ApkId $id, ItemGoogle $google, string $dirify): Url
    {
        return new Url($google->dirify().'/'.$id->value().'/'.$dirify.'.apk');
    }
}
