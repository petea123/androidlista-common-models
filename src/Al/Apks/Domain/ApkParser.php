<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain;

use App\Al\Apks\Domain\Exception\VersionExists;
use App\Al\Apks\Domain\Service\ApkParserInterface;
use App\Al\Apks\Domain\Service\ApkRepositoryInterface;
use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class ApkParser
{

    /**
     * @var ApkParserInterface
     */
    private $parser;
    /**
     * @var ApkRepositoryInterface
     */
    private $repository;

    public function __construct(ApkRepositoryInterface $repository, ApkParserInterface $parser)
    {
        $this->parser = $parser;
        $this->repository = $repository;
    }

    public function parseApkStream(\SplFileInfo $resource, Apk $apk): bool
    {
        $this->parser->setApkStream($resource);

        $version = $this->parser->displayedVersion();
        if (!$this->versionExists($apk, $version)) {
            $apk->downloaded($version, $this->parser->size(), $this->parser->signature(), $this->parser->sha1());
            return true;
        }

        return false;
    }


    private function versionExists(Apk $apk, Version $version): bool
    {
        $filters = Filters::fromValues([
            [
                'field' => 'itemId',
                'operator' => '=',
                'value' => (string)$apk->itemId()->value()
            ],
            [
                'field' => 'version.value',
                'operator' => '=',
                'value' => $version->value()
            ]
        ]);
        $order = Order::fromValues(null, null);

        $apks = $this->repository->matching(new Criteria($filters, $order, null, null));

        if (!empty($apks)) {
            $this->repository->delete($apk);
            return true;
        }

        return false;
    }
}
