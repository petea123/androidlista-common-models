<?php

declare(strict_types=1);


namespace App\Al\Apks\Domain\Exception;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\DomainError;

final class ApkReportNotRetrieved extends DomainError
{
    private $apkId;

    public function __construct(ApkId $apkId)
    {
        $this->apkId = $apkId;
        parent::__construct();

    }

    public function errorCode(): string
    {
        return 'apk_report_not_retrieved';
    }

    protected function errorMessage(): string
    {
        return sprintf('The apk with id <%s> failed retrieving report', $this->apkId->value());
    }
}