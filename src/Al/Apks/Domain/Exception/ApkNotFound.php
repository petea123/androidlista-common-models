<?php

declare(strict_types=1);


namespace App\Al\Apks\Domain\Exception;

use App\Al\Apks\Domain\ValueObject\ApkId;
use App\Shared\Domain\DomainError;

final class ApkNotFound extends DomainError
{

    private $id;

    public function __construct(ApkId $id)
    {
        $this->id = $id;

        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'apk_not_found';
    }

    protected function errorMessage(): string
    {
        return sprintf('The apk <%s> has not been found', $this->id->value());
    }
}