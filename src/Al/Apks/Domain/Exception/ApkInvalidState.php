<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\Exception;

use App\Shared\Domain\DomainError;

final class ApkInvalidState extends DomainError
{

    public function __construct()
    {
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'apk_invalid_state';
    }

    protected function errorMessage(): string
    {
        return sprintf('Apk has invalid state');
    }
}