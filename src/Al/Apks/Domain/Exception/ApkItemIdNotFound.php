<?php

declare(strict_types=1);


namespace App\Al\Apks\Domain\Exception;

use App\Shared\Domain\DomainError;
use App\Al\Shared\Domain\Items\ItemId;

final class ApkItemIdNotFound extends DomainError
{
    private $itemId;

    public function __construct(ItemId $itemId)
    {
        $this->itemId = $itemId;
        parent::__construct();

    }

    public function errorCode(): string
    {
        return 'apk_item_id_not_found';
    }

    protected function errorMessage(): string
    {
        return sprintf('The apk with itemid <%s> not found', $this->itemId->value());
    }
}