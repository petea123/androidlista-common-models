<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain\Exception;

use App\Al\Apks\Domain\ValueObject\Version;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\DomainError;

final class VersionExists extends DomainError
{

    private $id;
    private $version;

    public function __construct(ItemId $id, Version $version)
    {
        $this->id = $id;
        $this->version = $version;
        parent::__construct();
    }

    public function errorCode(): string
    {
        return 'apk_version_exists';
    }

    protected function errorMessage(): string
    {
        return sprintf('The apk version <%s> exists on item <%s>', $this->version->value(), $this->id->value());
    }
}