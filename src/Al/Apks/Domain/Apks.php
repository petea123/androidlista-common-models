<?php

declare(strict_types=1);

namespace App\Al\Apks\Domain;

use App\Shared\Domain\Collection;

final class Apks extends Collection
{
    protected function type(): string
    {
        return Apk::class;
    }
}