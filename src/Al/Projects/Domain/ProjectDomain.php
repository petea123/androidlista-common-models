<?php declare(strict_types=1);

namespace App\Al\Projects\Domain;

class ProjectDomain
{
    /** @var int */
    private $id;
    /** @var Project */
    private $project;
    /** @var string */
    private $languageCode;
    /** @var string */
    private $subdomainBase;
    /** @var string */
    private $domainLocal;
    /** @var string */
    private $domainDevel;
    /** @var string */
    private $domainStage;
    /** @var string */
    private $domainProd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;
        return $this;
    }

    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    public function setLanguageCode(?string $languageCode): self
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    public function getSubdomainBase(): ?string
    {
        return $this->subdomainBase;
    }

    public function setSubdomainBase(?string $subdomainBase): self
    {
        $this->subdomainBase = $subdomainBase;
        return $this;
    }

    public function getDomainLocal(): ?string
    {
        return $this->domainLocal;
    }

    public function setDomainLocal(?string $domainLocal): self
    {
        $this->domainLocal = $domainLocal;
        return $this;
    }

    public function getDomainDevel(): ?string
    {
        return $this->domainDevel;
    }

    public function setDomainDevel(?string $domainDevel): self
    {
        $this->domainDevel = $domainDevel;
        return $this;
    }

    public function getDomainStage(): ?string
    {
        return $this->domainStage;
    }

    public function setDomainStage(?string $domainStage): self
    {
        $this->domainStage = $domainStage;
        return $this;
    }

    public function getDomainProd(): ?string
    {
        return $this->domainProd;
    }

    public function setDomainProd(?string $domainProd): self
    {
        $this->domainProd = $domainProd;
        return $this;
    }

    public function getDomainByEnv(string $env): string
    {
        switch ($env) {
            case 'local':
                return $this->getDomainLocal();
            case 'dev':
                return $this->getDomainDevel();
            case 'stage':
                return $this->getDomainStage();
            case 'prod':
                return $this->getDomainProd();
        }

        throw new \LogicException(sprintf('Provided environment "%s" does not meet any of the supported: local, develop, stage, prod', $env));
    }

    public function getBaseUrlByEnv(string $env): string
    {
        return sprintf('https://%s.%s', $this->subdomainBase, $this->getDomainByEnv($env));
    }
}
