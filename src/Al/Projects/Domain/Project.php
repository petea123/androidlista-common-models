<?php declare(strict_types=1);

namespace App\Al\Projects\Domain;

class Project
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $nameDirify;
    /** @var string */
    private $app;
    /** @var bool */
    private $isMultilanguage;
    /** @var string */
    private $defaultLanguage;
    /** @var string */
    private $languages;
    /** @var string */
    private $subdomainBase;
    /** @var string */
    private $domainLocal;
    /** @var string */
    private $domainPre;
    /** @var string */
    private $domainProd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getNameDirify(): ?string
    {
        return $this->nameDirify;
    }

    public function setNameDirify(?string $nameDirify): self
    {
        $this->nameDirify = $nameDirify;
        return $this;
    }

    public function getApp(): ?string
    {
        return $this->app;
    }

    public function setApp(?string $app): self
    {
        $this->app = $app;
        return $this;
    }

    public function isMultilanguage(): ?bool
    {
        return $this->isMultilanguage;
    }

    public function setIsMultilanguage(?bool $isMultilanguage): self
    {
        $this->isMultilanguage = $isMultilanguage;
        return $this;
    }

    public function getDefaultLanguage(): ?string
    {
        return $this->defaultLanguage;
    }

    public function setDefaultLanguage(?string $defaultLanguage): self
    {
        $this->defaultLanguage = $defaultLanguage;
        return $this;
    }

    public function getLanguages(): ?string
    {
        return $this->languages;
    }

    public function setLanguages(?string $languages): self
    {
        $this->languages = $languages;
        return $this;
    }

    public function getSubdomainBase(): ?string
    {
        return $this->subdomainBase;
    }

    public function setSubdomainBase(?string $subdomainBase): self
    {
        $this->subdomainBase = $subdomainBase;
        return $this;
    }

    public function getDomainLocal(): ?string
    {
        return $this->domainLocal;
    }

    public function setDomainLocal(?string $domainLocal): self
    {
        $this->domainLocal = $domainLocal;
        return $this;
    }

    public function getDomainPre(): ?string
    {
        return $this->domainPre;
    }

    public function setDomainPre(?string $domainPre): self
    {
        $this->domainPre = $domainPre;
        return $this;
    }

    public function getDomainProd(): ?string
    {
        return $this->domainProd;
    }

    public function setDomainProd(?string $domainProd): self
    {
        $this->domainProd = $domainProd;
        return $this;
    }
}
