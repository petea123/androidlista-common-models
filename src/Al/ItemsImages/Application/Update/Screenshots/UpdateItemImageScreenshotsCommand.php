<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Update\Screenshots;

use App\Shared\Domain\Bus\Command\Command;

final class UpdateItemImageScreenshotsCommand extends Command
{
    /**
     * @var int
     */
    private $itemId;

    /**
     * @var array
     */
    private $urls;

    public function __construct(int $itemId, array $urls)
    {
        $this->itemId = $itemId;
        $this->urls = $urls;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return array
     */
    public function urls(): array
    {
        return $this->urls;
    }
}
