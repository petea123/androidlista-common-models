<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Update\Screenshots;

use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFileName;
use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshots;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotsRepository;
use App\Al\Shared\Domain\HashGenerator;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\FilesystemInterface;
use App\Shared\Domain\DownloaderManager;
use App\Shared\Domain\UuidGenerator;
use SplFileInfo;

final class ItemImageScreenshotsUpdater
{
    /**
     * @var ItemImageScreenshotsRepository
     */
    private $repository;
    /**
     * @var DownloaderManager
     */
    private $downloader;
    /**
     * @var HashGenerator
     */
    private $hashGenerator;
    /**
     * @var FilesystemInterface
     */
    private $filesystem;
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var UuidGenerator
     */
    private $generator;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    public function __construct(
        ItemImageScreenshotsRepository $repository,
        DownloaderManager $downloader,
        HashGenerator $hashGenerator,
        FilesystemInterface $imagesStorage,
        QueryBus $queryBus,
        UuidGenerator $generator,
        DomainEventPublisher $publisher
    ) {
        $this->repository = $repository;
        $this->downloader = $downloader;
        $this->hashGenerator = $hashGenerator;
        $this->filesystem = $imagesStorage;
        $this->queryBus = $queryBus;
        $this->generator = $generator;
        $this->publisher = $publisher;
    }

    public function __invoke(ItemId $itemId, array $urls)
    {
        $screenshots = $this->repository->searchByItemId($itemId) ?:
            new ItemImageScreenshots(
                new ItemImageId($this->generator->next()),
                $itemId
            );

        /** @var ItemView $item */
        $item = $this->queryBus->ask(new FindItemByIdQuery($itemId->value(), 'en'));
        $count = 1;
        $screenshots->emptyFiles();
        foreach ($urls as $url) {
            if (null === $tmpImage = $this->downloader->proxyDownload($url)) {
                continue;
            }

            $file = $this->createFile($item, $count, $tmpImage);
            if ($this->guardAndAddScreenshot($screenshots, $file, $tmpImage)) {
                ++$count;
            }
        }

        if (!empty($screenshots->files())) {
            $this->repository->save($screenshots);
            $this->publisher->record(...$screenshots->pullDomainEvents());
        }
    }

    private function createFile(ItemView $item, int $count, SplFileInfo $resource): ItemImageFile
    {
        $hash = new ItemImageFileHash($this->hashGenerator->hash($resource));
        $name = ItemImageFileName::create($item->name(), $count);
        $size = getimagesizefromstring(file_get_contents($resource->getRealPath()));
        $dimensions = new ItemImageFileDimensions($size[0], $size[1]);

        return ItemImageFile::create(
            $name,
            ItemImageFilePath::createWithParams(new ItemId($item->id()), $item->dirify(), $count),
            $dimensions,
            'jpg',
            $hash
        );
    }

    private function guardAndAddScreenshot(
        ItemImageScreenshots $screenshots,
        ItemImageFile $file,
        SplFileInfo $resource
    ): bool {
        if ($this->filesystem->putStream(ItemImageFilePath::create($file->path()->value()), $resource)) {
            $screenshots->addScreenshot($file);
            return true;
        }
        return false;
    }
}
