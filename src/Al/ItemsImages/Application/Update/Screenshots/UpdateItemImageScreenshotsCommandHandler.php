<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Update\Screenshots;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\ExternalUrl;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\map;

final class UpdateItemImageScreenshotsCommandHandler implements CommandHandler
{
    /**
     * @var ItemImageScreenshotsUpdater
     */
    private $updater;

    public function __construct(ItemImageScreenshotsUpdater $updater)
    {
        $this->updater = $updater;
    }

    public function __invoke(UpdateItemImageScreenshotsCommand $command)
    {
        $itemId = new ItemId($command->itemId());
        $urls = map(function ($url) {
            return new ExternalUrl($url);
        }, $command->urls());

        apply($this->updater, [$itemId, $urls]);
    }
}
