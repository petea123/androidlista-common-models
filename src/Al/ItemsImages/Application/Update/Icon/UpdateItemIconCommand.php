<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Update\Icon;

use App\Shared\Domain\Bus\Command\Command;

final class UpdateItemIconCommand extends Command
{
    /**
     * @var int
     */
    private $itemId;

    /**
     * @var string
     */
    private $downloadUrl;

    public function __construct(int $itemId, string $downloadUrl)
    {
        $this->itemId = $itemId;
        $this->downloadUrl = $downloadUrl;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function downloadUrl(): string
    {
        return $this->downloadUrl;
    }
}
