<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Update\Icon;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\ValueObject\ExternalUrl;
use function Lambdish\Phunctional\apply;

final class UpdateItemIconCommandHandler implements CommandHandler
{
    /**
     * @var ItemIconUpdater
     */
    private $updater;

    public function __construct(ItemIconUpdater $updater)
    {
        $this->updater = $updater;
    }

    public function __invoke(UpdateItemIconCommand $command)
    {
        $itemId = new ItemId($command->itemId());
        $externalUrl = new ExternalUrl($command->downloadUrl());

        apply($this->updater, [$itemId, $externalUrl]);
    }
}
