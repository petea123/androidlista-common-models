<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Update\Icon;

use App\Al\ItemsImages\Domain\Icon\ItemImageIconRepository;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\Shared\Domain\HashGenerator;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\DownloaderManager;
use App\Shared\Domain\FilesystemInterface;
use App\Shared\Domain\ValueObject\ExternalUrl;

final class ItemIconUpdater
{
    /**
     * @var FilesystemInterface
     */
    private $filesystem;
    /**
     * @var HashGenerator
     */
    private $hashGenerator;
    /**
     * @var ItemImageIconRepository
     */
    private $repository;
    /**
     * @var DownloaderManager
     */
    private $downloader;
    /**
     * @var DomainEventPublisher
     */
    private $eventPublisher;

    public function __construct(
        FilesystemInterface $imagesStorage,
        HashGenerator $hashGenerator,
        ItemImageIconRepository $repository,
        DownloaderManager $downloader,
        DomainEventPublisher $eventPublisher
    ) {
        $this->filesystem = $imagesStorage;
        $this->hashGenerator = $hashGenerator;
        $this->repository = $repository;
        $this->downloader = $downloader;
        $this->eventPublisher = $eventPublisher;
    }

    public function __invoke(ItemId $itemId, ExternalUrl $downloadUrl)
    {
        if (null === $tmpImage = $this->downloader->proxyDownload($downloadUrl)) {
            return;
        }

        $icon = $this->repository->searchByItemId($itemId);

        $hash = new ItemImageFileHash($this->hashGenerator->hash($tmpImage));

        if (null === $icon || $icon->hash()->equals($hash)) {
            return;
        }

        $size = getimagesizefromstring(file_get_contents($tmpImage->getRealPath()));
        $dimensions = new ItemImageFileDimensions($size[0], $size[1]);

        $icon->changeFileDimensions($dimensions);
        $icon->changeFileHash($hash);

        if ($this->filesystem->putStream(ItemImageFilePath::create($icon->path()->value()), $tmpImage)) {
            $icon->updated();
            $this->repository->save($icon);
            $this->eventPublisher->record(...$icon->pullDomainEvents());
        }
    }
}
