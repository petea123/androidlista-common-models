<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Create\Icon;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class CreateItemIconFromApkPureCommandHandler implements CommandHandler
{
    /**
     * @var ItemIconCreator
     */
    private $creator;

    public function __construct(ItemIconApkPureCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(CreateItemIconFromApkPureCommand $command): void
    {
        $itemId = new ItemId($command->itemId());

        apply($this->creator, [$itemId]);
    }
}
