<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Create\Icon;

use App\Al\Apks\Domain\Service\WebParserFactory;
use App\Al\Apks\Infrastructure\WebParser\ApkPureWebParser;
use App\Al\Items\Application\Find\FindItemQuery;
use App\Al\Items\Application\Find\ItemResponse;
use App\Al\Items\Domain\ApkPure\ItemApkPurePackage;
use App\Al\Shared\Domain\Apks\ApkSource;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Query\QueryBus;

final class ItemIconApkPureCreator
{

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var WebParserFactory
     */
    private $webParserFactory;
    /**
     * @var ItemIconCreator
     */
    private $creator;

    public function __construct(QueryBus $queryBus, WebParserFactory $webParserFactory, ItemIconCreator $creator)
    {
        $this->queryBus = $queryBus;
        $this->webParserFactory = $webParserFactory;
        $this->creator = $creator;
    }

    public function __invoke(ItemId $itemId)
    {
        /** @var ItemResponse $item */
        if (null === $item = $this->queryBus->ask(new FindItemQuery($itemId->value()))) {
            return;
        }

        /** @var ApkPureWebParser $parser */
        if (null === $parser = $this->webParserFactory->instance(
            ApkSource::apkpure(),
            ItemApkPurePackage::createFromPackage($item->googleId())
        )) {
            return;
        }

        if (null === $icon = $parser->icon()) {
            return;
        }

        $this->creator->create($itemId, $icon);
    }
}
