<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Create\Icon;

use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\ItemsImages\Domain\Icon\ItemImageIcon;
use App\Al\ItemsImages\Domain\Icon\ItemImageIconRepository;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFileName;
use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\Shared\Domain\HashGenerator;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\DownloaderManager;
use App\Shared\Domain\FilesystemInterface;
use App\Shared\Domain\UuidGenerator;
use App\Shared\Domain\ValueObject\ExternalUrl;

final class ItemIconCreator
{
    /**
     * @var FilesystemInterface
     */
    private $filesystem;
    /**
     * @var HashGenerator
     */
    private $hashGenerator;
    /**
     * @var ItemImageIconRepository
     */
    private $repository;
    /**
     * @var DownloaderManager
     */
    private $downloader;
    /**
     * @var DomainEventPublisher
     */
    private $eventPublisher;
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    public function __construct(
        FilesystemInterface $imagesStorage,
        HashGenerator $hashGenerator,
        ItemImageIconRepository $repository,
        DownloaderManager $downloader,
        QueryBus $queryBus,
        UuidGenerator $uuidGenerator,
        DomainEventPublisher $eventPublisher
    ) {
        $this->filesystem = $imagesStorage;
        $this->hashGenerator = $hashGenerator;
        $this->repository = $repository;
        $this->downloader = $downloader;
        $this->eventPublisher = $eventPublisher;
        $this->queryBus = $queryBus;
        $this->uuidGenerator = $uuidGenerator;
    }

    public function create(ItemId $itemId, ExternalUrl $downloadUrl)
    {
        $item = $this->queryBus->ask(new FindItemByIdQuery($itemId->value()));
        if ($this->repository->searchByItemId($itemId) && null === $item) {
            return;
        }

        if (null === $tmpImage = $this->downloader->proxyDownload($downloadUrl)) {
            return;
        }

        $size = getimagesizefromstring(file_get_contents($tmpImage->getRealPath()));
        $file = ItemImageFile::create(
            ItemImageFileName::createIcon($item->originalName()),
            ItemImageFilePath::createIconWithParams($itemId, $item->dirify()),
            new ItemImageFileDimensions($size[0], $size[1]),
            'jpg',
            new ItemImageFileHash($this->hashGenerator->hash($tmpImage))
        );

        if ($this->filesystem->putStream(ItemImageFilePath::create($file->path()->value()), $tmpImage)) {
            $icon = ItemImageIcon::create(
                new ItemImageId($this->uuidGenerator->next()),
                $itemId,
                $file,
            );
            $this->repository->save($icon, true);
            $this->eventPublisher->record(...$icon->pullDomainEvents());
        }
    }
}
