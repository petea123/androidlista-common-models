<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Application\Create\Icon;

use App\Al\Items\Domain\ApkPure\ItemApkPureWasCreated;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class CreateItemIconOnItemApkPureCreated implements DomainEventSubscriber
{
    /**
     * @var CommandBus
     */
    private $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function __invoke(ItemApkPureWasCreated $event): void
    {
        $this->bus->handle(new CreateItemIconFromApkPureCommand((int)$event->aggregateId()));
    }

    public static function subscribedTo(): array
    {
        return [ItemApkPureWasCreated::class];
    }
}
