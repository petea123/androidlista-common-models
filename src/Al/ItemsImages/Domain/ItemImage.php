<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\Aggregate\AggregateRoot;

abstract class ItemImage extends AggregateRoot
{

    /**
     * @var ItemImageId
     */
    private $id;
    /**
     * @var ItemId
     */
    private $itemId;


    public function __construct(ItemImageId $id, ItemId $itemId)
    {
        $this->itemId = $itemId;
        $this->id = $id;
    }

    /**
     * @return ItemImageId
     */
    public function id(): ItemImageId
    {
        return $this->id;
    }

    /**
     * @return ItemId
     */
    public function itemId(): ItemId
    {
        return $this->itemId;
    }

    public static function fileFromValues(): callable
    {
        return static function (array $values): ItemImageFile {
            return ItemImageFile::fromValues($values);
        };
    }

    public static function fileToValues(): callable
    {
        return static function (ItemImageFile $file): array {
            return $file->toValues();
        };
    }
}
