<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

final class ItemImageFile
{
    /**
     * @var ItemImageFileName
     */
    private $name;

    /**
     * @var ItemImageFilePath
     */
    private $path;
    /**
     * @var ItemImageFileDimensions
     */
    private $dimensions;
    /**
     * @var string
     */
    private $extension;
    /**
     * @var ItemImageFileHash
     */
    private $hash;

    public function __construct(
        ItemImageFileName $name,
        ItemImageFilePath $path,
        ItemImageFileDimensions $dimensions,
        string $extension,
        ItemImageFileHash $hash
    ) {
        $this->path = $path;
        $this->dimensions = $dimensions;
        $this->extension = $extension;
        $this->hash = $hash;
        $this->name = $name;
    }

    public static function create(
        ItemImageFileName $name,
        ItemImageFilePath $path,
        ItemImageFileDimensions $dimensions,
        string $extension,
        ItemImageFileHash $hash
    ) {
        return new self($name, $path, $dimensions, $extension, $hash);
    }

    /**
     * @return ItemImageFileName
     */
    public function name(): ItemImageFileName
    {
        return $this->name;
    }

    /**
     * @return ItemImageFilePath
     */
    public function path(): ItemImageFilePath
    {
        return $this->path;
    }

    /**
     * @return ItemImageFileDimensions
     */
    public function dimensions(): ItemImageFileDimensions
    {
        return $this->dimensions;
    }

    /**
     * @return string
     */
    public function extension(): string
    {
        return $this->extension;
    }

    /**
     * @return ItemImageFileHash
     */
    public function hash(): ItemImageFileHash
    {
        return $this->hash;
    }

    public function changeHash(ItemImageFileHash $hash): void
    {
        $this->hash = $hash;
    }

    public function changeDimensions(ItemImageFileDimensions $dimensions): void
    {
        $this->dimensions = $dimensions;
    }

    public function toValues(): array
    {
        return [
            'name' => $this->name->value(),
            'path' => $this->path->value(),
            'dimensions' => $this->dimensions->toValues(),
            'extension' => $this->extension,
            'hash' => $this->hash->value()
        ];
    }

    public static function fromValues(array $values): self
    {
        return new self(
            new ItemImageFileName($values['name']),
            new ItemImageFilePath($values['path']),
            new ItemImageFileDimensions($values['dimensions']['width'], $values['dimensions']['height']),
            $values['extension'],
            new ItemImageFileHash($values['hash'])
        );
    }
}
