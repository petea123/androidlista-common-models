<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

use App\Shared\Domain\ValueObject\Uuid;

final class ItemImageId extends Uuid
{

}
