<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain\Screenshots;

use App\Al\ItemsImages\Domain\ItemImage;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\Shared\Domain\Items\ItemId;
use function Lambdish\Phunctional\search;

final class ItemImageScreenshots extends ItemImage
{
    /**
     * @var ItemImageFile[]
     */
    private $files;

    public function __construct(ItemImageId $id, ItemId $itemId, ItemImageFile ...$files)
    {
        parent::__construct($id, $itemId);
        $this->files = $files;
    }

    public static function create(ItemImageId $id, ItemId $itemId, ItemImageFile ...$files)
    {
        return new self($id, $itemId, ...$files);
    }

    public function hasHash(ItemImageFileHash $hash): bool
    {
        return null !== search($this->hashComparator($hash), $this->files);
    }

    private function hashComparator(ItemImageFileHash $hash): callable
    {
        return static function (ItemImageFile $other) use ($hash) {
            return $hash->equals($other->hash());
        };
    }

    public function addScreenshot(ItemImageFile $file): void
    {
        $this->files[] = $file;
        $this->record(new ItemImageScreenshotWasAdded($this->id()->value(), $this->itemId()->value()));
    }

    public function removeScreenshot(ItemImageFile $file)
    {
        $this->files = array_values(array_filter($this->files, function (ItemImageFile $other) use ($file) {
            return !$this->hashComparator($other->hash())($file);
        }));
        $this->record(new ItemImageScreenshotWasRemoved($this->id()->value(), $this->itemId()->value()));
    }

    public function emptyFiles(): void
    {
        $this->files = [];
    }

    /**
     * @return ItemImageFile[]
     */
    public function files(): array
    {
        return $this->files;
    }
}
