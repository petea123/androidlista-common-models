<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain\Screenshots;

use App\Al\Shared\Domain\Items\ItemId;

interface ItemImageScreenshotsRepository
{
    public function save(ItemImageScreenshots $screenshots): void;

    public function searchByItemId(ItemId $id): ?ItemImageScreenshots;
}
