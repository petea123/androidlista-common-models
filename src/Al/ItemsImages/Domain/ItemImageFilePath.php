<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Domain\ValueObject\PathValueObject;

final class ItemImageFilePath extends PathValueObject
{
    public static function withPrefix(ItemImageFilePath $path): self
    {
        return new self("/item_images/{$path->value()}");
    }

    public static function createWithParams(ItemId $itemId, $name, $count): self
    {
        return new self(sprintf('%s/imagen-%s-%sori.jpg', $itemId->value(), $name, $count));
    }

    public static function createIconWithParams(ItemId $itemId, $name): self
    {
        return self::createWithParams($itemId, $name, 0);
    }
}
