<?php

namespace App\Al\ItemsImages\Domain\Icon;

use App\Al\Shared\Domain\Items\ItemId;

interface ItemImageIconRepository
{
    public function save(ItemImageIcon $icon, $create = false): void;

    public function searchByItemId(ItemId $id): ?ItemImageIcon;
}
