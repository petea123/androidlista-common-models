<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain\Icon;

use App\Al\ItemsImages\Domain\ItemImage;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\Shared\Domain\Items\ItemId;

final class ItemImageIcon extends ItemImage
{

    /**
     * @var ItemImageFile
     */
    private $file;

    public function __construct(ItemImageId $id, ItemId $itemId, ItemImageFile $file)
    {
        parent::__construct($id, $itemId);
        $this->file = $file;
    }

    public static function create(ItemImageId $id, ItemId $itemId, ItemImageFile $file)
    {
        $icon = new self($id, $itemId, $file);

        $icon->record(new ItemImageIconWasCreated(
            $id->value(),
            $itemId->value()
        ));

        return $icon;
    }

    /**
     * @return ItemImageFile
     */
    public function file(): ItemImageFile
    {
        return $this->file;
    }

    public function path(): ItemImageFilePath
    {
        return $this->file->path();
    }

    public function hash(): ItemImageFileHash
    {
        return $this->file->hash();
    }

    public function changeFileDimensions(ItemImageFileDimensions $dimensions): void
    {
        $this->file->changeDimensions($dimensions);
    }

    public function changeFileHash(ItemImageFileHash $hash): void
    {
        $this->file->changeHash($hash);
    }

    public function updated(): void
    {
        $this->record(new ItemImageIconWasUpdated(
            $this->id()->value(),
            $this->itemId()->value()
        ));
    }
}
