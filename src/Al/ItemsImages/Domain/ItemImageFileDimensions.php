<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

final class ItemImageFileDimensions
{
    /**
     * @var int
     */
    private $width;
    /**
     * @var int
     */
    private $height;

    public function __construct(int $width, int $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public static function fromString(string $image): self
    {
        $dimensions = getimagesizefromstring($image);
        return new self($dimensions[0], $dimensions[1]);
    }

    /**
     * @return int
     */
    public function width(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function height(): int
    {
        return $this->height;
    }

    public function toValues(): array
    {
        return [
            'width' => $this->width,
            'height' => $this->height
        ];
    }
}
