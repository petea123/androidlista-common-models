<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

use App\Shared\Domain\Utils;
use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class ItemImageFileName extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->guardNotEmptyName($value);

        parent::__construct($value);
    }

    private function guardNotEmptyName(string $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException(sprintf('Empty item name'));
        }
    }

    public static function create(string $name, int $count): self
    {
        return new self(sprintf('%s - Imagen: %s', $name, $count));
    }

    public static function createIcon(string $name): self
    {
        return self::create($name, 0);
    }

    /**
     * @return string
     */
    public function dirify(): string
    {
        return Utils::myDirify($this->value);
    }
}
