<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Domain;

use App\Shared\Domain\ValueObject\StringValueObject;
use InvalidArgumentException;

final class ItemImageFileHash extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->guardHash($value);
        parent::__construct($value);
    }

    private function guardHash(string $value): void
    {
        if (empty($value)) {
            throw new InvalidArgumentException('Invalid image hash');
        }
    }
}
