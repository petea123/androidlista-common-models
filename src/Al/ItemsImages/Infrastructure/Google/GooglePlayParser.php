<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Google;

use App\Al\Shared\Infrastructure\ProxyBonanza\ProxyBonanza;

final class GooglePlayParser
{

    private const PLAYSTORE_URL = 'https://play.google.com/store/apps/details';

    private $scriptData;

    private function __construct(array $scriptData)
    {
        $this->scriptData = $scriptData;
    }

    public static function isPublished(string $appId, string $lang = 'en', string $country = 'us'): bool
    {
        $response = @fopen(
            self::applyId($appId, $lang, $country),
            'r',
            false,
            ProxyBonanza::sharedStreamContext('HEAD')
        );

        return is_resource($response) && fclose($response) ? true : false;
    }
    public static function getApp(string $appId, string $lang = 'en', string $country = 'us'): ?self
    {

        $response = @fopen(
            self::applyId($appId, $lang, $country),
            'r',
            false,
            ProxyBonanza::sharedStreamContext()
        );

        if (false === $response || !is_resource($response)) {
            return null;
        }

        $scriptData = self::parseScripData(stream_get_contents($response));
        fclose($response);

        return new self($scriptData);
    }

    public function icon(): ?string
    {
        return $this->scriptData['ds:5'][0][12][1][3][2] ?? null;
    }

    public function gallery(): array
    {
        $gallery = $this->scriptData['ds:5'][0][12][0];

        return array_map(function (array $image) {
            return $image[3][2];
        }, $gallery);
    }

    private static function parseScripData($html): array
    {
        $parser = [];
        preg_match_all('/>AF_initDataCallback[\s\S]*?<\/script/', $html, $m);
        foreach ($m[0] as $match) {
            preg_match('/(ds:.*?)\'/', $match, $keyMatch);
            preg_match('/return ([\s\S]*?)}}\);<\//', $match, $valueMatch);

            if (!empty($keyMatch) && !empty($valueMatch) !== false) {
                $parser[$keyMatch[1]] = json_decode($valueMatch[1], true);
            }
        }

        return $parser;
    }

    private static function applyId(string $appId, string $lang = 'en', string $country = 'us'): string
    {
        return sprintf(
            '%s?id=%s&hl=%s&gl=%s',
            self::PLAYSTORE_URL,
            $appId,
            $lang,
            $country
        );
    }
}
