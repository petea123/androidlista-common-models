<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence;

use App\Al\ItemsImages\Domain\Icon\ItemImageIcon;
use App\Al\ItemsImages\Domain\Icon\ItemImageIconRepository;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\ItemsImages\Domain\ItemImageFileName;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use App\Shared\Infrastructure\RamseyUuidGenerator;
use Doctrine\DBAL\FetchMode;

final class MySqlItemImageIconRepository extends DoctrineRepository implements ItemImageIconRepository
{

    public function save(ItemImageIcon $icon, $create = false): void
    {
        $this->persist($icon);
        $this->entityManager()->flush();
        if ($create) {
            $this->entityManager()->getConnection()
                ->executeUpdate(
                    "INSERT INTO tbl_item_images(name, name_dirify, url_image, count, width, height, extension, item_id, type_id, hash) 
                        VALUES (:name, :nameDirify, :url, :count, :width, :height, :extension, :itemId, 1, :hash)",
                    [
                        'name' => $icon->file()->name()->value(),
                        'nameDirify' => $icon->file()->name()->dirify(),
                        'count' => 0,
                        'width' => $icon->file()->dimensions()->width(),
                        'height' => $icon->file()->dimensions()->height(),
                        'extension' => $icon->file()->extension(),
                        'itemId' => $icon->itemId()->value(),
                        'hash' => $icon->hash()->value(),
                        'url' => '/item_images/'.$icon->path()->value()
                    ]
                );

            return;
        }

        $this->entityManager()->getConnection()
            ->executeUpdate(
                "UPDATE tbl_item_images SET url_image = :url, hash = :hash WHERE item_id = :itemId AND count = 0",
                [
                    'itemId' => $icon->itemId()->value(),
                    'hash' => $icon->hash()->value(),
                    'url' => '/item_images/'.$icon->path()->value()
                ]
            );
    }

    public function searchByItemId(ItemId $itemId): ?ItemImageIcon
    {
        $icon = $this->repository(ItemImageIcon::class)->findOneBy(['itemId' => $itemId]);

        if (null != $icon) {
            return $icon;
        }

        $icon = $this->entityManager()->getConnection()
            ->executeQuery("SELECT * from tbl_item_images WHERE item_id = :itemId AND count = 0", [
                'itemId' => $itemId->value()
            ])
            ->fetchAll(FetchMode::ASSOCIATIVE);

        if (empty($icon)) {
            return null;
        }

        $dirify = explode('-imagen', $icon[0]['name_dirify']);
        $hash = empty($icon[0]['hash']) ? 'hash' : $icon[0]['hash'];
        return new ItemImageIcon(
            new ItemImageId((new RamseyUuidGenerator())->next()),
            $itemId,
            ItemImageFile::create(
                new ItemImageFileName($icon[0]['name']),
                new ItemImageFilePath(sprintf('%s/imagen-%s-0ori.jpg', $itemId->value(), $dirify[0])),
                new ItemImageFileDimensions(intval($icon[0]['width']), intval($icon[0]['height'])),
                'jpg',
                new ItemImageFileHash($hash)
            )
        );
    }
}
