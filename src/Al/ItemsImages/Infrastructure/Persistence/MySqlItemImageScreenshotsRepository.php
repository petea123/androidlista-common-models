<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence;

use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Al\ItemsImages\Domain\ItemImageFileDimensions;
use App\Al\ItemsImages\Domain\ItemImageFileHash;
use App\Al\ItemsImages\Domain\ItemImageFileName;
use App\Al\ItemsImages\Domain\ItemImageFilePath;
use App\Al\ItemsImages\Domain\ItemImageId;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshots;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotsRepository;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use App\Shared\Infrastructure\RamseyUuidGenerator;
use Doctrine\DBAL\FetchMode;

final class MySqlItemImageScreenshotsRepository extends DoctrineRepository implements ItemImageScreenshotsRepository
{

    public function save(ItemImageScreenshots $screenshots): void
    {
        $this->persist($screenshots);
        $this->entityManager()->flush();

        $this->entityManager()->getConnection()
            ->executeUpdate(
                "DELETE FROM tbl_item_images WHERE item_id = :itemId AND count > 0",
                [
                    'itemId' => $screenshots->itemId()->value()
                ]
            );

        $qb = $this->entityManager()->getConnection()->createQueryBuilder();

        foreach ($screenshots->files() as $key => $file) {
            $count = $key + 1;
            $qb->insert('tbl_item_images')
                ->values([
                    'name' => ':name',
                    'name_dirify' => ':name_dirify',
                    'item_id' => ':item_id',
                    'url_image' => ':url_image',
                    'count' => ':count',
                    'width' => ':width',
                    'height' => ':height',
                    'extension' => ':extension',
                    'type_id' => ':type_id',
                    'hash' => ':hash'
                ])
                ->setParameters([
                    ':name' => $file->name()->value(),
                    ':name_dirify' => $file->name()->dirify(),
                    ':item_id' => $screenshots->itemId()->value(),
                    ':url_image' => '/item_images/'.$file->path()->value(),
                    ':count' => $count,
                    ':width' => $file->dimensions()->width(),
                    ':height' => $file->dimensions()->height(),
                    ':extension' => $file->extension(),
                    ':type_id' => 1,
                    ':hash' => $file->hash()->value()
                ])->execute();
        }
    }

    public function searchByItemId(ItemId $itemId): ?ItemImageScreenshots
    {
        $screenshots = $this->repository(ItemImageScreenshots::class)->findOneBy(['itemId' => $itemId]);

        if (null !== $screenshots) {
            return $screenshots;
        }

        $screenshots = $this->entityManager()->getConnection()
            ->executeQuery("SELECT * from tbl_item_images WHERE item_id = :itemId AND count > 0", [
                'itemId' => $itemId->value()
            ])
            ->fetchAll(FetchMode::ASSOCIATIVE);

        if (empty($screenshots)) {
            return null;
        }

        $files = [];
        foreach ($screenshots as $screenshot) {
            $dirify = explode('-imagen', $screenshot['name_dirify']);
            $hash = empty($screenshot['hash']) ? 'hash' : $screenshot['hash'];
            $files[] = new ItemImageFile(
                new ItemImageFileName($screenshot['name']),
                new ItemImageFilePath(sprintf(
                    '%s/imagen-%s-%sori.jpg',
                    $itemId->value(),
                    $dirify[0],
                    $screenshot['count']
                )),
                new ItemImageFileDimensions(intval($screenshot['width']), intval($screenshot['height'])),
                'jpg',
                new ItemImageFileHash($hash)
            );
        }

        return new ItemImageScreenshots(
            new ItemImageId((new RamseyUuidGenerator())->next()),
            $itemId,
            ...$files
        );
    }
}
