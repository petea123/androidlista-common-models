<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence\Doctrine;

use App\Al\ItemsImages\Domain\ItemImageId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class ItemImageIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'item_image_id';
    }

    protected function typeClassName(): string
    {
        return ItemImageId::class;
    }

}
