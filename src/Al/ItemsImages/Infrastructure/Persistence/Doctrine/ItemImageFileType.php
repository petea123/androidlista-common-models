<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence\Doctrine;

use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

final class ItemImageFileType extends JsonType implements DoctrineCustomType
{

    public static function customTypeName(): string
    {
        return 'item_image_file';
    }

    public function getName(): string
    {
        return self::customTypeName();
    }

    /**
     * @param AbstractPlatform $platform
     * @return false|mixed|string|null
     * @throws \Doctrine\DBAL\Types\ConversionException
     * @var ItemImageFile $value
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue($value->toValues(), $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $scalars = parent::convertToPHPValue($value, $platform);

        return ItemImageFile::fromValues($scalars);
    }
}
