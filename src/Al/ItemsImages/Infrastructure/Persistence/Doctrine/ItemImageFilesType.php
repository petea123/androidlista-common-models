<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence\Doctrine;

use App\Al\ItemsImages\Domain\ItemImage;
use App\Al\ItemsImages\Domain\ItemImageFile;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use function Lambdish\Phunctional\map;

final class ItemImageFilesType extends JsonType implements DoctrineCustomType
{
    public function getName(): string
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return map(ItemImage::fileFromValues(), parent::convertToPHPValue($value, $platform));
    }

    /** @var ItemImageFile[] $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue(map(ItemImage::fileToValues(), $value), $platform);
    }

    public static function customTypeName(): string
    {
        return 'item_image_files';
    }
}
