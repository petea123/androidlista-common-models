<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence;

use App\Al\ItemsImages\Domain\Icon\ItemImageIcon;
use App\Al\ItemsImages\Domain\Icon\ItemImageIconRepository;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Persistence\Solr\SolrRepository;
use App\Shared\Infrastructure\Solr\SolrClient;

final class SolrItemImageIconRepository extends SolrRepository implements ItemImageIconRepository
{
    /**
     * @var MySqlItemImageIconRepository
     */
    private $repository;

    public function __construct(SolrClient $solrClient, MySqlItemImageIconRepository $repository)
    {
        parent::__construct($solrClient);
        $this->repository = $repository;
    }

    public function save(ItemImageIcon $icon, $create = false): void
    {
        $this->repository->save($icon, $create);
        if (!$this->search($icon->itemId()->value())) {
            return;
        }

        if (!$create) {
            $this->persist($icon->itemId()->value(), ['imagen_principal' => '/item_images/'.$icon->path()->value()]);
        }
    }

    public function searchByItemId(ItemId $id): ?ItemImageIcon
    {
        return $this->repository->searchByItemId($id);
    }
}
