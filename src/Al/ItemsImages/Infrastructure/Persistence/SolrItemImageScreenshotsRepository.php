<?php

declare(strict_types=1);

namespace App\Al\ItemsImages\Infrastructure\Persistence;

use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshots;
use App\Al\ItemsImages\Domain\Screenshots\ItemImageScreenshotsRepository;
use App\Al\Shared\Domain\Items\ItemId;
use App\Shared\Infrastructure\Persistence\Solr\SolrRepository;
use App\Shared\Infrastructure\Solr\SolrClient;

final class SolrItemImageScreenshotsRepository extends SolrRepository implements ItemImageScreenshotsRepository
{
    /**
     * @var MySqlItemImageScreenshotsRepository
     */
    private $repository;

    public function __construct(SolrClient $solrClient, MySqlItemImageScreenshotsRepository $repository)
    {
        parent::__construct($solrClient);
        $this->repository = $repository;
    }

    public function save(ItemImageScreenshots $screenshots): void
    {
        $this->repository->save($screenshots);
        $paths = [];
        foreach ($screenshots->files() as $file) {
            $paths[] = '/item_images/'.$file->path()->value();
        }

        if (!empty($paths)) {
            $this->persist($screenshots->itemId()->value(), ['imagenes' => $paths]);
        }
    }

    public function searchByItemId(ItemId $id): ?ItemImageScreenshots
    {
        return $this->repository->searchByItemId($id);
    }
}
