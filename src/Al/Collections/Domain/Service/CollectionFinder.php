<?php

declare(strict_types=1);

namespace App\Al\Collections\Domain\Service;

use App\Al\Collections\Domain\CollectionId;

final class CollectionFinder
{

    /** @var CollectionReadModelRepositoryInterface */
    private $repository;

    public function __construct(CollectionReadModelRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function find(CollectionId $id, string $language): array
    {
        $collection = $this->repository->search($id, $language);

        if (empty($collection)) {
            throw new \RuntimeException("Collection ".$id." not found");
        }

        return $collection;
    }
}
