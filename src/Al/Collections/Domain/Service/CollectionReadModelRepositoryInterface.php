<?php


namespace App\Al\Collections\Domain\Service;


use App\Al\Collections\Domain\CollectionId;
use App\Shared\Domain\Criteria\Criteria;

interface CollectionReadModelRepositoryInterface
{
    public function countWithItemIdAndLanguageCode(array $ids, string $languageCode): array;

    public function search(CollectionId $id, string $language): ?array;

    public function matching(Criteria $criteria): array;
}
