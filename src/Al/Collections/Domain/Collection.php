<?php

declare(strict_types=1);

namespace App\Al\Collections\Domain;

use App\Al\Collections\Domain\ValueObject\CollectionName;
use App\Shared\Domain\Users\UserId;

final class Collection
{

    /** @var CollectionId */
    private $id;
    /** @var int */
    private $collectionGroup;
    /** @var CollectionName */
    private $name;
    /** @var ?string */
    private $description;
    /** @var string */
    private $language;
    /** @var bool */
    private $isPublished;

    public function __construct(
        CollectionId $id,
        int $collectionGroup,
        CollectionName $name,
        ?string $description,
        string $language,
        bool $isPublished
    ) {
        $this->id = $id;
        $this->collectionGroup = $collectionGroup;
        $this->name = $name;
        $this->description = $description;
        $this->language = $language;
        $this->isPublished = $isPublished;
    }

    /**
     * @return CollectionId
     */
    public function id(): CollectionId
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function collectionGroup(): int
    {
        return $this->collectionGroup;
    }

    /**
     * @return CollectionName
     */
    public function name(): CollectionName
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }
}
