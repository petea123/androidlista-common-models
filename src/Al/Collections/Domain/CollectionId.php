<?php

declare(strict_types=1);

namespace App\Al\Collections\Domain;

use App\Shared\Domain\ValueObject\IntValueObject;

final class CollectionId extends IntValueObject
{

}
