<?php

declare(strict_types=1);

namespace App\Al\Collections\Domain\ValueObject;

final class CollectionName
{
    /** @var string */
    private $value;
    /** @var string */
    private $dirify;
    /** @var ?string */
    private $shortname;


    public function __construct(string $name, string $dirify, ?string $shortname)
    {
        $this->value = $name;
        $this->dirify = $dirify;
        $this->shortname = $shortname;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function dirify(): string
    {
        return $this->dirify;
    }

    /**
     * @return string
     */
    public function shortname(): ?string
    {
        return $this->shortname;
    }
}
