<?php

declare(strict_types=1);

namespace App\Al\Collections\Infrastructure\Persistence\Doctrine;

use App\Al\Collections\Domain\CollectionId;
use App\Shared\Infrastructure\Persistence\Doctrine\IntegerIdType;

final class CollectionIdType extends IntegerIdType
{

    public static function customTypeName(): string
    {
        return 'collection_id';
    }

    protected function typeClassName(): string
    {
        return CollectionId::class;
    }
}
