<?php

declare(strict_types=1);

namespace App\Al\Collections\Infrastructure\Persistence;

use App\Al\Collections\Domain\Collection;
use App\Al\Collections\Domain\CollectionId;
use App\Al\Collections\Domain\Service\CollectionReadModelRepositoryInterface;
use App\Al\Collections\Infrastructure\Projections\CollectionCountView;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Utils;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use Doctrine\DBAL\FetchMode;

final class MySqlCollectionReadModelRepository extends DoctrineRepository implements CollectionReadModelRepositoryInterface
{

    public function countWithItemIdAndLanguageCode(array $ids, string $languageCode): array
    {
        $query = "
            SELECT c.id,c.short_name,c.collection_id,c.name_dirify, count( DISTINCT ci.item_id) AS total, group_concat(DISTINCT ci.item_id) AS items 
            FROM tbl_collections_items ci
            INNER JOIN tbl_collections c ON c.collection_id=ci.collection_id
            WHERE ci.item_id IN (".implode(',',$ids).")  AND ci.language_code= '$languageCode' AND ci.is_deleted = 0
            AND c.language_code='$languageCode' AND c.is_published=1
            GROUP BY ci.collection_id
            ORDER BY total DESC
        ";

        $stmt = $this->entityManager()->getConnection()->prepare($query);
        $stmt->execute();
        $collections = $stmt->fetchAll();

        $result = [];
        foreach ($collections as $collection) {
            $result[] = new CollectionCountView(
                $collection['collection_id'],
                $collection['short_name'],
                $collection['total'],
                explode(',',$collection['items']),
                Utils::domainForLanguage($languageCode).'/collections/'.$collection['id'].'/'.$collection['name_dirify'].'/'
            );
        }

        return $result;
    }

    public function search(CollectionId $id, string $language): ?array
    {
        return $this->entityManager()->getConnection()
            ->executeQuery("SELECT * FROM tbl_collections WHERE language_code='".$language."' AND id = ".$id->value())
            ->fetchAll(FetchMode::ASSOCIATIVE);
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        return $this->repository(Collection::class)->matching($doctrineCriteria)->toArray();
    }
}
