<?php

declare(strict_types=1);

namespace App\Al\Collections\Infrastructure\Projections;

final class CollectionCountView
{
    private $id;

    private $name;

    private $total;

    private $items;

    private $href;

    /**
     * CollectionsContainingItemView constructor.
     * @param $id
     * @param $name
     * @param $total
     * @param $items
     */
    public function __construct($id, $name, $total, $items, $href)
    {
        $this->id = $id;
        $this->name = $name;
        $this->total = $total;
        $this->items = $items;
        $this->href = $href;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function total()
    {
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function items()
    {
        return $this->items;
    }

    /**
     * @return mixed
     */
    public function href()
    {
        return $this->href;
    }
}
