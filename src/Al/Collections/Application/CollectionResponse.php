<?php

declare(strict_types=1);

namespace App\Al\Collections\Application;

use App\Shared\Domain\Bus\Query\Response;

final class CollectionResponse implements Response
{

    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $description;
    /** @var string */
    private $shortname;
    /** @var string */
    private $nameDirify;

    private $language;
    /** @var int */
    private $collectionId;

    public function __construct(int $id, int $collectionId, string $name, string $description, string $shortname, string $nameDirify, string $language)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->shortname = $shortname;
        $this->nameDirify = $nameDirify;
        $this->language = $language;
        $this->collectionId = $collectionId;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function shortname(): string
    {
        return $this->shortname;
    }

    /**
     * @return string
     */
    public function nameDirify(): string
    {
        return $this->nameDirify;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return int
     */
    public function collectionId(): int
    {
        return $this->collectionId;
    }
}
