<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\Search\ByCriteria;

use App\Al\Collections\Application\CollectionsResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchCollectionsByCriteriaQueryHandler implements QueryHandler
{

    /** @var CollectionsByCriteriaSearcher */
    private $searcher;

    public function __construct(CollectionsByCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchCollectionsByCriteriaQuery $query): CollectionsResponse
    {
        $filters = Filters::fromValues($query->filters());
        $order   = Order::fromValues($query->orderBy(), $query->order());

        return $this->searcher->search($filters, $order, $query->limit(), $query->offset());
    }
}
