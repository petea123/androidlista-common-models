<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\Search\ByCriteria;

use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Application\CollectionsResponse;
use App\Al\Collections\Domain\Collection;
use App\Al\Collections\Domain\Service\CollectionReadModelRepositoryInterface;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class CollectionsByCriteriaSearcher
{
    /** @var CollectionReadModelRepositoryInterface */
    private $repository;

    public function __construct(CollectionReadModelRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search(Filters $filters, Order $order, ?int $limit, ?int $offset): CollectionsResponse
    {
        $criteria = new Criteria($filters, $order, $offset, $limit);

        return new CollectionsResponse(...map($this->toResponse(), $this->repository->matching($criteria)));
    }

    private function toResponse(): callable
    {
        return static function (Collection $collection) {
            return new CollectionResponse(
                $collection->id()->value(),
                $collection->collectionGroup(),
                $collection->name()->value(),
                $collection->description() ?? '',
                $collection->name()->shortname() ?? '',
                $collection->name()->dirify(),
                $collection->language()
            );
        };
    }
}
