<?php

declare(strict_types=1);

namespace App\Al\Collections\Application;

use App\Shared\Domain\Bus\Query\Response;

final class CollectionsResponse implements Response
{
    /** @var CollectionResponse[] */
    private $collections;

    public function __construct(CollectionResponse ...$collections)
    {
        $this->collections = $collections;
    }

    public function collections(): array
    {
        return $this->collections;
    }
}
