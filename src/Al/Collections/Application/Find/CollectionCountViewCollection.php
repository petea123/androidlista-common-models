<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\Find;

use App\Al\Collections\Infrastructure\Projections\CollectionCountView;
use App\Shared\Domain\Bus\Query\ResponseCollection;

final class CollectionCountViewCollection extends ResponseCollection
{
    protected function type(): string
    {
        return CollectionCountView::class;
    }
}