<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\Find\CountWithItemAndLanguage;

use App\Al\Collections\Application\Find\CollectionCountViewCollection;
use App\Al\Collections\Domain\Service\CollectionReadModelRepositoryInterface;
use App\Shared\Domain\Bus\Query\QueryHandler;

final class FindCollectionCountWithItemAndLanguageQueryHandler implements QueryHandler
{
    /** @var CollectionReadModelRepositoryInterface */
    private $repository;

    /**
     * FindCollectionCountWithItemAndLanguageHandler constructor.
     * @param CollectionReadModelRepositoryInterface $repository
     */
    public function __construct(CollectionReadModelRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(FindCollectionCountWithItemAndLanguageQuery $query): ?CollectionCountViewCollection
    {
        return new CollectionCountViewCollection($this->repository->countWithItemIdAndLanguageCode($query->items(), $query->language()));
    }
}
