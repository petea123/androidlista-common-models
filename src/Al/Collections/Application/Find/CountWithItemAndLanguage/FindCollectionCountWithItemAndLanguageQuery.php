<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\Find\CountWithItemAndLanguage;

use App\Shared\Domain\Bus\Query\Query;

final class FindCollectionCountWithItemAndLanguageQuery implements Query
{
    /** @var array */
    private $items;

    /** @var string */
    private $language;

    /**
     * FindCollectionCountWithItemAndLanguageQuery constructor.
     * @param array $items
     * @param string $language
     */
    public function __construct(array $items, string $language)
    {
        $this->items = $items;
        $this->language = $language;
    }

    /**
     * @return array
     */
    public function items(): array
    {
        return $this->items;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }


}