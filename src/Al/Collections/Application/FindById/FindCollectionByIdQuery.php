<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\FindById;

use App\Shared\Domain\Bus\Query\Query;

final class FindCollectionByIdQuery implements Query
{

    /** @var int */
    private $id;
    /** @var string */
    private $language;

    public function __construct(int $id, string $language)
    {
        $this->id = $id;
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
