<?php

declare(strict_types=1);

namespace App\Al\Collections\Application\FindById;

use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Domain\CollectionId;
use App\Al\Collections\Domain\Service\CollectionFinder;
use App\Shared\Domain\Bus\Query\QueryHandler;

final class FindCollectionByIdQueryHandler implements QueryHandler
{

    /** @var CollectionFinder */
    private $finder;

    public function __construct(CollectionFinder $finder)
    {
        $this->finder = $finder;
    }

    public function __invoke(FindCollectionByIdQuery $query): CollectionResponse
    {
        $collection = $this->finder->find(new CollectionId($query->id()), $query->language());

        return new CollectionResponse(
            $query->id(),
            (int)$collection[0]['collection_id'],
            $collection[0]['name'],
            strip_tags(html_entity_decode($collection[0]['description'] ?? '')),
            $collection[0]['short_name'] ?? $collection[0]['name'],
            $collection[0]['name_dirify'],
            $query->language()
        );
    }
}
