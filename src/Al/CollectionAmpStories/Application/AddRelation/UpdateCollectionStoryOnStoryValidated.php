<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Application\AddRelation;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Application\StoryResponse;
use App\Amp\Stories\Domain\Event\StoryWasValidated;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Bus\Query\QueryBus;
use function Lambdish\Phunctional\map;

final class UpdateCollectionStoryOnStoryValidated implements DomainEventSubscriber
{
    /**
     * @var CollectionAmpStoryRelationUpdater
     */
    private $updater;
    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(CollectionAmpStoryRelationUpdater $updater, QueryBus $queryBus)
    {
        $this->updater = $updater;
        $this->queryBus = $queryBus;
    }

    public function __invoke(StoryWasValidated $event)
    {
        /** @var StoryResponse $story */
        $story = $this->queryBus->ask(new SearchStoryQuery($event->aggregateId()));

        $storyId = new StoryId($event->aggregateId());
        $collections = map(function (int $collection) { return new CollectionId($collection);}, $story->categories());

        $this->updater->updateBatch($collections, $storyId);
    }

    public static function subscribedTo(): array
    {
        return [StoryWasValidated::class];
    }
}
