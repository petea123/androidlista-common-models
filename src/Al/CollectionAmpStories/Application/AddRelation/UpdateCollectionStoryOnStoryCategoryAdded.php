<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Application\AddRelation;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Event\StoryCategoryWasAdded;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class UpdateCollectionStoryOnStoryCategoryAdded implements DomainEventSubscriber
{

    /**
     * @var CollectionAmpStoryRelationUpdater
     */
    private $updater;

    public function __construct(CollectionAmpStoryRelationUpdater $updater)
    {
        $this->updater = $updater;
    }

    public function __invoke(StoryCategoryWasAdded $event)
    {
        $storyId = new StoryId($event->aggregateId());
        $collectionId = new CollectionId($event->category());

        $this->updater->update($collectionId, $storyId);
    }

    public static function subscribedTo(): array
    {
        return [StoryCategoryWasAdded::class];
    }
}
