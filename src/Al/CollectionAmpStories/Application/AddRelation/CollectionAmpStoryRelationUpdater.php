<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Application\AddRelation;

use App\Al\CollectionAmpStories\Domain\CollectionAmpStory;
use App\Al\CollectionAmpStories\Domain\CollectionAmpStoryRepository;
use App\Al\Collections\Application\CollectionResponse;
use App\Al\Collections\Application\CollectionsResponse;
use App\Al\Collections\Application\Search\ByCriteria\SearchCollectionsByCriteriaQuery;
use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Application\StoryResponse;
use App\Shared\Domain\Bus\Query\QueryBus;

final class CollectionAmpStoryRelationUpdater
{

    /**
     * @var CollectionAmpStoryRepository
     */
    private $repository;
    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(CollectionAmpStoryRepository $repository, QueryBus $queryBus)
    {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
    }

    public function update(CollectionId $collectionId, StoryId $storyId)
    {
        /** @var StoryResponse $story */
        $story = $this->queryBus->ask(new SearchStoryQuery($storyId->value()));
        if (!$this->guardValid($story, $collectionId)) {
            return;
        }

        $collection = $this->searchCollection($collectionId, $story->language());

        if (null !== $collection) {
            $this->addStory(new CollectionId($collection->id()), $storyId);
        }
    }

    public function updateBatch(array $collectionIds, StoryId $storyId)
    {
        /** @var StoryResponse $story */
        $story = $this->queryBus->ask(new SearchStoryQuery($storyId->value()));

        foreach ($collectionIds as $collectionId) {
            if ($this->guardValid($story, $collectionId)) {
                $collection = $this->searchCollection($collectionId, $story->language());
                if (null !== $collection) {
                    $this->addStory(new CollectionId($collection->id()), $storyId);
                }
            }
        }
    }

    private function addStory(CollectionId $collectionId, StoryId $storyId)
    {
        $relation = $this->repository->search($collectionId) ?: $this->initializeRelation($collectionId);

        if (!$relation->hasStory($storyId)) {
            $relation->addStory($storyId);
            $this->repository->save($relation);
        }
    }

    private function guardValid(StoryResponse $response, CollectionId $collectionId)
    {
        return $response->validated() && in_array($collectionId->value(), $response->categories());
    }

    private function initializeRelation(CollectionId $id): CollectionAmpStory
    {
        return CollectionAmpStory::initialize($id);
    }

    private function searchCollection(CollectionId $collectionId, string $language): ?CollectionResponse
    {
        /** @var CollectionsResponse $collection */
        $collection = $this->queryBus->ask(new SearchCollectionsByCriteriaQuery([
            [
                'field' => 'collectionGroup',
                'operator' => '=',
                'value' => (string)$collectionId->value()
            ],
            [
                'field' => 'language',
                'operator' => '=',
                'value' => $language
            ]
        ]))->collections();

        return $collection[0] ?? null;
    }
}
