<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Domain;

use App\Al\Collections\Domain\CollectionId;

interface CollectionAmpStoryRepository
{
    public function save(CollectionAmpStory $collectionAmpStory): void;

    public function search(CollectionId $id): ?CollectionAmpStory;
}
