<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Domain;

use App\Al\Collections\Domain\CollectionId;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Shared\Domain\Aggregate\AggregateRoot;
use function Lambdish\Phunctional\map;
use function Lambdish\Phunctional\search;

final class CollectionAmpStory extends AggregateRoot
{
    /** @var CollectionId */
    private $collectionId;

    /** @var array */
    private $storiesId;

    /**
     * CollectionAmpStory constructor.
     * @param CollectionId $collectionId
     * @param array $storiesId
     */
    public function __construct(CollectionId $collectionId, StoryId ...$storiesId)
    {
        $this->collectionId = $collectionId;
        $this->storiesId = $storiesId;
    }

    public static function initialize(CollectionId $collectionId): self
    {
        return new self($collectionId);
    }
    /**
     * @return CollectionId
     */
    public function collectionId(): CollectionId
    {
        return $this->collectionId;
    }

    /**
     * @return array
     */
    public function storiesId(): array
    {
        return $this->storiesId;
    }

    public function storiesIdToValues(): array
    {
        return map(self::storyIdToValue(), $this->storiesId());
    }

    public static function storyIdToValue(): callable
    {
        return static function (StoryId $id): string {
            return $id->value();
        };
    }

    public function addStory(StoryId $id)
    {
        $this->storiesId[] = $id;
    }

    public function removeStory(StoryId $id)
    {
        $this->categories = array_values(array_filter($this->storiesId, function($other) use ($id) {
            return !$this->storyIdComparator($other)($id);
        }));
    }

    public function hasStory(StoryId $id): bool
    {
        return null !== search($this->storyIdComparator($id), $this->storiesId());
    }

    private function storyIdComparator(StoryId $id): callable
    {
        return static function (StoryId $other) use ($id) {
            return $id->equals($other);
        };
    }
}
