<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Infrastructure\Persistence;

use App\Al\CollectionAmpStories\Domain\CollectionAmpStory;
use App\Al\CollectionAmpStories\Domain\CollectionAmpStoryRepository;
use App\Al\Collections\Domain\CollectionId;
use App\Shared\Infrastructure\Persistence\Redis\RedisRepository;
use App\Shared\Infrastructure\Redis\RedisClient;

final class RedisCacheCollectionAmpStoryRepository extends RedisRepository implements CollectionAmpStoryRepository
{

    /**
     * @var MySqlCollectionAmpStoryRepository
     */
    private $repository;

    public function __construct(RedisClient $client, MySqlCollectionAmpStoryRepository $repository)
    {
        parent::__construct($client);
        $this->repository = $repository;
    }

    public function save(CollectionAmpStory $collectionAmpStory): void
    {
        $this->repository->save($collectionAmpStory);
        $this->persist((string)$collectionAmpStory->collectionId()->value(), $collectionAmpStory->storiesIdToValues());
    }

    protected function aggregateName(): string
    {
        return 'al_collection_stories';
    }

    public function search(CollectionId $id): ?CollectionAmpStory
    {
        return $this->repository->search($id);
    }
}
