<?php

declare(strict_types=1);

namespace App\Al\CollectionAmpStories\Infrastructure\Persistence;

use App\Al\CollectionAmpStories\Domain\CollectionAmpStory;
use App\Al\CollectionAmpStories\Domain\CollectionAmpStoryRepository;
use App\Al\Collections\Domain\CollectionId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlCollectionAmpStoryRepository extends DoctrineRepository implements CollectionAmpStoryRepository
{

    public function save(CollectionAmpStory $collectionAmpStory): void
    {
        $this->persist($collectionAmpStory);
        $this->entityManager()->flush($collectionAmpStory);
    }

    public function search(CollectionId $id): ?CollectionAmpStory
    {
        return $this->repository(CollectionAmpStory::class)->find($id);
    }
}
