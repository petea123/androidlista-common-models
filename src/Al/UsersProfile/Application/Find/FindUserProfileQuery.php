<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Application\Find;

use App\Shared\Domain\Bus\Query\Query;

final class FindUserProfileQuery implements Query
{

    /** @var int */
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
