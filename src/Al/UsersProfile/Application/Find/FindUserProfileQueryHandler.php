<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Application\Find;

use App\Al\UsersProfile\Application\UserProfileResponse;
use App\Al\UsersProfile\Domain\Service\UserProfileFinder;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\Users\UserId;

final class FindUserProfileQueryHandler implements QueryHandler
{

    /** @var UserProfileFinder */
    private $finder;

    public function __construct(UserProfileFinder $finder)
    {
        $this->finder = $finder;
    }

    public function __invoke(FindUserProfileQuery $query): ?Response
    {
        $user = $this->finder->find(new UserId($query->id()));

        return new UserProfileResponse(
            $user->id()->value(),
            $user->userId()->value(),
            $user->firstname()->value(),
            $user->lastname()->value(),
            $user->avatar()->value()
        );
    }
}
