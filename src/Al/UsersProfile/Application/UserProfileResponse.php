<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Application;

use App\Shared\Domain\Bus\Query\Response;

final class UserProfileResponse implements Response
{
    /** @var int */
    private $id;
    /** @var int */
    private $userId;
    /** @var string */
    private $firstname;
    /** @var string */
    private $lastname;
    /** @var string */
    private $avatar;

    public function __construct(int $id, int $userId, string $firstname, string $lastname, string $avatar)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->avatar = $avatar;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function userId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function firstname(): string
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function lastname(): string
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function avatar(): string
    {
        return $this->avatar;
    }

    public function username(): string
    {
        return $this->firstname.' '.$this->lastname;
    }
}
