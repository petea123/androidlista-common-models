<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Infrastructure\Persistence;

use App\Al\UsersProfile\Domain\Service\UserProfileRepository;
use App\Al\UsersProfile\Domain\UserProfile;
use App\Shared\Domain\Users\UserId;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlUserProfileRepository extends DoctrineRepository implements UserProfileRepository
{
    public function search(UserId $id): ?UserProfile
    {
        return $this->repository(UserProfile::class)->findOneBy(['userId' => $id]);
    }
}
