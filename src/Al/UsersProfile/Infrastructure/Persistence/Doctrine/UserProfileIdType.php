<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Infrastructure\Persistence\Doctrine;

use App\Al\UsersProfile\Domain\UserProfileId;
use App\Shared\Infrastructure\Persistence\Doctrine\IntegerIdType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

final class UserProfileIdType extends IntegerIdType
{

    public static function customTypeName(): string
    {
        return 'user_profile_id';
    }

    protected function typeClassName(): string
    {
        return UserProfileId::class;
    }
}
