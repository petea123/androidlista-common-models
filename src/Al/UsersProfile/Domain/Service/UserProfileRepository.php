<?php


namespace App\Al\UsersProfile\Domain\Service;


use App\Al\UsersProfile\Domain\UserProfile;
use App\Shared\Domain\Users\UserId;

interface UserProfileRepository
{
    public function search(UserId $id): ?UserProfile;
}
