<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Domain\Service;

use App\Al\UsersProfile\Domain\UserProfile;
use App\Shared\Domain\Users\UserId;
use Exception;

final class UserProfileFinder
{
    /** @var UserProfileRepository */
    private $repository;

    public function __construct(UserProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function find(UserId $id) : UserProfile
    {
        $user = $this->repository->search($id);

        if (null === $user) {
            throw new Exception('User with id '.$id->value().' not found');
        }

        return $user;
    }
}
