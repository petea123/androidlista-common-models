<?php

declare(strict_types=1);

namespace App\Al\UsersProfile\Domain;

use App\Shared\Domain\Users\UserId;

final class UserProfile
{

    /** @var UserProfileId */
    private $id;

    /** @var UserId */
    private $userId;

    /** @var UserProfileFirstname */
    private $firstname;

    /** @var UserProfileLastname */
    private $lastname;

    /** @var UserProfileAvatar */
    private $avatar;

    public function __construct(UserProfileId $id, UserId $userId, UserProfileFirstname $firstname, UserProfileLastname $lastname, UserProfileAvatar $avatar)
    {

        $this->id = $id;
        $this->userId = $userId;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->avatar = $avatar;
    }

    /**
     * @return UserProfileId
     */
    public function id(): UserProfileId
    {
        return $this->id;
    }

    /**
     * @return UserId
     */
    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return UserProfileFirstname
     */
    public function firstname(): UserProfileFirstname
    {
        return $this->firstname;
    }

    /**
     * @return UserProfileLastname
     */
    public function lastname(): UserProfileLastname
    {
        return $this->lastname;
    }

    /**
     * @return UserProfileAvatar
     */
    public function avatar(): UserProfileAvatar
    {
        return $this->avatar;
    }
}
