<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Infrastructure\Persistence;

use App\Al\CollectionItems\Domain\CollectionItem;
use App\Al\CollectionItems\Domain\CollectionItemRepository;
use App\Al\Collections\Domain\CollectionId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use Doctrine\DBAL\FetchMode;
use function Lambdish\Phunctional\map;

final class MySqlCollectionItemRepository extends DoctrineRepository implements CollectionItemRepository
{

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);
        $qb = $this->queryBuilder()
            ->select('c')
            ->from(CollectionItem::class, 'c')
            ->addCriteria($doctrineCriteria)
            ->groupBy('c.itemId')
            ->getQuery();

        $items = $qb->execute();

        return $items;
    }

    public function relevantItems(CollectionId $id, string $language): array
    {
        $rawItems = $this->entityManager()->getConnection()
            ->executeQuery("SELECT 
                                    if(l.language_code_text='es',1,0 ) as descri,
                                    c.id, c.item_id as itemId, c.collection_id as collectionId, c.is_deleted as isDeleted, c.is_manual as isManual, c.language_code as language 
                                    FROM tbl_collections_items c 
                                    LEFT JOIN tbl_item i ON i.id = c.item_id
                                    LEFT JOIN tbl_item_data id ON id.item_id=c.item_id
                                    LEFT JOIN tbl_desc_item_lang l ON l.id_item=c.item_id
                                    WHERE c.collection_id = $id AND c.language_code = '$language'
                                    AND id.typefield_id=17 AND c.is_deleted = 0
                                    GROUP BY c.item_id, id.value, i.nr_votes
                                    ORDER BY descri DESC, i.nr_votes DESC, str_to_date(id.value, '%d-%m-%Y') DESC, i.nr_votes DESC
                                    LIMIT 10")
            ->fetchAll(FetchMode::ASSOCIATIVE);

        return map($this->toCollectionItem(), $rawItems);
    }

    private function toCollectionItem()
    {
        return static function (array $primitives) {
            return CollectionItem::fromPrimitives($primitives);
        };
    }
}
