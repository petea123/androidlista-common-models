<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Application;

use App\Shared\Domain\Bus\Query\Response;

final class CollectionItemsResponse implements Response
{

    /** @var CollectionItemResponse[] */
    private $collectionItemsResponse;

    public function __construct(CollectionItemResponse ...$collectionItemsResponse)
    {
        $this->collectionItemsResponse = $collectionItemsResponse;
    }

    /**
     * @return CollectionItemResponse[]
     */
    public function collectionItemsResponse(): array
    {
        return $this->collectionItemsResponse;
    }
}
