<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Application;

use App\Shared\Domain\Bus\Query\Response;

final class CollectionItemResponse implements Response
{
    /** @var int */
    private $id;
    /** @var int */
    private $collectionId;
    /** @var int */
    private $itemId;
    /** @var string */
    private $language;
    /** @var bool */
    private $isDeleted;
    /** @var bool */
    private $isManual;

    public function __construct(int $id, int $collectionId, int $itemId, string $language, bool $isDeleted, bool $isManual)
    {
        $this->id = $id;
        $this->collectionId = $collectionId;
        $this->itemId = $itemId;
        $this->language = $language;
        $this->isDeleted = $isDeleted;
        $this->isManual = $isManual;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function collectionId(): int
    {
        return $this->collectionId;
    }

    /**
     * @return int
     */
    public function itemId(): int
    {
        return $this->itemId;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @return bool
     */
    public function isManual(): bool
    {
        return $this->isManual;
    }
}
