<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Application\SearchByCriteria;

use App\Al\CollectionItems\Application\CollectionItemsResponse;
use App\Al\Collections\Domain\CollectionId;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchCollectionItemsByCriteriaQueryHandler implements QueryHandler
{
    /** @var CollectionItemsByCriteriaSearcher */
    private $searcher;

    public function __construct(CollectionItemsByCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchCollectionItemsByCriteriaQuery $query): CollectionItemsResponse
    {

        return $this->searcher->search(new CollectionId($query->collectionId()), $query->language());
    }
}
