<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Application\SearchByCriteria;

use App\Shared\Domain\Bus\Query\Query;

final class SearchCollectionItemsByCriteriaQuery implements Query
{

    /** @var int */
    private $collectionId;
    /** @var string */
    private $language;

    public function __construct(
        int $collectionId,
        string $language
    ) {
        $this->collectionId = $collectionId;
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function collectionId(): int
    {
        return $this->collectionId;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
