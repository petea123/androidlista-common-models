<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Application\SearchByCriteria;

use App\Al\CollectionItems\Application\CollectionItemResponse;
use App\Al\CollectionItems\Application\CollectionItemsResponse;
use App\Al\CollectionItems\Domain\CollectionItem;
use App\Al\CollectionItems\Domain\CollectionItemRepository;
use App\Al\Collections\Domain\CollectionId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class CollectionItemsByCriteriaSearcher
{
    /** @var CollectionItemRepository */
    private $repository;

    public function __construct(CollectionItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(CollectionId $id, string $language): CollectionItemsResponse
    {
        return new CollectionItemsResponse(...map($this->toResponse(), $this->repository->relevantItems($id, $language)));
    }

    private function toResponse(): callable
    {
        return static function (CollectionItem $collectionItem) {
            return new CollectionItemResponse(
                $collectionItem->id(),
                $collectionItem->collectionId(),
                $collectionItem->itemId()->value(),
                $collectionItem->language(),
                $collectionItem->isDeleted(),
                $collectionItem->isManual()
            );
        };
    }
}
