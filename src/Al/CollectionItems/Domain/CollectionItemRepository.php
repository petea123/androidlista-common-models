<?php


namespace App\Al\CollectionItems\Domain;


use App\Al\Collections\Domain\CollectionId;
use App\Shared\Domain\Criteria\Criteria;

interface CollectionItemRepository
{

    public function relevantItems(CollectionId $id, string $language): array;

    public function matching(Criteria $criteria): array;
}
