<?php

declare(strict_types=1);

namespace App\Al\CollectionItems\Domain;

use App\Al\Shared\Domain\Items\ItemId;

final class CollectionItem
{

    /** @var int */
    private $id;
    /** @var int */
    private $collectionId;
    /** @var ItemId */
    private $itemId;
    /** @var bool */
    private $isDeleted;
    /** @var bool */
    private $isManual;
    /** @var string */
    private $language;

    public function __construct(int $id, int $collectionId, ItemId $itemId, string $language, bool $isDeleted, bool $isManual)
    {
        $this->id = $id;
        $this->collectionId = $collectionId;
        $this->itemId = $itemId;
        $this->isDeleted = $isDeleted;
        $this->isManual = $isManual;
        $this->language = $language;
    }

    public static function fromPrimitives(array $primitives)
    {
        return new self(
            intval($primitives['id']),
            intval($primitives['collectionId']),
            new ItemId(intval($primitives['itemId'])),
            $primitives['language'],
            boolval($primitives['isDeleted']),
            boolval($primitives['isManual'])
        );
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function collectionId(): int
    {
        return $this->collectionId;
    }

    /**
     * @return ItemId
     */
    public function itemId(): ItemId
    {
        return $this->itemId;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @return bool
     */
    public function isManual(): bool
    {
        return $this->isManual;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
