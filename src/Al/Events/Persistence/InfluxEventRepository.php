<?php

declare(strict_types=1);

namespace App\Al\Events\Persistence;

use App\Al\Events\Domain\Service\EventRepositoryInterface;
use App\Shared\Domain\Bus\Event\DomainEvent;
use InfluxDB\Client;
use InfluxDB\Database;
use InfluxDB\Point;

class InfluxEventRepository implements EventRepositoryInterface
{

    private const TAGS = [
        'source' => 'attributes',
        'class' => 'meta',
        'change' => 'attributes'
    ];

    private const FIELDS = [
        'itemId' => 'attributes'
    ];
    /**
     * @var Client
     */
    private $influxClient;

    public function __construct(string $influxDsn)
    {

        $this->influxClient = Client::fromDSN($influxDsn);
    }

    public function save(array ...$events): void
    {
        $points = [];

        foreach ($events as $event) {

            $event = $event['data'];

            $tags = array_merge($this->getData(self::TAGS, $event), [
                'event_name' => $event['event_name']
            ]);

            $fields = array_merge($this->getData(self::FIELDS, $event), [
                'aggregate_id' => (string)$event['aggregate_id'],
                'event' => json_encode($event)
            ]);

            $points[] = new Point(
                'event_store',
                null,
                $tags,
                $fields,
                $event['occurred_on']
            );
        }

        $result = $this->influxClient->writePoints($points, Database::PRECISION_SECONDS);
    }

    private function getData(array $data, array $event)
    {

        $fields = [];
        foreach ($data as $key => $place) {
            if (isset($event[$place][$key])) {
                $fields[$key] = $event[$place][$key];
            }
        }

        return $fields;
    }
}
