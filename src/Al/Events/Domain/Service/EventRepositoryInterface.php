<?php

declare(strict_types=1);

namespace App\Al\Events\Domain\Service;

interface EventRepositoryInterface
{
    public function save(array ...$event): void;
}