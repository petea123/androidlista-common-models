<?php

declare(strict_types=1);

namespace App\Analytics\Notifications\Application\Send;

use Nexy\Slack\Client;

final class Sender
{
    /**
     * @var Client
     */
    private $slack;

    public function __construct(Client $slack)
    {
        $this->slack = $slack;
    }

    public function send(string $channel, string $text)
    {
        $this->slack
            ->to($channel)
            ->send($text);
    }
}
