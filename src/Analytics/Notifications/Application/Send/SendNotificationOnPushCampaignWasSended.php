<?php

declare(strict_types=1);

namespace App\Analytics\Notifications\Application\Send;

use App\Retention\Push\Domain\Campaign\PushCampaignWasSended;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class SendNotificationOnPushCampaignWasSended implements DomainEventSubscriber
{

    /**
     * @var Sender
     */
    private $sender;

    public function __construct(Sender $sender)
    {
        $this->sender = $sender;
    }

    public function __invoke(PushCampaignWasSended $event)
    {
        $this->sender->send('#push_notification',
            sprintf("*Push sent*: %s\n *Title*: %s\n *Url*: %s\n Icon: %s",
                strtoupper($event->language()), $event->title(), $event->href(), $event->icon()
            ));
    }

    public static function subscribedTo(): array
    {
        return [PushCampaignWasSended::class];
    }
}
