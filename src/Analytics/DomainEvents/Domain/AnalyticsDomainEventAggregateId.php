<?php

declare(strict_types = 1);

namespace App\Analytics\DomainEvents\Domain;

final class AnalyticsDomainEventAggregateId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }
}
