<?php

declare(strict_types = 1);

namespace App\Analytics\DomainEvents\Domain;


use App\Shared\Domain\ValueObject\Uuid;

final class AnalyticsDomainEventId extends Uuid
{
}
