<?php

declare(strict_types = 1);

namespace App\Analytics\DomainEvents\Domain;


use App\Shared\Domain\ValueObject\StringValueObject;

final class AnalyticsDomainEventName extends StringValueObject
{
}
