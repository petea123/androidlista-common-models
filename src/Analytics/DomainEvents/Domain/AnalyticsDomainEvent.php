<?php

declare(strict_types=1);

namespace App\Analytics\DomainEvents\Domain;

final class AnalyticsDomainEvent
{
    private $id;
    private $aggregateId;
    private $name;
    private $body;
    /** @var AnalyticsDomainEventOccurredOn */
    private $occurredOn;

    public function __construct(
        AnalyticsDomainEventId $id,
        AnalyticsDomainEventAggregateId $aggregateId,
        AnalyticsDomainEventName $name,
        AnalyticsDomainEventBody $body,
        AnalyticsDomainEventOccurredOn $occurredOn
    ) {
        $this->id          = $id;
        $this->aggregateId = $aggregateId;
        $this->name        = $name;
        $this->body        = $body;
        $this->occurredOn = $occurredOn;
    }

    public function id(): AnalyticsDomainEventId
    {
        return $this->id;
    }

    public function aggregateId(): AnalyticsDomainEventAggregateId
    {
        return $this->aggregateId;
    }

    public function name(): AnalyticsDomainEventName
    {
        return $this->name;
    }

    public function body(): AnalyticsDomainEventBody
    {
        return $this->body;
    }

    /**
     * @return AnalyticsDomainEventOccurredOn
     */
    public function occurredOn(): AnalyticsDomainEventOccurredOn
    {
        return $this->occurredOn;
    }
}
