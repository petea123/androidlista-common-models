<?php

declare(strict_types=1);

namespace App\Analytics\DomainEvents\Infrastructure\Persistence\Influx;

use InfluxDB\Database;
use InfluxDB\Point;

abstract class InfluxRepository
{

    /** @var Database */
    private $client;

    public function __construct(Database $client)
    {
        $this->client = $client;
    }

    protected function persist(Point $point): void
    {
        $this->client->writePoints([$point], Database::PRECISION_SECONDS);
    }
}
