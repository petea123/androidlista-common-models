<?php

declare(strict_types=1);

namespace App\Analytics\DomainEvents\Infrastructure;

use App\Analytics\DomainEvents\Domain\AnalyticsDomainEvent;
use App\Analytics\DomainEvents\Domain\DomainEventsRepository;
use App\Analytics\DomainEvents\Infrastructure\Persistence\Influx\InfluxRepository;
use InfluxDB\Point;

final class InfluxAnalyticsDomainEventRepository extends InfluxRepository implements DomainEventsRepository
{

    public function save(AnalyticsDomainEvent $event): void
    {
        $tags = ['event_name' => $event->name()->value()];

        if (!empty($event->body()->value()['source'])) {
            $tags['source'] = $event->body()->value()['source'];
        }

        $point = new Point(
            'event_store',
            null,
            $tags,
            [
                'event_id' => $event->id()->value(),
                'aggregate_id' => $event->aggregateId()->value(),
                'body' => json_encode($event->body()->value())
            ],
            $event->occurredOn()->timestamp() + mt_rand(0, 100)
        );

        $this->persist($point);
    }
}
