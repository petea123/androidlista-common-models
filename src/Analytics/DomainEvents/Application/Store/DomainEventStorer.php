<?php

declare(strict_types=1);

namespace App\Analytics\DomainEvents\Application\Store;

use App\Analytics\DomainEvents\Domain\AnalyticsDomainEvent;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventAggregateId;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventBody;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventId;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventName;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventOccurredOn;
use App\Analytics\DomainEvents\Domain\DomainEventsRepository;

final class DomainEventStorer
{
    private $repository;

    public function __construct(DomainEventsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(
        AnalyticsDomainEventId $id,
        AnalyticsDomainEventAggregateId $aggregateId,
        AnalyticsDomainEventName $name,
        AnalyticsDomainEventBody $body,
        AnalyticsDomainEventOccurredOn $occurredOn
    ): void {
        $domainEvent = new AnalyticsDomainEvent($id, $aggregateId, $name, $body, $occurredOn);

        $this->repository->save($domainEvent);
    }
}
