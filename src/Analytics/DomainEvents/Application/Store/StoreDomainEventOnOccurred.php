<?php

declare(strict_types=1);

namespace App\Analytics\DomainEvents\Application\Store;

use App\Al\Items\Domain\Event\ItemApkStatusWasPublished;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventAggregateId;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventBody;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventId;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventName;
use App\Analytics\DomainEvents\Domain\AnalyticsDomainEventOccurredOn;
use App\Retention\Newsletter\Domain\NewsletterWasUnsubscribed;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class StoreDomainEventOnOccurred implements DomainEventSubscriber
{
    private $storer;

    public function __construct(DomainEventStorer $storer)
    {
        $this->storer = $storer;
    }

    public static function subscribedTo(): array
    {
        return [
            DomainEvent::class,
            ItemApkStatusWasPublished::class,
            NewsletterWasUnsubscribed::class
        ];
    }

    public function __invoke(DomainEvent $event)
    {
        $id          = new AnalyticsDomainEventId($event->eventId());
        $aggregateId = new AnalyticsDomainEventAggregateId($event->aggregateId());
        $name        = new AnalyticsDomainEventName($event::eventName());
        $body        = new AnalyticsDomainEventBody($event->toPrimitives());
        $occurredOn = AnalyticsDomainEventOccurredOn::createFromTimestamp((int)$event->occurredOn());
        $this->storer->store($id, $aggregateId, $name, $body, $occurredOn);
    }
}
