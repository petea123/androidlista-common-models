<?php

declare(strict_types=1);

namespace App\Retention\Shared\Campaign\Domain;

use App\Shared\Domain\ValueObject\Uuid;

final class CampaignId extends Uuid
{

}
