<?php

declare(strict_types=1);

namespace App\Retention\Shared\Campaign\Domain;

use App\Shared\Domain\ValueObject\Enum;

/**
 * @method static CampaignChannel facebook()
 * @method static CampaignChannel push()
 */
final class CampaignChannel extends Enum
{

    public const FACEBOOK = 'fb';
    public const PUSH = 'push';

    protected function throwExceptionForInvalidValue($value)
    {
        // TODO: Implement throwExceptionForInvalidValue() method.
    }
}
