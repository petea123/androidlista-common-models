<?php

declare(strict_types=1);

namespace App\Retention\Push\Application;

use App\Shared\Domain\Bus\Query\Response;

final class PushesResponse implements Response
{
    private $pushes;

    public function __construct(PushResponse ... $pushes)
    {
        $this->pushes = $pushes;
    }

    /**
     * @return PushResponse[]
     */
    public function pushes(): array
    {
        return $this->pushes;
    }
}
