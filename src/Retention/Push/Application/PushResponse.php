<?php

declare(strict_types=1);

namespace App\Retention\Push\Application;

use function Lambdish\Phunctional\last;

final class PushResponse
{

    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $sentAt;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $href;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $sentTo;

    public function __construct(
        string $id,
        int $sentAt,
        string $title,
        string $href,
        string $icon,
        string $sentTo
    ) {
        $this->id = $id;
        $this->sentAt = $sentAt;
        $this->title = $title;
        $this->href = $href;
        $this->icon = $icon;
        $this->sentTo = $sentTo;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function sentAt(): int
    {
        return $this->sentAt;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function sentTo(): string
    {
        return $this->sentTo;
    }
}
