<?php

declare(strict_types=1);

namespace App\Retention\Push\Application\SearchByCriteria;

use App\Retention\Push\Application\PushesResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchPushesByCriteriaQueryHandler implements QueryHandler
{
    /** @var PushesCriteriaSearcher */
    private $searcher;

    public function __construct(PushesCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchPushesByCriteriaQuery $query): PushesResponse
    {
        $filters = Filters::fromValues($query->filters());
        $order   = Order::fromValues($query->orderBy(), $query->order());

        return $this->searcher->search($filters, $order, $query->limit(), $query->offset());
    }
}
