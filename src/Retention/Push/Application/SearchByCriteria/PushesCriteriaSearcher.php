<?php

declare(strict_types=1);

namespace App\Retention\Push\Application\SearchByCriteria;

use App\Retention\Push\Application\PushesResponse;
use App\Retention\Push\Application\PushResponse;
use App\Retention\Push\Domain\Push;
use App\Retention\Push\Domain\PushRepository;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class PushesCriteriaSearcher
{
    /** @var PushRepository */
    private $repository;

    public function __construct(PushRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(Filters $filters, Order $order, ?int $limit, ?int $offset): PushesResponse
    {
        $criteria = new Criteria($filters, $order, $offset, $limit);

        return new PushesResponse(...map($this->toResponse(), $this->repository->matching($criteria)));
    }

    private function toResponse(): callable
    {
        return static function (Push $push) {
            return new PushResponse(
                $push->id()->value(),
                $push->sentAt()->getTimestamp(),
                $push->title(),
                $push->href(),
                $push->icon(),
                $push->to()->value(),
            );
        };
    }
}
