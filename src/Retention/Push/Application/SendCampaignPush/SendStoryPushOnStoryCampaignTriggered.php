<?php

declare(strict_types=1);

namespace App\Retention\Push\Application\SendCampaignPush;

use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaignTriggered;
use App\Retention\Push\Domain\PushLanguage;
use App\Retention\Push\Domain\PushTo;
use App\Retention\Shared\Campaign\Domain\CampaignChannel;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use function Lambdish\Phunctional\apply;

final class SendStoryPushOnStoryCampaignTriggered implements DomainEventSubscriber
{

    /**
     * @var PushCampaignSender
     */
    private $pushSender;

    public function __construct(PushCampaignSender $pushSender)
    {
        $this->pushSender = $pushSender;
    }

    public function __invoke(StoryCampaignTriggered $event)
    {
        $channels = CampaignChannels::fromValues($event->channels());
        if (!$channels->hasChannel(CampaignChannel::push())) {
            return;
        }

        $topic = PushTo::localeTopic(LocaleValueObject::fromString($event->language()));
        $campaignId = new CampaignId($event->aggregateId());
        $language = new PushLanguage($event->language());
        $href = $event->href().'?utm_source=stories_push&utm_medium=push&utm_campaign=stories';
        apply($this->pushSender, [$event->title(), $event->description(), $event->icon(), $event->horizontalImg(), $href, $topic, $campaignId, $language, 'stories']);
    }

    public static function subscribedTo(): array
    {
        return [StoryCampaignTriggered::class];
    }
}
