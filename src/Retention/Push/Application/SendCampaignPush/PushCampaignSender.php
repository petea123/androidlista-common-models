<?php

declare(strict_types=1);

namespace App\Retention\Push\Application\SendCampaignPush;

use App\Retention\Push\Domain\Campaign\PushCampaign;
use App\Retention\Push\Domain\PushId;
use App\Retention\Push\Domain\PushLanguage;
use App\Retention\Push\Domain\PushRepository;
use App\Retention\Push\Domain\PushSenderClient;
use App\Retention\Push\Domain\PushTo;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\UuidGenerator;

final class PushCampaignSender
{
    /**
     * @var PushSenderClient
     */
    private $sender;
    /**
     * @var UuidGenerator
     */
    private $generator;
    /**
     * @var PushRepository
     */
    private $repository;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    public function __construct(
        PushRepository $repository,
        PushSenderClient $sender,
        UuidGenerator $generator,
        DomainEventPublisher $publisher
    ) {
        $this->sender = $sender;
        $this->generator = $generator;
        $this->repository = $repository;
        $this->publisher = $publisher;
    }

    public function __invoke(
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        string $href,
        PushTo $to,
        CampaignId $campaignId,
        PushLanguage $language,
        string $type
    ) {
        if (null !== $this->repository->searchCampaign($campaignId)) {
            return;
        }

        $push = PushCampaign::create(
            new PushId($this->generator->next()),
            $campaignId,
            $title,
            $description,
            $icon,
            $horizontalImg,
            $href,
            $to,
            $language,
            $type
        );

        if ($this->sender->send($push)) {
            $push->send();
            $this->publisher->publish(...$push->pullDomainEvents());
            $this->repository->save($push);
        }
    }
}
