<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain;

use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Domain\ValueObject\StringValueObject;

final class PushTo extends StringValueObject
{
        private const TOPICS = [
            'es' => 'news_es',
            'br' => 'news_br',
            'en' => 'news_en',
            'de' => 'news_de',
            'fr' => 'news_fr',
            'gr' => 'news_gr',
            'it' => 'news_it',
            'nl' => 'news_nl',
            'th' => 'news_th',
            'tr' => 'news_tr',
            'ru' => 'news_ru',
            'ko' => 'news_ko',
            'ja' => 'news_ja',
            'id' => 'news_id',
            'ro' => 'news_ro',
            'pl' => 'news_pl',
            'vn' => 'news_vn'
        ];

    public static function localeTopic(LocaleValueObject $locale): self
    {
        return new self(self::TOPICS[$locale->value()]);
    }

    public function isTopic(): bool
    {
        return in_array($this->value, self::TOPICS, true);
    }
}
