<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain;

interface PushSenderClient
{
    public function send(Push $push): bool;
}
