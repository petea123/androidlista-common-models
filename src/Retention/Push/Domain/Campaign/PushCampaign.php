<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain\Campaign;

use App\Retention\Push\Domain\Push;
use App\Retention\Push\Domain\PushId;
use App\Retention\Push\Domain\PushLanguage;
use App\Retention\Push\Domain\PushTo;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\ValueObject\DateValueObject;
use DateTimeImmutable;

final class PushCampaign extends Push
{

    /**
     * @var CampaignId
     */
    private $campaignId;

    public function __construct(
        PushId $id,
        CampaignId $campaignId,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        string $href,
        PushTo $topic,
        PushLanguage $language,
        DateTimeImmutable $sentTo,
        string $type
    ) {
        parent::__construct($id, $title, $description, $icon, $horizontalImg, $href, $topic, $language, $sentTo, $type);
        $this->campaignId = $campaignId;
    }

    public static function create(
        PushId $id,
        CampaignId $campaignId,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        string $href,
        PushTo $topic,
        PushLanguage $language,
        string $type
    ) : self {
        return new self($id, $campaignId, $title, $description, $icon, $horizontalImg, $href, $topic, $language, DateValueObject::now()->value(), $type);
    }

    public function send()
    {
        $this->record(new PushCampaignWasSended(
            $this->id()->value(),
            $this->campaignId->value(),
            $this->title(),
            $this->description(),
            $this->icon(),
            $this->horizontalImg(),
            $this->to()->value(),
            $this->language()->value(),
            $this->href()
        ));
    }
}
