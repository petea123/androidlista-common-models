<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain\Campaign;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class PushCampaignWasSended extends DomainEvent
{

    /**
     * @var string
     */
    private $campaignId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var string
     */
    private $to;
    /**
     * @var string
     */
    private $language;
    /**
     * @var string
     */
    private $href;

    public function __construct(
        string $aggregateId,
        string $campaignId,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        string $to,
        string $language,
        string $href,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->campaignId = $campaignId;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->horizontalImg = $horizontalImg;
        $this->to = $to;
        $this->language = $language;
        $this->href = $href;
    }

    public static function eventName(): string
    {
        return 'PushCampaignWasSended';
    }

    public static function stream(): string
    {
        return PushCampaign::class;
    }

    public function toPrimitives(): array
    {
        return [
            'campaignId' => $this->campaignId,
            'title' => $this->title,
            'description' => $this->description,
            'icon' => $this->icon,
            'horizontalImg' => $this->horizontalImg,
            'to' => $this->to,
            'language' => $this->language,
            'href' => $this->href
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self(
            $aggregateId,
            $body['campaignId'],
            $body['title'],
            $body['description'],
            $body['icon'],
            $body['horizontalImg'],
            $body['to'],
            $body['language'],
            $body['href'],
            $aggregateVersion,
            $eventId,
            $occurredOn
        );
    }

    /**
     * @return string
     */
    public function campaignId(): string
    {
        return $this->campaignId;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    /**
     * @return string
     */
    public function to(): string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }
}
