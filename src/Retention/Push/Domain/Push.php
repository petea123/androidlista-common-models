<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;
use DateTimeImmutable;

abstract class Push extends AggregateRoot
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var string
     */
    private $href;
    /**
     * @var PushId
     */
    private $id;
    /**
     * @var PushTo
     */
    private $to;
    /**
     * @var PushLanguage
     */
    private $language;
    /**
     * @var DateTimeImmutable
     */
    private $sentAt;
    /**
     * @var string
     */
    private $type;

    public function __construct(
        PushId $id,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        string $href,
        PushTo $to,
        PushLanguage $language,
        DateTimeImmutable $sentAt,
        string $type
    )
    {
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->horizontalImg = $horizontalImg;
        $this->href = $href;
        $this->id = $id;
        $this->to = $to;
        $this->language = $language;
        $this->sentAt = $sentAt;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return PushId
     */
    public function id(): PushId
    {
        return $this->id;
    }

    /**
     * @return PushTo
     */
    public function to(): PushTo
    {
        return $this->to;
    }

    abstract public function send();

    /**
     * @return PushLanguage
     */
    public function language(): PushLanguage
    {
        return $this->language;
    }

    public function isSendedToTopic(): bool
    {
        return $this->to->isTopic();
    }

    /**
     * @return DateTimeImmutable
     */
    public function sentAt(): DateTimeImmutable
    {
        return $this->sentAt;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }
}
