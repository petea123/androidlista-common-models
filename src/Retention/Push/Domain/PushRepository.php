<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain;

use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Criteria\Criteria;

interface PushRepository
{
    public function save(Push $push): void;

    public function searchCampaign(CampaignId $id): ?Push;

    public function matching(Criteria $criteria): array;
}
