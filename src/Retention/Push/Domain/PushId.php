<?php

declare(strict_types=1);

namespace App\Retention\Push\Domain;

use App\Shared\Domain\ValueObject\Uuid;

final class PushId extends Uuid
{

}
