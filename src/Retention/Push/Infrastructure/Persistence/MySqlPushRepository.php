<?php

declare(strict_types=1);

namespace App\Retention\Push\Infrastructure\Persistence;

use App\Retention\Push\Domain\Campaign\PushCampaign;
use App\Retention\Push\Domain\Push;
use App\Retention\Push\Domain\PushRepository;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlPushRepository extends DoctrineRepository implements PushRepository
{

    public function save(Push $push): void
    {
        $this->persist($push);
        $this->entityManager()->flush($push);
    }

    public function searchCampaign(CampaignId $id): ?Push
    {
        return $this->repository(PushCampaign::class)->findOneBy(['campaignId' => $id]);
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        $qb = $this->queryBuilder()
            ->select('c')
            ->from(Push::class, 'c')
            ->addCriteria($doctrineCriteria)
            ->getQuery();

        return $qb->execute();
    }
}
