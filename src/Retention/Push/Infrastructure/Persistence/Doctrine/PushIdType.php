<?php

declare(strict_types=1);

namespace App\Retention\Push\Infrastructure\Persistence\Doctrine;

use App\Retention\Push\Domain\PushId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class PushIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'push_id';
    }

    protected function typeClassName(): string
    {
        return PushId::class;
    }
}
