<?php

declare(strict_types=1);

namespace App\Retention\Push\Infrastructure\Persistence\Doctrine;

use App\Retention\Push\Domain\PushLanguage;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class PushLanguageType extends StringType implements DoctrineCustomType
{

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new PushLanguage($value);
    }

    /** @var PushLanguage $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->value();
    }

    public static function customTypeName(): string
    {
        return 'push_language';
    }
}
