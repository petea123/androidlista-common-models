<?php

declare(strict_types=1);

namespace App\Retention\Push\Infrastructure\Firebase;

use App\Retention\Push\Domain\Push;
use App\Retention\Push\Domain\PushSenderClient;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;

final class FirebasePushSenderClient implements PushSenderClient
{

    /** @var Messaging  */
    private static $client;

    /**
     * @return Messaging
     */
    private function client(): Messaging
    {
        if (!self::$client) {
            $client = (new Factory())->createMessaging();
            self::$client = $client;
        }

        return self::$client;
    }

    public function send(Push $push): bool
    {
        $cdnUrl = $this->getCdnAmpUrl($push->href());

        $message = CloudMessage::withTarget($push->isSendedToTopic() ? 'topic' : 'token', $push->to()->value())
            ->withNotification([
                'title' => $push->title(),
                'body' => $push->description(),
                'image' => $push->horizontalImg()
            ])
            ->withData([
                "title" => $push->title(),
                "body" => $push->description(),
                "click_action" => $cdnUrl,
                "icon" => $push->icon(),
                "image" => $push->horizontalImg(),
                "requireInteraction" => "true",
                "locale" => $push->language()->value(),
                "tag" => $push->type()
            ])
            ->withWebPushConfig([
                'notification' => [
                    'icon' => $push->icon(),
                ],
                'headers' => [
                    'Urgency' => 'high'
                ],
                'fcm_options' => [
                    'link' => $cdnUrl
                ]
            ]);

        try {
            $this->client()->validate($message);
            $response = $this->client()->send($message);
        } catch (InvalidMessage $e) {
            return false;
        } catch (MessagingException $e) {
            return false;
        } catch (FirebaseException $e) {
            return false;
        }

        return true;
    }

    private function getCdnAmpUrl($url)
    {
        $explode = explode('/', str_replace('https://', '', $url));
        $subDomain = str_replace('.', '-', str_replace('-', '--', $explode[0]));
        return 'https://'.$subDomain.'.cdn.ampproject.org/c/s/'.implode('/', $explode);
    }
}
