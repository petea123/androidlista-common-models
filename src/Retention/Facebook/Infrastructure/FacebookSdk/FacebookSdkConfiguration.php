<?php

declare(strict_types=1);

namespace App\Retention\Facebook\Infrastructure\FacebookSdk;

final class FacebookSdkConfiguration
{

    /**
     * @var string
     */
    private $appId;
    /**
     * @var string
     */
    private $appSecret;
    /**
     * @var string
     */
    private $graphVersion;

    /**
     * @var string
     */
    private $accessToken;

    private const CONFIGURATION = [
        'appId' => '835085239859354',
        'appSecret' => 'f25aa279b9d48dff87ad05195639156b',
        'graphVersion' => 'v5.0',
        'token' => [
            'en' => 'EAAL3gW8UOJoBAMZCuxpnn6jZAZBaNtr6OXGupzTZB7WvCmFDEWnSbeUtvLGmQRFNv8VpA4zo4Lgi2prZANk0WbfBTCdEUekXm1YeZAalvelvPZBKsY76MNsZBjXOQ9aTlRMpGNbd4baFVlgIg5eZCBTruMuXZAo83yjzzf8dnAbjVgcwZDZD',
            'vn' => 'EAAL3gW8UOJoBANnmwBcZCZCBYyx271QTlHJH84gWF8wknZAaDYBlFKtXLwHAtdZCTx9KNpFOo0ZBvZBulrYbcxbstgBzzKVVWBJBg5ZB1IqLbFwu6NCnp3wmjT5uz3atFKFqOmK8cdx6zzZCIbwddGBbngW9QnzcILYsSFyRyFuGdwZDZD',
            'tr' => 'EAAL3gW8UOJoBANRkk7dPnp5ZCTwS0ax8ZBVzgpBDDW7d57VxAX8daGSLssGtpzmfCDRe4Gvs7A2LzOQeF3EjbMHDSYHK7RZAawooYKm2rFQYRSYnZBNmLg7rfdd7jBwbFWZB71mwBml2GdkOwS56z9F3MYrFR5QU0G7z9rbZC6YQZDZD',
            'id' => 'EAAL3gW8UOJoBAGvQzOcprHtZCZAjJqMl5e0xGuglhulysf1BHK3FAI5XJyAGjmPwh9v3ZAVL3CWc9H1O3ajYhgz6YILNy0hsNZBfm0qZCDbIwohuafwAE8P6OrLTFA8moROmxBZBf848s0G5OAfIgsX0LtvnoAMoh7DB9BZATgnnwZDZD',
            'es' => 'EAAL3gW8UOJoBACfrUs87izbniMWfsDFymjsnGXITvhZCZBhZCJV22VExroqBvM4fToc6piwYA6lsKd48LfSfS0BbEaP8pN0ZBMhEqcRJuOL8vBw2lZCXi7OuYo7cO1bbfaNeOESWZAnpmGAgAYqNWZCZAdFZCvXlfuzHWbdjxmYlyZAwZDZD',
            'de' => 'EAAL3gW8UOJoBAHxNInCZCOxcwuDvojRXkvF9e5Q3L1HTO7zAKG1XXXJD7VYez6KcSchyV4jNUJYwybzwwMcGeSr27ofN43rxvz7uKaRcqxrzMj2xoGKJmRiwN43br0MNS0yZCihyVKX031n2LZC07nOeZCZBqBW4WU579wKkaoQZDZD',
            'pl' => 'EAAL3gW8UOJoBAKncxRir8lkH42ZBzfF7wxf32lFlujnz5nuxG6ERXxuvopKCwDNaiE75GzuMOUPgTYinH6sn7nUnTt5VZB1HZBZBwrObe3VbCE4TrEnZBLXbOiKok8OEfNkpUOyVAapiZAWM8gD9G6tMLKZCvF1dFAv2ug6UiLrZBgZDZD',
            'ro' => 'EAAL3gW8UOJoBABItnKO3NsTCAOBIlyjf90yVa6bEAV8CBtNaGuryG0tXYCbnSDrYu7V2567dPw036XiIQztZA1ePC4sCZBKPc6vV8hlTRlVJZBX9rBqlDksgPBUYnLKKG01q5XuRaUUOeP58VwzluZClSUAasAglSBzNTIZABBAZDZD',
            'it' => 'EAAL3gW8UOJoBAL66Cq7LoZBNPktXtiTLNmRl9naS8QN07xIHJi58uZAoQr4lBGuzDNZAA6Mz8rG7oZCW4JaOWZC9m6N1vR2ZAe4zRWps7tHezbveoFHCfewjiDOB23Cas9FdEeDZCM7xHoNC03hs6ZCoUZBre1CHku6EW1CDY9qi5BgZDZD',
            'gr' => 'EAAL3gW8UOJoBAOOIBWqTaJZCW6JL3Vf7gBMLdQqI1qCmy7S7lTNgsfh8giZBNf3GOH57UxyiinjD2ngoa2FlZANs55lrF1UY6rl6ihbLZBux8eL0cbSq1GnTPoxgcNS4z4zUOAuDSYwEl6SCNmEYeskRZA1sU8CgxyxckXMj6gQZDZD',
            'ja' => 'EAAL3gW8UOJoBAMTnuf7cjsB5yvBAezFksXDYGfSDfx2JhARX1BPhAkKNPZAdrOuZBvIqKLBxzajojFLaQZBSUDmorAp7xf1BkqhZBnaERRo0uakBF4rWCyXvVbd7dEUUYYLN1X2JjINPurYYxBzYBGH0kZBTtTDdi082XyASDHgZDZD',
            'fr' => 'EAAL3gW8UOJoBADJJx01VZCOZAdwDyw7xf8gm1uG4lggLiVyjRSHYgQSvW4SiM0F767qvWde4CwacD9PudZApAoDVZALJZB3SCLZBftZBISoHdvZByJUzREZCCpCGMump8ZBOfyftfn300OnoBL8uZB0I0eYPDVkotOh29QWbfbEQJG2wwZDZD',
            'ru' => 'EAAL3gW8UOJoBAKZAZCPTaILpqD0HROpQdQo0oOb9j0vgPE6OU1MGLOAfVDLV3E6ekYyHcKhLet1qwFJtK2LGcyNx063avx7zco8VPgDdPNrQ2n3BZAR4X53WBRFrpMpSArs5Ey9CZCxB3dX3dSz4evRsOE77sQuEYYotn04ONwZDZD',
            'ko' => 'EAAL3gW8UOJoBAKDSLIbCZBOxQP3NkDenKvXx9vKZBO4DOZCwON4EEqxTpgh7e7QZCnLSCZC0WGTSjZC5xPLn0HfIQIyKpzQN71dHZAWlujFjQYsO9siT3w1bLSURFUe3fzWmPZCXW4VZCCPXm5bZA5lTtfq3CgP7PfVnUQVZCkMgwZBkAwZDZD',
            'th' => 'EAAL3gW8UOJoBAEqlftRye9F9bCeeFra4d8eDqTgre1Iz7VUdFk4grYIdlsNqlEXusvMRpd8gQg19mudETzkpDotnxmjULYg9zLq513bEieYMQIbeYOLdr83i2LZCltM14lSzZB8Ly4IAcXoeQmKQ7ZAIbZAsgz754pvrm3XQxgZDZD',
            'nl' => 'EAAL3gW8UOJoBABXlplZB0XulKKWa86LmOJHhBGOVxbxYW9ZCZBxzQGRfz8woIbnpVTkZA5ZCVhYLU2Ya7Y27GyrkGu9EnKly0sarrKemZB1EOR2dTcjw0tAMShEwS1ivLONHl6MaoMG5u37tfyrZCUc0mjwdgsh0KmnBchWDUC10QZDZD',
            'br' => 'EAAL3gW8UOJoBAAZAmrpMnsVVEuxoDgnBTi1onTU1a0GeRtIC0LXja6cWLlLUxKbXDytkc4HQtzS7sFxRRBxGJIDV1LkjCnSZCG5ZB0PhY8nZC0EjK34cFObsa0u8YMPzrjXbEDeYxwHnEZAZClqqVpiwtr2YFasBnWq6lC4S1aRAZDZD'
        ]
    ];

    public function __construct(string $appId, string $appSecret, string $graphVersion, string $accessToken)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->graphVersion = $graphVersion;
        $this->accessToken = $accessToken;
    }


    public static function fromLanguage(string $language): self
    {
        if (self::CONFIGURATION['token'][$language]) {
            return new self(self::CONFIGURATION['appId'], self::CONFIGURATION['appSecret'], self::CONFIGURATION['graphVersion'], self::CONFIGURATION['token'][$language]);
        }

        throw new \DomainException('No configuration specified for Facebook Sdk');
    }

    /**
     * @return string
     */
    public function appId(): string
    {
        return $this->appId;
    }

    /**
     * @return string
     */
    public function appSecret(): string
    {
        return $this->appSecret;
    }

    /**
     * @return string
     */
    public function graphVersion(): string
    {
        return $this->graphVersion;
    }

    /**
     * @return string
     */
    public function accessToken(): string
    {
        return $this->accessToken;
    }
}
