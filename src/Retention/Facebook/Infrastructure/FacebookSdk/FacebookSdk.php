<?php

declare(strict_types=1);

namespace App\Retention\Facebook\Infrastructure\FacebookSdk;

use App\Retention\Facebook\Domain\FacebookClient;
use App\Retention\Facebook\Domain\FacebookPost;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use GuzzleHttp\Client;
use function Lambdish\Phunctional\map;

final class FacebookSdk implements FacebookClient
{
    /** @var array  */
    private static $client;

    /**
     * @return Facebook
     */
    private function client(string $language): Facebook
    {
        if (!self::$client[$language]) {
            $client = new Client;
            $fbConf = FacebookSdkConfiguration::fromLanguage($language);
            self::$client[$language] = new Facebook([
                'app_id' => $fbConf->appId(),
                'app_secret' => $fbConf->appSecret(),
                'graph_api_version' => $fbConf->graphVersion(),
                'http_client_handler' => new Guzzle6HttpClientFacebook($client),
                'default_access_token' => $fbConf->accessToken()
            ]);
        }

        return self::$client[$language];
    }

    public function post(FacebookPost $post): bool
    {
        $client = $this->client($post->language());

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $client->post('/me/feed', [
                'message' => sprintf('%s %s', $post->title(), implode(' ',$this->tags($post->tags()))),
                'link' => $post->href()
            ]);
        } catch(FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            return false;
        } catch(FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            return false;
        }

        return true;
    }

    private function tags(array $tags, int $limit = 2): array
    {
        $tags = array_chunk($tags, $limit);

        return map(function ($tag){
            return "#".str_replace("-","",strtolower($tag));
        }, $tags[0]);
    }
}
