<?php


namespace App\Retention\Facebook\Domain;


interface FacebookClient
{
    public function post(FacebookPost $post): bool;
}
