<?php

declare(strict_types=1);

namespace App\Retention\Facebook\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;

final class FacebookPost extends AggregateRoot
{

    /**
     * @var string
     */
    private $language;
    /**
     * @var string
     */
    private $href;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var array
     */
    private $tags;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var string
     */
    private $id;

    public function __construct(
        string $id,
        string $language,
        string $title,
        string $description,
        array $tags,
        string $horizontalImg,
        string $href
    ) {
        $this->language = $language;
        $this->title = $title;
        $this->description = $description;
        $this->tags = $tags;
        $this->horizontalImg = $horizontalImg;
        $this->href = $href;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function tags(): array
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    public function post(): void
    {
        $this->record(new FacebookPostWasPosted(
            $this->id,
            $this->title,
            $this->description,
            $this->href,
            $this->language
        ));
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
