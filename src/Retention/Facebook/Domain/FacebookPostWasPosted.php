<?php

declare(strict_types=1);

namespace App\Retention\Facebook\Domain;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class FacebookPostWasPosted extends DomainEvent
{

    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $language;
    /**
     * @var string
     */
    private $href;

    public function __construct(
        string $aggregateId,
        string $title,
        string $description,
        string $language,
        string $href,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->title = $title;
        $this->description = $description;
        $this->language = $language;
        $this->href = $href;
    }

    public static function eventName(): string
    {
        return 'FacebookPostWasPosted';
    }

    public static function stream(): string
    {
        return FacebookPost::class;
    }

    public function toPrimitives(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'language' => $this->language,
            'href' => $this->href
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['title'], $body['description'], $body['language'], $body['href'], $aggregateVersion, $eventId, $occurredOn);
    }
}
