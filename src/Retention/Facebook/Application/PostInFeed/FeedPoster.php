<?php

declare(strict_types=1);

namespace App\Retention\Facebook\Application\PostInFeed;

use App\Retention\Facebook\Domain\FacebookClient;
use App\Retention\Facebook\Domain\FacebookPost;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Shared\Domain\UuidGenerator;

final class FeedPoster
{
    /**
     * @var FacebookClient
     */
    private $client;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;
    /**
     * @var UuidGenerator
     */
    private $generator;

    public function __construct(FacebookClient $client, DomainEventPublisher $publisher, UuidGenerator $generator)
    {
        $this->client = $client;
        $this->publisher = $publisher;
        $this->generator = $generator;
    }

    public function __invoke(string $language, string $title, string $description, array $tags, string $horizontalImg, string $href)
    {
        $post = new FacebookPost($this->generator->next(), $language, $title, $description, $tags, $horizontalImg, $href);

        if ($this->client->post($post)) {
            $post->post();
            $this->publisher->publish(...$post->pullDomainEvents());
        }
    }
}
