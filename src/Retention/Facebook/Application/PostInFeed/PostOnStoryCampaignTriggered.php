<?php

declare(strict_types=1);

namespace App\Retention\Facebook\Application\PostInFeed;

use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaignTriggered;
use App\Retention\Shared\Campaign\Domain\CampaignChannel;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use function Lambdish\Phunctional\apply;

final class PostOnStoryCampaignTriggered implements DomainEventSubscriber
{

    /**
     * @var FeedPoster
     */
    private $poster;

    public function __construct(FeedPoster $poster)
    {
        $this->poster = $poster;
    }

    public function __invoke(StoryCampaignTriggered $event)
    {
        $channels = CampaignChannels::fromValues($event->channels());
        if ($channels->hasChannel(CampaignChannel::facebook())) {
            apply($this->poster, [
                $event->language(),
                $event->title(),
                $event->description(),
                $event->tags(),
                $event->horizontalImg(),
                $event->href()
            ]);
        }
    }

    public static function subscribedTo(): array
    {
        return [StoryCampaignTriggered::class];
    }
}
