<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Application\Unsubscribe;

use App\Shared\Domain\Bus\Command\Command;

final class UnsubscribeEmailCommand extends Command
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $locale;

    public function __construct(string $email, string $locale)
    {
        $this->email = $email;
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function locale(): string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function email(): string
    {
        return $this->email;
    }
}
