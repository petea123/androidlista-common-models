<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Application\Unsubscribe;

use App\Retention\Newsletter\Domain\NewsletterEmail;
use App\Retention\Newsletter\Domain\NewsletterLocale;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class UnsubscribeEmailCommandHandler implements CommandHandler
{
    /**
     * @var Unsubscribing
     */
    private $unsubscriber;

    public function __construct(Unsubscribing $unsubscriber)
    {
        $this->unsubscriber = $unsubscriber;
    }

    public function __invoke(UnsubscribeEmailCommand $command): void
    {
        $email = new NewsletterEmail($command->email());
        $locale = new NewsletterLocale($command->locale());

        apply($this->unsubscriber, [$email, $locale]);
    }
}
