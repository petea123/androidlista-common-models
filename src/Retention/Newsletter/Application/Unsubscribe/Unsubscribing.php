<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Application\Unsubscribe;

use App\Retention\Newsletter\Domain\NewsletterEmail;
use App\Retention\Newsletter\Domain\NewsletterLocale;
use App\Retention\Newsletter\Domain\NewsletterRepository;
use App\Shared\Domain\Bus\Event\DomainEventPublisher;

final class Unsubscribing
{
    /**
     * @var NewsletterRepository
     */
    private $repository;
    /**
     * @var DomainEventPublisher
     */
    private $publisher;

    public function __construct(NewsletterRepository $repository, DomainEventPublisher $publisher)
    {
        $this->repository = $repository;
        $this->publisher = $publisher;
    }

    public function __invoke(NewsletterEmail $email, NewsletterLocale $locale): void
    {
        $newsletter = $this->repository->searchByEmailAndLocale($email, $locale);

        if (null === $newsletter || !$newsletter->isActive()) {
            return;
        }

        $newsletter->unsubscribe();
        $this->repository->save($newsletter);
        $this->publisher->record(...$newsletter->pullDomainEvents());
    }
}
