<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Domain;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class NewsletterWasUnsubscribed extends DomainEvent
{

    /**
     * @var string
     */
    private $email;

    public function __construct(
        string $aggregateId,
        string $email,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);
        $this->email = $email;
    }

    public static function eventName(): string
    {
        return 'NewsletterWasUnsubscribed';
    }

    public static function stream(): string
    {
        return Newsletter::class;
    }

    public function toPrimitives(): array
    {
        return [
            'email' => $this->email
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['email'], $aggregateVersion, $eventId, $occurredOn);
    }
}
