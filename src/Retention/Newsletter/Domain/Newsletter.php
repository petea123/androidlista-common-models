<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;

final class Newsletter extends AggregateRoot
{

    /**
     * @var NewsletterEmail
     */
    private $email;
    /**
     * @var int
     */
    private $active;
    /**
     * @var NewsletterLocale
     */
    private $locale;
    /**
     * @var NewsletterId
     */
    private $id;

    public function __construct(NewsletterId $id, NewsletterEmail $email, bool $active, NewsletterLocale $locale)
    {
        $this->email = $email;
        $this->active = $active;
        $this->locale = $locale;
        $this->id = $id;
    }

    public function unsubscribe()
    {
        $this->active = 0;
        $this->record(new NewsletterWasUnsubscribed(
            (string)$this->id->value(),
            $this->email->value()
        ));
    }

    public function isActive(): int
    {
        return $this->active;
    }

}
