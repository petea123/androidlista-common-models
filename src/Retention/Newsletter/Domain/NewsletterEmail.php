<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Domain;

use App\Shared\Domain\ValueObject\EmailValueObject;

final class NewsletterEmail extends EmailValueObject
{

}
