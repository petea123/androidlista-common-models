<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Domain;

use App\Shared\Domain\ValueObject\LocaleValueObject;

final class NewsletterLocale extends LocaleValueObject
{

}
