<?php


namespace App\Retention\Newsletter\Domain;

interface NewsletterRepository
{
    public function save(Newsletter $newsletter): void;
    public function searchByEmailAndLocale(NewsletterEmail $email, NewsletterLocale $locale): ?Newsletter;
}
