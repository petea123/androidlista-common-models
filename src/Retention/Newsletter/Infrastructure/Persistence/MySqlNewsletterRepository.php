<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Infrastructure\Persistence;

use App\Retention\Newsletter\Domain\Newsletter;
use App\Retention\Newsletter\Domain\NewsletterEmail;
use App\Retention\Newsletter\Domain\NewsletterLocale;
use App\Retention\Newsletter\Domain\NewsletterRepository;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlNewsletterRepository extends DoctrineRepository implements NewsletterRepository
{

    public function save(Newsletter $newsletter): void
    {
        $this->persist($newsletter);
    }

    public function searchByEmailAndLocale(NewsletterEmail $email, NewsletterLocale $locale): ?Newsletter
    {
        return $this->repository(Newsletter::class)->findOneBy(['email.value' => $email->value(), 'locale' => $locale]);
    }
}
