<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Infrastructure\Persistence\Doctrine;

use App\Retention\Newsletter\Domain\NewsletterLocale;
use App\Shared\Domain\ValueObject\LocaleValueObject;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class NewsletterLocaleType extends StringType implements DoctrineCustomType
{

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new NewsletterLocale($value);
    }

    /** @var NewsletterLocale $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof LocaleValueObject ? $value->value() : (string)$value;
    }

    public static function customTypeName(): string
    {
        return 'newsletter_locale';
    }
}
