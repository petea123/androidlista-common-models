<?php

declare(strict_types=1);

namespace App\Retention\Newsletter\Infrastructure\Persistence\Doctrine;

use App\Retention\Newsletter\Domain\NewsletterId;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

final class NewsletterIdType extends IntegerType implements DoctrineCustomType
{
    public static function customTypeName(): string
    {
        return 'newsletter_id';
    }

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new NewsletterId((int)$value);
    }

    /** @var NewsletterId $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (is_string($value) || is_int($value)) {
            return (new NewsletterId((int)$value))->value();
        }

        return $value->value();
    }
}
