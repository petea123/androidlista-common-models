<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Infrastructure\Persistence;

use App\Retention\Campaign\Domain\Campaign;
use App\Retention\Campaign\Domain\CampaignLanguage;
use App\Retention\Campaign\Domain\CampaignRepository;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaign;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use DateTimeImmutable;
use DateTimeZone;

final class MySqlCampaignRepository extends DoctrineRepository implements CampaignRepository
{

    public function save(Campaign $campaign): void
    {
        $this->persist($campaign);
        $this->entityManager()->flush($campaign);
    }

    public function search(CampaignId $id): ?Campaign
    {
        $qb = $this->queryBuilder()
            ->select('c')
            ->from(Campaign::class, 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id->value())
            ->getQuery();

        return $qb->execute()[0] ?? null;
    }

    public function mustTrigger(): array
    {
        return $this->queryBuilder()
            ->select('c')
            ->from(Campaign::class, 'c')
            ->where('c.triggerOn.value < :now')
            ->andWhere('c.triggered = false')
            ->setParameter('now', (new DateTimeImmutable('now',new DateTimeZone('UTC')))->format('Y-m-d H:i:s'))
            ->getQuery()
            ->execute();
    }

    public function delete(Campaign $campaign): void
    {
        $this->remove($campaign);
        $this->entityManager()->flush($campaign);
    }

    public function lastScheduledStoryCampaignForLanguage(CampaignLanguage $language): ?Campaign
    {
        return $this->repository(StoryCampaign::class)->findOneBy(['language' => $language], ['triggerOn.value' => 'DESC']);
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        $qb = $this->queryBuilder()
            ->select('c')
            ->from(Campaign::class, 'c')
            ->addCriteria($doctrineCriteria)
            ->getQuery();

        return $qb->execute();
    }
}
