<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Infrastructure\Persistence\Doctrine;

use App\Retention\Campaign\Domain\CampaignLanguage;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

final class CampaignLanguageType extends StringType implements DoctrineCustomType
{

    public function getName()
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new CampaignLanguage($value);
    }

    /** @var CampaignLanguage $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (is_string($value)) {
            return (new CampaignLanguage($value))->value();
        }

        return $value->value();
    }

    public static function customTypeName(): string
    {
        return 'campaign_language';
    }
}
