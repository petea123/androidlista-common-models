<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Infrastructure\Persistence\Doctrine;

use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class CampaignIdType extends UuidType
{

    public static function customTypeName(): string
    {
        return 'campaign_id';
    }

    protected function typeClassName(): string
    {
        return CampaignId::class;
    }
}
