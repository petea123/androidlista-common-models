<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Infrastructure\Persistence\Doctrine;

use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Shared\Campaign\Domain\CampaignChannel;
use App\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use function Lambdish\Phunctional\map;

final class CampaignChannelsType extends JsonType implements DoctrineCustomType
{

    public function getName(): string
    {
        return self::customTypeName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $scalars = parent::convertToPHPValue($value, $platform);
        return new CampaignChannels(map($this->toCampaignChannel(), $scalars));
    }

    /** @var CampaignChannel[] $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return parent::convertToDatabaseValue(map($this->values(), $value), $platform);
    }

    public static function customTypeName(): string
    {
        return 'campaign_channels';
    }

    private function values()
    {
        return static function (CampaignChannel $channel) {
            return $channel->value();
        };
    }

    private function toCampaignChannel()
    {
        return static function (string $value) {
            return CampaignChannel::fromString($value);
        };
    }
}
