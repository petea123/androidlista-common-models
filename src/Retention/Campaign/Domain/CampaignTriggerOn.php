<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain;

use App\Shared\Domain\ValueObject\DateValueObject;

class CampaignTriggerOn extends DateValueObject
{
}
