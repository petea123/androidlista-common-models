<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain;

use App\Retention\Shared\Campaign\Domain\CampaignChannel;
use App\Shared\Domain\Collection;
use function Lambdish\Phunctional\map;
use function Lambdish\Phunctional\search;

final class CampaignChannels extends Collection
{
    protected function type(): string
    {
        return CampaignChannel::class;
    }

    public static function fromValues(array $items): self
    {
        return new self(map(function ($channel) {
            return CampaignChannel::fromString($channel);
        }, $items));
    }

    public function toValues(): array
    {
        return map(function(CampaignChannel $channel) {
            return $channel->value();
        }, $this->getIterator());
    }

    public function hasChannel(CampaignChannel $channel): bool
    {
        return null != search(function (CampaignChannel $other) use ($channel) {
            return $channel->equals($other);
        }, $this->getIterator());
    }
}
