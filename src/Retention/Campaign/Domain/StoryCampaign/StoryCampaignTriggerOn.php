<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\StoryCampaign;

use App\Retention\Campaign\Domain\CampaignTriggerOn;
use DateInterval;
use DateTimeInterface;

final class StoryCampaignTriggerOn extends CampaignTriggerOn
{
    public static function forTomorrow(DateTimeInterface $date): self
    {
        $date = $date->add(DateInterval::createFromDateString('1 day'));

        return new self($date);
    }
}
