<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\StoryCampaign;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Retention\Campaign\Domain\Campaign;
use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Shared\Campaign\Domain\CampaignId;

final class StoryCampaign extends Campaign
{

    /**
     * @var StoryId
     */
    private $storyId;

    public function __construct(
        CampaignId $id,
        StoryId $storyId,
        StoryCampaignHref $href,
        string $title,
        string $description,
        StoryCampaignLanguage $language,
        StoryCampaignTriggerOn $triggerOn,
        CampaignChannels $channels,
        array $tags,
        string $icon,
        string $horizontalImg,
        bool $triggered
    )
    {
        parent::__construct($id, $language, $triggerOn, $channels,$title, $description, $icon, $horizontalImg, $tags, $href, $triggered);
        $this->storyId = $storyId;
    }

    public static function create(
        CampaignId $id,
        StoryId $storyId,
        StoryCampaignHref $href,
        string $title,
        string $description,
        StoryCampaignLanguage $language,
        StoryCampaignTriggerOn $triggerOn,
        CampaignChannels $channels,
        array $tags,
        string $icon,
        string $horizontalImg
    )
    {
        return new self($id, $storyId, $href, $title, $description, $language, $triggerOn, $channels, $tags, $icon, $horizontalImg, false);
    }

    function trigger(): void
    {
        $this->triggered = true;
        $this->record(new StoryCampaignTriggered(
            $this->id->value(),
            $this->storyId->value(),
            $this->language->value(),
            $this->title,
            $this->description,
            $this->href->value(),
            $this->channels->toValues(),
            $this->icon,
            $this->horizontalImg,
            $this->tags
        ));
    }
}
