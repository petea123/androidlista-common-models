<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\StoryCampaign;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Retention\Campaign\Domain\CampaignHref;

final class StoryCampaignHref extends CampaignHref
{
    public static function fromIdAndLanguage(StoryId $storyId, StoryCampaignLanguage $language)
    {
        return new self($language->domain().'/stories/'.$storyId->value().'/');
    }
}
