<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\StoryCampaign;

use App\Shared\Domain\Bus\Event\DomainEvent;

final class StoryCampaignTriggered extends DomainEvent
{

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $href;
    /**
     * @var string
     */
    private $storyId;
    /**
     * @var array
     */
    private $channels;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var array
     */
    private $tags;

    public function __construct(
        string $aggregateId,
        string $storyId,
        string $language,
        string $title,
        string $description,
        string $href,
        array $channels,
        string $icon,
        string $horizontalImg,
        array $tags,
        int $aggregateVersion = null,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($aggregateId, $aggregateVersion, $eventId, $occurredOn);

        $this->language = $language;
        $this->href = $href;
        $this->storyId = $storyId;
        $this->channels = $channels;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->horizontalImg = $horizontalImg;
        $this->tags = $tags;
    }

    public static function stream(): string
    {
        return StoryCampaign::class;
    }

    public static function eventName(): string
    {
        return 'StoryCampaignTriggered';
    }

    public function toPrimitives(): array
    {
        return [
            'storyId' => $this->storyId,
            'language' => $this->language,
            'href' => $this->href,
            'title' => $this->title,
            'description' => $this->description,
            'channels' => $this->channels,
            'icon' => $this->icon,
            'horizontalImg' =>$this->horizontalImg,
            'tags' => $this->tags
        ];
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        int $aggregateVersion,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self(
            $aggregateId,
            $body['storyId'],
            $body['language'],
            $body['title'],
            $body['description'],
            $body['href'],
            $body['channels'],
            $body['icon'],
            $body['horizontalImg'],
            $body['tags'],
            $aggregateVersion,
            $eventId,
            $occurredOn
        );
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function storyId(): string
    {
        return $this->storyId;
    }

    /**
     * @return array
     */
    public function channels(): array
    {
        return $this->channels;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    /**
     * @return array
     */
    public function tags(): array
    {
        return $this->tags;
    }
}
