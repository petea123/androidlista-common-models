<?php


namespace App\Retention\Campaign\Domain;


use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Criteria\Criteria;

interface CampaignRepository
{
    public function save(Campaign $campaign): void;

    public function search(CampaignId $id): ?Campaign;

    public function matching(Criteria $criteria): array;

    public function delete(Campaign $campaign): void;

    public function lastScheduledStoryCampaignForLanguage(CampaignLanguage $language): ?Campaign;
}
