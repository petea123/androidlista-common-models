<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\WordpressPostCampaign;

use App\Retention\Campaign\Domain\CampaignTriggerOn;

final class WpPostCampaignTriggerOn extends CampaignTriggerOn
{
    public static function createTriggerFromTimestamp(int $timestamp): self
    {
        return self::createFromTimestamp($timestamp);
    }
}
