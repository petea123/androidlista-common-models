<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\WordpressPostCampaign;

use App\Retention\Campaign\Domain\Campaign;
use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\CampaignHref;
use App\Retention\Shared\Campaign\Domain\CampaignId;

final class WpPostCampaign extends Campaign
{

    public function __construct(
        CampaignId $id,
        WpPostCampaignLanguage $language,
        WpPostCampaignTriggerOn $triggerOn,
        CampaignChannels $channels,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        array $tags,
        CampaignHref $href,
        bool $triggered
    ) {
        parent::__construct($id, $language, $triggerOn, $channels, $title, $description, $icon, $horizontalImg, $tags, $href, $triggered);
    }

    public static function create(
        CampaignId $id,
        WpPostCampaignLanguage $language,
        WpPostCampaignTriggerOn $triggerOn,
        CampaignChannels $channels,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        array $tags,
        CampaignHref $href
    ): self {
        return new self($id, $language, $triggerOn, $channels, $title, $description, $icon, $horizontalImg, $tags, $href, false);
    }

    public function trigger(): void
    {
        $this->triggered = true;
        $this->record(new WpPostCampaignTriggered(
            $this->id->value(),
            $this->language->value(),
            $this->title,
            $this->description,
            $this->channels->toValues(),
            $this->icon,
            $this->horizontalImg,
            $this->tags,
            $this->href->value()
        ));
    }
}


