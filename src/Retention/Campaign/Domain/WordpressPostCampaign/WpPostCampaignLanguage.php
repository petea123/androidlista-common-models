<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain\WordpressPostCampaign;

use App\Retention\Campaign\Domain\CampaignLanguage;

final class WpPostCampaignLanguage extends CampaignLanguage
{
    const LANGUAGE_CODE = [
        1 => "br",
        2 => "es",
        3 => "en",
        4 => "id",
        5 => "fr",
        6 => "it",
        7 => "de",
        8 => "tr",
        9 => "ru",
        10 => "ko",
        11 => "ja",
        12 => "pl",
        13 => "ro",
        14 => "vn",
        15 => "th",
        16 => "nl",
        17 => "gr"
    ];

    public static function fromBlogId(int $blogId): self
    {
        return new self(self::LANGUAGE_CODE[$blogId]);
    }

    protected function throwExceptionForInvalidValue($value)
    {
        // TODO: Implement throwExceptionForInvalidValue() method.
    }
}
