<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Domain;

use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Aggregate\AggregateRoot;

abstract class Campaign extends AggregateRoot
{

    /**
     * @var CampaignId
     */
    protected $id;
    /**
     * @var CampaignTriggerOn
     */
    protected $triggerOn;
    /**
     * @var CampaignLanguage
     */
    protected $language;

    /**
     * @var CampaignChannels
     */
    protected $channels;
    /**
     * @var string
     */
    protected $icon;
    /**
     * @var string
     */
    protected $horizontalImg;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var array
     */
    protected $tags;
    /**
     * @var CampaignHref
     */
    protected $href;
    /**
     * @var bool
     */
    protected $triggered;

    public function __construct(
        CampaignId $id,
        CampaignLanguage $language,
        CampaignTriggerOn $triggerOn,
        CampaignChannels $channels,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        array $tags,
        CampaignHref $href,
        bool $triggered
    ) {
        $this->id = $id;
        $this->triggerOn = $triggerOn;
        $this->language = $language;
        $this->channels = $channels;
        $this->icon = $icon;
        $this->horizontalImg = $horizontalImg;
        $this->title = $title;
        $this->description = $description;
        $this->tags = $tags;
        $this->href = $href;
        $this->triggered = $triggered;
    }

    /**
     * @return CampaignId
     */
    public function id(): CampaignId
    {
        return $this->id;
    }

    /**
     * @return CampaignTriggerOn
     */
    public function triggerOn(): CampaignTriggerOn
    {
        return $this->triggerOn;
    }

    /**
     * @return CampaignLanguage
     */
    public function language(): CampaignLanguage
    {
        return $this->language;
    }

    abstract function trigger(): void;

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return CampaignChannels
     */
    public function channels(): CampaignChannels
    {
        return $this->channels;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return CampaignHref
     */
    public function href(): CampaignHref
    {
        return $this->href;
    }

    /**
     * @return array
     */
    public function tags(): array
    {
        return $this->tags;
    }

    public function triggered(): bool
    {
        return $this->triggered;
    }
}
