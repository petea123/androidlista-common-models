<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\SearchByCriteria;

use App\Retention\Campaign\Application\CampaignResponse;
use App\Retention\Campaign\Application\CampaignsResponse;
use App\Retention\Campaign\Domain\Campaign;
use App\Retention\Campaign\Domain\CampaignRepository;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class CampaignsCriteriaSearcher
{
    /** @var CampaignRepository */
    private $repository;

    public function __construct(CampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(Filters $filters, Order $order, ?int $limit, ?int $offset): CampaignsResponse
    {
        $criteria = new Criteria($filters, $order, $offset, $limit);

        return new CampaignsResponse(...map($this->toResponse(), $this->repository->matching($criteria)));
    }

    private function toResponse(): callable
    {
        return static function (Campaign $campaign) {
            return new CampaignResponse(
                $campaign->id()->value(),
                $campaign->triggerOn()->timestamp(),
                $campaign->title(),
                $campaign->description(),
                $campaign->href()->value(),
                $campaign->channels()->toValues(),
                $campaign->icon(),
                $campaign->horizontalImg(),
                $campaign->tags(),
                $campaign->triggered(),
                get_class($campaign)
            );
        };
    }
}
