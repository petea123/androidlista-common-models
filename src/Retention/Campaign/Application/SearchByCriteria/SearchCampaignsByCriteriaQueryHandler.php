<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\SearchByCriteria;

use App\Retention\Campaign\Application\CampaignsResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchCampaignsByCriteriaQueryHandler implements QueryHandler
{
    /** @var CampaignsCriteriaSearcher */
    private $searcher;

    public function __construct(CampaignsCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchCampaignsByCriteriaQuery $query): CampaignsResponse
    {
        $filters = Filters::fromValues($query->filters());
        $order   = Order::fromValues($query->orderBy(), $query->order());

        return $this->searcher->search($filters, $order, $query->limit(), $query->offset());
    }
}
