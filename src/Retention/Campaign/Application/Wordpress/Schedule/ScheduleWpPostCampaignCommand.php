<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Wordpress\Schedule;

use App\Shared\Domain\Bus\Command\Command;

final class ScheduleWpPostCampaignCommand extends Command
{

    /**
     * @var int
     */
    private $triggerOn;
    /**
     * @var string
     */
    private $language;
    /**
     * @var array
     */
    private $channels;
    /**
     * @var string
     */
    private $href;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var array
     */
    private $tags;
    /**
     * @var string
     */
    private $id;

    public function __construct(
        string $id,
        int $triggerOn,
        string $language,
        array $channels,
        string $href,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        array $tags
    ) {
        $this->triggerOn = $triggerOn;
        $this->language = $language;
        $this->channels = $channels;
        $this->href = $href;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->horizontalImg = $horizontalImg;
        $this->tags = $tags;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function triggerOn(): int
    {
        return $this->triggerOn;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return array
     */
    public function channels(): array
    {
        return $this->channels;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    /**
     * @return array
     */
    public function tags(): array
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
