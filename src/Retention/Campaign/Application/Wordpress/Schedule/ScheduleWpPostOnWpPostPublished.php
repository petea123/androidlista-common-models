<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Wordpress\Schedule;

use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\CampaignHref;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaignLanguage;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaignTriggerOn;
use App\Retention\Shared\Campaign\Domain\CampaignChannel;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\UuidGenerator;
use App\Wordpress\Post\Domain\WpPostPublished;
use function Lambdish\Phunctional\apply;

final class ScheduleWpPostOnWpPostPublished implements DomainEventSubscriber
{

    /**
     * @var WpPostCampaignScheduler
     */
    private $scheduler;

    public function __construct(WpPostCampaignScheduler $scheduler)
    {
        $this->scheduler = $scheduler;
    }

    public function __invoke(WpPostPublished $event)
    {
        $triggerOn = WpPostCampaignTriggerOn::createTriggerFromTimestamp($event->publishedOn());
        $language = WpPostCampaignLanguage::fromBlogId($event->language());
        $channels = new CampaignChannels([CampaignChannel::facebook()]);
        $href = new CampaignHref($event->href());
        $id = new CampaignId($event->aggregateId());

        apply($this->scheduler, [
            $id,
            $triggerOn,
            $language,
            $href,
            $channels,
            $event->title(),
            $event->description(),
            $event->images()[0],
            $event->images()[1],
            $event->tags()
        ]);
    }

    public static function subscribedTo(): array
    {
        return [WpPostPublished::class];
    }
}
