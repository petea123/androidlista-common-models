<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Wordpress\Schedule;

use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\CampaignHref;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaignLanguage;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaignTriggerOn;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class ScheduleWpPostCampaignCommandHandler implements CommandHandler
{

    /**
     * @var WpPostCampaignScheduler
     */
    private $scheduler;

    public function __construct(WpPostCampaignScheduler $scheduler)
    {
        $this->scheduler = $scheduler;
    }

    public function __invoke(ScheduleWpPostCampaignCommand $command)
    {
        $triggerOn = WpPostCampaignTriggerOn::createTriggerFromTimestamp($command->triggerOn());
        $language = WpPostCampaignLanguage::fromString($command->language());
        $channels = CampaignChannels::fromValues($command->channels());
        $href = new CampaignHref($command->href());
        $id = new CampaignId($command->id());

        apply($this->scheduler, [
            $id,
            $triggerOn,
            $language,
            $href,
            $channels,
            $command->title(),
            $command->description(),
            $command->icon(),
            $command->horizontalImg(),
            $command->tags()
        ]);
    }
}
