<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Wordpress\Schedule;

use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\CampaignHref;
use App\Retention\Campaign\Domain\CampaignRepository;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaign;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaignLanguage;
use App\Retention\Campaign\Domain\WordpressPostCampaign\WpPostCampaignTriggerOn;
use App\Retention\Shared\Campaign\Domain\CampaignId;

final class WpPostCampaignScheduler
{
    /**
     * @var CampaignRepository
     */
    private $repository;

    public function __construct(CampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(
        CampaignId $id,
        WpPostCampaignTriggerOn $triggerOn,
        WpPostCampaignLanguage $language,
        CampaignHref $href,
        CampaignChannels $channels,
        string $title,
        string $description,
        string $icon,
        string $horizontalImg,
        array $tags
    ) {

        $post = WpPostCampaign::create(
            $id,
            $language,
            $triggerOn,
            $channels,
            $title,
            $description,
            $icon,
            $horizontalImg,
            $tags,
            $href
        );

        $this->repository->save($post);
    }
}
