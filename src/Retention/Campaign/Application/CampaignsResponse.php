<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application;

use App\Shared\Domain\Bus\Query\Response;

final class CampaignsResponse implements Response
{
    private $campaigns;

    public function __construct(CampaignResponse ... $campaigns)
    {
        $this->campaigns = $campaigns;
    }

    /**
     * @return CampaignResponse[]
     */
    public function campaigns(): array
    {
        return $this->campaigns;
    }
}
