<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application;

use function Lambdish\Phunctional\last;

final class CampaignResponse
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $triggerOn;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $href;
    /**
     * @var array
     */
    private $channels;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $type;
    /**
     * @var bool
     */
    private $triggered;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var array
     */
    private $tags;

    public function __construct(
        string $id,
        int $triggerOn,
        string $title,
        string $description,
        string $href,
        array $channels,
        string $icon,
        string $horizontalImg,
        array $tags,
        bool $triggered,
        string $type
    ) {
        $this->id = $id;
        $this->triggerOn = $triggerOn;
        $this->title = $title;
        $this->href = $href;
        $this->channels = $channels;
        $this->icon = $icon;
        $this->type = $type;
        $this->triggered = $triggered;
        $this->description = $description;
        $this->horizontalImg = $horizontalImg;
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function triggerOn(): int
    {
        return $this->triggerOn;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function href(): string
    {
        return $this->href;
    }

    /**
     * @return array
     */
    public function channels(): array
    {
        return $this->channels;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return (string)last(explode('\\', $this->type));
    }

    /**
     * @return bool
     */
    public function triggered(): bool
    {
        return $this->triggered;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}
