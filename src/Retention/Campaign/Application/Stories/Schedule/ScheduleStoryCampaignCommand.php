<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Stories\Schedule;

use App\Shared\Domain\Bus\Command\Command;

final class ScheduleStoryCampaignCommand extends Command
{

    /**
     * @var string
     */
    private $storyId;
    /**
     * @var int
     */
    private $triggerOn;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $horizontalImg;
    /**
     * @var array
     */
    private $channels;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id, string $storyId, string $title, string $description, array $channels, int $triggerOn, string $icon, string $horizontalImg)
    {
        $this->storyId = $storyId;
        $this->triggerOn = $triggerOn;
        $this->icon = $icon;
        $this->horizontalImg = $horizontalImg;
        $this->channels = $channels;
        $this->title = $title;
        $this->description = $description;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function storyId(): string
    {
        return $this->storyId;
    }

    /**
     * @return int
     */
    public function triggerOn(): int
    {
        return $this->triggerOn;
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function horizontalImg(): string
    {
        return $this->horizontalImg;
    }

    /**
     * @return array
     */
    public function channels(): array
    {
        return $this->channels;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
