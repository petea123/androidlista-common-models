<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Stories\Schedule;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaignTriggerOn;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class ScheduleStoryCampaignCommandHandler implements CommandHandler
{

    /**
     * @var StoryCampaignScheduler
     */
    private $scheduler;

    public function __construct(StoryCampaignScheduler $scheduler)
    {
        $this->scheduler = $scheduler;
    }

    public function __invoke(ScheduleStoryCampaignCommand $command)
    {
        $campaignId = new CampaignId($command->id());
        $storyId = new StoryId($command->storyId());
        $channels = CampaignChannels::fromValues($command->channels());
        $triggerOn = StoryCampaignTriggerOn::createFromTimestamp($command->triggerOn());
        $icon = $command->icon();
        $horizontalImg = $command->horizontalImg();

        apply($this->scheduler, [$campaignId, $storyId, $channels, $command->title(), $command->description(), $triggerOn, $icon, $horizontalImg]);
    }
}
