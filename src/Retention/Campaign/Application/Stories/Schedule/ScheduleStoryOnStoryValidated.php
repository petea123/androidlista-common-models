<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Stories\Schedule;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Event\StoryWasValidated;
use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Shared\Campaign\Domain\CampaignChannel;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\UuidGenerator;
use function Lambdish\Phunctional\apply;

final class ScheduleStoryOnStoryValidated implements DomainEventSubscriber
{

    /**
     * @var StoryCampaignScheduler
     */
    private $scheduler;
    /**
     * @var UuidGenerator
     */
    private $generator;

    public function __construct(StoryCampaignScheduler $scheduler, UuidGenerator $generator)
    {
        $this->scheduler = $scheduler;
        $this->generator = $generator;
    }

    public function __invoke(StoryWasValidated $event)
    {
        $id = new CampaignId($this->generator->next());
        $storyId = new StoryId($event->aggregateId());
        $channels = new CampaignChannels([CampaignChannel::facebook()]);

        apply($this->scheduler, [$id, $storyId, $channels]);
    }
    public static function subscribedTo(): array
    {
        return [StoryWasValidated::class];
    }
}
