<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Stories\Schedule;

use App\Al\Collections\Application\CollectionsResponse;
use App\Al\Collections\Application\Search\ByCriteria\SearchCollectionsByCriteriaQuery;
use App\Al\Items\Application\Find\ById\FindItemByIdQuery;
use App\Al\Items\Infrastructure\Projections\ItemView;
use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Application\Search\SearchStoryQuery;
use App\Amp\Stories\Application\StoryResponse;
use App\Retention\Campaign\Domain\CampaignChannels;
use App\Retention\Campaign\Domain\CampaignRepository;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaign;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaignHref;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaignLanguage;
use App\Retention\Campaign\Domain\StoryCampaign\StoryCampaignTriggerOn;
use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Query\QueryBus;

final class StoryCampaignScheduler
{

    /**
     * @var CampaignRepository
     */
    private $repository;
    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(CampaignRepository $repository, QueryBus $queryBus)
    {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
    }

    public function __invoke(CampaignId $id, StoryId $storyId, CampaignChannels $channels, string $title = '', string $description = '', StoryCampaignTriggerOn $triggerOn = null, string $icon = '', string $horizontalImg = '')
    {
        /** @var StoryResponse|null $story */
        $story = $this->queryBus->ask(new SearchStoryQuery($storyId->value()));

        if (null === $story || !$story->validated()) {
            return;
        }

        /** @var StoryCampaignLanguage $language */
        $language = StoryCampaignLanguage::fromString($story->language());
        $href = StoryCampaignHref::fromIdAndLanguage($storyId, $language);

        $images = $this->generateImages($icon, $horizontalImg, $story->pages(), $language);

        $storyCampaign = StoryCampaign::create(
            $id,
            $storyId,
            $href,
            empty($title) ? $story->title() : $title,
            empty($description) ? $story->description() : $description,
            $language,
            $this->generateTriggerDate($triggerOn, $language),
            $channels,
            $this->tags($language, $story->categories()),
            $images['icon'],
            $images['horizontalImg']
        );

        $this->repository->save($storyCampaign);
    }

    private function generateTriggerDate(?StoryCampaignTriggerOn $triggerOn, StoryCampaignLanguage $language)
    {
        if (null === $triggerOn) {
            /** @var StoryCampaign $lastScheduled */
            $lastScheduled = $this->repository->lastScheduledStoryCampaignForLanguage($language);

            return null === $lastScheduled ? StoryCampaignTriggerOn::now() : StoryCampaignTriggerOn::forTomorrow($lastScheduled->triggerOn()->value());
        }

        return $triggerOn;
    }

    private function generateImages(string $icon, string $horizontalImg, array $pages, StoryCampaignLanguage $language): array
    {
        $images = [
            'icon' => $icon,
            'horizontalImg' => $horizontalImg
        ];

        if (empty($icon) || empty($horizontalImg)) {
            foreach ($pages as $page) {
                if(!empty($page['itemId']) && (empty($images['icon']) || empty($images['horizontalImg']))) {
                    /** @var ItemView $itemResponse */
                    $itemResponse = $this->queryBus->ask(new FindItemByIdQuery((int)$page['itemId'], $language->value()));

                    if ('' === $images['icon']) {
                        $images['icon'] = $itemResponse->icon()['path'] ?? $icon;
                    }

                    if ('' === $images['horizontalImg']) {
                        $images['horizontalImg'] = $itemResponse->horizontalImg()['path'] ?? '';
                    }
                }
            }
        }

        return $images;
    }

    private function tags(StoryCampaignLanguage $language, array $categories): array
    {
        $tags[] = 'AMPStories';

        if (empty($categories[0])) {
            return $tags;
        }

        /** @var CollectionsResponse $collection */
        $collection = $this->queryBus->ask(new SearchCollectionsByCriteriaQuery([
            [
                'field' => 'collectionGroup',
                'operator' => '=',
                'value' => (string)$categories[0]
            ],
            [
                'field' => 'language',
                'operator' => '=',
                'value' => $language->value()
            ]
        ]))->collections();

        if (empty($collection[0])) {
            return $tags;
        }

        $tags[] = $collection[0]->shortname();

        return $tags;
    }
}
