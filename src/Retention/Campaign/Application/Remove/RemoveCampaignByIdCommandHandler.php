<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Remove;

use App\Retention\Shared\Campaign\Domain\CampaignId;
use App\Shared\Domain\Bus\Command\CommandHandler;
use function Lambdish\Phunctional\apply;

final class RemoveCampaignByIdCommandHandler implements CommandHandler
{

    /**
     * @var CampaignRemover
     */
    private $remover;

    public function __construct(CampaignRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveCampaignByIdCommand $command): void
    {
        $id = new CampaignId($command->id());

        apply($this->remover, [$id]);
    }
}
