<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Remove;

use App\Shared\Domain\Bus\Command\Command;

final class RemoveCampaignByIdCommand extends Command
{

    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }
}
