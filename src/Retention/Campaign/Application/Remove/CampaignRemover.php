<?php

declare(strict_types=1);

namespace App\Retention\Campaign\Application\Remove;

use App\Retention\Campaign\Domain\CampaignRepository;
use App\Retention\Shared\Campaign\Domain\CampaignId;

final class CampaignRemover
{
    /**
     * @var CampaignRepository
     */
    private $repository;

    public function __construct(CampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(CampaignId $id): void
    {
        $campaign = $this->repository->search($id);
        if (null !== $campaign) {
            $this->repository->delete($campaign);
        }
    }
}
