<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Domain;

use DomainException;

final class BackofficeStoryFinder
{

    /** @var BackofficeStoryRepository */
    private $repository;

    public function __construct(BackofficeStoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function find(string $id)
    {
        $story = $this->repository->search($id);

        if (null === $story) {
            throw new DomainException('Backoffice story not found');
        }

        return $story;
    }
}
