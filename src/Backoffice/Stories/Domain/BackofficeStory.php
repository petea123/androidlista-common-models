<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;
use DateTimeImmutable;

final class BackofficeStory extends AggregateRoot
{

    /** @var string */
    private $id;
    /** @var string */
    private $groupId;
    /** @var string */
    private $template;
    /** @var array */
    private $categories;
    /** @var array */
    private $user;
    /** @var string */
    private $language;
    /** @var bool */
    private $validated;
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var DateTimeImmutable */
    private $createdAt;
    /** @var DateTimeImmutable */
    private $updatedAt;
    /** @var int */
    private $totalPages;
    /** @var int */
    private $views;
    /** @var int */
    private $likes;
    /** @var array */
    private $pages;
    /** @var string */
    private $featuredImage;

    public function __construct(
        string $id,
        string $groupId,
        string $template,
        array $categories,
        array $user,
        string $language,
        bool $validated,
        string $title,
        string $description,
        DateTimeImmutable $createdAt,
        DateTimeImmutable $updatedAt,
        int $totalPages,
        int $views,
        int $likes,
        string $featuredImage,
        array $pages
    ) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->template = $template;
        $this->categories = $categories;
        $this->user = $user;
        $this->language = $language;
        $this->validated = $validated;
        $this->title = $title;
        $this->description = $description;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->totalPages = $totalPages;
        $this->views = $views;
        $this->likes = $likes;
        $this->pages = $pages;
        $this->featuredImage = $featuredImage;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function template(): string
    {
        return $this->template;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return array
     */
    public function user(): array
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function validated(): bool
    {
        return $this->validated;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function totalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return int
     */
    public function views(): int
    {
        return $this->views;
    }

    /**
     * @return int
     */
    public function likes(): int
    {
        return $this->likes;
    }

    /**
     * @return array
     */
    public function pages(): array
    {
        return $this->pages;
    }

    public function changeTemplate(string $template): void
    {
        $this->template = $template;

    }

    public function changeTitle(string $title): void
    {
        $this->title = $title;
    }

    public function changeDescription(string $description): void
    {
        $this->description = $description;
    }

    public function changeCategories(array $categories): void
    {
        $this->categories = $categories;
    }

    public function changeFeaturedImage(string $featuredImage): void
    {
        $this->featuredImage = $featuredImage;
    }

    public function changePages(array $pages): void
    {
        $this->pages = $pages;
    }

    public function changeValidated(bool $validated): void
    {
        $this->validated = $validated;
    }

    public function updated(DateTimeImmutable $occurredOn): void
    {
        $this->updatedAt = $occurredOn;
    }

    public function recalculateTotalPages(): void
    {
        $this->totalPages = count($this->pages);
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage ?? '';
    }
}
