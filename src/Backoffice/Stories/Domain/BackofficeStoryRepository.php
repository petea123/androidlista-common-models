<?php


namespace App\Backoffice\Stories\Domain;


use App\Shared\Domain\Criteria\Criteria;

interface BackofficeStoryRepository
{
    public function save(BackofficeStory $story): void;

    public function search(string $id): ?BackofficeStory;

    public function matching(Criteria $criteria): array;

    public function searchByGroupId(string $groupId): ?BackofficeStory;

    public function delete(BackofficeStory $story): void;
}
