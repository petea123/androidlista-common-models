<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\Remove;

use App\Backoffice\Stories\Domain\BackofficeStoryFinder;
use App\Backoffice\Stories\Domain\BackofficeStoryRepository;

final class BackofficeStoryRemover
{

    /** @var BackofficeStoryRepository */
    private $repository;

    public function __construct(BackofficeStoryRepository $repository)
    {
        $this->repository = $repository;
        $this->finder = new BackofficeStoryFinder($repository);
    }

    public function remove(string $id): void
    {
        $story = $this->finder->find($id);

        $this->repository->delete($story);
    }
}
