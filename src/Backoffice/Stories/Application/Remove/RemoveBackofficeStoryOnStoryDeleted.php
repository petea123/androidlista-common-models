<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\Remove;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Event\StoryWasDeleted;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class RemoveBackofficeStoryOnStoryDeleted implements DomainEventSubscriber
{

    /** @var BackofficeStoryRemover */
    private $remover;

    public function __construct(BackofficeStoryRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(StoryWasDeleted $event)
    {
        $this->remover->remove($event->aggregateId());
    }

    public static function subscribedTo(): array
    {
        return [StoryWasDeleted::class];
    }
}
