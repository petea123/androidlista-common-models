<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\SearchByCriteria;

use App\Backoffice\Stories\Application\BackofficeStoriesResponse;
use App\Backoffice\Stories\Application\BackofficeStoryResponse;
use App\Backoffice\Stories\Domain\BackofficeStory;
use App\Backoffice\Stories\Domain\BackofficeStoryRepository;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;
use function Lambdish\Phunctional\map;

final class StoriesByCriteriaSearcher
{

    /** @var BackofficeStoryRepository */
    private $repository;

    public function __construct(BackofficeStoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(Filters $filters, Order $order, ?int $limit, ?int $offset): BackofficeStoriesResponse
    {
        $criteria = new Criteria($filters, $order, $offset, $limit);

        return new BackofficeStoriesResponse(...map($this->toResponse(), $this->repository->matching($criteria)));
    }

    private function toResponse(): callable
    {
        return static function (BackofficeStory $story) {
            return new BackofficeStoryResponse(
                $story->id(),
                $story->groupId(),
                $story->template(),
                $story->categories(),
                $story->user(),
                $story->language(),
                $story->validated(),
                $story->title(),
                $story->description(),
                $story->createdAt()->getTimestamp(),
                $story->updatedAt()->getTimestamp(),
                $story->pages(),
                $story->totalPages(),
                $story->featuredImage()
            );
        };
    }
}
