<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\SearchByCriteria;

use App\Backoffice\Stories\Application\BackofficeStoriesResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Criteria\Filters;
use App\Shared\Domain\Criteria\Order;

final class SearchBackofficeStoriesByCriteriaQueryHandler implements QueryHandler
{
    /** @var StoriesByCriteriaSearcher */
    private $searcher;

    public function __construct(StoriesByCriteriaSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchBackofficeStoriesByCriteriaQuery $query): BackofficeStoriesResponse
    {
        $filters = Filters::fromValues($query->filters());
        $order   = Order::fromValues($query->orderBy(), $query->order());

        return $this->searcher->search($filters, $order, $query->limit(), $query->offset());
    }
}
