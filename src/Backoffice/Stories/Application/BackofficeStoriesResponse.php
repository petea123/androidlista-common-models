<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application;

use App\Shared\Domain\Bus\Query\Response;

final class BackofficeStoriesResponse implements Response
{
    private $stories;

    public function __construct(BackofficeStoryResponse ... $stories)
    {
        $this->stories = $stories;
    }

    /**
     * @return BackofficeStoryResponse[]
     */
    public function stories(): array
    {
        return $this->stories;
    }
}
