<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application;

final class BackofficeStoryResponse
{

    /** @var string */
    private $id;
    /** @var string */
    private $groupId;
    /** @var string */
    private $template;
    /** @var array */
    private $categories;
    /** @var array */
    private $user;
    /** @var string */
    private $language;
    /** @var bool */
    private $validated;
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var int */
    private $createdAt;
    /** @var array */
    private $pages;
    /** @var int */
    private $totalPages;
    /** @var int */
    private $updatedAt;
    /** @var string */
    private $featuredImage;

    public function __construct(
        string $id,
        string $groupId,
        string $template,
        array $categories,
        array $user,
        string $language,
        bool $validated,
        string $title,
        string $description,
        int $createdAt,
        int $updatedAt,
        array $pages,
        int $totalPages,
        string $featuredImage
    ) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->template = $template;
        $this->categories = $categories;
        $this->user = $user;
        $this->language = $language;
        $this->validated = $validated;
        $this->title = $title;
        $this->description = $description;
        $this->createdAt = $createdAt;
        $this->pages = $pages;
        $this->totalPages = $totalPages;
        $this->updatedAt = $updatedAt;
        $this->featuredImage = $featuredImage;
    }

    /**
     * @return array
     */
    public function categories(): array
    {
        return $this->categories;
    }

    /**
     * @return int
     */
    public function createdAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return array
     */
    public function pages(): array
    {
        return $this->pages;
    }

    /**
     * @return string
     */
    public function template(): string
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function user(): array
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function validated(): bool
    {
        return $this->validated;
    }

    public function totalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return int
     */
    public function updatedAt(): int
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function featuredImage(): string
    {
        return $this->featuredImage;
    }
}
