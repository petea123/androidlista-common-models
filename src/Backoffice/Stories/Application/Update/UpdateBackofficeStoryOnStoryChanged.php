<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\Update;

use App\Amp\Stories\Domain\Event\StoryCategoryWasAdded;
use App\Amp\Stories\Domain\Event\StoryDescriptionWasChanged;
use App\Amp\Stories\Domain\Event\StoryPageWasChanged;
use App\Amp\Stories\Domain\Event\StoryTitleWasChanged;
use App\Amp\Stories\Domain\Event\StoryWasInvalidated;
use App\Amp\Stories\Domain\Event\StoryWasValidated;
use App\Shared\Domain\Bus\Event\DomainEvent;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Utils;
use function Lambdish\Phunctional\apply;

final class UpdateBackofficeStoryOnStoryChanged implements DomainEventSubscriber
{

    /** @var BackofficeStoryUpdater */
    private $updater;

    public function __construct(BackofficeStoryUpdater $updater)
    {
        $this->updater = $updater;
    }

    public function __invoke(DomainEvent $event)
    {
        apply($this->updater, [$event->aggregateId(), Utils::timestampToDate((int)$event->occurredOn())]);
    }

    public static function subscribedTo(): array
    {
        return [
            StoryTitleWasChanged::class,
            StoryDescriptionWasChanged::class,
            StoryPageWasChanged::class,
            StoryCategoryWasAdded::class,
            StoryWasInvalidated::class,
            StoryWasValidated::class
        ];
    }
}
