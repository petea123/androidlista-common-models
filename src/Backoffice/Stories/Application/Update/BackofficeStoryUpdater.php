<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\Update;

use App\Amp\Stories\Application\Find\FindStoryQuery;
use App\Amp\Stories\Application\StoryResponse;
use App\Backoffice\Stories\Domain\BackofficeStoryFinder;
use App\Backoffice\Stories\Domain\BackofficeStoryRepository;
use App\Shared\Domain\Bus\Query\QueryBus;
use DateTimeImmutable;

final class BackofficeStoryUpdater
{

    /** @var BackofficeStoryFinder */
    private $finder;
    /** @var QueryBus */
    private $queryBus;
    /** @var BackofficeStoryRepository */
    private $repository;

    public function __construct(BackofficeStoryRepository $repository, QueryBus $queryBus)
    {
        $this->finder = new BackofficeStoryFinder($repository);
        $this->queryBus = $queryBus;
        $this->repository = $repository;
    }

    public function __invoke(string $id, DateTimeImmutable $occurredOn)
    {
        $story = $this->finder->find($id);

        /** @var StoryResponse $ampStory */
        $ampStory = $this->queryBus->ask(new FindStoryQuery($id));

        $story->changeTemplate($ampStory->template());
        $story->changeTitle($ampStory->title());
        $story->changeDescription($ampStory->description());
        $story->changeCategories($ampStory->categories());
        $story->changeFeaturedImage($ampStory->featuredImage());
        $story->changePages($ampStory->pages());
        $story->recalculateTotalPages();
        $story->changeValidated($ampStory->validated());
        $story->updated($occurredOn);

        $this->repository->save($story);

    }
}
