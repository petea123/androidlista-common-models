<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\Create;

use App\Amp\Stories\Domain\Event\StoryWasCreated;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Shared\Domain\Utils;
use function Lambdish\Phunctional\apply;

final class CreateBackofficeStoryOnStoryCreated implements DomainEventSubscriber
{

    /** @var BackofficeStoryCreator */
    private $creator;

    public function __construct(BackofficeStoryCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(StoryWasCreated $event)
    {
        apply($this->creator, [
            $event->aggregateId(),
            $event->groupId(),
            'GAMES',
            $event->categories(),
            $event->user(),
            $event->language(),
            $event->title(),
            $event->description(),
            Utils::timestampToDate((int)$event->occurredOn()),
            $event->featuredImage(),
            $event->pages()
        ]);
    }

    public static function subscribedTo(): array
    {
        return [StoryWasCreated::class];
    }
}
