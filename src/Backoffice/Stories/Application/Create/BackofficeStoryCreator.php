<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Application\Create;

use App\Al\UsersProfile\Application\Find\FindUserProfileQuery;
use App\Al\UsersProfile\Application\UserProfileResponse;
use App\Backoffice\Stories\Domain\BackofficeStory;
use App\Backoffice\Stories\Domain\BackofficeStoryRepository;
use App\Shared\Domain\Bus\Query\QueryBus;
use DateTimeImmutable;

final class BackofficeStoryCreator
{

    /** @var BackofficeStoryRepository */
    private $repository;

    public function __construct(BackofficeStoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke
    (
        string $id,
        string $groupId,
        string $template,
        array $categories,
        array $user,
        string $language,
        string $title,
        string $description,
        DateTimeImmutable $creationDate,
        string $featuredImage,
        array $pages
    ) {

        $story = new BackofficeStory(
            $id,
            $groupId,
            $template,
            $categories,
            $user,
            $language,
            false,
            $title,
            $description,
            $creationDate,
            $creationDate,
            count($pages),
            0,
            0,
            $featuredImage,
            $pages
        );

        $this->repository->save($story);
    }
}
