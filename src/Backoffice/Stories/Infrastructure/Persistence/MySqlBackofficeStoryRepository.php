<?php

declare(strict_types=1);

namespace App\Backoffice\Stories\Infrastructure\Persistence;

use App\Backoffice\Stories\Domain\BackofficeStory;
use App\Backoffice\Stories\Domain\BackofficeStoryRepository;
use App\Shared\Domain\Criteria\Criteria;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use function Lambdish\Phunctional\map;

final class MySqlBackofficeStoryRepository extends DoctrineRepository implements BackofficeStoryRepository
{

    public function save(BackofficeStory $story): void
    {
        $this->persist($story);
        $this->entityManager()->flush();
    }

    public function matching(Criteria $criteria): array
    {
        $doctrineCriteria = DoctrineCriteriaConverter::convert($criteria);

        return $this->repository(BackofficeStory::class)->matching($doctrineCriteria)->toArray();
    }

    public function search(string $id): ?BackofficeStory
    {
        return $this->repository(BackofficeStory::class)->find($id);
    }

    public function delete(BackofficeStory $story): void
    {
        $this->remove($story);
        $this->entityManager()->flush();
    }

    public function searchByGroupId(string $groupId): ?BackofficeStory
    {
        return $this->repository(BackofficeStory::class)->findOneBy(['groupId' => $groupId]);
    }
}
