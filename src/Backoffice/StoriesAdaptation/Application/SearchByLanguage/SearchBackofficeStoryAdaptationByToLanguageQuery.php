<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\SearchByLanguage;

use App\Shared\Domain\Bus\Query\Query;

final class SearchBackofficeStoryAdaptationByToLanguageQuery implements Query
{

    /**
     * @var string
     */
    private $language;

    public function __construct(string $language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }
}
