<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\SearchByLanguage;

use App\Backoffice\StoriesAdaptation\Application\BackofficeStoriesAdaptationResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;

final class SearchBackofficeStoryAdaptationByToLanguageQueryHandler implements QueryHandler
{

    /**
     * @var BackofficeStoriesAdaptationByToLanguageSearcher
     */
    private $searcher;

    public function __construct(BackofficeStoriesAdaptationByToLanguageSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchBackofficeStoryAdaptationByToLanguageQuery $query): BackofficeStoriesAdaptationResponse
    {
        return $this->searcher->search($query->language());
    }
}
