<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\SearchByLanguage;

use App\Backoffice\StoriesAdaptation\Application\BackofficeStoriesAdaptationResponse;
use App\Backoffice\StoriesAdaptation\Application\BackofficeStoryAdaptationResponse;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptation;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationRepository;
use function Lambdish\Phunctional\map;

final class BackofficeStoriesAdaptationByToLanguageSearcher
{

    /**
     * @var BackofficeStoryAdaptationRepository
     */
    private $repository;

    public function __construct(BackofficeStoryAdaptationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(string $language): BackofficeStoriesAdaptationResponse
    {
        return new BackofficeStoriesAdaptationResponse(...map($this->toResponse(), $this->repository->searchByToLanguage($language)));
    }

    private function toResponse(): callable
    {
        return static function (BackofficeStoryAdaptation $story) {
            return new BackofficeStoryAdaptationResponse(
                $story->id(),
                $story->groupId(),
                $story->originalStoryId(),
                $story->fromLanguage(),
                $story->toLanguage(),
                $story->sentences()->title(),
                $story->sentences()->description(),
                $story->sentences()->pages(),
                $story->references()->title(),
                $story->references()->description(),
                $story->references()->pages(),
                $story->createdAt()->getTimestamp(),
                $story->updatedAt()->getTimestamp(),
            );
        };
    }
}
