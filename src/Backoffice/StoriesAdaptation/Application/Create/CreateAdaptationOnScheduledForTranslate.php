<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\Create;

use App\Amp\Stories\Domain\Event\StoryWasScheduledForTranslate;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;
use function Lambdish\Phunctional\apply;

final class CreateAdaptationOnScheduledForTranslate implements DomainEventSubscriber
{

    /** @var BackofficeStoryAdaptationCreator */
    private $creator;

    public function __construct(BackofficeStoryAdaptationCreator $creator)
    {
        $this->creator = $creator;
    }

    public function __invoke(StoryWasScheduledForTranslate $event)
    {
        apply($this->creator, [
            $event->aggregateId(),
            $event->groupId(),
            $event->language(),
            $event->toLanguage(),
            $event->title(),
            $event->description(),
            $event->pages()
        ]);

    }

    public static function subscribedTo(): array
    {
        return [StoryWasScheduledForTranslate::class];
    }
}
