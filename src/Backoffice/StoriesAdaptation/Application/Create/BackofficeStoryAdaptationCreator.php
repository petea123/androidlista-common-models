<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\Create;

use App\Amp\Stories\Application\SearchByCriteria\SearchStoriesByCriteriaQuery;
use App\Amp\Stories\Application\StoriesResponse;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptation;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationRepository;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationSentences;
use App\Backoffice\StoriesAdaptation\Domain\Translator;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\UuidGenerator;
use function Lambdish\Phunctional\flatten;
use function Lambdish\Phunctional\map;

final class BackofficeStoryAdaptationCreator
{

    /** @var BackofficeStoryAdaptationRepository */
    private $repository;
    /** @var UuidGenerator */
    private $uuidGenerator;
    /** @var Translator */
    private $translator;
    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(
        BackofficeStoryAdaptationRepository $repository,
        UuidGenerator $uuidGenerator,
        Translator $translator,
        QueryBus $queryBus
    ) {
        $this->repository = $repository;
        $this->uuidGenerator = $uuidGenerator;
        $this->translator = $translator;
        $this->queryBus = $queryBus;
    }

    public function __invoke(
        string $id,
        string $groupId,
        string $originalLanguage,
        string $language,
        string $title,
        string $description,
        array $pages
    ) {

        if (!$this->ensureDontExistPendingAdaptation($id, $language)
            || !$this->ensureDontExistStoryAdaptationForGroup($groupId, $language)) {
            return;
        }

        $sentences = flatten([$title, $description, map(function ($page) {
            return [$page['title'], $page['description']];
        }, $pages)]);

        $story = new BackofficeStoryAdaptation(
            $this->uuidGenerator->next(),
            $groupId,
            $id,
            $originalLanguage,
            $language,
            $this->translate($sentences, $originalLanguage, $language),
            $this->translate($sentences, $originalLanguage, 'en')
        );

        $this->repository->save($story);
    }

    private function translate(array $sentences, string $from, string $to): BackofficeStoryAdaptationSentences
    {
        return $this->translator->batchTranslate($sentences, $from, $to);
    }

    private function ensureDontExistPendingAdaptation(string $originalId, string $toLanguage): bool
    {
        return $this->repository->searchByOriginalStoryIdAndLanguage($originalId, $toLanguage) ? false : true;
    }

    private function ensureDontExistStoryAdaptationForGroup(string $groupId, string $toLanguage): bool
    {
        /** @var StoriesResponse $stories */
        $stories = $this->queryBus->ask(new SearchStoriesByCriteriaQuery([
            [
                'field' => 'groupId',
                'operator' => '=',
                'value' => $groupId
            ],
            [
                'field' => 'language',
                'operator' => '=',
                'value' => $toLanguage
            ]
        ]));

        return empty($stories->stories());
    }
}
