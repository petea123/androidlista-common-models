<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\RemoveById;

use App\Amp\Shared\Domain\Stories\StoryId;
use App\Amp\Stories\Domain\Event\StoryWasDeleted;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class RemoveBackofficeStoryAdaptationCommandHandler implements CommandHandler
{

    /** @var BackofficeStoryAdaptationRemover */
    private $remover;

    public function __construct(BackofficeStoryAdaptationRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(RemoveBackofficeStoryAdaptationCommand $command)
    {
        $this->remover->remove($command->id());
    }
}
