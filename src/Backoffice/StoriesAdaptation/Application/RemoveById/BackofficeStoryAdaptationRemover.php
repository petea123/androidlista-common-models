<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\RemoveById;

use App\Backoffice\Stories\Domain\BackofficeStoryRepository;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationFinder;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationRepository;

final class BackofficeStoryAdaptationRemover
{

    /** @var BackofficeStoryRepository */
    private $repository;

    public function __construct(BackofficeStoryAdaptationRepository $repository)
    {
        $this->repository = $repository;
        $this->finder = new BackofficeStoryAdaptationFinder($repository);
    }

    public function remove(string $id): void
    {
        $story = $this->finder->find($id);

        $this->repository->delete($story);
    }
}
