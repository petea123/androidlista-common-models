<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application;

use App\Shared\Domain\Bus\Query\Response;

final class BackofficeStoriesAdaptationResponse implements Response
{
    private $stories;

    public function __construct(BackofficeStoryAdaptationResponse ... $stories)
    {
        $this->stories = $stories;
    }

    /**
     * @return BackofficeStoryAdaptationResponse[]
     */
    public function stories(): array
    {
        return $this->stories;
    }
}
