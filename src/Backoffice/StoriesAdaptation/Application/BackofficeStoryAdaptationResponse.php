<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application;

use App\Shared\Domain\Bus\Query\Response;

final class BackofficeStoryAdaptationResponse implements Response
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $groupId;
    /**
     * @var string
     */
    private $originalStoryId;
    /**
     * @var string
     */
    private $fromLanguage;
    /**
     * @var string
     */
    private $toLanguage;
    /**
     * @var string
     */
    private $proposedTitle;
    /**
     * @var string
     */
    private $proposedDescription;
    /**
     * @var array
     */
    private $proposedPages;
    /**
     * @var int
     */
    private $createdAt;
    /**
     * @var int
     */
    private $updatedAt;
    /**
     * @var string
     */
    private $referenceTitle;
    /**
     * @var string
     */
    private $referenceDescription;
    /**
     * @var array
     */
    private $referencePages;

    public function __construct(
        string $id,
        string $groupId,
        string $originalStoryId,
        string $fromLanguage,
        string $toLanguage,
        string $proposedTitle,
        string $proposedDescription,
        array $proposedPages,
        string $referenceTitle,
        string $referenceDescription,
        array $referencePages,
        int $createdAt,
        int $updatedAt
    ) {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->originalStoryId = $originalStoryId;
        $this->fromLanguage = $fromLanguage;
        $this->toLanguage = $toLanguage;
        $this->proposedTitle = $proposedTitle;
        $this->proposedDescription = $proposedDescription;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->proposedPages = $proposedPages;
        $this->referenceTitle = $referenceTitle;
        $this->referenceDescription = $referenceDescription;
        $this->referencePages = $referencePages;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function originalStoryId(): string
    {
        return $this->originalStoryId;
    }

    /**
     * @return string
     */
    public function fromLanguage(): string
    {
        return $this->fromLanguage;
    }

    /**
     * @return string
     */
    public function toLanguage(): string
    {
        return $this->toLanguage;
    }

    /**
     * @return string
     */
    public function proposedTitle(): string
    {
        return $this->proposedTitle;
    }

    /**
     * @return string
     */
    public function proposedDescription(): string
    {
        return $this->proposedDescription;
    }

    /**
     * @return int
     */
    public function createdAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function updatedAt(): int
    {
        return $this->updatedAt;
    }

    public function totalPages(): int
    {
        return count($this->proposedPages);
    }

    /**
     * @return array
     */
    public function proposedPages(): array
    {
        return $this->proposedPages;
    }

    /**
     * @return string
     */
    public function referenceTitle(): string
    {
        return $this->referenceTitle;
    }

    /**
     * @return string
     */
    public function referenceDescription(): string
    {
        return $this->referenceDescription;
    }

    /**
     * @return array
     */
    public function referencePages(): array
    {
        return $this->referencePages;
    }
}
