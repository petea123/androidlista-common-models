<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\SearchById;

use App\Backoffice\StoriesAdaptation\Application\BackofficeStoryAdaptationResponse;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationRepository;

final class BackofficeStoriesAdaptationByIdSearcher
{

    /**
     * @var BackofficeStoryAdaptationRepository
     */
    private $repository;

    public function __construct(BackofficeStoryAdaptationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(string $id): ?BackofficeStoryAdaptationResponse
    {
        $story = $this->repository->search($id);

        if (null === $story) {
            return null;
        }

        return new BackofficeStoryAdaptationResponse(
            $story->id(),
            $story->groupId(),
            $story->originalStoryId(),
            $story->fromLanguage(),
            $story->toLanguage(),
            $story->sentences()->title(),
            $story->sentences()->description(),
            $story->sentences()->pages(),
            $story->references()->title(),
            $story->references()->description(),
            $story->references()->pages(),
            $story->createdAt()->getTimestamp(),
            $story->updatedAt()->getTimestamp(),
        );
    }
}
