<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Application\SearchById;

use App\Backoffice\StoriesAdaptation\Application\BackofficeStoryAdaptationResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;

final class SearchBackofficeStoryAdaptationByIdQueryHandler implements QueryHandler
{

    /**
     * @var BackofficeStoriesAdaptationByIdSearcher
     */
    private $searcher;

    public function __construct(BackofficeStoriesAdaptationByIdSearcher $searcher)
    {
        $this->searcher = $searcher;
    }

    public function __invoke(SearchBackofficeStoryAdaptationByIdQuery $query): BackofficeStoryAdaptationResponse
    {
        return $this->searcher->search($query->id());
    }
}
