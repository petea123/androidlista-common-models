<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Infrastructure\Translator;

use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationSentences;
use App\Backoffice\StoriesAdaptation\Domain\Translator;
use Google\Cloud\Translate\V3\TranslationServiceClient;
use function Lambdish\Phunctional\flatten;

final class GoogleTranslator implements Translator
{
    /**
     * @var TranslationServiceClient
     */
    private $client;
    /**
     * @var string
     */
    private const PROJECT_ID = 'custom-sun-797';

    private const LANGUAGE_TRANSFORMER = [
        'br' => 'pt',
        'es' => 'es',
        'fr' => 'fr',
        'ru' => 'ru',
        'en' => 'en',
        'tr' => 'tr'
    ];

    public function __construct(TranslationServiceClient $client)
    {
        $this->client = $client;
    }

    public function batchTranslate(array $sentences, string $from, string $to): BackofficeStoryAdaptationSentences
    {
        $response = $this->client->translateText(flatten($sentences), self::LANGUAGE_TRANSFORMER[$to], TranslationServiceClient::locationName(self::PROJECT_ID, 'global'));

        $newSentences = [];
        foreach ($response->getTranslations() as $key => $translation) {
            $newSentences[] = $translation->getTranslatedText();
        }

        return BackofficeStoryAdaptationSentences::createFromArray($newSentences);
    }
}
