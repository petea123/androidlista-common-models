<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Infrastructure\Persistence;

use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptation;
use App\Backoffice\StoriesAdaptation\Domain\BackofficeStoryAdaptationRepository;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class MySqlBackofficeStoryAdaptationRepository extends DoctrineRepository implements BackofficeStoryAdaptationRepository
{

    public function save(BackofficeStoryAdaptation $adaptation): void
    {
        $this->persist($adaptation);
        $this->entityManager()->flush($adaptation);
    }

    public function searchByOriginalStoryIdAndLanguage(
        string $origStoryId,
        string $language
    ): ?BackofficeStoryAdaptation {
        return $this->repository(BackofficeStoryAdaptation::class)->findOneBy(['originalStoryId' => $origStoryId, 'toLanguage' => $language]);
    }

    public function searchByToLanguage(string $language): array
    {
        return $this->repository(BackofficeStoryAdaptation::class)->findBy(['toLanguage' => $language]);
    }

    public function search(string $id): ?BackofficeStoryAdaptation
    {
        return $this->repository(BackofficeStoryAdaptation::class)->find($id);
    }

    public function delete(BackofficeStoryAdaptation $adaptation): void
    {
        $this->entityManager()->remove($adaptation);
    }
}
