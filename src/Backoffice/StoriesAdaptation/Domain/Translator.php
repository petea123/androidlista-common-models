<?php


namespace App\Backoffice\StoriesAdaptation\Domain;


interface Translator
{
    public function batchTranslate(array $sentences, string $from, string $to): BackofficeStoryAdaptationSentences;
}
