<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Domain;

use DomainException;

final class BackofficeStoryAdaptationFinder
{

    /** @var BackofficeStoryAdaptationRepository */
    private $repository;

    public function __construct(BackofficeStoryAdaptationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function find(string $id)
    {
        $story = $this->repository->search($id);

        if (null === $story) {
            throw new DomainException('Backoffice story adaptation not found');
        }

        return $story;
    }
}
