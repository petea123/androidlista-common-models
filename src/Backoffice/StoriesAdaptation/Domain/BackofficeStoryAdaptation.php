<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Domain;

use App\Shared\Domain\Aggregate\AggregateRoot;
use App\Shared\Domain\ValueObject\DateValueObject;
use DateTimeImmutable;

final class BackofficeStoryAdaptation extends AggregateRoot
{

    /** @var string */
    private $id;
    /** @var string */
    private $groupId;
    /** @var string */
    private $originalStoryId;
    /** @var DateTimeImmutable */
    private $createdAt;
    /** @var DateTimeImmutable */
    private $updatedAt;
    /** @var string */
    private $fromLanguage;

    /** @var string */
    private $toLanguage;
    /**
     * @var BackofficeStoryAdaptationSentences
     */
    private $sentences;
    /**
     * @var BackofficeStoryAdaptationSentences
     */
    private $references;

    public function __construct(
        string $id,
        string $groupId,
        string $originalStoryId,
        string $fromLanguage,
        string $toLanguage,
        BackofficeStoryAdaptationSentences $sentences,
        BackofficeStoryAdaptationSentences $references
    ) {

        $this->id = $id;
        $this->groupId = $groupId;
        $this->originalStoryId = $originalStoryId;
        $this->createdAt = DateValueObject::now()->value();
        $this->updatedAt = DateValueObject::now()->value();
        $this->fromLanguage = $fromLanguage;
        $this->toLanguage = $toLanguage;
        $this->sentences = $sentences;
        $this->references = $references;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function groupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return string
     */
    public function originalStoryId(): string
    {
        return $this->originalStoryId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function createdAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function updatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function fromLanguage(): string
    {
        return $this->fromLanguage;
    }

    /**
     * @return string
     */
    public function toLanguage(): string
    {
        return $this->toLanguage;
    }

    /**
     * @return BackofficeStoryAdaptationSentences
     */
    public function sentences(): BackofficeStoryAdaptationSentences
    {
        return $this->sentences;
    }

    /**
     * @return BackofficeStoryAdaptationSentences
     */
    public function references(): BackofficeStoryAdaptationSentences
    {
        return $this->references;
    }
}
