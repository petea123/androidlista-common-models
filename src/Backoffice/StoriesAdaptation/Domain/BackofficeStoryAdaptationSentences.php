<?php

declare(strict_types=1);

namespace App\Backoffice\StoriesAdaptation\Domain;

final class BackofficeStoryAdaptationSentences
{
    /** @var string */
    private $title;
    /** @var string */
    private $description;
    /** @var array */
    private $pages;

    public function __construct(string $title, string $description, array $pages)
    {
        $this->title = $title;
        $this->description = $description;
        $this->pages = $pages;
    }

    public static function createFromArray(array $sentences): self
    {

        $pages = [];
        $chunks = array_chunk($sentences, 2);
        $title = $chunks[0][0];
        $description = $chunks[0][1];
        unset($chunks[0]);
        foreach ($chunks as $chunk) {
            $pages[] = [
                'title' => $chunk[0],
                'description' => $chunk[1]
            ];
        }

        return new self($title, $description, $pages);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function pages(): array
    {
        return $this->pages;
    }
}
