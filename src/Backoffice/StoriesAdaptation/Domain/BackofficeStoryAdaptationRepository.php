<?php


namespace App\Backoffice\StoriesAdaptation\Domain;


interface BackofficeStoryAdaptationRepository
{
    public function searchByOriginalStoryIdAndLanguage(string $origStoryId, string $language): ?BackofficeStoryAdaptation;

    public function save(BackofficeStoryAdaptation $adaptation): void;

    public function searchByToLanguage(string $language): array;

    public function search(string $id): ?BackofficeStoryAdaptation;

    public function delete(BackofficeStoryAdaptation $adaptation): void;
}
