# Common models and their configuration for Androidlista projects

## Builing and testing project

To build the project the following command should be run
```
docker-compose up -d
```

Run tests
```
docker exec -it al_models_php bash
./vendor/bin/phpunit
```